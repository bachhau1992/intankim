<?php
/* Testi */
add_shortcode('hkt_testi', 'hkt_testi_func');
function hkt_testi_func() {
	ob_start();?>

   <div id="testi">
      <div class="testi_users">
         <div data-for="user1" class="img_user">
            <div class="mask"></div>
            <img src="http://macinsta.vn/wp-content/uploads/2017/09/21909023-1465489256821810-562639696-o.jpg">
         </div>
         <div data-for="user2" class="img_user active">
            <div class="mask"></div>
            <img src="http://macinsta.vn/wp-content/uploads/2017/09/19114510-1465497950154274-833960016-n.jpg">
         </div>
         <div data-for="user3" class="img_user">
            <div class="mask"></div>
            <img src="http://macinsta.vn/wp-content/uploads/2017/09/18193994-838820572936250-4936064375297964400-n.jpg">
         </div>
         <div data-for="user4" class="img_user">
            <div class="mask"></div>
            <img src="http://macinsta.vn/wp-content/uploads/2017/09/21912863-1465499450154124-2079481406-n.jpg">
         </div>
         <div data-for="user5" class="img_user">
            <div class="mask"></div>
            <img src="http://macinsta.vn/wp-content/uploads/2017/09/10462962-286919261487707-410660597112505792-n.jpg">
         </div>
         <div data-for="user6" class="img_user">
            <div class="mask"></div>
            <img src="http://macinsta.vn/wp-content/uploads/2017/09/21919311-1465502080153861-1882214238-n.jpg">
         </div>
      </div>
      <div class="testi_info">
         <div class="info" id="user1">
            <p>Tôi đã tham khảo chi phí thiết kế và in ấn bộ nhận diện thương hiệu cho công ty của mình từ nhiều đơn vị khác nhau. Tuy nhiên, In Sư Tử Vàng là nơi tôi đặt niềm tin và tất nhiên, họ đã không làm tôi thất vọng.</p>
            <p class="name">Nguyễn Duy Đạt</p>
         </div>
         <div class="info active" id="user2">
            <p>Tôi cảm nhận được sự nhiệt tình, chuyên nghiệp của nhân viên công ty. Dù là khách hàng nhỏ lẻ nhưng tôi luôn được hỗ trợ rất tốt từ công đoạn thiết kế, in ấn, đến thi công lắp đặt để có sản phẩm ưng ý nhất.</p>
            <p class="name">Trang Anie</p>
         </div>
         <div class="info" id="user3">
            <p>Tôi đã bất ngờ khi nhìn thấy mẫu thiết kế card visit của mình. Thật sự rất ấn tượng! </p>
            <p class="name">Thanh Huyền</p>
         </div>
         <div class="info" id="user4">
            <p>Là khách hàng quen của công ty từ nhiều năm nay, tôi vẫn luôn tin tưởng và hài lòng về dịch vụ của In TÂN KIM. Nhờ có họ mà công ty tôi đã thu hút thêm được rất nhiều khách hàng tiềm năng.</p>
            <p class="name">Hải NV</p>
         </div>
      </div>
   </div>

<?php
return ob_get_clean();
}
