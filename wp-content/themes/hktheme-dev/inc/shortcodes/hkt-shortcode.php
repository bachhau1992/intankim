<?php
/*
Plugin Name: HKT ShortCode
Plugin URI: http://thietkeweb88.net/
Description: ShortCode From HK Theme
Author: Khanh Bui
Version: 1.0
Author URI: http://thietkeweb88.net/
*/
class HKT_Shortcode
{
	protected $arrShortcodes = array();
	public function __construct(){
		$this->hkt_constant();

		$this->hkt_initArrShortcodes();
		$this->hkt_initShortcodes();
	}

	protected function hkt_initArrShortcodes() {
		$this->arrShortcodes = array('myshortcode', 'hkt-posts', 'hkt-posts-extra', 'hkt-products', 'product-categories', 'featured-products', 'news-favorite', 'contact', 'procedure');
	}

	protected function hkt_initShortcodes() {
		foreach($this->arrShortcodes as $shortcode){
			//echo SC_ShortCode."{$shortcode}.php <br/>";
			if(file_exists(SC_ShortCode."/{$shortcode}.php")){
				require_once SC_ShortCode."/{$shortcode}.php";
			}	
		}
	}

	protected function hkt_constant() {
		//define('DS',DIRECTORY_SEPARATOR);	
		define('SC_BASE'	,  	plugins_url( '', __FILE__ ));
		define('SC_ShortCode'	, 	plugin_dir_path( __FILE__ ) . '/shortcode'	);
		define('SC_JS'		, 	SC_BASE . '/js'			);
		define('SC_CSS'		, 	SC_BASE . '/css'		);
		define('SC_IMAGE'	, 	SC_BASE . '/images'		);
	}
}

$_hkt_shortcode = new HKT_Shortcode; // Start an instance of the plugin class