<?php

add_shortcode('hkt_news', 'hkt_news_func');
function hkt_news_func($atts, $content = null) {
    extract(shortcode_atts(array(
        'number_post' => '',
        'news_id' => ''
    ), $atts));
    ob_start();
?>
    <div class="clearfix">
        <?php
            $args_news = array('posts_per_page' => $number_post, 'cat' => $news_id);
            query_posts($args_news);
            if(have_posts()):while (have_posts()):the_post();
        ?>
        <div class="col-xs-6 col-sm-6 col-md-3">
            <div class="column-in">
                <div class="featured-image-new">
                    <a href="<?php the_permalink() ?>">
                        <img width="500" height="360" src="<?php the_post_thumbnail_url() ?>"
                             class="attachment-custom500x360 size-custom500x360 wp-post-image" alt="<?php the_title() ?>">
                    </a>
                </div>
                <h3>
                    <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" style="font-size:14px; font-weight:normal;"><?php the_title() ?></a>
                </h3>
                <div class="excerpt-c"><?php the_excerpt() ?></div>
            </div>
        </div>
        <?php
            endwhile;
            endif;
            wp_reset_postdata();
            wp_reset_query();
        ?>
    </div>

<?php
    return ob_get_clean();
}
