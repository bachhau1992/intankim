<?php
if (!function_exists('hkt_posts_extra_func')) {
   function hkt_posts_extra_func($atts, $content = false) {
      global $get_meta;

      $class = $layout = $post_type = $column = $category = $offset = $number_posts = $thumbnail = $post_meta = $excerpt = $view_all = $title = $link_cat = $is_slider = '';
      extract(shortcode_atts(array(
         'title' => '',
         'layout' => 'slider',
         'is_slider' => 'yes',
         'post_type' => 'post',
         'link_cat' => '##',
         'class' => '',
         'column' => 3,
         'category' => '',
         'offset' => 0,
         'ids' => '',
         'number_posts' => 9,
         'thumbnail' => 'sociallyviral-featured',
         'post_meta' => 'no',
         'excerpt' => 'yes',
         'view_all' => 'yes',
         'no_found_rows' => true,
         'ignore_sticky_posts' => true,
      ), $atts));

      $class = empty($class) ? '' : (' ' . $class);
      $title = empty($title) ? (($category != '') ? get_category_by_slug($category)->name : '') : ($title);

      wp_reset_query();

      $args = array(
         'post_type' => $post_type,
         'ignore_sticky_posts' => 1,
         'posts_per_page' => $number_posts,
      );
      if (strlen($category) > 0) {
         $args['category_name'] = $category;
         $args['offset'] = $offset;
      }
      if (!empty($ids) && strlen($category) == 0) {
         $ids = explode(',', $ids);
         $args['post__in'] = $ids;
      }
      $num_count = count(query_posts($args));

      ob_start();

      $random_id = rand(); ?>

      <?php if($layout == 'bxslider') { ?>

         <div class="hkt-slider-shortcode<?php echo esc_attr($class); ?>">
            <div class="container">
               <?php if ($title != '' && $title != 'hide') echo '<div class="cat-box-title clearfix"><h2 class="heading-title">' . $title . '</h2></div>'; ?>

               <div class="hkt-bxslider hkt-bxslider-<?php echo $random_id; ?>">
                  <?php if (have_posts()): while (have_posts()): the_post(); ?>
                     <?php global $post; ?>
                     <?php $terms = get_the_terms( $post->ID, 'category' );?>

                     <div class="slide post-item col-md-12 nopad">
                        <div class="recent-item">
                           <div class="img-caption" onclick="location.href='<?php the_permalink(); ?>'"><div class="desc">+</div></div>
                           <?php if (function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumbnail != 'none') : ?>
                              <div class="post-thumbnail">
                                 <a rel="nofollow" href="<?php the_permalink(); ?>" rel="bookmark">
                                    <?php the_post_thumbnail($thumbnail); ?>
                                 </a>
                              </div><!-- post-thumbnail /-->
                           <?php endif; ?>
                           <div class="line"></div>
                           <div class="post-box-title">
                              <a href="<?php the_permalink($post->ID); ?>" rel="bookmark"><?php echo get_the_title($post->ID); ?></a>
                              <p class="tt"><?php echo $terms[0]->name; ?></p>
                           </div>
                        </div>
                     </div>

                  <?php endwhile;endif; ?>
                  <?php wp_reset_query(); ?>
               </div>

               <?php if($is_slider == 'yes') { ?>
                  <script type="text/javascript">
                     jQuery(document).ready(function () {
                        jQuery('.hkt-bxslider-<?php echo $random_id; ?>').bxSlider({
                           slideWidth: 360,
                           minSlides: 2,
                           maxSlides: 3,
                           moveSlides: 3,
                           slideMargin: 45,
                           auto: false,
                        });
                     });
                  </script>
               <?php } ?>

            </div>
         </div>

      <?php } else if($layout == 'featured') { ?>
         <div class="hkt-blog-featured-posts">
         <?php if (have_posts()): while (have_posts()): the_post(); ?>
            <?php global $post; ?>

            <div class="news-item col-xs-12 col-sm-12 col-md-4 col-lg-4 nopad">
               <div class="hkt-blog-post-thumb">
                  <?php if (function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumbnail != 'none') : ?>
                        <a rel="nofollow" href="<?php the_permalink(); ?>" rel="bookmark">
                           <?php the_post_thumbnail($thumbnail); ?>
                        </a>
                     <?php endif; ?>
               </div>
               <div class="hkt-blog-post-container">
                  <div class="kt-blog-post-head"></div>
                  <div class="kt-blog-post-header">
                     <h3 class="entry-title"><a href="<?php the_permalink($post->ID); ?>" rel="bookmark"><?php echo get_the_title($post->ID); ?></a></h3>
                  </div>
               </div>
            </div>

         <?php endwhile;endif; ?>
         <?php wp_reset_query(); ?>
         </div>
      <?php } else if($layout == 'list') { ?>
            <div class="hkt-posts">
               <?php if ($title != '' && $title != 'hide') echo '<div class="cat-box-title clearfix"><h2 class="heading-title">' . $title . '</h2></div>'; ?>
               <?php
               $options = array();
               $options['nav'] = false;
               $options['dots'] = true;
               $options = json_encode($options);
               if($is_slider == 'yes') echo '<div class="slider-wrapper-wrap" data-owl-config="' . esc_attr($options) . '">';
               ?>
               <div class="hkt-posts-inner <?php if($is_slider == 'yes') echo 'slider-wrapper'; ?>">
                  <div class="hkt-list-posts  <?php if($is_slider == 'yes') echo 'owl-carousel'; ?>">

                     <?php if (have_posts()) : while (have_posts()): the_post(); ?>
                     <?php global $post; ?>

                        <div class="post-item grid col-xs-12 col-sm-6 col-md-<?php echo 12/$column; ?> col-lg-<?php echo 12/$column; ?> nopad">
                           <div class="recent-item">
                              <?php if (function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumbnail != 'none') : ?>
                              <div class="post-thumbnail">
                                 <a rel="nofollow" href="<?php the_permalink(); ?>" rel="bookmark">
                                    <?php the_post_thumbnail($thumbnail); ?>
                                 </a>
                              </div><!-- post-thumbnail /-->
                              <?php endif; ?>

                              <div class="post-box-title">
                                 <a href="<?php the_permalink($post->ID); ?>" rel="bookmark"><?php echo get_the_title($post->ID); ?></a>
                              </div>

                              <?php if ($excerpt == 'yes') : ?>
                              <div class="entry">
                                 <p><?php hkt_excerpt() ?></p>
                              </div>
                              <?php endif; ?>
                           </div>
                        </div>

                     <?php endwhile; ?>
                     <?php endif; ?>
                     <?php wp_reset_query(); ?>
                     
                  </div>
               </div>
               <?php if($is_slider == 'yes') echo '</div>'; ?>
            </div>
      <?php } ?>

      <?php
      return ob_get_clean();
   }
   add_shortcode('hkt_posts_extra', 'hkt_posts_extra_func');
}