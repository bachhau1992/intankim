<?php

add_shortcode('hkt_contact_home', 'hkt_contact_home_func');
function hkt_contact_home_func($atts, $content = null) {
    extract(shortcode_atts(array(
        'number_post' => '',
    ), $atts));
    ob_start();
    ?>

    <div class="clearfix">
        <?php echo do_shortcode('[contact-form-7 id="676" title="Contact Home"]') ?>
    </div>

<?php
return ob_get_clean();
}

