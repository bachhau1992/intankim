<?php
/* Load Options update: 20170621 */
if (!function_exists('hkt_load_option_func')) {
   function hkt_load_option_func($atts, $content = null) {
      $atts = shortcode_atts(array(
         'id' => '',
         'editor' => '1',
      ), $atts);
      ob_start(); ?>

      <?php
      if($atts['editor'] == 1) {
         echo apply_filters('the_content', hkt_get_option($atts['id']));
      } else {
         echo ( hkt_get_option($atts['id']) );
      }
      ?>

      <?php
      return ob_get_clean();
   }
}
add_shortcode('hkt_load_option', 'hkt_load_option_func');

/* hkt section */
if (!function_exists('hkt_section_func')) {
   function hkt_section_func($atts, $content = null) {
      extract(shortcode_atts(array(
         'class' => '',
      ), $atts));
      ob_start(); ?>

      <section class="hkt-section <?php echo $class ?>">
         <div class="hkt-section-content hkt-container">
            <?php echo do_shortcode($content); ?>
         </div>
      </section>
      <?php
      return ob_get_clean();
   }
}
add_shortcode('hkt_section', 'hkt_section_func');

/* Shortcode show sidebar */
if (!function_exists('hkt_widget_sidebar_func')) {
   function hkt_widget_sidebar_func($atts) {
      extract(shortcode_atts(array(
         'sidebar_name' => '',
      ), $atts));
      ob_start();
      if ('' === $sidebar_name) {
         return null;
      }
      dynamic_sidebar($sidebar_name);

      return ob_get_clean();
   }
}
add_shortcode('hkt_widget_sidebar', 'hkt_widget_sidebar_func');

/* Open/Close Tag */
if (!function_exists('hkt_tagopen_func')) {
   function hkt_tagopen_func($atts, $content = null) {
      $atts = shortcode_atts(array(
         'tag' => 'div',
         'class' => '',
         'data-section-name' => '0',
      ), $atts);
      $data_section = ($atts['data-section-name'] == '0' )?'':' data-section-name="' . $atts['data-section-name'] . '"';
      return '<' . $atts['tag'] . ' class="hkt-tag ' . $atts['class'] . $data_section . '">';
   }
}
add_shortcode('hkt_tagopen', 'hkt_tagopen_func');

if (!function_exists('hkt_tagclose_func')) {
   function hkt_tagclose_func($atts) {
      $atts = shortcode_atts(array(
         'tag' => 'div'
      ), $atts);

      return '</' . $atts['tag'] . '>';
   }
}
add_shortcode('hkt_tagclose', 'hkt_tagclose_func');

//shortcode hkt_block
if (!function_exists('hkt_block_func')) {
   function hkt_block_func($atts, $content = null) {
      extract(shortcode_atts(array(
         'class' => '',
         'block_type' => '',
         'id_option' => 'hkt_customer',
      ), $atts));

      $class = empty($class) ? '' : (' ' . $class);
      $block_type = empty($block_type) ? '' : ($block_type);
      $id_option = empty($id_option) ? '' : ($id_option);

      ob_start(); ?>

      <?php if ($block_type == '') { ?>
      <div class="box-block<?php echo esc_attr($class); ?>">
         <?php echo do_shortcode($content); ?>
      </div>
   <?php } ?>

      <?php if ($block_type == 'bx-slider') {

      wp_enqueue_script('hkt-bx-slider-scripts');
      $random_id = rand(); ?>

      <div class="hkt-slider-shortcode<?php echo esc_attr($class); ?>">
         <div class="container">
            <div class="hkt-bxslider hkt-bxslider-<?php echo $random_id; ?>">
               <?php echo hkt_get_option($id_option); ?>
            </div>

            <script type="text/javascript">
               jQuery(document).ready(function () {
                  jQuery('.hkt-bxslider-<?php echo $random_id; ?>').bxSlider({
                     slideWidth: 150,
                     minSlides: 2,
                     maxSlides: 5,
                     moveSlides: 1,
                     slideMargin: 15,
                     auto: true
                  });
               });
            </script>

         </div>
      </div>

   <?php } ?>

      <?php
      return ob_get_clean();
   }
}
add_shortcode('hkt_block', 'hkt_block_func');

/**/
if (!function_exists('hkt_marquee_func')) {
   function hkt_marquee_func($atts, $content = null) {
      extract(shortcode_atts(array(
         'class' => '',
      ), $atts));
      ob_start(); ?>

      <div class="hkt-marquee <?php echo $class ?>">
         <div class="hkt-marquee-content">
            <marquee class="<?php echo $class ?>" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="2" direction="up" width="100%" height="200" align="center">
               <?php echo do_shortcode($content); ?>
            </marquee>
         </div>
      </div>

      <?php
      return ob_get_clean();
   }
}
add_shortcode('hkt_marquee', 'hkt_marquee_func');

/* hkt parallax */
if (!function_exists('hkt_parallax_func')) {
   function hkt_parallax_func($atts, $content = null) {
      $atts = shortcode_atts(array(
         'class' => '',
         'url_parallax' => '',
      ), $atts);

      $html = '<div class="hkt-parallax ' . $atts['class'] . '" id="parallax" style="background-image: url(' . $atts['url_parallax'] . ');">';
      $html .= do_shortcode($content);
      $html .= '</div>';

      return $html;
   }
}
add_shortcode('hkt_parallax', 'hkt_parallax_func');