<?php

add_shortcode('hkt_procedure_home', 'hkt_procedure_home_func');
function hkt_procedure_home_func($atts, $content = null) {
    extract(shortcode_atts(array(
        'number_post' => '',
    ), $atts));
    ob_start();
    ?>

    <div class="tier tier-c quytrinh" style="">
    <div class="container">
        <div class="row">
            <h2 class="title-t-b" style="color: white !important;margin-top:30px;margin-bottom:40px;">Quy trình của chúng tôi</h2>
            <div class="clearfix ful-b">
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="divbg">
                        <span>
                            Khảo sát
                            <br> &amp;
                            <br> Tư vấn </span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="divbg">
                        <span>
                            Hợp đồng
                            <br> &amp;
                            <br> Thiết kế </span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="divbg">
                        <span>
                            Báo giá
                            <br> &amp;
                            <br> Thi công </span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="divbg">
                        <span>
                            Nghiệm thu
                            <br> &amp;
                            <br> Quyết toán </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
return ob_get_clean();
}
