<?php
if ( ! defined( 'ABSPATH' ) ) {
   exit;
}

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if (wp_get_current_user()->ID != 1) {
   require_once dirname( __FILE__ ) . '/admin/disable-updates.php';
}
require_once( HKT_INC_DIR . '/admin/hkt-slider.php' );
require_once( HKT_INC_DIR . '/admin/admin-fix.php' );

if (is_admin()) {

   /*-----------------------------------------------------------------------------------*/
   # Register main Scripts and Styles
   /*-----------------------------------------------------------------------------------*/
   function hkt_admin_register() {
      //global $pagenow;

      wp_register_script('hkt-admin-scripts', HKT_THEME_DIRECTORY_URI . '/inc/admin/hkt-admin-scripts.js', array('jquery'), false, false);
      wp_register_style('hkt-admin-style', HKT_THEME_DIRECTORY_URI . '/inc/admin/hkt-admin-style.css', array(), '', 'all');
      wp_enqueue_script( 'hkt-admin-scripts' );
      wp_enqueue_style( 'hkt-admin-style' );

      if (wp_get_current_user()->ID != 1) {
         echo '<style>

                #wp-admin-bar-mts-theme-options, .widget-liquid-left, #menu-appearance ul li:nth-child(6){display: none !important;}.widget-liquid-right{width: 100% !important;}.widget.open {margin-left: 0!important;margin-right: -250px!important;}
                
            </style>';
      }
   }
   add_action('admin_enqueue_scripts', 'hkt_admin_register', 1000);
}

/* STYLE FOR LOGIN PAGE */
function login_css() {
   wp_enqueue_style( 'login_css', HKT_THEME_DIRECTORY_URI. '/inc/admin/hkt-login-style.css' );
}
add_action('login_head', 'login_css');

function hkt_slider_img_src( $image_id , $size ) {
   global $post;
   $image_url = wp_get_attachment_image_src($image_id, $size );
   return $image_url[0];
}