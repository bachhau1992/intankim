<?php
if ( ! defined( 'ABSPATH' ) ) {
   exit;
}

//Use Redux Framework
$redux_path = HKT_INC_DIR . '/lib/redux-framework';
if( is_dir($redux_path) ):
   if(!class_exists('ReduxFramework')) {
      require_once($redux_path . '/ReduxCore/framework.php');
   }
   if(!isset($redux_demo)) {
      require_once(HKT_INC_DIR . '/admin/hkt-settings/config/redux.config.php');
   }
endif;
$redux_extensions_path = HKT_INC_DIR . '/lib/redux-extensions';
if( is_dir($redux_path) && is_dir($redux_extensions_path) ):
   require_once($redux_extensions_path . '/loader.php');
endif;

//Require file
require_once (HKT_INC_DIR . '/lib/meta-box/meta-box.php');
require_once (HKT_INC_DIR . '/lib/tax-meta-class/Tax-meta-class.php');