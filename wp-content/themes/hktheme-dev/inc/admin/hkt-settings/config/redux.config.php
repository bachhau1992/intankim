<?php
if (!class_exists('Redux')) {
   return;
}

$options_pages = array();
$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
$options_pages[''] = 'Select a page:';
foreach ($options_pages_obj as $page) {
   $options_pages[$page->ID] = $page->post_title;
}

$opt_name = HKT_THEME_NAME;
$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
   'opt_name' => $opt_name,
   'disable_tracking' => true,
   'display_name' => $theme->get('Name'),
   'display_version' => $theme->get('Version'),
   'menu_type' => 'menu',
   'allow_sub_menu' => false,
   'menu_title' => __('HKT Options', HKT_THEME_TEXTDOMAIN),
   'page_title' => __('HKT Options', HKT_THEME_TEXTDOMAIN),
   'google_api_key' => '',
   'google_update_weekly' => false,
   'async_typography' => true,
   //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
   'admin_bar' => true,
   'admin_bar_icon' => 'dashicons-portfolio',
   'admin_bar_priority' => 50,
   'global_variable' => HKT_THEME_NAME,
   'dev_mode' => false,
   'update_notice' => false,
   'customizer' => true,
   //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
   //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
   'page_priority' => null,
   'page_parent' => 'themes.php',
   'page_permissions' => 'manage_options',
   'menu_icon' => '',
   'last_tab' => '',
   'page_icon' => 'icon-themes',
   'page_slug' => 'hkt-options',
   'save_defaults' => true,
   'default_show' => false,
   'default_mark' => '',
   'show_import_export' => true,
   'transient_time' => 60 * MINUTE_IN_SECONDS,
   'output' => true,
   'output_tag' => true,
   // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
   'database' => '',
   'system_info' => false,
   //'compiler'             => true,
   'hints' => array(
      'icon' => 'el el-question-sign',
      'icon_position' => 'right',
      'icon_color' => 'lightgray',
      'icon_size' => 'normal',
      'tip_style' => array(
         'color' => 'red',
         'shadow' => true,
         'rounded' => false,
         'style' => '',
      ),
      'tip_position' => array(
         'my' => 'top left',
         'at' => 'bottom right',
      ),
      'tip_effect' => array(
         'show' => array(
            'effect' => 'slide',
            'duration' => '500',
            'event' => 'mouseover',
         ),
         'hide' => array(
            'effect' => 'slide',
            'duration' => '500',
            'event' => 'click mouseleave',
         ),
      ),
   )
);

$args['share_icons'][] = array(
   'url' => 'https://www.facebook.com/buikhanh88',
   'title' => 'Follow us on FB',
   'icon' => 'el el-facebook'
);

$args['intro_text'] = '';
$args['footer_text'] = '<strong>&copy; 2017 <a href="http://thietkeweb88.net/" target="_blank">HK THEME</a></strong>';

Redux::setArgs($opt_name, $args);

$tabs = array(
   array(
      'id' => 'redux-help-tab-1',
      'title' => __('Theme Information', HKT_THEME_TEXTDOMAIN),
      'content' => __('<p>If you have any question please check documentation <a href="http://samsoi.net">Documentation</a>. And that are beyond the scope of documentation, please feel free to contact us.</p>', HKT_THEME_TEXTDOMAIN)
   ),
);
Redux::setHelpTab($opt_name, $tabs);

// Set the help sidebar
$content = __('<p></p>', HKT_THEME_TEXTDOMAIN);
Redux::setHelpSidebar($opt_name, $content);

//Basic Settings
Redux::setSection($opt_name, array(
   'title' => __('Basic Settings', HKT_THEME_TEXTDOMAIN),
   'id' => 'basic-settings',
   'icon' => 'el el-home',
   'fields' => array(
      array(
         'id'       => 'frontend_logo',
         'type'     => 'media',
         'url'      => true,
         'title'    => __('Custom Logo', HKT_THEME_TEXTDOMAIN),
         'compiler' => 'true',
         'desc'     => '',
         'subtitle' => __( 'Set an image file for your logo', HKT_THEME_TEXTDOMAIN ),
         'default'  => array( 'url' => HKT_THEME_DIRECTORY_URI . "/assets/images/logo.png" ),
      ),
      array(
         'id' => 'phone',
         'type' => 'text',
         'title' => __('Phone', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '0988.395.331',
      ),
      array(
         'id' => 'hotline',
         'type' => 'text',
         'title' => __('Hotline', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('Set hotline number in header. Leave blank to hide phone number field', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '0988.395.331',
      ),
      array(
         'id' => 'email',
         'type' => 'text',
         'title' => __('Email', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => 'bvkhanh88@gmail.com',
      ),
      array(
         'id' => 'address',
         'type' => 'text',
         'title' => __('Address', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => 'HV Nông Nghiệp Việt Nam',
      ),
      array(
         'id' => 'hkt_footer',
         'type' => 'editor',
         'title' => __('Footer', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => file_get_contents(dirname(__FILE__) . '/templates/header-content.html'),
         'args' => array(
            'teeny' => true,
            'textarea_rows' => 20,
         )
      ),
      array(
         'id' => 'gallery_home',
         'type' => 'gallery',
         'title' => __('Add/Edit Gallery Home', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', HKT_THEME_TEXTDOMAIN),
         'desc' => __('This is the description field, again good for additional info.', HKT_THEME_TEXTDOMAIN),
      ),
   )
));

//Config Image Size
Redux::setSection($opt_name, array(
   'title' => __('Config Image Size', HKT_THEME_TEXTDOMAIN),
   'id' => 'config-image-size',
   'icon' => 'el el-picture',
   'fields' => array(
      array(
         'id' => 'section-featured-size',
         'type' => 'section',
         'title' => esc_html__('Featured Image Size', HKT_THEME_TEXTDOMAIN),
         'indent' => true
      ),
      array(
         'id' => 'hkt-featured-width',
         'type' => 'text',
         'title' => __('Featured Width (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '370',
      ),
      array(
         'id' => 'hkt-featured-height',
         'type' => 'text',
         'title' => __('Featured Height (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '297',
      ),
      array(
         'id' => 'section-slider-size',
         'type' => 'section',
         'title' => esc_html__('Slider Image Size', HKT_THEME_TEXTDOMAIN),
         'indent' => true
      ),
      array(
         'id' => 'hkt-slider-width',
         'type' => 'text',
         'title' => __('Slider Width (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '1170',
      ),
      array(
         'id' => 'hkt-slider-height',
         'type' => 'text',
         'title' => __('Slider Height (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '400',
      ),
      array(
         'id' => 'section-featuredbig-size',
         'type' => 'section',
         'title' => esc_html__('Featuredbig Image Size', HKT_THEME_TEXTDOMAIN),
         'indent' => true
      ),
      array(
         'id' => 'hkt-featuredbig-width',
         'type' => 'text',
         'title' => __('Featuredbig Width (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '770',
      ),
      array(
         'id' => 'hkt-featuredbig-height',
         'type' => 'text',
         'title' => __('Featuredbig Height (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '297',
      ),
      array(
         'id' => 'section-related-size',
         'type' => 'section',
         'title' => esc_html__('Related Image Size', HKT_THEME_TEXTDOMAIN),
         'indent' => true
      ),
      array(
         'id' => 'hkt-related-width',
         'type' => 'text',
         'title' => __('Related Width (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '400',
      ),
      array(
         'id' => 'hkt-related-height',
         'type' => 'text',
         'title' => __('Related Height (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '250',
      ),
      array(
         'id' => 'section-widgetthumb-size',
         'type' => 'section',
         'title' => esc_html__('Widgetthumb Image Size', HKT_THEME_TEXTDOMAIN),
         'indent' => true
      ),
      array(
         'id' => 'hkt-widgetthumb-width',
         'type' => 'text',
         'title' => __('Widgetthumb Width (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '65',
      ),
      array(
         'id' => 'hkt-widgetthumb-height',
         'type' => 'text',
         'title' => __('Widgetthumb Height (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '65',
      ),
      array(
         'id' => 'section-widgetfull-size',
         'type' => 'section',
         'title' => esc_html__('Widgetfull Image Size', HKT_THEME_TEXTDOMAIN),
         'indent' => true
      ),
      array(
         'id' => 'hkt-widgetfull-width',
         'type' => 'text',
         'title' => __('Widgetfull Width (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '300',
      ),
      array(
         'id' => 'hkt-widgetfull-height',
         'type' => 'text',
         'title' => __('Widgetfull Height (px)', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '200',
      ),
   )
));

//Styling Options
Redux::setSection($opt_name, array(
   'title' => __('Styling Options', HKT_THEME_TEXTDOMAIN),
   'id' => 'styling-settings',
   'icon' => 'el el-brush',
   'fields' => array(
      array(
         'id' => 'excerpt_length',
         'type' => 'text',
         'title' => __('Excerpt Length', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '60',
      ),
      array(
         'id' => 'sidebar_megamenu_number',
         'type' => 'select',
         'title' => __('Sidebar Megamenu Number', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'options' => array(
            '0' => 'Default',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
         ),
         'default' => '0'
      ),
      array(
         'id' => 'sticky_menu',
         'type' => 'switch',
         'title' => __('Sticky Menu', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
      array(
         'id' => 'sticky_header',
         'type' => 'switch',
         'required' => array('sticky_menu', '=', false),
         'title' => __('Sticky Header', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
      array(
         'id' => 'sticky_sidebar',
         'type' => 'switch',
         'title' => __('Sticky Sidebar', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
      array(
         'id' => 'scrollify',
         'type' => 'switch',
         'title' => __('Scrollify', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
      array(
         'id' => 'fullpage_scroll',
         'type' => 'switch',
         'title' => __('Fullpage Scroll', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
      array(
         'id' => 'lazy_load',
         'type' => 'switch',
         'title' => __('Lazy Load For Images', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
      array(
         'id' => 'smoth_scroll',
         'type' => 'switch',
         'title' => __('Smoth Scroll', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => true,
      ),
      array(
         'id' => 'lightbox_gallery',
         'type' => 'select',
         'title' => __('Lightbox for WordPress Galleries', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'options' => array(
            'lightgallery' => 'Light Gallery',
            'ilightbox' => 'ilightbox',
            'viewer' => 'viewer',
         ),
         'default' => 'lightgallery'
      ),
      array(
         'id' => 'responsive',
         'type' => 'switch',
         'title' => __('Responsive', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => true,
      ),
      array(
         'id' => 'breadcrumbs',
         'type' => 'switch',
         'title' => __('Breadcrumbs', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => true,
      ),
      array(
         'id' => 'breadcrumbs_delimiter',
         'type' => 'text',
         'required' => array('breadcrumbs', '=', true),
         'title' => __('Breadcrumbs Delimiter', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '',
      ),
   )
));

//Advance Settings
Redux::setSection($opt_name, array(
   'title' => __('Advance Settings', HKT_THEME_TEXTDOMAIN),
   'id' => 'advance-settings',
   //'icon'  => '',
   'fields' => array(
      array(
         'id' => 'dashboard_logo',
         'type' => 'media',
         'url' => true,
         'title' => __('WordPress Login page Logo'),
         'compiler' => 'true',
         'desc' => '',
         'subtitle' => __('Set an image file for your logo login page', HKT_THEME_TEXTDOMAIN),
         'default' => array('url' => HKT_THEME_DIRECTORY_URI . "/assets/images/logo.png"),
      ),
      array(
         'id' => 'dashboard_logo_url',
         'type' => 'text',
         'title' => __('WordPress Login page Logo URL', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => '',
      ),
      array(
         'id' => 'hide_adminbar_fontend',
         'type' => 'switch',
         'title' => __('Disable Frontend Adminbar', HKT_THEME_TEXTDOMAIN),
         'subtitle' => __('', HKT_THEME_TEXTDOMAIN),
         'desc' => __('', HKT_THEME_TEXTDOMAIN),
         'default' => false,
      ),
   )
));

// add-on compatibility
$acc_add_on_settings = apply_filters('hkt_options_acc_addon_settings', array());
if (!empty($acc_add_on_settings)) {
   Redux::setSection($opt_name, array(
      'title' => __('Accommodation Add-On Settings', HKT_THEME_TEXTDOMAIN),
      'id' => 'accommodation-add-settings',
      'subsection' => true,
      'fields' => array($acc_add_on_settings)
   ));
}

if (file_exists(dirname(__FILE__) . '/../README.md')) {
   $section = array(
      'icon' => 'el el-list-alt',
      'title' => __('Documentation', HKT_THEME_TEXTDOMAIN),
      'fields' => array(
         array(
            'id' => '17',
            'type' => 'raw',
            'markdown' => true,
            'content' => file_get_contents(dirname(__FILE__) . '/../README.md')
         ),
      ),
   );
   Redux::setSection($opt_name, $section);
}