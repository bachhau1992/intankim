<?php
if (hkt_get_option('hide_adminbar_fontend') == 1) {
    add_filter('show_admin_bar', '__return_false');
}

function hkt_maintenance_mode() {
    if(current_user_can('edit_themes') || is_user_logged_in()){
         //code here
    } else {
        if (!is_home() && !is_front_page() && !is_page('home')) {
            wp_die('<h1 style="color:#f00000">'.__('System maintenance', HKT_THEME_TEXTDOMAIN).'</h1><br /><p></p><br /><br /><a href="/">'.__('RETURN HOME', HKT_THEME_TEXTDOMAIN).'</a>');
        }
    }
}
if(hkt_get_option('maintenance_mode_enable')) add_action('get_header', 'hkt_maintenance_mode');

/* Redirect after Login on Wordpress */
function hkt_login_redirect() {
    return site_url() . '/wp-admin/index.php';
}
if (!current_user_can('manage_options')) { add_filter('login_redirect', 'hkt_login_redirect', 10, 3); }

/** REMOVE DEFAULT WIDGET IN ADMIN **/
function hkt_remove_dashboard_widgets() {
    global $wp_meta_boxes;

    //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    //unset($wp_meta_boxes['dashboard']['normal']['core']['welcome-panel']);

    unset($wp_meta_boxes['dashboard']['normal']['core']['woocommerce_dashboard_recent_reviews']);
    //unset($wp_meta_boxes['dashboard']['normal']['core']['woocommerce_dashboard_status']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);
    //remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
    //unset($wp_meta_boxes['dashboard']['side']['core']['redux_dashboard_widget']);
    remove_meta_box('redux_dashboard_widget', 'dashboard', 'side');

    remove_action('welcome_panel', 'wp_welcome_panel');
}
add_action('wp_dashboard_setup', 'hkt_remove_dashboard_widgets', 999 );
//if (!current_user_can('manage_options')) { add_action('wp_dashboard_setup', 'hkt_remove_dashboard_widgets', 999 ); }

/* CREAT WIDGET IN ADMIN PAGE */
function hkt_create_admin_widget_notice() {
    wp_add_dashboard_widget( 'hkt_notice', __('Introduction', HKT_THEME_TEXTDOMAIN).'', 'hkt_create_admin_widget_notice_callback' );
}
add_action( 'wp_dashboard_setup', 'hkt_create_admin_widget_notice' );
function hkt_create_admin_widget_notice_callback() {
    //echo do_shortcode('[hkt_category_posts]');
    echo '<h2>'. __('Website Management System', HKT_THEME_TEXTDOMAIN) .'</h2>';
    echo '<p><strong>'. __('WEBSITE INFORMATION', HKT_THEME_TEXTDOMAIN) .': </strong>' . get_bloginfo('name') . '</p>';
    echo '<p><strong>'. __('Hotline', HKT_THEME_TEXTDOMAIN) .': </strong>'. hkt_get_option('hotline') .'</p>';
    echo '<p><strong>'. __('Email', HKT_THEME_TEXTDOMAIN) .': </strong>'. hkt_get_option('email') .'</p>';
}

/* CUSTOMIZE FOOTER THANNK YOU */
function hkt_edit_footer_text($content) {
    return '<span id="footer-thankyou">Developed by <a href="http://thietkeweb88.net" target="_blank"><strong>Khanh Bui</strong></a></span>';
}
add_filter( 'admin_footer_text', 'hkt_edit_footer_text' );

/* CUSTOMIZE WP VERSION (FOOTER) */
function hkt_change_footer_version() {
    return '';
}
if (wp_get_current_user()->ID != 1) { add_filter( 'update_footer', 'hkt_change_footer_version', 9999); }

// Hide the "Please update now" notification
function hkt_hide_update_notice() {
    remove_action( 'admin_notices', 'update_nag', 3 );
}
if (wp_get_current_user()->ID != 1) { add_action( 'admin_notices', 'hkt_hide_update_notice', 1 ); }

// CUSTOMIZE WP-ADMIN BAR
function hkt_admin_bar_render() {
    global $wp_admin_bar;

    //Admin bar Back-end
    $wp_admin_bar->remove_menu('wp-logo');          /** Remove the WordPress logo **/
    $wp_admin_bar->remove_menu('about');            /** Remove the about WordPress link **/
    $wp_admin_bar->remove_menu('wporg');            /** Remove the WordPress.org link **/
    $wp_admin_bar->remove_menu('documentation');    /** Remove the WordPress documentation link **/
    $wp_admin_bar->remove_menu('support-forums');   /** Remove the support forums link **/
    $wp_admin_bar->remove_menu('feedback');         /** Remove the feedback link **/
    //$wp_admin_bar->remove_menu('site-name');      /** Remove the site name menu **/
    //$wp_admin_bar->remove_menu('view-site');        /** Remove the view site link **/
    $wp_admin_bar->remove_menu('wpseo-menu');        /** Remove the view site link **/
    $wp_admin_bar->remove_menu('updates');          /** Remove the updates link **/
    $wp_admin_bar->remove_menu('comments');         /** Remove the comments link **/
    $wp_admin_bar->remove_menu('new-content');      /** Remove the content link **/
    $wp_admin_bar->remove_menu('w3tc');             /** If you use w3 total cache remove the performance link **/
    //$wp_admin_bar->remove_menu('my-account');     /** Remove the user details tab **/

    //Admin bar Front-end
    if (wp_get_current_user()->ID != 1) {
        $wp_admin_bar->remove_menu('customize');
        $wp_admin_bar->remove_menu('themes');
        $wp_admin_bar->remove_menu('widgets');
        $wp_admin_bar->remove_menu('menus');
        $wp_admin_bar->remove_menu('mts-theme-options');
    }
}
add_action( 'wp_before_admin_bar_render', 'hkt_admin_bar_render', 999 );

// CHANGE TITLE LOGO ADMIN
function hkt_login_logo_title($title) {
    return get_bloginfo('title');
}
add_filter('login_headertitle', 'hkt_login_logo_title');

/*-----------------------------------------------------------------------------------*/
# Custom Dashboard login page logo
/*-----------------------------------------------------------------------------------*/
function hkt_login_logo() {
    if( hkt_get_option('dashboard_logo') ) {
        $logo = hkt_get_option('dashboard_logo');
        echo '<style  type="text/css"> .login h1 a {  background-image:url('.$logo["url"].')  !important; background-size: 274px 63px; width: 326px; height: 67px; } </style>';
    }
}
add_action('login_head',  'hkt_login_logo');

function hkt_login_logo_url() {
    return hkt_get_option('dashboard_logo_url');
}
if( hkt_get_option('dashboard_logo_url') ) add_filter( 'login_headerurl', 'hkt_login_logo_url' );

/*-----------------------------------------------------------------------------------*/
# Custom Favicon
/*-----------------------------------------------------------------------------------*/
function hkt_favicon() {
    $get_favacon = hkt_get_option('favicon_admin');
    $default_favicon = HKT_THEME_DIRECTORY_URI ."/favicon.ico";
    $custom_favicon = $get_favacon['url'];
    $favicon = (empty($custom_favicon)) ? $default_favicon : $custom_favicon;
    echo '<link rel="shortcut icon" href="'.$favicon.'" title="Favicon" />';
}
//add_action('wp_head', 'hkt_favicon');

/* ADD-REMOVE MENU ADMIN */
function hkt_admin_menus() {
   if (!is_admin())
      return;

   $submenu_remove_name = (hkt_get_option('submenu_remove_name'))?hkt_get_option('submenu_remove_name'):'theme_options';
   remove_submenu_page( 'themes.php', $submenu_remove_name.'' );

   global $submenu;
   if ( isset( $submenu[ 'themes.php' ] ) ) {
      foreach ( $submenu[ 'themes.php' ] as $index => $menu_item ) {
         if ( in_array( 'Header', $menu_item ) || in_array( 'Background', $menu_item ) ) {
            unset( $submenu[ 'themes.php' ][ $index ] );
         }
      }
   }

    if (wp_get_current_user()->ID != 1) {
        //remove_menu_page( 'index.php' ); // Menu Bảng tin
        //remove_menu_page( 'edit.php' ); // Menu Bài viết
        //remove_menu_page( 'upload.php' ); // Menu Media
        remove_menu_page('edit-comments.php'); // Menu Bình luận
        remove_submenu_page('index.php', 'update-core.php'); // Submenu Cập nhật
        remove_menu_page('mts-install-plugins');
    }
    if (wp_get_current_user()->ID != 1 && wp_get_current_user()->user_login != 'admin') {
        remove_menu_page('edit.php?post_type=page'); // Menu Trang
        remove_menu_page('themes.php'); // Menu Giao diện
        remove_menu_page('plugins.php'); // Menu Plugins
        remove_menu_page('users.php'); // Menu Thành viên
        remove_menu_page('tools.php'); // Menu Công cụ
        remove_menu_page('options-general.php'); // Menu cài đặt
        remove_menu_page('wpcf7'); // Xóa Menu Contact form 7
    }

    add_menu_page(__('Edit Module', HKT_THEME_TEXTDOMAIN), __('Edit Module', HKT_THEME_TEXTDOMAIN), 'manage_options', 'hkt-widget-menu', 'hkt_menu_pages_output');
    //add_submenu_page('hkt-widget-menu', 'Submenu Page Title', 'Whatever You Want', 'manage_options', 'my-menu' );
    //add_submenu_page('hkt-widget-menu', 'Submenu Page Title2', 'Whatever You Want2', 'manage_options', 'my-menu2' );
}
add_action('admin_menu', 'hkt_admin_menus', 50);

function hkt_menu_pages_output() {
    $menu_redirect = isset($_GET['page']) ? $_GET['page'] : false;
    if ($menu_redirect === 'hkt-widget-menu') {
      wp_safe_redirect(home_url('/wp-admin/widgets.php'), 301);
      exit;
    }
}

// remove customize theme
function hkt_customize_register($wp_customize) {
   $wp_customize->remove_section( 'background_color');
   $wp_customize->remove_section( 'background_image');
   $wp_customize->remove_section( 'header_image');
   $wp_customize->remove_section( 'add_partial');
   $wp_customize->remove_section( 'storefront_more');
   $wp_customize->remove_section( 'storefront_buttons');
   $wp_customize->remove_section( 'storefront_layout');
   $wp_customize->remove_section( 'storefront_typography');
   $wp_customize->remove_section( 'storefront_footer');
   $wp_customize->remove_section( 'title_tagline' );
}
add_action( 'customize_register', 'hkt_customize_register', 50 );

//Fix HTML5 contact form 7 on Firefox
//add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

//Not load css, js contact form 7
//add_filter( 'wpcf7_load_js', '__return_false' );
//add_filter( 'wpcf7_load_css', '__return_false' );

//Gavatar
function __hkt_default_local_avatar() {
    // this assumes default_avatar.png is in wp-content/themes/active-theme/images
    return get_bloginfo('template_directory') . '/images/default_avatar.png';
}
//add_filter( 'pre_option_avatar_default', '__hkt_default_local_avatar' );