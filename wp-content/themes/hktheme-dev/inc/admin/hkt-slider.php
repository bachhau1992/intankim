<?php
function hkt_slider_register() {

    $labels = array(
        'name' => __('HKT Sliders', HKT_THEME_TEXTDOMAIN),
        'singular_name' => __('Slider', HKT_THEME_TEXTDOMAIN),
        'all_items' => __('All Slider', HKT_THEME_TEXTDOMAIN),
        'add_new_item' => __('Add New Slider', HKT_THEME_TEXTDOMAIN),
        'add_new' => __('Add New', HKT_THEME_TEXTDOMAIN),
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'show_ui' => true,
        'menu_icon' => 'dashicons-images-alt', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true,
        'exclude_from_search' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 5,
        'rewrite' => array('slug' => 'slider', 'with_front' => false),
        'supports' => array('title')
    );

    register_post_type('hkt_slider', $args);
}
add_action('init', 'hkt_slider_register');

function hkt_slider_init() {
    add_meta_box("hkt_slider_config", "Config Slider", "hkt_slider_config", "hkt_slider", "normal", "high");
    add_meta_box("hkt_slider_slides", "HK Slides", "hkt_slider_slides", "hkt_slider", "normal", "high");
}
add_action("admin_init", "hkt_slider_init");

function hkt_slider_config() {
    global $post;
    // Add an nonce field so we can check for it later.
    wp_nonce_field('hkt_inner_slider_config_metabox', 'hkt_inner_slider_config_metabox_nonce');

    $slide_type = ( get_post_meta($post->ID, 'hkt_slide_type', true) )?get_post_meta($post->ID, 'hkt_slide_type', true):'';
    $slide_effect = ( get_post_meta($post->ID, 'hkt_slide_effect', true) )?get_post_meta($post->ID, 'hkt_slide_effect', true):'';
    $slide_theme = ( get_post_meta($post->ID, 'hkt_slide_theme', true) )?get_post_meta($post->ID, 'hkt_slide_theme', true):'';

    $output_html = '<div class="hkt-metabox hkt-field">';
    $output_html .=  '<div class="hkt-label"><label>'.__('Slide Type: ', HKT_THEME_TEXTDOMAIN ).'</label></div>';
    $output_html .= '<div class="hkt-input hkt-select">';
    $output_html .= '<select name="hkt_slide_type" id="hkt_slide_type">';
    $output_html .= '<option value="iosslider" '.selected("iosslider", $slide_type, false).'>iosSlider</option>';
    $output_html .= '<option value="nivo_slider" '.selected("nivo_slider", $slide_type, false).'>Nivo Slider</option>';
    $output_html .= '</select>';
    $output_html .= '</div></div>';

    $output_html .= '<div class="hkt-metabox hkt-field">';
    $output_html .=  '<div class="hkt-label"><label>'.__('Slide Theme: ', HKT_THEME_TEXTDOMAIN ).'</label></div>';
    $output_html .= '<div class="hkt-input hkt-select">';
    $output_html .= '<select name="hkt_slide_theme" id="hkt_slide_theme">';
    $output_html .= '<option value="default">Default</option>';
    $output_html .= '<option value="bar" '.selected("bar", $slide_theme, false).'>Bar</option>';
    $output_html .= '<option value="dark" '.selected("dark", $slide_theme, false).'>Dark</option>';
    $output_html .= '<option value="light" '.selected("light", $slide_theme, false).'>Light</option>';
    $output_html .= '</select>';
    $output_html .= '</div></div>';

    $output_html .= '<div class="hkt-metabox hkt-field">';
    $output_html .=  '<div class="hkt-label"><label>'.__('Slide Effect: ', HKT_THEME_TEXTDOMAIN ).'</label></div>';
    $output_html .= '<div class="hkt-input hkt-select">';
    $output_html .= '<select name="hkt_slide_effect" id="hkt_slide_effect">';
    $output_html .= '<option value="random">Random</option>';
    $output_html .= '<option value="sliceDown" '.selected("sliceDown", $slide_effect, false).'>(1) sliceDown</option>';
    $output_html .= '<option value="sliceDownLeft" '.selected("sliceDownLeft", $slide_effect, false).'>(2) sliceDownLeft</option>';
    $output_html .= '<option value="sliceUp" '.selected("sliceUp", $slide_effect, false).'>(3) sliceUp</option>';
    $output_html .= '<option value="sliceUpLeft" '.selected("sliceUpLeft", $slide_effect, false).'>(4) sliceUpLeft</option>';
    $output_html .= '<option value="sliceUpDown" '.selected("sliceUpDown", $slide_effect, false).'>(5) sliceUpDown</option>';
    $output_html .= '<option value="sliceUpDownLeft" '.selected("sliceUpDownLeft", $slide_effect, false).'>(6) sliceUpDownLeft</option>';
    $output_html .= '<option value="fold" '.selected("fold", $slide_effect, false).'>(7) fold</option>';
    $output_html .= '<option value="fade" '.selected("fade", $slide_effect, false).'>(8) fade</option>';
    $output_html .= '<option value="slideInRight" '.selected("slideInRight", $slide_effect, false).'>(9) slideInRight</option>';
    $output_html .= '<option value="slideInLeft" '.selected("slideInLeft", $slide_effect, false).'>(10) slideInLeft</option>';
    $output_html .= '<option value="boxRandom" '.selected("boxRandom", $slide_effect, false).'>(11) boxRandom</option>';
    $output_html .= '<option value="boxRain" '.selected("boxRain", $slide_effect, false).'>(12) boxRain</option>';
    $output_html .= '<option value="boxRainReverse" '.selected("boxRainReverse", $slide_effect, false).'>(13) boxRainReverse</option>';
    $output_html .= '<option value="boxRainGrow" '.selected("boxRainGrow", $slide_effect, false).'>(14) boxRainGrow</option>';
    $output_html .= '<option value="boxRainGrowReverse" '.selected("boxRainGrowReverse", $slide_effect, false).'>(15) boxRainGrowReverse</option>';
    $output_html .= '</select>';
    $output_html .= '</div></div>';

    echo $output_html;
}

function hkt_save_config($post_id) {
    // Check if our nonce is set.
    if ( ! isset( $_POST['hkt_inner_slider_config_metabox_nonce'] ) )
        return $post_id;
    $nonce = $_POST['hkt_inner_slider_config_metabox_nonce'];
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $nonce, 'hkt_inner_slider_config_metabox' ) )
        return $post_id;
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return $post_id;

    // Check the user's permissions.
    if ( 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) )
            return $post_id;

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) )
            return $post_id;
    }

    /* OK, its safe for us to save the data now. */
    global $post;

    if (!empty($_POST['hkt_slide_type']) && $_POST['hkt_slide_type'] != "") {
        update_post_meta($post->ID, 'hkt_slide_type', $_POST['hkt_slide_type']);
    } else {
        if (isset($post->ID))
            delete_post_meta($post->ID, 'hkt_slide_type');
    }

    if (!empty($_POST['hkt_slide_theme']) && $_POST['hkt_slide_theme'] != "") {
        update_post_meta($post->ID, 'hkt_slide_theme', $_POST['hkt_slide_theme']);
    } else {
        if (isset($post->ID))
            delete_post_meta($post->ID, 'hkt_slide_theme');
    }

    if (!empty($_POST['hkt_slide_effect']) && $_POST['hkt_slide_effect'] != "") {
        update_post_meta($post->ID, 'hkt_slide_effect', $_POST['hkt_slide_effect']);
    } else {
        if (isset($post->ID))
            delete_post_meta($post->ID, 'hkt_slide_effect');
    }


}
add_action('save_post', 'hkt_save_config');

function hkt_slider_slides() {
    global $post;
    $slider = '';
    $custom = get_post_custom($post->ID);

    if (!empty($custom["hkt_slider"][0]))
        $slider = unserialize($custom["hkt_slider"][0]);

    wp_enqueue_media();
    ?>
    <script>
        jQuery(document).ready(function () {

            jQuery(function () {
                jQuery("#hkt-slider-items").sortable({placeholder: "ui-state-highlight"});
            });

            /* Uploading files */
            var hkt_uploader;
            jQuery('#upload_add_slide').live('click', function (event) {

                event.preventDefault();
                hkt_uploader = wp.media.frames.hkt_uploader = wp.media({
                    title: '<?php _e( 'Insert Images | Hold CTRL to Multi Select .', HKT_THEME_TEXTDOMAIN ) ?>',
                    library: {
                        type: 'image'
                    },
                    button: {
                        text: 'Select',
                    },
                    multiple: true
                });

                hkt_uploader.on('select', function () {
                    var selection = hkt_uploader.state().get('selection');

                    selection.map(function (attachment) {
                        attachment = attachment.toJSON();
                        jQuery('#hkt-slider-items').append('<li id="listItem_' + nextCell + '" class="ui-state-default"><div class="widget-content option-item"><div class="slider-img"><img src="' + attachment.url + '" alt=""></div><label for="hkt_slider[' + nextCell + '][title]"><span><?php _e( 'Slide Title:', HKT_THEME_TEXTDOMAIN ) ?></span><input id="hkt_slider[' + nextCell + '][title]" name="hkt_slider[' + nextCell + '][title]" value="" type="text" /></label><label for="hkt_slider[' + nextCell + '][link]"><span><?php _e( 'Slide Link:', HKT_THEME_TEXTDOMAIN ) ?></span><input id="hkt_slider[' + nextCell + '][link]" name="hkt_slider[' + nextCell + '][link]" value="" type="text" /></label><label for="hkt_slider[' + nextCell + '][caption]"><span class="slide-caption"><?php _e( 'Slide Caption:', HKT_THEME_TEXTDOMAIN ) ?></span><textarea name="hkt_slider[' + nextCell + '][caption]" id="hkt_slider[' + nextCell + '][caption]"></textarea></label><input id="hkt_slider[' + nextCell + '][id]" name="hkt_slider[' + nextCell + '][id]" value="' + attachment.id + '" type="hidden" /><a class="del-cat"></a></div></li>');
                        nextCell++;
                    });
                });

                hkt_uploader.open();
            });

        });

    </script>

    <input id="upload_add_slide" type="button" class="button button-large button-primary builder_active"
           value="<?php _e('Add New Slide', HKT_THEME_TEXTDOMAIN) ?>">

    <ul id="hkt-slider-items">
        <?php
        $i = 0;
        if (!empty($slider)) {
            foreach ($slider as $slide):
                $i++; ?>
                <li id="listItem_<?php echo $i ?>" class="ui-state-default">
                    <div class="widget-content option-item">
                        <div class="slider-img"><?php echo wp_get_attachment_image($slide['id'], 'thumbnail'); ?></div>
                        <label
                            for="hkt_slider[<?php echo $i ?>][title]"><span><?php _e('Slide Title:', HKT_THEME_TEXTDOMAIN) ?> </span><input
                                id="hkt_slider[<?php echo $i ?>][title]"
                                name="hkt_slider[<?php echo $i ?>][title]"
                                value="<?php echo stripslashes($slide['title']) ?>" type="text"/></label>
                        <label
                            for="hkt_slider[<?php echo $i ?>][link]"><span><?php _e('Slide Link:', HKT_THEME_TEXTDOMAIN) ?></span><input
                                id="hkt_slider[<?php echo $i ?>][link]" name="hkt_slider[<?php echo $i ?>][link]"
                                value="<?php echo stripslashes($slide['link']) ?>" type="text"/></label>
                        <label for="hkt_slider[<?php echo $i ?>][caption]"><span
                                class="slide-caption"><?php _e('Slide Caption:', HKT_THEME_TEXTDOMAIN) ?></span><textarea
                                name="hkt_slider[<?php echo $i ?>][caption]"
                                id="hkt_slider[<?php echo $i ?>][caption]"><?php echo stripslashes($slide['caption']); ?></textarea></label>
                        <input id="hkt_slider[<?php echo $i ?>][id]" name="hkt_slider[<?php echo $i ?>][id]"
                               value="<?php echo $slide['id'] ?>" type="hidden"/>
                        <a class="del-cat"></a>
                    </div>
                </li>
            <?php endforeach;
        } else {
            echo '<p>' . __('Use the button above to add slides.', HKT_THEME_TEXTDOMAIN) . '</p>';
        } ?>
    </ul>
    <script> var nextCell = <?php echo $i+1 ?>;</script>

    <?php
}

function hkt_save_slide() {
    global $post;

    if (!empty($_POST['hkt_slider']) && $_POST['hkt_slider'] != "") {
        update_post_meta($post->ID, 'hkt_slider', $_POST['hkt_slider']);
    } else {
        if (isset($post->ID))
            delete_post_meta($post->ID, 'hkt_slider');
    }
}
add_action('save_post', 'hkt_save_slide');

function hkt_slider_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => __( 'Title', HKT_THEME_TEXTDOMAIN ),
    "slides" => __( 'Number of slides', HKT_THEME_TEXTDOMAIN ),
    "id" => __( 'ID', HKT_THEME_TEXTDOMAIN ),
    "date" => __( 'Date', HKT_THEME_TEXTDOMAIN ),
  );

  return $columns;
}
add_filter("manage_edit-hkt_slider_columns", "hkt_slider_edit_columns");

function hkt_slider_custom_columns($column){
    global $post;

    $original_post = $post;

    switch ($column) {
        case "slides":
            $custom_slider_args = array( 'post_type' => 'hkt_slider', 'p' => $post->ID, 'no_found_rows' => 1  );
            $custom_slider = new WP_Query( $custom_slider_args );
            while ( $custom_slider->have_posts() ) {
                $number =0;
                $custom_slider->the_post();
                $custom = get_post_custom($post->ID);
                if( !empty($custom["hkt_slider"][0])){
                    $slider = unserialize( $custom["hkt_slider"][0] );
                    echo $number = count($slider);
                }
                else echo 0;
            }

            $post = $original_post;
            wp_reset_query();
        break;

        case "id":
            echo $post->ID;
        break;
    }
}
add_action("manage_hkt_slider_posts_custom_column",  "hkt_slider_custom_columns");

?>