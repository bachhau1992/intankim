<?php
/**
 * Recent Product Scroll Widget
 */
if(!class_exists('HKT_Widget_Recent_Product_Scroll')) {
	class HKT_Widget_Recent_Product_Scroll extends WP_Widget
	{
		function HKT_Widget_Recent_Product_Scroll() {
			$widgetOps = array('classname' => 'HKT_Widget_recent_product_scroll woocommerce widget_products', 'description' => __('Display WooCommerce Recent Product Scroll', HKT_THEME_TEXTDOMAIN));
			parent::__construct('hkt_recent_product_scroll', __('HKT - Recent Product Scroll', HKT_THEME_TEXTDOMAIN), $widgetOps);
		}

		function widget( $args, $instance ) {

			$_actived = apply_filters( 'active_plugins', get_option( 'active_plugins' )  );
			if ( !in_array( "woocommerce/woocommerce.php", $_actived ) ) {
				return;
			}
			extract($args);
			$title = esc_attr(apply_filters( 'widget_title', $instance['title'] ));
			$cat = empty($instance['cat'])?'':esc_attr($instance['cat']);
			$number = empty($instance['number'])?8:absint($instance['number']);

			echo $before_widget;
			echo $before_title . $title . $after_title;

			$args = array(
				'post_type'	=> 'product',
				'product_cat'	=> $cat,
				'post_status' => 'publish',
				'ignore_sticky_posts'	=> 1,
				'posts_per_page' => $number,
				'orderby' => 'date',
				'order' => 'desc',				
				'meta_query' => array(
					array(
						'key' => '_visibility',
						'value' => array('catalog', 'visible'),
						'compare' => 'IN'
					)
				),
			);

			wp_reset_query();
			$recent_products = new WP_Query( $args );
			global $post;

			if($recent_products->post_count>0) {
				wp_enqueue_script('scrollbox');
				$random_id = 'hkt_recent_product_scroll_'.rand();
			?>

				<div class="hkt_recent_product_scroll scroll-img" id="<?php echo $random_id; ?>">
					<ul class="product_list_widget">
						<?php while ($recent_products->have_posts()) : $recent_products->the_post();?>
							<li <?php post_class(); ?>>
								<div class="product-inner">
									<a title="<?php echo esc_attr(get_the_title()) ?>" href="<?php echo get_permalink($post->ID); ?>">
									<?php  
										if ( has_post_thumbnail() ) {
											the_post_thumbnail('shop_thumbnail',array('title' => esc_attr(get_the_title()),'alt' => esc_attr(get_the_title()) ));
										} 
									?>
									<span class="product-title"><?php the_title(); ?></span>
								</a>
								<?php woocommerce_template_loop_price(); ?>									
								</div>
							</li>
						<?php endwhile;?>
					</ul>
				</div>

			<?php } ?>
			<?php wp_reset_query(); ?>
			<div class="clear"></div>

			<?php echo $after_widget; ?>

			<script type="text/javascript">
				jQuery(document).ready(function () {
					jQuery('#<?php echo $random_id; ?>').scrollbox({
						linear: true,
						step: 1,
						delay: 0,
						speed:30
					});			
				}); 
			</script>

<?php
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;	
			$instance['title'] 					=  $new_instance['title'];
			$instance['cat'] 					=  $new_instance['cat'];
			$instance['number'] 				=  $new_instance['number'];									
			
			return $instance;
		}

		function form( $instance ) {
			$array_default = array(
				'title' 				=> 'Recent Products Scroll'
				,'cat'				=> ''
				,'number' 			=> 8
			);
			$instance = wp_parse_args( (array) $instance, $array_default );
			$instance['title'] = esc_attr($instance['title']);
?>

		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Enter your title', HKT_THEME_TEXTDOMAIN); ?> : </label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" />
		</p>
		<p><label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Enter your Product Category Slug', HKT_THEME_TEXTDOMAIN); ?> : </label>
			<input class="widefat" id="<?php echo $this->get_field_id('cat'); ?>" name="<?php echo $this->get_field_name('cat'); ?>" type="text" value="<?php echo $instance['cat']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('A number of products', HKT_THEME_TEXTDOMAIN); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $instance['number']; ?>" />
		</p>

<?php	
		}

	}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "HKT_Widget_Recent_Product_Scroll" );' ) );