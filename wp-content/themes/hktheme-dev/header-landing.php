<?php
/**
 * The template for displaying the header.
 *
 * Displays everything from the doctype declaration down to the navigation.
 */
?>
<!DOCTYPE html>
<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<html class="no-js" <?php language_attributes(); ?>>
<head itemscope itemtype="http://schema.org/WebSite">
   <meta charset="<?php bloginfo('charset'); ?>">
   <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
   <!--[if IE ]>
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <![endif]-->
   <link rel="profile" href="http://gmpg.org/xfn/11"/>
   <?php mts_meta(); ?>
   <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
   <?php wp_head(); ?>

   <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
   new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
   j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
   'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
   })(window,document,'script','dataLayer','GTM-W8V7J3D');</script>
   <!-- End Google Tag Manager -->
</head>
<body id="blog" <?php body_class('main'); ?> itemscope itemtype="http://schema.org/WebPage">
<div id="fb-root"></div>
<script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1524800134509668";
      fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8V7J3D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php //$hkt_options_var = get_option( HKT_THEME_NAME );echo '<pre>';print_r( $hkt_options_var );echo '</pre>'; //hkt test data ?>

<div id="wrapper" class="hkt stretched">
   <div class="hkt-content">
      <!-- hktcustom: TOPBAR -->
      <?php if (1 == '2') { ?>
         <div id="topbar" class="topbar">
            <div class="container">
               <div class="pull-left">
                  <span>SỨC KHỎE, HẠNH PHÚC VÀ THỊNH VƯỢNG CHO MỌI KHÁCH HÀNG <span class="fa fa-heart"></span></span>
               </div>
               <div class="pull-right">
                  <?php echo hkt_woocommerce_cart_link(); ?>
               </div>
            </div>
         </div>
      <?php } ?>

      <header id="site-header" class="landing-page sticky-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
         <div id="header">
            <div class="container">
               <div class="logo-wrap">
                  <?php if(hkt_get_option('frontend_logo') && hkt_get_option('frontend_logo') != '') { ?>

                     <?php
                        //echo '<pre>';print_r(hkt_get_option('frontend_logo'));echo '</pre>';
                        $hkt_logo = hkt_get_option('frontend_logo');
                        $hkt_logo_w_h = ' width="' . $hkt_logo['width'] . '" height="' . $hkt_logo['height'] . '"';
                     ?>
                     <?php if (is_front_page() || is_home() || is_404()) { ?>
                        <h1 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($hkt_logo['url']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $hkt_logo_w_h; ?>></a>
                        </h1><!-- END #logo -->
                     <?php } else { ?>
                        <h2 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($hkt_logo['url']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $hkt_logo_w_h; ?>></a>
                        </h2><!-- END #logo -->
                     <?php } ?>

                  <?php } elseif ($mts_options['mts_logo'] != '') { ?>

                     <?php
                     $logo_id = mts_get_image_id_from_url($mts_options['mts_logo']);
                     $logo_w_h = '';
                     if ($logo_id) {
                        $logo = wp_get_attachment_image_src($logo_id, 'full');
                        if (!empty($logo[1]) && !empty($logo[2])) $logo_w_h = ' width="' . $logo[1] . '" height="' . $logo[2] . '"';
                     }
                     ?>
                     <?php if (is_front_page() || is_home() || is_404()) { ?>
                        <h1 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($mts_options['mts_logo']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $logo_w_h; ?>></a>
                        </h1><!-- END #logo -->
                     <?php } else { ?>
                        <h2 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($mts_options['mts_logo']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $logo_w_h; ?>></a>
                        </h2><!-- END #logo -->
                     <?php } ?>

                  <?php } else { ?>

                     <?php if (is_front_page() || is_home() || is_404()) { ?>
                        <h1 id="logo" class="text-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a>
                        </h1><!-- END #logo -->
                     <?php } else { ?>
                        <h2 id="logo" class="text-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a>
                        </h2><!-- END #logo -->
                     <?php } ?>
                     
                  <?php } ?>
               </div>

               <?php if ($mts_options['mts_header_search'] == '1') { ?>
                  <div class="header-search"><?php get_search_form(); ?></div>
               <?php } ?>

               <?php if ($mts_options['mts_show_header_social'] == '1' && !empty($mts_options['mts_header_social']) && is_array($mts_options['mts_header_social'])) { ?>
                  <div class="header-social">
                     <?php foreach ($mts_options['mts_header_social'] as $header_icons) : ?>
                        <?php if (!empty($header_icons['mts_header_icon']) && isset($header_icons['mts_header_icon'])) : ?>
                           <a href="<?php print $header_icons['mts_header_icon_link'] ?>" class="header-<?php print $header_icons['mts_header_icon'] ?>" style="background: <?php print $header_icons['mts_header_icon_bg_color'] ?>" target="_blank"><span class="fa fa-<?php print $header_icons['mts_header_icon'] ?>"></span></a>
                        <?php endif; ?>
                     <?php endforeach; ?>
                  </div>
               <?php } ?>

               <!-- hktcustom: Navigation in header -->
               <?php if (1 == '1') { ?>
                  <div id="hkt-navigation">
                     <nav class="navigation clearfix">
                        <?php if (has_nav_menu('landing-page-menu')) { ?>
                           <?php wp_nav_menu(array('theme_location' => 'landing-page-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                        <?php } ?>
                     </nav>
                  </div>
               <?php } ?>

            </div><!-- /.container header-->
         </div><!--#header-->

         <!-- hktcustom: Navigation bellow header -->
         <?php if ($mts_options['mts_show_primary_nav'] == '1') { ?>
         <?php if ($mts_options['mts_sticky_nav'] == '1') { ?>
            <div id="catcher" class="clear"></div>
            <div id="primary-navigation" class="sticky-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
         <?php } else { ?>
            <div id="primary-navigation" class="primary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
               <?php } ?>
               <a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'sociallyviral'); ?></a>
               <?php if (has_nav_menu('mobile')) { ?>
                  <nav class="navigation clearfix">
                     <?php if (has_nav_menu('landing-page-menu')) { ?>
                        <?php wp_nav_menu(array('theme_location' => 'landing-page-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                     <?php } else { ?>
                        <ul class="menu clearfix">
                           <?php wp_list_categories('title_li='); ?>
                        </ul>
                     <?php } ?>
                  </nav>
                  <nav class="navigation mobile-only clearfix mobile-menu-wrapper">
                     <?php wp_nav_menu(array('theme_location' => 'landing-page-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                  </nav>
               <?php } else { ?>
                  <nav class="navigation clearfix mobile-menu-wrapper">
                     <?php if (has_nav_menu('landing-page-menu')) { ?>
                        <?php wp_nav_menu(array('theme_location' => 'landing-page-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                     <?php } else { ?>
                        <ul class="menu clearfix">
                           <?php wp_list_categories('title_li='); ?>
                        </ul>
                     <?php } ?>
                  </nav>
               <?php } ?>
            </div>
         <?php } ?>
      </header>
      
      <div class="main-container<?php echo $hkt_sidebar_class; ?>">