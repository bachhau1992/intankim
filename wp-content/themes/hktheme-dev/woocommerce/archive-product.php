<?php
$options = get_option(MTS_THEME_NAME);
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header('shop'); ?>

<?php
global $wp_query;
$product_cat_columns = $layout_custom = '';
$product_cat_name = $wp_query->query_vars['product_cat'];
$product_cat_object = get_term_by('name', $product_cat_name, 'product_cat');
$product_cat_id = $product_cat_object->term_id;
$product_cat_columns = get_term_meta($product_cat_id, '_hkt_product_cat_columns', true);
$layout_custom = get_term_meta($product_cat_id, '_hkt_layout_custom_taxonomy', true);
?>
<div id="page">
	<article class="ss-full-width">
		<div id="content_box">
			<?php do_action('woocommerce_before_main_content'); ?>

				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>
				<?php do_action( 'woocommerce_archive_description' ); ?>
            <?php //hktcustom: custom loop wc
               global $woocommerce_loop;
               $_old_woocommerce_loop = $woocommerce_loop;
               $woocommerce_loop = $_old_woocommerce_loop;
               if( absint($product_cat_columns) > 0 ){
                  $woocommerce_loop['columns'] = absint($product_cat_columns);
               }
            ?>
				<?php if ( have_posts() ) : ?>
					<?php do_action( 'woocommerce_before_shop_loop' ); ?>
					<?php woocommerce_product_loop_start(); ?>
						<?php woocommerce_product_subcategories(); ?>
						<?php while ( have_posts() ) : the_post(); ?>
                     <?php if($layout_custom == ''): ?>
							   <?php wc_get_template_part( 'content', 'product' ); ?>
                     <?php else: ?>
                        <?php wc_get_template_part( 'content', 'product-'.$layout_custom ); ?>
                     <?php endif; ?>
						<?php endwhile; // end of the loop. ?>
					<?php woocommerce_product_loop_end(); ?>
					<?php do_action( 'woocommerce_after_shop_loop' ); ?>
				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
					<?php wc_get_template( 'loop/no-products-found.php' ); ?>
				<?php endif; ?>

			<?php do_action('woocommerce_after_main_content'); ?>
		</div>

      <?php if($layout_custom == 'scrolling') { ?>
         <div class="hkt-box-content product-scrolling col-md-12 nopad clear">
            <?php if ( have_posts() ) : ?>
               <div class="product-listing">
                  <?php while ( have_posts() ) : the_post(); ?>
                     <?php wc_get_template_part( 'content', 'product-scrolling-content' ); ?>
                  <?php endwhile; // end of the loop. ?>
               </div>
            <?php endif; ?>
         </div>
      <?php } ?>
	</article>
	<?php /*do_action('woocommerce_sidebar');*/ ?>
	<?php //get_sidebar(); ?>
</div><!--#page-->
<?php get_footer(); ?>
