<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php //hktcustom
$hkt_args = array( 'taxonomy' => 'product_cat' );
$hkt_terms = get_terms('product_cat', $hkt_args);
?>
<form class="woocommerce-ordering" method="get">
	<select name="product_cat" class="orderby">
		<option value="">Danh mục</option>
		<?php foreach ( $hkt_terms as $hkt_term ) : ?>
			<?php if($hkt_term->parent == 0) { ?>
				<option value="<?php echo esc_attr( $hkt_term->slug ); ?>"><?php echo esc_html( $hkt_term->name ); ?></option>
			<?php } else { ?>
				<option value="<?php echo esc_attr( $hkt_term->slug ); ?>"><?php echo '&nbsp;&nbsp;&nbsp;' . esc_html( $hkt_term->name ); ?></option>
			<?php } ?>
		<?php endforeach; ?>
	</select>
	<?php wc_query_string_form_fields( null, array( 'product_cat', 'submit' ) ); ?>
</form>

<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit' ) ); ?>
</form>
