<?php
if ( ! defined( 'ABSPATH' ) ) {
   exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
   return;
}
?>
<?php $product_image_size = 'product-scrolling'; ?>

<div id="product-<?php echo $product->get_slug(); ?>" <?php post_class('product-item'); ?>>
   <div class="product-item-inner hkt-container">
      <div class="product-thumb col-xs-12 col-sm-12 col-md-5 col-lg-5 nopad">
         <?php if (has_post_thumbnail()) { ?>
            <div class="product-thumb-one">
               <a href="##"><?php echo woocommerce_get_product_thumbnail($product_image_size);?></a>
            </div>
         <?php } ?>
      </div>
      <div class="product-info col-xs-12 col-sm-12 col-md-7 col-lg-7 nopad">
         <h3 class="product-name"><a href="##"><?php the_title(); ?></a></h3>
         <?php wc_get_template( 'loop/price.php' ); ?>
         <div class="text-center"><?php woocommerce_template_loop_add_to_cart(); ?></div>
         <div class="product-excerpt"><?php the_excerpt(); ?></div>
      </div>
      <div class="product-info-tab col-xs-12 col-sm-12 col-md-12 col-lg-12 nopad">
         <ul class="hkt-tabs hkt-tabs-<?php echo get_the_ID(); ?>">
            <li class="tab-link current" data-tab="tab-1-<?php echo get_the_ID(); ?>"><?php _e('Lợi ích', HKT_THEME_TEXTDOMAIN); ?></li>
            <li class="tab-link" data-tab="tab-2-<?php echo get_the_ID(); ?>"><?php _e('Thành phần', HKT_THEME_TEXTDOMAIN); ?></li>
            <li class="tab-link" data-tab="tab-3-<?php echo get_the_ID(); ?>"><?php _e('Sử dụng', HKT_THEME_TEXTDOMAIN); ?></li>
         </ul>
         <div id="tab-1-<?php echo get_the_ID(); ?>" class="tab-content tab-content-<?php echo get_the_ID(); ?> current">
            <?php
            $loi_ich = get_post_meta( get_the_ID(), '_hkt_product_loiich', true );
            $loi_ich = stripslashes( htmlspecialchars_decode(base64_decode($loi_ich)) );
            $loi_ich = apply_filters('the_content', $loi_ich);

            echo $loi_ich;
            ?>
         </div>
         <div id="tab-2-<?php echo get_the_ID(); ?>" class="tab-content tab-content-<?php echo get_the_ID(); ?>">
            <?php
            $thanh_phan = get_post_meta( get_the_ID(), '_hkt_product_thanhphan', true );
            $thanh_phan = stripslashes( htmlspecialchars_decode(base64_decode($thanh_phan)) );
            $thanh_phan = apply_filters('the_content', $thanh_phan);

            echo $thanh_phan;
            ?>
         </div>
         <div id="tab-3-<?php echo get_the_ID(); ?>" class="tab-content tab-content-<?php echo get_the_ID(); ?>">
            <?php
            $su_dung = get_post_meta( get_the_ID(), '_hkt_product_sudung', true );
            $su_dung = stripslashes( htmlspecialchars_decode(base64_decode($su_dung)) );
            $su_dung = apply_filters('the_content', $su_dung);

            echo $su_dung;
            ?>
         </div>

         <script>
            jQuery(document).ready(function ($) {
               $('ul.hkt-tabs-<?php echo get_the_ID(); ?> li').click(function() {
                  var tab_id = $(this).attr('data-tab');

                  $('ul.hkt-tabs-<?php echo get_the_ID(); ?> li').removeClass('current');
                  $('.tab-content-<?php echo get_the_ID(); ?>').removeClass('current');

                  $(this).addClass('current');
                  $("#"+tab_id).addClass('current');
               });
            });
         </script>
      </div>
   </div>
</div>

