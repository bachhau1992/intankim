<?php
/**
 * The template for displaying the footer.
 */
?>
<?php $mts_options = get_option(MTS_THEME_NAME);
$first_footer_num = empty($mts_options['mts_top_footer_num']) ? 3 : $mts_options['mts_top_footer_num']; ?>
</div><!--.main-container-->
<footer id="site-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
   <div class="container">
       <!--hktcustom-->
   <div class="general col-xs-12 col-sm-6 col-md-3 col-lg-3">
      <?php echo do_shortcode("[hkt_widget_sidebar sidebar_name=hkt-footer-1]"); ?>
   </div>
   <div class="ho-tro col-xs-12 col-sm-6 col-md-3 col-lg-3 clearfix">
      <?php echo do_shortcode("[hkt_widget_sidebar sidebar_name=hkt-footer-2]"); ?>
   </div>
   <div class="doi-tac col-xs-12 col-sm-6 col-md-3 col-lg-3 clearfix">
      <?php echo do_shortcode("[hkt_widget_sidebar sidebar_name=hkt-footer-3]"); ?>
   </div>
   <div class="facebook col-xs-12 col-sm-6 col-md-3 col-lg-3 clearfix">
      <?php echo do_shortcode("[hkt_widget_sidebar sidebar_name=hkt-footer-4]"); ?>
   </div>

   </div><!--.container-->

  
</footer><!--#site-footer-->
<div class="copyright_footer">
    <div class="container">
        <p>© Copyright 2016 Decobox</p>
    </div>
</div>

</div><!--/.hkt-content-->
</div><!--/#wrapper-->
<div id="topcontrol" class="fa fa-angle-up" title="Scroll To Top"></div>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b181d058859f57bdc7be81f/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<?php mts_footer(); ?>
<?php wp_footer(); ?>
</body>
</html>