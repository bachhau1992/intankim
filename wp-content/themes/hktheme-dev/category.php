<?php get_header(); ?>
<div id="page" class="home-<?php echo $mts_options['mts_home_layout']; ?>">
    <div id="content_box">
        <h1 class="postsby category">
            <span><?php single_cat_title(); ?></span>
        </h1>
        <?php 
        $category_description = category_description();
        $category_description = apply_filters('the_content', $category_description);
        if ( ! empty( $category_description ) )
            echo '<div class="clear"></div><div class="archive-meta">' . $category_description . '</div>';
        ?>
        <div class="ss-full-width box-news">
            <div class="hkt-list-posts">
                <?php $thumbnail = 'sociallyviral-featured'; ?>
                <?php $j = 1; if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <div class="post-item grid col-xs-12 col-sm-6 col-md-4 col-lg-4 nopad">
                        <div class="recent-item">
                            <?php if (function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumbnail != 'none') : ?>
                            <div class="post-thumbnail">
                             <a rel="nofollow" href="<?php the_permalink(); ?>" rel="bookmark">
                                <?php the_post_thumbnail($thumbnail); ?>
                             </a>
                            </div><!-- post-thumbnail /-->
                            <?php endif; ?>

                            <div class="post-box-title">
                             <a href="<?php the_permalink($post->ID); ?>" rel="bookmark"><?php echo get_the_title($post->ID); ?></a>
                            </div>

                            <div class="entry">
                             <p><?php hkt_excerpt() ?></p>
                            </div>
                        </div>
                    </div>

                <?php $j++; endwhile; endif; ?>
                <?php if ( $j !== 0 ) { // No pagination if there is no posts ?>
                    <?php mts_pagination(); ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div><!--#page-->
<?php get_footer(); ?>
