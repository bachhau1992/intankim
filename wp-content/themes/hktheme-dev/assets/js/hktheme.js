jQuery(document).ready(function ($) {
   $window = jQuery(window);
   $the_post = jQuery('#the-post');
   $wrapper = jQuery("#wrapper");

//Scroll To top
   var $topcontrol = jQuery('#topcontrol');
   $window.scroll(function () {
      if (jQuery(this).scrollTop() > 100) {
         $topcontrol.css({bottom: "82px"});
      } else {
         $topcontrol.css({bottom: "-100px"});
      }
   });
   $topcontrol.click(function () {
      jQuery('html, body').animate({scrollTop: '0px'}, 800);
      return false;
   });

//Dropdown list - select change language
   jQuery(".dropdown dt a").click(function () {
      jQuery(this).toggleClass('language-clicked');
      jQuery(".dropdown dd ul").toggle();
   });

   jQuery(".dropdown dd ul li a").click(function () {
      //var text = jQuery(this).html();
      var clone = jQuery(this).clone();
      clone.find('span.language-show').remove();
      var text = clone.html();
      jQuery(".dropdown dt > a > span").html(text);
      jQuery(".dropdown dt a").removeClass('language-clicked');
      jQuery(".dropdown dd ul").hide();
      //change_url(getSelectedValue("sample"));
   });
   function getSelectedValue(id) {
      return jQuery("#" + id).find("dt a span.value").html();
   }
   jQuery(document).bind('click', function (e) {
      var $clicked = jQuery(e.target);
      if (!$clicked.parents().hasClass("dropdown")) {
         jQuery(".dropdown dd ul").hide();
         jQuery(".dropdown dt a").removeClass('language-clicked');
      }
   });

//TESTI
   $('.testi_users .img_user').on('click', function(){
      $('.testi_users .img_user').removeClass('active');
      $(this).addClass('active');

      $('.testi_info .info').removeClass('active');
      var id = $(this).data('for'); //alert($(this).data('for'));
      $('#'+id+'.info').addClass('active');
   });

//WOOCOMMERCE
   $('.product-item-inner.scrolling-head').each(function() {
      var hkt_url_scrolling = $(this).children('a.woocommerce-LoopProduct-link').find('span.hkt-product-scrolling').data('href');
      $(this).find('a.woocommerce-LoopProduct-link').attr( 'href', hkt_url_scrolling);
   });

   $('#ah_search').bind('click', function() {
      if (!$('#search_mini_form').hasClass('show')) {
         $('#search_mini_form').addClass('show');
         $('.tool-search .phone-text').hide();
      } else {
         $('#search_mini_form').removeClass('show');
         $('.tool-search .phone-text').show();
      }
   });

   $('#ah_search_m').on('click', function() {
      $('#search_mini_form_m').addClass('show');
      $('.header-inner-mobile').addClass('large');
   });
   $('#cancel_search_m').on('click', function() {
      $('#search_mini_form_m').removeClass('show');
      $('.header-inner-mobile').removeClass('large');
   });

   $('body.home ul.products li.product-category').last().find('a img').attr({'src': '/wp-content/uploads/2017/09/more-more.jpg', 'srcset':''});

}); /**-- End jQuery(document).ready(); --**/

//images Scroll
jQuery(function () {
   var win_height_padded = $window.height() * .9;

   $window.on('scroll', hktRevealOnScroll);

   function hktRevealOnScroll() {
      var scrolled = $window.scrollTop(),
         win_height_padded = $window.height() * .9;

      jQuery("body.lazy-enabled #footer .post-thumbnail, body.lazy-enabled #main div.post-thumbnail, body.lazy-enabled #page img:not(.viewer-player img), body.lazy-enabled #featured-posts").each(function () {
         var $this = jQuery(this),
            offsetTop = $this.offset().top;

         if (scrolled + win_height_padded > offsetTop) {
            jQuery(this).addClass('hkt-appear');
         }
      });

      jQuery("#page .hkt-fade").each(function () {
         var $this = jQuery(this),
            offsetTop = $this.offset().top;

         if (scrolled + win_height_padded > offsetTop) {
            jQuery(this).addClass('in-view');
         } else {
            jQuery(this).removeClass('in-view');
         }
      });
   }

   hktRevealOnScroll();
});


//JS ADVANCE

var hkt = hkt || {};

(function ($) {
   "use strict";

   var $window = $(window),
      $body = $('body'),
      isRTL = $body.hasClass('rtl'),
      deviceAgent = navigator.userAgent.toLowerCase(),
      isMobile = deviceAgent.match(/(iphone|ipod|android|iemobile)/),
      isMobileAlt = deviceAgent.match(/(iphone|ipod|ipad|android|iemobile)/),
      isAppleDevice = deviceAgent.match(/(iphone|ipod|ipad)/),
      isIEMobile = deviceAgent.match(/(iemobile)/);

   hkt.common = {
      init: function () {
         this.owlCarousel();
         setTimeout(hkt.common.owlCarouselRefresh,1000);
         setTimeout(hkt.common.owlCarouselCenter,1000);
         setTimeout(hkt.common.owlCarouselCenter,2000);
      },
      windowResized: function () {
         this.canvasSidebar();
         this.adminBarProcess();
         setTimeout(hkt.common.owlCarouselRefresh, 1000);
         setTimeout(hkt.common.owlCarouselCenter, 1000);
      },
      windowScroll: function () {
         this.canvasSidebar();
      },
      owlCarousel: function () {
         $('.slider-wrapper ul.products').addClass('owl-carousel');
         $('.slider-wrapper .owl-carousel:not(.manual):not(.owl-loaded)').each(function () {
            var $slider = $(this),
               defaults = {
                  items: 3,
                  nav: true,
                  navText: ['<i class="fa fa-arrow-left"></i> ' + 'Back', 'Next' + ' <i class="fa fa-arrow-right"></i>'],
                  dots: false,
                  loop: true,
                  center: false,
                  mouseDrag: true,
                  touchDrag: true,
                  pullDrag: true,
                  freeDrag: false,

                  margin: 0,
                  stagePadding: 0,

                  merge: false,
                  mergeFit: true,
                  autoWidth: false,

                  startPosition: 0,
                  rtl: isRTL,

                  smartSpeed: 250,
                  fluidSpeed: false,
                  dragEndSpeed: false,

                  autoplay: false,
                  autoPlaySpeed: 5000,
                  autoPlayTimeout: 5000,
                  autoplayHoverPause: true
               },
               config = $.extend({}, defaults, $slider.parent().parent().data("owl-config"));
            if ($slider.parent().parent().hasClass('product-split-screen')) {
               config = $.extend({}, config, {
                  responsive: {
                     0: {
                        items: 1
                     },
                     480: {
                        items: 2
                     },
                     768: {
                        items: 1
                     },
                     800: {
                        items: 2
                     },
                     1200: {
                        items: 3
                     }
                  }
               });
            }
            // Initialize Slider
            $slider.imagesLoaded({background: true}, function () {
               $slider.owlCarousel(config);
            });

            $slider.on('changed.owl.carousel', function (e) {
               var $container = $('.archive-masonry .blog-wrap');
               setTimeout(function () {
                  $container.isotope('layout');
               }, 500);
            });
         });
      },
      owlCarouselRefresh : function() {
         $('.owl-carousel.owl-loaded').each(function(){
            var $this = $(this),
               $slider = $this.parents('.slider-wrapper1').data('owl-config');
            if (typeof ($slider) != 'undefined') {
               if ($slider.options.autoHeight) {
                  var maxHeight = 0;
                  $('.owl-item.active',$this).each(function(){
                     if ($(this).outerHeight() > maxHeight) {
                        maxHeight = $(this).outerHeight();
                     }
                  });

                  $('.owl-height',$this).css('height', maxHeight + 'px');
               }
            }
         });
      },
      owlCarouselCenter: function() {
         $('.product-listing > .owl-nav-center').each(function(){
            var $this = $(this);
            $this.imagesLoaded({background: true},function(){
               var top = $('img',$this).height() / 2  ;
               if (window.matchMedia('(min-width: 1350px)').matches) {
                  $('.owl-nav > div',$this).css('top', top +  'px');
               } else {
                  $('.owl-nav > div',$this).css('top','');
               }
            });
         });
      },
   };

   hkt.woocommerce = {}

   hkt.onReady = {
      init: function () {
         hkt.common.init();
      }
   };

   hkt.onLoad = {
      init: function () {

      }
   };

   hkt.onResize = {
      init: function () {

      }
   };

   hkt.onScroll = {
      init: function () {

      }
   };

   $(window).resize(hkt.onResize.init);
   $(window).scroll(hkt.onScroll.init);
   $(document).ready(hkt.onReady.init);
   $(window).load(hkt.onLoad.init);

})(jQuery);