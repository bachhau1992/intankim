<?php
/**
 * hktheme engine room
 *
 * @package hktheme
 */

// Child theme enqueue
add_action('wp_enqueue_scripts', 'hkt_child_theme_enqueue_styles', 11);
function hkt_child_theme_enqueue_styles() {
   wp_deregister_style('hkt-parent-style');
   wp_register_style('hkt-parent-style', get_template_directory_uri() . '/style.css');
   wp_enqueue_style('hkt-parent-style');
}

session_start();
ob_start();

//constants
define( 'HKT_THEME_NAME', 'hkt_developer' );
define( 'HKT_THEME_TEXTDOMAIN', 'hkt' );
define( 'HKT_THEME_DIRECTORY', get_stylesheet_directory() );
define( 'HKT_THEME_DIRECTORY_URI', get_stylesheet_directory_uri() );
define( 'HKT_INC_DIR', dirname(__FILE__) . '/inc' );

// set content width
if (!isset($content_width)) $content_width = 900;

//** GET THEME OPTIONS ====**********=>
$hkt_options = get_option(HKT_THEME_NAME);
function hkt_get_option($name) {
   $hkt_options = get_option(HKT_THEME_NAME);
   if (!empty($hkt_options[$name]))
      return $hkt_options[$name];

   return false;
}

//** GLOB INCLUDE FILE ====**********=>
$hkt_boot_files = glob( dirname(__FILE__). "/inc/*.php" );
sort( $hkt_boot_files );
foreach ( $hkt_boot_files as $filename ) {
   $file = basename( $filename, '.php' );
   if ( file_exists( $filename ) && apply_filters( "hkt_pre_boot_$file", '__return_true' ) ) {
      require_once( $filename );
   }
}
//custom excerpt length
function wpdocs_custom_excerpt_length( $length ) {
    return 10;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

//** SETUP THEME ====**********=>
add_action('after_setup_theme', 'hkt_setup_theme', 999);
function hkt_setup_theme() {
   add_theme_support('automatic-feed-links');
   add_theme_support('title-tag');
   add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
   add_theme_support('post-thumbnails');
   add_theme_support('post-formats', array('video'));

   add_filter('enable_post_format_ui', '__return_false');

   load_child_theme_textdomain('sociallyviral', HKT_THEME_DIRECTORY . '/languages');

   add_theme_support( 'post-thumbnails' );
   set_post_thumbnail_size( 300, 220, true );
   add_image_size( 'sociallyviral-featured', ( hkt_get_option('hkt-featured-width')==false )?370:absint(hkt_get_option('hkt-featured-width')), ( hkt_get_option('hkt-featured-height')==false )?297:absint(hkt_get_option('hkt-featured-height')), true ); //featured
   add_image_size( 'sociallyviral-slider', ( hkt_get_option('hkt-slider-width')==false )?1170:absint(hkt_get_option('hkt-slider-width')), ( hkt_get_option('hkt-slider-height')==false )?400:absint(hkt_get_option('hkt-slider-height')), true );//slider
   add_image_size( 'sociallyviral-featuredbig', ( hkt_get_option('hkt-featuredbig-width')==false )?770:absint(hkt_get_option('hkt-featuredbig-width')), ( hkt_get_option('hkt-featuredbig-height')==false )?297:absint(hkt_get_option('hkt-featuredbig-height')), true ); //featured full width
   add_image_size( 'sociallyviral-related', ( hkt_get_option('hkt-related-width')==false )?400:absint(hkt_get_option('hkt-related-width')), ( hkt_get_option('hkt-related-height')==false )?250:absint(hkt_get_option('hkt-related-height')), true ); //related
   add_image_size( 'sociallyviral-widgetthumb', ( hkt_get_option('hkt-widgetthumb-width')==false )?65:absint(hkt_get_option('hkt-widgetthumb-width')), ( hkt_get_option('hkt-widgetthumb-height')==false )?65:absint(hkt_get_option('hkt-widgetthumb-height')), true ); //widget
   add_image_size( 'sociallyviral-widgetfull', ( hkt_get_option('hkt-widgetfull-width')==false )?300:absint(hkt_get_option('hkt-widgetfull-width')), ( hkt_get_option('hkt-widgetfull-height')==false )?200:absint(hkt_get_option('hkt-widgetfull-height')), true ); //sidebar full width
   add_image_size('product-scrolling', 550, 350, true);

   add_filter('widget_text', 'do_shortcode');

   // add post formats
   //add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio', 'chat'));

   register_nav_menus( array(
      'primary-menu' => __( 'HKT Header', HKT_THEME_TEXTDOMAIN ),
      'footer-menu' => __( 'HKT Footer', HKT_THEME_TEXTDOMAIN ),
      'mobile' => __( 'HKT Mobile', HKT_THEME_TEXTDOMAIN ),
      'landing-page-menu' => __( 'HKT Landing Page', HKT_THEME_TEXTDOMAIN )
   ) );

   if (hkt_isWooCommerce()) {
      add_theme_support('woocommerce');
      add_theme_support( 'wc-product-gallery-zoom' );
      add_theme_support( 'wc-product-gallery-lightbox' );
      add_theme_support( 'wc-product-gallery-slider' );
   }

   // Declare support for selective refreshing of widgets.
   add_theme_support( 'customize-selective-refresh-widgets' );

   // This theme styles the visual editor with editor-style.css to match the theme style.
   add_editor_style();
}

//** ADD CSS ====**********=>
add_action('wp_enqueue_scripts', 'hkt_css', 1000);
function hkt_css() {
   wp_enqueue_style('hktheme-css', HKT_THEME_DIRECTORY_URI . '/assets/css/hktheme.css', array());
   wp_enqueue_style('hkt-plugins-css', HKT_THEME_DIRECTORY_URI . '/assets/css/hkt-plugins.css', array());
}

//** ADD JQUERY ====**********=>
add_action('wp_enqueue_scripts', 'hkt_scripts', 1000);
function hkt_scripts() {
   if(!hkt_check_plugin_active('hkt-framework/hkt-framework')) {
      wp_enqueue_script( 'hktheme-script', HKT_THEME_DIRECTORY_URI . '/assets/js/hktheme.js', array( 'jquery' ),  false, true );
      wp_enqueue_script( 'hkt-plugins-scripts', HKT_THEME_DIRECTORY_URI . '/assets/js/plugins.js', array( 'jquery' ), false, true );
   }

   wp_register_script( 'hkt-parallax', HKT_THEME_DIRECTORY_URI . '/assets/js/parallax.js', array( 'jquery' ), false, true );
   if(is_singular()) wp_enqueue_script('hkt-parallax');
}

//** Remove inline styles processed by the handle name. ====**********=>
if(!is_customize_preview()) add_action("print_styles_array", 'hkt_remove_inline_style', 100000 );
function hkt_remove_inline_style($styles) {
   $my_handles = array('', ''); // your custom handle here, the one declared as $style in question
   if ( !empty( $styles ) ) {
      foreach ( $styles as $i => $style ) {
         foreach($my_handles as $my_handle) {
            if ( $my_handle === $style ) {
               unset( $styles[$i] );
            }
         }
      }
   }
   return $styles;
}

//Checks plugin activate
function hkt_check_plugin_active($plugin) {
   if (is_multisite()) {
      include_once(ABSPATH . 'wp-admin/includes/plugin.php');
      return is_plugin_active($plugin . '.php');
   } else {
      return in_array($plugin . '.php', apply_filters('active_plugins', get_option('active_plugins')));
   }
}

//** CHECK WOOCOMMERCE FUNCTION ====**********=>
function hkt_isWooCommerce() {
   if (is_multisite()) {
      include_once(ABSPATH . 'wp-admin/includes/plugin.php');
      return is_plugin_active('woocommerce/woocommerce.php');
   } else {
      return in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')));
   }
}

//Checks if the current page is a product archive (@return boolean)
function hkt_is_product_archive() {
   if ( hkt_isWooCommerce() ) {
      if ( is_shop() || is_product_taxonomy() || is_product_category() || is_product_tag() ) {
         return true;
      } else {
         return false;
      }
   } else {
      return false;
   }
}

//Get sidebar location
function hkt_sidebar_location() {
   global $wp_query, $post, $mts_options, $hkt_options;
   $hkt_sidebar_location = $mts_options['mts_layout'];

   if ( is_page() || is_single() ) {
      $hkt_sidebar_location_singular_key = '_mts_sidebar_location';
      $sidebar_location_single = get_post_meta( get_the_ID(), $hkt_sidebar_location_singular_key.'', true );
      if (!empty($sidebar_location_single) && $sidebar_location_single != '') $hkt_sidebar_location = $sidebar_location_single;
   }

   if ( is_category() ) {
      $category_id = get_query_var('cat') ;
      $sidebar_location_taxonomy = get_term_meta( $category_id, '_hkt_sidebar_location_taxonomy', true );
      if (!empty($sidebar_location_taxonomy) && $sidebar_location_taxonomy != '')
         $hkt_sidebar_location = $sidebar_location_taxonomy;
   }

   if ( hkt_is_product_archive() ) {
      if ( is_product_category() ) {
         $product_cat_name = $wp_query->query_vars['product_cat'];
         $product_cat_object = get_term_by('name', $product_cat_name, 'product_cat');
         $product_cat_id = $product_cat_object->term_id;
         $sidebar_location_taxonomy = get_term_meta( $product_cat_id, '_hkt_sidebar_location_taxonomy', true );
         if (!empty($sidebar_location_taxonomy) && $sidebar_location_taxonomy != '')
            $hkt_sidebar_location = $sidebar_location_taxonomy;
      }
   }

   return $hkt_sidebar_location;
}

//Custom sidebar.
function hkt_custom_sidebar() {
   global $wp_query, $post, $wp_registered_sidebars, $hkt_options;

   $sidebar = $hkt_sidebar_custom = '';

   if ((function_exists('mts_custom_sidebar'))) {
      $sidebar = mts_custom_sidebar();
   }

   if (is_category()) {
      $category_id = get_query_var('cat');
      $sidebar_custom_taxonomy = get_term_meta($category_id, '_hkt_sidebar_custom_taxonomy', true);
      if (!empty($sidebar_custom_taxonomy) && $sidebar_custom_taxonomy != '')
         $sidebar = $sidebar_custom_taxonomy;
   }

   if(hkt_isWooCommerce()) {
      if (is_product_category()) {
         $sidebar = 'sidebar';
         $product_cat_name = $wp_query->query_vars['product_cat'];
         $product_cat_object = get_term_by('name', $product_cat_name, 'product_cat');
         $product_cat_id = $product_cat_object->term_id;
         $sidebar_custom_taxonomy = get_term_meta($product_cat_id, '_hkt_sidebar_custom_taxonomy', true);
         if (!empty($sidebar_custom_taxonomy) && $sidebar_custom_taxonomy != '')
            $sidebar = $sidebar_custom_taxonomy;
      }

      if (is_shop()) {
         $sidebar_custom_shop = 'sidebar';
         $sidebar = $sidebar_custom_shop;
      }

      if (is_product()) {
         $sidebar_custom_single_product = 'sidebar';
         $sidebar = $sidebar_custom_single_product;
      }
   }

   return $sidebar;
}

//Custom `<article>` class name.
if ( ! function_exists( 'hkt_article_class' ) ) {
   function hkt_article_class() {
      $class = 'article';

      // sidebar or full width
      if (hkt_custom_sidebar() == 'hkt_nosidebar') {
         $class = 'ss-full-width';
      } else if (function_exists('mts_custom_sidebar')) {
         if ( mts_custom_sidebar() == 'mts_nosidebar' ) {
            $class = 'ss-full-width';
         }
      }

      echo $class;
   }
}

//Remove ver parameter from CSS and JS file calls
if ( hkt_get_option('hkt_remove_ver_params') && hkt_get_option('hkt_remove_ver_params') == 1 ) {
   function hkt_remove_script_version( $src ) {
      if ( is_admin() )
         return $src;

      $parts = explode( '?ver', $src );
      return $parts[0];
   }
   add_filter( 'script_loader_src', 'hkt_remove_script_version', 15, 1 );
   add_filter( 'style_loader_src', 'hkt_remove_script_version', 15, 1 );
}

//Sanitize file upload filenames
function hkt_sanitize_file_name_chars($filename) {
   $sanitized_filename = remove_accents($filename); // Convert to ASCII
   // Standard replacements
   $invalid = array(
      ' ' => '-',
      '%20' => '-',
      '_' => '-'
   );
   $sanitized_filename = str_replace(array_keys($invalid), array_values($invalid), $sanitized_filename);

   $sanitized_filename = preg_replace('/[^A-Za-z0-9-\. ]/', '', $sanitized_filename); // Remove all non-alphanumeric except .
   $sanitized_filename = preg_replace('/\.(?=.*\.)/', '', $sanitized_filename); // Remove all but last .
   $sanitized_filename = preg_replace('/-+/', '-', $sanitized_filename); // Replace any more than one - in a row
   $sanitized_filename = str_replace('-.', '.', $sanitized_filename); // Remove last - if at the end
   $sanitized_filename = strtolower($sanitized_filename); // Lowercase

   return $sanitized_filename;
}
add_filter('sanitize_file_name', 'hkt_sanitize_file_name_chars', 10);

//Custom Classes for body
add_filter('body_class','hkt_body_custom_class');
function hkt_body_custom_class($classes) {
   if( hkt_get_option('dark_skin') )
      $classes[] = 'dark-skin';

   if( hkt_get_option('lazy_load') )
      $classes[] = 'lazy-enabled';
   return $classes;
}

//Fix Shortcodes
add_filter('widget_text', 'hkt_fix_shortcodes');
add_filter('the_content', 'hkt_fix_shortcodes');
function hkt_fix_shortcodes($content) {
   $array = array (
      '[raw]'         => '',
      '[/raw]'        => '',
      '<p>[raw]'      => '',
      '[/raw]</p>'    => '',
      '[/raw]<br />'  => '',
      '<p>['          => '[',
      ']</p>'         => ']',
      ']<br />'       => ']'
   );

   $content = strtr($content, $array);

   return $content;
}

//Change The Default WordPress Excerpt Length
function hkt_excerpt_global_length( $length ) {
   if( hkt_get_option( 'excerpt_length' ) )
      return hkt_get_option( 'excerpt_length' );
   else return 60;
}
function hkt_excerpt() {
   add_filter( 'excerpt_length', 'hkt_excerpt_global_length', 999 );
   echo get_the_excerpt();
}

//Title limit
if(!function_exists ('hkt_title_limit')) {
   function hkt_title_limit($title, $char) {
      //$title = substr($title,0,$char);
      if (strlen($title) > $char){
         $title = substr(substr($title, 0, $char), 0, strrpos(substr($title, 0, $char), " ", 0));
         $title = $title . "...";
      }
      return $title;
   }
}

//Show HKT Search Form
function hkt_search_form($type='') {
   ob_start(); ?>

   <?php if($type == '') { ?>
   <div id="hkt-search" class="hidden-mobile">
      <div class="icon-for-search"><span class="fa fa-search"></span></div>
      <div id="hkt-collapse-s" class="search-wrapper">
         <div class="hkt-container">
            <div class="form">
               <form method="get" action="" id="frmsearch">
                  <div class="col-md-12 col-sm-6 close"><span aria-expanded="false" data-toggle="collapse" href="#collapse_s" class="collapsed">&nbsp;</span></div>
                  <div class="col-md-12 col-sm-6"><hr class="hr1"></div><div class="clearfix"></div>
                  <div class="col-md-12 col-sm-6">
                     <div class="col-md-10 nopad"><span class="sp1">Tìm kiếm:</span><input type="text" name="s" id="keyword" class="s" value=""></div>
                     <div class="col-md-2 nopad"><input type="submit" value="" name="cmdsearch" class="cmdsearch"></div>
                  </div>
                  <div class="col-md-12 col-sm-6"><hr class="hr2"></div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <?php } else { ?>
      <form id="search_mini_form" action="" method="get">
         <div class="search_form_mini">
            <input id="" autocomplete="off" type="text" name="s" value="" class="input-text required-entry form-control" maxlength="128" placeholder="Bạn cần tìm gì ?">
            <input type="submit" value="<?php _e('Tìm', HKT_THEME_TEXTDOMAIN); ?>" name="cmdsearch" class="cmdsearch">
            <input type="hidden" name="post_type" value="product">
            <span id="cancel_search" class=""><?php _e('Hủy', HKT_THEME_TEXTDOMAIN); ?></span>
         </div>
      </form>
   <?php } ?>

   <?php
   return ob_get_clean();
}

/**
 * Is SubPage?
 * Checks if the current page is a sub-page and returns true or false.
 *
 * @param  $page mixed optional ( post_name or ID ) to check against.
 * @return boolean
 */
function hkt_is_subpage( $page = null ) {
   global $post;
   // is this even a page?
   if ( ! is_page() )
      return false;
   // does it have a parent?
   if ( ! isset( $post->post_parent ) OR $post->post_parent <= 0 )
      return false;
   // is there something to check against?
   if ( ! isset( $page ) ) {
      // yup this is a sub-page
      return true;
   } else {
      // if $page is an integer then its a simple check
      if ( is_int( $page ) ) {
         // check
         if ( $post->post_parent == $page )
            return true;
      } else if ( is_string( $page ) ) {
         // get ancestors
         $parent = get_ancestors( $post->ID, 'page' );
         // does it have ancestors?
         if ( empty( $parent ) )
            return false;
         // get the first ancestor
         $parent = get_post( $parent[0] );
         // compare the post_name
         if ( $parent->post_name == $page )
            return true;
      }
      return false;
   }
}

//Banner top generator
function hkt_get_banner_generator() {
   global $post;

   if(is_page()) {
      $image_url = '';
      if(hkt_is_subpage()) {
         $post_parent_id = $post->post_parent;
         $image = (has_post_thumbnail($post_parent_id))?wp_get_attachment_image_src( get_post_thumbnail_id($post_parent_id), 'full' ):array();
         $image_url = (has_post_thumbnail(get_the_ID()))?mts_get_thumbnail_url():$image[0];
      } else {
         $image_url = (has_post_thumbnail(get_the_ID()))?mts_get_thumbnail_url():'';
      }

      return ($image_url!='')?'<div class="hkt-banner clear"><img src="'.$image_url.'" alt=""></div>':'';
   }

   if(is_category()) {
      return '';
   }

   if (hkt_isWooCommerce()) {
      if (is_product_category()) {
         $term = get_term_by('slug', get_query_var('term'), 'product_cat');
         $cat_custom_content = get_term_meta($term->term_id, '_hkt_cat_custom_content', true);
         $cat_custom_content = stripslashes( htmlspecialchars_decode(base64_decode($cat_custom_content)) );
         $cat_custom_content = apply_filters('the_content', $cat_custom_content);
         if(isset($cat_custom_content) && $cat_custom_content != '') {
            return '<div class="hkt-banner product-category-banner clear">'.$cat_custom_content.'</div>';
            //echo do_shortcode('[rev_slider alias="home-page-slider"]');
         }
      }
   }
}

//Redirect page contact form 7
add_action('wp_footer', 'hkt_custom_cf7_wp_footer'); 
function hkt_custom_cf7_wp_footer() {
?>
   <script type="text/javascript">
   document.addEventListener( 'wpcf7mailsent', function( event ) {
      if ( '105' == event.detail.contactFormId ) { // Sends sumissions on form 105 to the first thank you page
       location = 'http://macinsta.vn/dat-hang-thanh-cong/';
       } else { // Sends submissions on all unaccounted for forms to the third thank you page
           location = 'http://macinsta.vn/dat-hang-thanh-cong/';
       }
   }, false );
   </script>
<?php
}

/*-----------------------------------------------------------------------------------*/
/* WOOCOMMERCE
/*-----------------------------------------------------------------------------------*/
if(hkt_isWooCommerce()) {
   global $pagenow;

   add_filter( 'subcategory_archive_thumbnail_size', function() {
      return 'sociallyviral-featuredbig';
   }, 999 );

   if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
      //Define WooCommerce image sizes.
      add_action( 'after_switch_theme', 'hkt_woocommerce_image_dimensions', 1000 );
      function hkt_woocommerce_image_dimensions() {
         $catalog = array(
            'width'     => '250',   // px
            'height'    => '250',   // px
            'crop'      => 1        // true
         );
         $single = array(
            'width'     => '400',   // px
            'height'    => '400',   // px
            'crop'      => 1        // true
         );
         $thumbnail = array(
            'width'     => '80',    // px
            'height'    => '80',   // px
            'crop'      => 0        // false
         );
         // Image sizes
         update_option( 'shop_catalog_image_size', $catalog );       // Product category thumbs
         update_option( 'shop_single_image_size', $single );         // Single product image
         update_option( 'shop_thumbnail_image_size', $thumbnail );   // Image gallery thumbs
      }
   }

   //Change number or products per row to  * @return int
   if ( !function_exists( 'hkt_loop_columns' )) {
      function hkt_loop_columns() {
         return 4; //products per row
      }
   }
   add_filter( 'loop_shop_columns', 'hkt_loop_columns', 999 );

   //Redefine woocommerce_output_related_products()
   function hkt_woocommerce_output_related_products() {
      $args = array(
         'posts_per_page' => 4,
         'columns' => 4,
      );
      woocommerce_related_products($args); // Display products in rows of 1
   }

   function hkt_woo_related_products_limit() {
      global $product;

      $args['posts_per_page'] = 4;
      return $args;
   }
   //add_filter( 'woocommerce_related_products_args', 'hkt_woo_related_products_limit', 9999 );

   //Change the number of product thumbnails to show per row to 4 * @return int
   function hkt_thumb_cols() {
      return 4; // .last class applied to every 4th thumbnail
   }
   add_filter( 'woocommerce_product_thumbnails_columns', 'hkt_thumb_cols', 999 );

   //HKT Cart Link
   function hkt_woocommerce_cart_link() {
      global $woocommerce;

      $html = '<a class="hkt-cart-content" href="'.esc_url( wc_get_cart_url() ).'" title="'.__( 'View your shopping cart', HKT_THEME_TEXTDOMAIN ).'">';
      $html .= '<span class="fa fa-shopping-cart"></span><br><span class="cart-text">'.__('Giỏ hàng', HKT_THEME_TEXTDOMAIN).'</span>';
         $html .= ' <span class="count hkt-cart-count">('.wp_kses_data( WC()->cart->get_cart_contents_count() ).')</span>';
         //$html .= ' - ';
         $html .= '<span class="amount hkt-cart-amount">'.wp_kses_data( WC()->cart->get_cart_subtotal() ).'</span>';
      $html .= '</a>';

      return $html;
   }

   // Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
   add_filter( 'woocommerce_add_to_cart_fragments', 'hkt_minicart_link_fragment' );
   function hkt_minicart_link_fragment( $fragments ) {
      $fragments['span.hkt-cart-count'] = '<span class="count hkt-cart-count">'.wp_kses_data( WC()->cart->get_cart_contents_count() ).'</span>';
      $fragments['span.hkt-cart-amount'] = '<span class="amount hkt-cart-amount">'. wp_kses_data( WC()->cart->get_cart_subtotal() ).'</span>';

      return $fragments;
   }

   //Replace “Free!” by a custom string
   add_filter('woocommerce_free_price_html', 'hkt_woo_free_message');
   add_filter('woocommerce_empty_price_html', 'hkt_woo_free_message');
   function hkt_woo_free_message() {
      return "Liên hệ";
   }

   add_filter( 'woocommerce_checkout_fields' , 'hkt_override_checkout_fields' );
   function hkt_override_checkout_fields( $fields ) {
      //unset($fields['billing']['billing_first_name']);
      //unset($fields['billing']['billing_last_name']);
      unset($fields['billing']['billing_company']);
      //unset($fields['billing']['billing_address_1']);
      unset($fields['billing']['billing_address_2']);
      unset($fields['billing']['billing_city']);
      unset($fields['billing']['billing_postcode']);
      unset($fields['billing']['billing_country']);
      unset($fields['billing']['billing_state']);
      //unset($fields['billing']['billing_phone']);
      unset($fields['billing']['billing_vat']);
      //unset($fields['billing']['billing_address_2']);
      //unset($fields['billing']['billing_postcode']);
      //unset($fields['billing']['billing_company']);
      //unset($fields['billing']['billing_last_name']);
      //unset($fields['billing']['billing_email']);
      //unset($fields['billing']['billing_city']);

      //unset($fields['order']['order_comments']);

      // Đổi tên Tỉnh / Thành phố thành Quận / Huyện
      //$fields['city']['label'] = "[:vi]nội dung tiếng việt [:en]Nội dung tiếng anh ";
      //$fields['billing']['billing_last_name']['label'] = "Họ tên quý khách";
      $fields['billing']['billing_address_1']['placeholder'] = '';
      $fields['order']['order_comments']['placeholder'] = '';
      $fields['order']['order_comments']['label'] = "Ghi chú thêm";

      return $fields;
   }

   remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
   add_action( 'woocommerce_shop_loop_item_title', 'hkt_woocommerce_template_loop_product_title', 10 );
   function hkt_woocommerce_template_loop_product_title() {
      echo '<h2 class="woocommerce-loop-product__title">' . hkt_title_limit(get_the_title(), 40) . '</h2>';
   }

   //product thumb
   remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
   add_action('woocommerce_before_shop_loop_item_title', 'hkt_woocommerce_template_loop_product_thumbnail', 20);
   function hkt_woocommerce_template_loop_product_thumbnail() {
      global $product;
      $product_image_size = 'shop_catalog';
      $attachment_ids = $product->get_gallery_attachment_ids();
      $secondary_image = '';

      if ($attachment_ids) {
         $secondary_image_id = $attachment_ids['0'];
         $secondary_image = wp_get_attachment_image($secondary_image_id, apply_filters('shop_catalog', $product_image_size));
      }
      ?>

      <div class="hkt product-images-hover change-image">
         <div class="product-thumb-primary">
            <?php echo woocommerce_get_product_thumbnail($product_image_size); ?>
         </div>
         <div class="product-thumb-secondary">
            <?php echo $secondary_image; ?>
         </div>
         <div class="hkt-hover"><span data-href="<?php echo get_the_permalink($product->ID) ?>"></span></div>
      </div>

<?php
   }

} //End if(hkt_isWooCommerce())



