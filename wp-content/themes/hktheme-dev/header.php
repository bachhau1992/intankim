<?php
/**
 * The template for displaying the header.
 *
 * Displays everything from the doctype declaration down to the navigation.
 */
?>
<!DOCTYPE html>
<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<html class="no-js" <?php language_attributes(); ?>>
<head itemscope itemtype="http://schema.org/WebSite">
   <meta charset="<?php bloginfo('charset'); ?>">
   <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
   <!--[if IE ]>
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <![endif]-->
   <link rel="profile" href="http://gmpg.org/xfn/11"/>
   <?php mts_meta(); ?>
   <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
   <?php wp_head(); ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W3HQMVD');</script>
<!-- End Google Tag Manager -->
  
</head>
<body id="blog" <?php body_class('main'); ?> itemscope itemtype="http://schema.org/WebPage">
<div id="fb-root"></div>
<script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1524800134509668";
      fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD""
height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php //$hkt_options_var = get_option( HKT_THEME_NAME );echo '<pre>';print_r( $hkt_options_var );echo '</pre>'; //hkt test data ?>

<div id="wrapper" class="hkt stretched">
   <div class="hkt-content">
      <!-- hktcustom: TOPBAR -->
      <?php if (1 == '2') { ?>
         <div id="topbar" class="topbar">
            <div class="container">
               <div class="pull-left">
                  <span>SỨC KHỎE, HẠNH PHÚC VÀ THỊNH VƯỢNG CHO MỌI KHÁCH HÀNG <span class="fa fa-heart"></span></span>
               </div>
               <div class="pull-right">
                  <?php echo hkt_woocommerce_cart_link(); ?>
               </div>
            </div>
         </div>
      <?php } ?>

      <header id="site-header" class="sticky-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
         <div id="header">
            <div class="container">
               <div class="logo-wrap">
                  <?php if(hkt_get_option('frontend_logo') && hkt_get_option('frontend_logo') != '') { ?>

                     <?php
                        //echo '<pre>';print_r(hkt_get_option('frontend_logo'));echo '</pre>';
                        $hkt_logo = hkt_get_option('frontend_logo');
                        $hkt_logo_w_h = ' width="' . $hkt_logo['width'] . '" height="' . $hkt_logo['height'] . '"';
                     ?>
                     <?php if (is_front_page() || is_home() || is_404()) { ?>
                        <h1 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($hkt_logo['url']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $hkt_logo_w_h; ?>></a>
                        </h1><!-- END #logo -->
                     <?php } else { ?>
                        <h2 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($hkt_logo['url']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $hkt_logo_w_h; ?>></a>
                        </h2><!-- END #logo -->
                     <?php } ?>

                  <?php } elseif ($mts_options['mts_logo'] != '') { ?>

                     <?php
                     $logo_id = mts_get_image_id_from_url($mts_options['mts_logo']);
                     $logo_w_h = '';
                     if ($logo_id) {
                        $logo = wp_get_attachment_image_src($logo_id, 'full');
                        if (!empty($logo[1]) && !empty($logo[2])) $logo_w_h = ' width="' . $logo[1] . '" height="' . $logo[2] . '"';
                     }
                     ?>
                     <?php if (is_front_page() || is_home() || is_404()) { ?>
                        <h1 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($mts_options['mts_logo']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $logo_w_h; ?>></a>
                        </h1><!-- END #logo -->
                     <?php } else { ?>
                        <h2 id="logo" class="image-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($mts_options['mts_logo']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"<?php echo $logo_w_h; ?>></a>
                        </h2><!-- END #logo -->
                     <?php } ?>

                  <?php } else { ?>

                     <?php if (is_front_page() || is_home() || is_404()) { ?>
                        <h1 id="logo" class="text-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a>
                        </h1><!-- END #logo -->
                     <?php } else { ?>
                        <h2 id="logo" class="text-logo" itemprop="headline">
                           <a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a>
                        </h2><!-- END #logo -->
                     <?php } ?>
                     
                  <?php } ?>

                  <div class="header-inner-mobile">
                     <div class="search-form">
                        <form id="search_mini_form_m" action="" method="get">
                           <div class="search_form_mini">
                              <input id="" autocomplete="off" type="text" name="s" value="" class="input-text required-entry form-control" maxlength="128" placeholder="Bạn cần tìm gì ?">
                              <input type="submit" value="Tìm" name="cmdsearch" class="cmdsearch">
                              <input type="hidden" name="post_type" value="product">
                              <span id="cancel_search_m" class="">Hủy</span>
                           </div>
                        </form>
                     </div>
                     <a href="<?php echo esc_url( wc_get_cart_url() ); ?>"><span class="fa fa-shopping-cart"> </span></a>
                     <a href="javascript:void(0)" id="ah_search_m"><i class="fa fa-fw fa-search"></i> </a>
                  </div>
               </div>

               <?php if ($mts_options['mts_header_search'] == '1') { ?>
                  <div class="header-search"><?php get_search_form(); ?></div>
               <?php } ?>

               <?php if ($mts_options['mts_show_header_social'] == '1' && !empty($mts_options['mts_header_social']) && is_array($mts_options['mts_header_social'])) { ?>
                  <div class="header-social">
                     <?php foreach ($mts_options['mts_header_social'] as $header_icons) : ?>
                        <?php if (!empty($header_icons['mts_header_icon']) && isset($header_icons['mts_header_icon'])) : ?>
                           <a href="<?php print $header_icons['mts_header_icon_link'] ?>" class="header-<?php print $header_icons['mts_header_icon'] ?>" style="background: <?php print $header_icons['mts_header_icon_bg_color'] ?>" target="_blank"><span class="fa fa-<?php print $header_icons['mts_header_icon'] ?>"></span></a>
                        <?php endif; ?>
                     <?php endforeach; ?>
                  </div>
               <?php } ?>

               <!-- hktcustom: Many language -->
               <?php if (1 == '2') { ?>
                  <?php hkt_select_language(); ?>
               <?php } ?>
               <!-- hktcustom: Navigation in header -->
               <?php if (1 == '2') { ?>
                  <div id="hkt-navigation">
                     <nav class="navigation clearfix">
                        <?php if (has_nav_menu('primary-menu')) { ?>
                           <?php wp_nav_menu(array('theme_location' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                        <?php } ?>
                     </nav>
                  </div>
               <?php } ?>

               <!-- hktcustom: info header right -->
               <?php if (1 == '1') { ?>
                  <div class="header-right pull-right">
                     <div class="inner">
                        <div class="tool-search">
                           <div class="search-form">
                              <?php echo hkt_search_form('mitssy'); ?>
                           </div>
                           <div class="phone-text">
                              <span>
                                 <i class="fa fa-fw fa-phone"></i>
                                 <?php echo do_shortcode('[hkt_load_option id="hotline" editor="0"]'); ?>
                              </span>
                           </div>
                        </div>
                        <div class="task-bar">
                           <ul>
                              <li><a href="javascript:void(0)" id="ah_search"><i class="fa fa-fw fa-search"></i> </a></li>
                              <li><a href="/tai-khoan" id="ah_user"><i class="fa fa-fw fa-user"></i> </a></li>
                              <li><a href="javascript:void(0)" id="ah_love"><i class="fa fa-fw fa-heart-o"></i> </a></li>
                           </ul>
                        </div>
                        <div class="hkt-minicart">
                           <?php echo hkt_woocommerce_cart_link(); ?>
                        </div>
                     </div>
                  </div>
               <?php } ?>

            </div><!-- /.container header-->
         </div><!--#header-->

         <!-- hktcustom: Navigation bellow header -->
         <?php if ($mts_options['mts_show_primary_nav'] == '1') { ?>
         <?php if ($mts_options['mts_sticky_nav'] == '1') { ?>
            <div id="catcher" class="clear"></div>
            <div id="primary-navigation" class="sticky-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
         <?php } else { ?>
            <div id="primary-navigation" class="primary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
               <?php } ?>
               <a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'sociallyviral'); ?></a>
               <?php if (has_nav_menu('mobile')) { ?>
                  <nav class="navigation clearfix">
                     <?php if (has_nav_menu('primary-menu')) { ?>
                        <?php wp_nav_menu(array('theme_location' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                     <?php } else { ?>
                        <ul class="menu clearfix">
                           <?php wp_list_categories('title_li='); ?>
                        </ul>
                     <?php } ?>
                  </nav>
                  <nav class="navigation mobile-only clearfix mobile-menu-wrapper">
                     <?php wp_nav_menu(array('theme_location' => 'mobile', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                  </nav>
               <?php } else { ?>
                  <nav class="navigation clearfix mobile-menu-wrapper">
                     <?php if (has_nav_menu('primary-menu')) { ?>
                        <?php wp_nav_menu(array('theme_location' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker)); ?>
                     <?php } else { ?>
                        <ul class="menu clearfix">
                           <?php wp_list_categories('title_li='); ?>
                        </ul>
                     <?php } ?>
                  </nav>
               <?php } ?>
            </div>
         <?php } ?>
      </header>

      <?php if (!empty($mts_options['mts_header_adcode'])) { ?>
         <div class="header-ad">
            <?php echo do_shortcode($mts_options['mts_header_adcode']); ?>
         </div>
      <?php } ?>

		<?php echo hkt_get_banner_generator(); //hktcustom: Banner or Slider Top ?>

      <?php //hktcustom: Sidebar Location Element Class
         $hkt_sidebar_class = '';
         if (hkt_sidebar_location() == 'left' || hkt_sidebar_location() == 'sclayout') $hkt_sidebar_class = ' hkt-left-sidebar';
         if (hkt_sidebar_location() == 'right' || hkt_sidebar_location() == 'cslayout') $hkt_sidebar_class = ' hkt-right-sidebar';
      ?>

      <div class="main-container<?php echo $hkt_sidebar_class; ?>">