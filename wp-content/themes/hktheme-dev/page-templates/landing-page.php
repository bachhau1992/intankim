<?php
/**
 * Template Name: Landing Page
 * The template for displaying the page with a slug of ``.
 */
?>

<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<?php get_header('landing'); ?>

<div id="page" class="<?php mts_single_page_class(); ?> nomar">
	<article class="<?php hkt_article_class(); ?>">
		<div id="content_box" >
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class('g post'); ?>>
					<div class="single_page">
						<?php if (!is_home() && !is_front_page()) { //hktcustom ?>
							<header style="display: none;">
							  <h1 class="title entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							</header>
                  <?php } ?>
						<div class="post-single-content box mark-links entry-content">
							<?php the_content(); ?>
						</div><!--.post-content box mark-links-->
					</div>
				</div>
				<?php comments_template( '', true ); ?>
			<?php endwhile; ?>
		</div>
	</article>
</div>

<?php get_footer(); ?>