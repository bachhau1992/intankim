<?

/*
Function generate search url
*/
function generate_search_url($module){
	global $con_mod_rewrite;
	// $module	= mb_strtolower($module, "UTF-8");
	if (function_exists('mb_strtolower')) $module = mb_strtolower($module, 'UTF-8');
	else $module = strtolower($string);

	if($con_mod_rewrite == 0){
		$link	= lang_path() . "search.php?keyword=" . $module;
	}
	else{
		$link	= lang_path() . $module . ".sps";
	}
	return $link;
}

/*
Function generate module url
*/
function generate_module_url($module){
	global $con_mod_rewrite;
	// $module	= mb_strtolower($module, "UTF-8");

	if (function_exists('mb_strtolower')) $module = mb_strtolower($module, 'UTF-8');
	else $module = strtolower($string);

	if($con_mod_rewrite == 0){
		$link	= lang_path() . "module.php?module=" . $module;
	}
	else{
		$link	= lang_path() . $module . ".html";
	}
	return $link;
}

/*
Function generate type url
*/
function generate_type_url($nCat, $iCat){
	global $con_mod_rewrite;
	$nCat		= replace_rewrite_url($nCat, "-");
	if($con_mod_rewrite == 0){
		$link	= lang_path() . "type.php?iCat=" . $iCat;
	}
	else{
		$link	= lang_path() . $nCat . "/" . $iCat . ".html";
	}
	return $link;
}

/*
Function generate detail url
*/
function generate_detail_url($module, $nCat, $nData, $iData){
	global $con_mod_rewrite;
	$nCat		= replace_rewrite_url($nCat, "-");
	$nData	= replace_rewrite_url($nData, "-");
	switch($module){
		case "product":
			$file = "detail_product.php";
			$ext	= ".html";
			break;
		case "news":
			$file = "detail_news.php";
			$ext	= ".nhtml";
			break;
		case "set":
			$file = "detail_set.php";
			$ext	= ".ghtml";
			break;
		case "static":
			$file	= "detail_static.php";
			$ext	= ".shtml";
			break;
		case "group":
			$file	= "detail_group.php";
			$ext	= ".ghtml";
			break;
	}
	$link = lang_path();
	if($con_mod_rewrite == 0){
		$link	.= $file . "?iData=" . $iData;
	}
	else{
		if($module == "static" || $module == "group") $link	.= $nData . "/" . $iData . $ext;
		else $link .= $nCat . "/" . $nData . "/" . $iData . $ext;
	}
	return $link;
}

/*
Function replace rewrite url
*/
function replace_rewrite_url($string, $rc="-", $urlencode=1){
	// $string	= mb_strtolower($string, "UTF-8");
	if (function_exists('mb_strtolower')) $string = mb_strtolower($string, 'UTF-8');
	else $string = strtolower($string);
	$string	= removeAccent($string);
	$string	= preg_replace('/[^A-Za-z0-9]+/', ' ', $string);
	$string	= str_replace("   ", " ", trim($string));
	$string	= str_replace("  ", " ", trim($string));
	$string	= str_replace(" ", $rc, $string);
	$string	= str_replace($rc . $rc, $rc, $string);
	$string	= str_replace($rc . $rc, $rc, $string);
	if($urlencode == 1) $string	= urlencode($string);
	return $string;
}
?>