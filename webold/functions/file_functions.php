<?
function checkExtension($filename, $allowList){
	$sExtension = $filename;
	$allowArray	= explode(",", $allowList);
	$allowPass	= 0;
	for($i=0; $i<count($allowArray); $i++){
		if($sExtension == $allowArray[$i]) $allowPass = 1;
	}
	return $allowPass;
}

function check_upload_extension($filename,$allow_list){
	$sExtension	= getExtension($filename);
	$allow_arr	= explode(",", $allow_list);
	$pass = 0;
	for($i=0; $i<count($allow_arr); $i++){
		if($sExtension == $allow_arr[$i]) $pass = 1;
	}
	return $pass;
}

function delete_file($path, $filename){
	if($filename == "") return;
	$array_file	= array("small_", "normal_", "larger_", "");
	for($i=0; $i<count($array_file); $i++){
		if(file_exists($path . $array_file[$i] . $filename)) @unlink($path . $array_file[$i] . $filename);
	}
}

function delete_picture_more($type, $record_id, $multi=0, $path="../../"){
	global $lang_id;
	global $con_lang_id;
	$lang	= (isset($lang_id) ? $lang_id : $con_lang_id);
	
	$fs_table	= $type . "_pictures";
	$prefix		= "";
	switch($type){
		case "product": $prefix = "pp"; break;
		case "gallery": $prefix = "gp"; break;
	}
	$id_field				= ($multi != 1) ? $prefix . "_id" : $prefix . "_" . $type . "_id";
	$fs_fieldupload		= $prefix . "_picture";
	$fs_filepath_fullsize= $path . $type . "_pictures_more/";
	$fs_filepath_small	= $path . $type . "_pictures_more/";
	
	$db_check = new db_query("SELECT " . $fs_fieldupload . " FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang);
	while($check = mysql_fetch_assoc($db_check->result)){
		if($check[$fs_fieldupload] != ""){
			delete_file($fs_filepath_fullsize, $check[$fs_fieldupload]);
			delete_file($fs_filepath_small, $check[$fs_fieldupload]);
		}
		$db_execute = new db_execute("DELETE FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id);
		unset($db_execute);
	}
	unset($db_check);
}

function generate_name($filename){
	$name = "";
	for($i=0; $i<3; $i++){
		$name .= chr(rand(97,122));
	}
	$today= getdate();
	$name.= $today[0];
	$ext	= substr($filename, (strrpos($filename, ".") + 1));
	return $name . "." . $ext;
}

function getExtension($filename){
	$sExtension = substr($filename, (strrpos($filename, ".") + 1));
	$sExtension = strtolower($sExtension);
	return $sExtension;
}
?>