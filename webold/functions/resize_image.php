<?
function check_image($path, $filename){
	$sExtension = getExtension($filename);
	//Check image file type extensiton
	$checkImg = true;
	switch($sExtension){
		case "gif":
			$checkImg = @imagecreatefromgif($path . $filename);
			break;
		case $sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg":
			$checkImg = @imagecreatefromjpeg($path . $filename);
			break;
		case "png":
			$checkImg = @imagecreatefrompng($path . $filename);
			break;
	}
	if(!$checkImg){
		delete_file($path, $filename);
		return 0;
	}
	return 1;
}

function resize_image($path, $filename, $maxwidth, $maxheight, $quality, $type = "small_", $new_path = ""){
	$sExtension = substr($filename, (strrpos($filename, ".") + 1));
	$sExtension = strtolower($sExtension);

	// Get new dimensions
	list($width, $height) = getimagesize($path . $filename);
	if($width != 0 && $height !=0){
		if($maxwidth / $width > $maxheight / $height) $percent = $maxheight / $height;
		else $percent = $maxwidth / $width;
	}

	$new_width	= $width * $percent;
	$new_height	= $height * $percent;

	// Resample
	$image_p = imagecreatetruecolor($new_width, $new_height);
	//check extension file for create
	switch($sExtension){
		case "gif":
			$image = imagecreatefromgif($path . $filename);
			break;
		case $sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg":
			$image = imagecreatefromjpeg($path . $filename);
			break;
		case "png":
			$image = imagecreatefrompng($path . $filename);
			break;
	}
	//Copy and resize part of an image with resampling
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	// Output

	// check new_path, nếu new_path tồn tại sẽ save ra đó, thay path = new_path
	if($new_path != "") $path = $new_path;

	switch($sExtension){
	case "gif":
		imagegif($image_p, $path . $type . $filename);
		break;
	case $sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg":
		imagejpeg($image_p, $path . $type . $filename, $quality);
		break;
	case "png":
		imagepng($image_p, $path . $type . $filename);
		break;
	}
	imagedestroy($image_p);
}
function cropImages($url_folder, $width, $height, $img_name){
	$url_img = $url_folder . '/' . $img_name;
	if(!file_exists($url_img)) return false;

	$imgExtension = substr($img_name, (strrpos($img_name, ".") + 1));
	$imgExtension = strtolower($imgExtension);
	$imgNameOrigin = substr($img_name, 0, (strrpos($img_name, ".")));

	switch ($imgExtension) {
		case 'gif':
			$im = imagecreatefromgif($url_img);
			break;
		case 'jpeg':
			$im = imagecreatefromjpeg($url_img);
			break;
		case 'png':
			$im = imagecreatefrompng($url_img);
			break;
		default:
			return false;
	}

	$size = min(imagesx($im), imagesy($im));
	$im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
	if ($im2 !== FALSE) {
		$imgCropNew = $url_folder . '/' . $imgNameOrigin . '-crop.' . $imgExtension;
	   imagepng($im2, $imgCropNew);
	   if(file_exists($imgCropNew)){
	   	return $imgNameOrigin . '-crop.' . $imgExtension;
	   }
	   return false;
	}
	return false;
}
?>