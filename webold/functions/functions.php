<?
function array_currency(){
	$arrReturn	= array(0 => "USD", 1 => "EUR");
	return $arrReturn;
}

function array_language(){
	$db_language	= new db_query("SELECT * FROM languages ORDER BY lang_id ASC");
	$arrReturn		= array();
	while($row = mysql_fetch_assoc($db_language->result)){
		$arrReturn[$row["lang_id"]] = array($row["lang_code"], $row["lang_name"]);
	}
	return $arrReturn;
}

function array_order_sourcing(){
	$arrReturn	= array (1	=> "Qua mạng",
								2	=> "Qua giới thiệu",
								);
	return $arrReturn;
}

function array_order_status($type=0){
	$arrReturn	= array (5	=> "Chưa xác nhận",
								10	=> "Chưa làm",
								13	=> "Chờ trang trí",
								15	=> "Chờ G.Hàng",
								18	=> "Đã giao",
								20	=> "Thu tiền",
								23	=> "Quyết toán",
								25	=> "Hủy",
								);
	if($type == 1){
		$arrReturn	= array (5	=> "#FFFF80",
									10	=> "#80FFFF",
									13	=> "#B400FF",
									15	=> "#FFB0D8",
									18	=> "#FFB400",
									20	=> "#80FF80",
									23	=> "#0080FF",
									25	=> "#FF3C3C",
									);
	}
	return $arrReturn;
}

function array_order_cashier(){
	$arrReturn	= array (0	=> "Chưa ai",
								1	=> "chú Đạt",
								2	=> "Hà - con chú Đạt",
								3	=> "Còi",
								4	=> "Khoa",
								5	=> "mẹ Nhu",
								6	=> "mẹ Nhung",
								7	=> "bố Thành",
								8	=> "Long công tử",
								9	=> "Quốc Anh",
								10	=> "Thành Cận",
								11	=> "Điệp Đô",
								12	=> "Cường béo",
								13	=> "Nguyệt",
								14	=> "chú Phúc",
								);
	return $arrReturn;
}

function array_random($arrData, $limit, $preserve_keys=false){
	$arrReturn	= array();
	$count		= count($arrData);
	$maxRand		= ($count > $limit ? $limit : $count);
	$arrTemp		= $arrData;
	for($i=0; $i<$maxRand; $i++){
		$rand_keys	= array_rand($arrTemp);
		$arrReturn[!$preserve_keys ? $i : $rand_keys] = $arrData[$rand_keys];
		unset($arrTemp[$rand_keys]);
	}
	return $arrReturn;
}

/**
generate image resize link
$image_type 		: product_pictures, news_pictures ....
$sub_dir				: Thư mục cấp dưới nếu có thường chỉ dành cho type gallery_img (1,2,3,4)
$resize_mode		: 0: 400x400
$image_file_name	: Tên ảnh
$title				: Tiêu đề (Tên sản phẩm, tin tức)
*/
function generate_image_resize_link($image_type, $sub_dir, $resize_mode, $image_file_name, $title){
	$str_path = "";

	switch($image_type){
		case "product_pictures"	: $str_path .= "/ir/product_pictures/"; break;
	}

	$str_path 	.= $resize_mode . "/";
	$str_path 	.= base64_url_encode($image_file_name) . "/";
	$img_ext		= getExtension($image_file_name);

	$str_path	.= replace_rewrite_url($title) . "." . $img_ext;

	return $str_path;
}

function base64_url_encode($input){
	return strtr(base64_encode($input), '+/=', '_,-');
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '_,-', '+/='));
}

function clearSpaceBuffer($buffer){
	$str		= array(chr(9), chr(10));
	$buffer	= str_replace($str, "", $buffer);
	return $buffer;
}

function check_email_address($email) {
	//First, we check that there's one @ symbol, and that the lengths are right
	if(!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)){
		//Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
		return false;
	}
	//Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for($i = 0; $i < sizeof($local_array); $i++){
		if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])){
			return false;
		}
	}
	if(!ereg("^\[?[0-9\.]+\]?$", $email_array[1])){
	//Check if domain is IP. If not, it should be valid domain name
		$domain_array = explode(".", $email_array[1]);
		if(sizeof($domain_array) < 2){
			return false; // Not enough parts to domain
		}
		for($i = 0; $i < sizeof($domain_array); $i++){
			if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])){
				return false;
			}
		}
	}
	return true;
}

function check_session_security($security_code){
	$return = 1;
	if(!isset($_SESSION["session_security_code"])) $_SESSION["session_security_code"] = generate_security_code();
	if($security_code != $_SESSION["session_security_code"]){
		$return = 0;
	}
	// Reset lại session security code
	$_SESSION["session_security_code"] = generate_security_code();
	return $return;
}

function checkServerOrLocal(){
	$return		= true;
	$serverName	= @$_SERVER["SERVER_NAME"];
	if($serverName == "localhost" || $serverName == "127.0.0.1" || $serverName == "192.168.1.218" || $serverName == "192.168.0.218"){
		$return	= false;
	}
	return $return;
}

//Đổi resultset ra dạng list
function convert_result_set_2_list($result_set, $id_field, $start_value="0", $type=0){
	$str_return = $start_value;

	//Move first resultset
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);
	while ($row_t = mysql_fetch_assoc($result_set)){
		switch($type){
			case 1 : if($row_t[$id_field] > 0) $str_return .= "," . $row_t[$id_field]; break;
			default: $str_return .= "," . $row_t[$id_field]; break;
		}
	}

	//Sau khi loop move first lại từ đầu
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);
	return $str_return;
}

//Đổi resultset ra array
function convert_result_set_2_array($result_set, $id_field="", $value_field=""){
	$array_return	= array();

	$id_field		= trim($id_field);
	$value_field	= trim($value_field);

	//Move first resultset
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);
	while ($row_t = mysql_fetch_assoc($result_set)){
		$value	= ($value_field == "" ? $row_t : $row_t[$value_field]);
		if($id_field == "") $array_return[] = $value;
		elseif(!isset($array_return[$row_t[$id_field]])) $array_return[$row_t[$id_field]] = $value;
	}

	//Sau khi loop move first lại từ đầu
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);

	return $array_return;
}

function convert_string_to_array($string){
	$arrReturn	= array();
	preg_match_all('/\[(.*?)\]/is', $string, $matches);
	if(isset($matches[1])){
		for($i=0; $i<count($matches[1]); $i++){
			$arrReturn[]	= $matches[1][$i];
		}
	}
	return $arrReturn;
}

function convert_string_to_list_id($string){
	$list_id	= 0;
	preg_match_all('/\[(.*?)\]/is', $string, $matches);
	if(isset($matches[1])){
		for($i=0; $i<count($matches[1]); $i++){
			$list_id	.= "," . $matches[1][$i];
		}
	}
	return $list_id;
}

function convert_list_to_list_id($string, $do_not_get_zero_value=1){
	// Bẻ $string để intval lại
	$arrTemp	= explode(",", $string);
	$list_id	= "";
	foreach($arrTemp as $key => $value){
		$value	= intval($value);
		if($do_not_get_zero_value == 1 && $value <= 0) continue;
		$list_id .= "," . $value;
	}
	// Remove dấu , đầu tiên
	if(strlen($list_id) > 0) $list_id = substr($list_id, 1);
	else $list_id	= 0;
	return $list_id;
}

function convert_list_to_array($string, $return_null_array = false, $df_null_value = 0){
	// Bẻ $string để intval lại
	$arrTemp	= explode(",", $string);
	$array_id	= array();
	foreach($arrTemp as $key => $value) $array_id[] = intval($value);

	//Nếu ra array rỗng và !$return_null_array không cho trả lại array rỗng thì gán cho giá trị trả về 1 phần tử df
	if(count($array_id) == 0 && !$return_null_array) $array_id = array($df_null_value);
	return $array_id;
}

function convert_array_to_list($array_input, $key="", $do_not_get_zero_value=1){
	$string_return	= "0";
	$arrTemp			= array();
	foreach($array_input as $value){
		$temp	= ($key == "" ? intval($value) : intval($value[$key]));
		if($do_not_get_zero_value == 1 && $temp == 0) continue;
		$arrTemp[]	= $temp;
	}
	//Loại bỏ phần tử trùng
	$arrTemp	= array_unique($arrTemp);
	if(count($arrTemp) > 0) $string_return = implode(",", $arrTemp);
	return $string_return;
}

function count_online(){
	$visited_timeout		= 10 * 60;
	$last_visited_time	= time();
	//Kiem tra co session_id hay ko, neu co
	if(session_id() != ""){
		$db_exec	= new db_execute("REPLACE INTO active_users(au_session_id, au_last_visit) VALUES('" . session_id() . "', " . $last_visited_time . ")");
		unset($db_exec);
	}
	// Delete timeout
	$db_exec	= new db_execute("DELETE FROM active_users WHERE au_last_visit < " . ($last_visited_time - $visited_timeout));
	unset($db_exec);
	// Select Count
	$db_count= new db_query("SELECT count(*) AS count FROM active_users");
	$row		= mysql_fetch_assoc($db_count->result);
	unset($db_count);
	// Return value
	return $row["count"];
}

function count_visited(){
	$db_count	= new db_query("SELECT vis_counter FROM visited");
	$row = mysql_fetch_assoc($db_count->result);
	unset($db_count);
	return $row["vis_counter"];
}

function cut_string($str, $length, $char="..."){
	//Nếu chuỗi cần cắt nhỏ hơn $length thì return luôn
	$strlen	= mb_strlen($str, "UTF-8");
	if($strlen <= $length) return $str;

	//Cắt chiều dài chuỗi $str tới đoạn cần lấy
	$substr	= mb_substr($str, 0, $length, "UTF-8");
	if(mb_substr($str, $length, 1, "UTF-8") == " ") return $substr . $char;

	//Xác định dấu " " cuối cùng trong chuỗi $substr vừa cắt
	$strPoint= mb_strrpos($substr, " ", "UTF-8");

	//Return string
	if($strPoint < $length - 20) return $substr . $char;
	else return mb_substr($substr, 0, $strPoint, "UTF-8") . $char;
}

function fixed_imagesize(&$width, &$height, $maxWidth=500, $maxHeight=300){
	if($width <= 0) $width	= ($maxWidth < $maxHeight ? $maxWidth : $maxHeight);
	if($height <= 0)$height	= ($maxWidth < $maxHeight ? $maxWidth : $maxHeight);
	$ratio	= $maxWidth / $width;
	$w			= $maxWidth;
	$h			= $height * $ratio;
	if($h > $maxHeight){
		$ratio= $maxHeight / $height;
		$w		= $width * $ratio;
		$h		= $maxHeight;
	}
	$width	= $w;
	$height	= $h;
}


function fixed_imagesize_return($width, $height, $maxWidth=500, $maxHeight=300){
	if($width <= 0) $width	= ($maxWidth < $maxHeight ? $maxWidth : $maxHeight);
	if($height <= 0) $height= ($maxWidth < $maxHeight ? $maxWidth : $maxHeight);
	$ratio	= $maxWidth / $width;
	$w			= $maxWidth;
	$h			= $height * $ratio;
	if($h > $maxHeight){
		$ratio= $maxHeight / $height;
		$w		= $width * $ratio;
		$h		= $maxHeight;
	}
	return array(intval($w), intval($h));
}

function format_number($number, $num_decimal=2, $edit=0){
	$sep		= ($edit == 0 ? array(",", ".") : array(".", ""));
	$stt		= -1;
	$return	= number_format($number, $num_decimal, $sep[0], $sep[1]);
	for($i=$num_decimal; $i>0; $i--){
		$stt++;
		if(intval(substr($return, -$i, $i)) == 0){
			$return = number_format($number, $stt, $sep[0], $sep[1]);
			break;
		}
	}
	return $return;
}

function generate_array_variable($variable){
	$list			= tdt($variable);
	$arrTemp		= explode("{-break-}", $list);
	$arrReturn	= array();
	for($i=0; $i<count($arrTemp); $i++) $arrReturn[$i] = trim($arrTemp[$i]);
	return $arrReturn;
}

function generate_background_css($background, $style){
	if($background == "" || $style == 0) $strReturn	= '';
	elseif($style == 1) $strReturn	= ' style="background: url(/banners/' . $background . ') center top fixed;"';
	else $strReturn	= ' style="background: url(/banners/' . $background . ') center top;"';
	return $strReturn;
}

function product_price($price, $unit="", $tag_name="div"){
	global $con_currency;
	global $con_price_updating;
	$strReturn	= "";
	if($price > 0) $strReturn .= '<' . $tag_name . ' class="price">' . format_number($price) . ' ' . $con_currency . ($unit != "" ? ' <span>/ ' . $unit . '</span>' : '') . '</' . $tag_name . '>';
	else $strReturn .= '<' . $tag_name . ' class="price_updating">' . $con_price_updating . '</' . $tag_name . '>';
	return $strReturn;
}

function generate_product_thumb($arrData, $option=array()){

	global $con_css_path;

	$arrOption	= array ("numCol"	=> 0,
								"length"	=> 60,
								"picture"=> "medium_",
								);
	if(count($option) > 0) $arrOption	= array_merge($arrOption, $option);

	$strReturn	= "";

	// Loop show data
	foreach($arrData as $key => $row){

		// Ảnh sản phẩm
		$picture	= $con_css_path . "no_photo_140x140.gif";
		if($row["pro_picture"] != "") $picture	= "/product_pictures/" . $arrOption["picture"] . $row["pro_picture"];
		// Link chi tiết
		$link		= generate_detail_url("product", $row["cat_name"], $row["pro_name"], $row["pro_id"]);

		$strReturn	.= '
			<div class="fl"' . ($arrOption["numCol"] > 0 ? ' style="width: ' . (100 / $arrOption["numCol"]) . '%;"' : '') . '>
				<div class="block">
					<div class="picture"><a href="' . $link . '"><img alt="' . htmlspecialbo($row["pro_name"]) . '" src="' . $picture . '" /></a></div>
					<div class="name"><a' . (mb_strlen($row["pro_name"], "UTF-8") > $arrOption["length"] ? ' title="' . htmlspecialbo($row["pro_name"]) . '"' : '') . ' href="' . $link . '">' . cut_string($row["pro_name"], $arrOption["length"]) . '</a></div>
					' . product_price($row["pro_price"], $row["pro_unit"]) . '
				</div>
			</div>
		';

	}// End foreach($arrData as $key => $row)

	$strReturn	.= '<div class="clear"></div>';

	return $strReturn;

}

function generate_security_code(){
	$code	= rand(1000, 9999);
	return $code;
}

function generate_sort($type, $sort, $current_sort, $image_path){
	if($type == "asc"){
		$title = tdt("Tang_dan");
		if($sort != $current_sort) $image_sort = "sortasc.gif";
		else $image_sort = "sortasc_selected.gif";
	}
	else{
		$title = tdt("Giam_dan");
		if($sort != $current_sort) $image_sort = "sortdesc.gif";
		else $image_sort = "sortdesc_selected.gif";
	}
	return '<a title="' . $title . '" href="' . getURL(0,0,1,1,"sort") . '&sort=' . $sort . '"><img border="0" src="' . $image_path . $image_sort . '" style="margin-top:3px" /></a>';
}

function getDomain($url){
 $url = trim($url);
 if($url == "") return "";

 $domain_return = "";
 $nowww   = ereg_replace('www\.', '', $url);
 $domain   = @parse_url($nowww);

 if($domain !== false){
  if( isset($domain["host"]) && $domain["host"] != "" ) $domain_return = $domain["host"];
  elseif( isset($domain["path"]) && $domain["path"] != "" ) $domain_return = $domain["path"];
 }

 return $domain_return;
}

function getURL($serverName=0, $scriptName=0, $fileName=1, $queryString=1, $varDenied=''){
	$url	 = '';
	$slash = '/';
	if($scriptName != 0)$slash	= "";
	if($serverName != 0){
		if(isset($_SERVER['SERVER_NAME'])){
			$url .= 'http://' . $_SERVER['SERVER_NAME'];
			if(isset($_SERVER['SERVER_PORT'])) $url .= ":" . $_SERVER['SERVER_PORT'];
			$url .= $slash;
		}
	}
	if($scriptName != 0){
		if(isset($_SERVER['SCRIPT_NAME']))	$url .= substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/') + 1);
	}
	if($fileName	!= 0){
		if(isset($_SERVER['SCRIPT_NAME']))	$url .= substr($_SERVER['SCRIPT_NAME'], strrpos($_SERVER['SCRIPT_NAME'], '/') + 1);
	}
	if($queryString!= 0){
		$url .= '?';
		reset($_GET);
		$i = 0;
		if($varDenied != ''){
			$arrVarDenied = explode('|', $varDenied);
			while(list($k, $v) = each($_GET)){
				if(array_search($k, $arrVarDenied) === false){
					$i++;
					if($i > 1) $url .= '&' . $k . '=' . @urlencode($v);
					else $url .= $k . '=' . @urlencode($v);
				}
			}
		}
		else{
			while(list($k, $v) = each($_GET)){
				$i++;
				if($i > 1) $url .= '&' . $k . '=' . @urlencode($v);
				else $url .= $k . '=' . @urlencode($v);
			}
		}
	}
	$url = str_replace('"', '&quot;', strval($url));
	return $url;
}

function getValue($value_name, $data_type = "int", $method = "GET", $default_value = 0, $advance = 0){
	$value = $default_value;
	switch($method){
		case "GET": if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
		case "POST": if(isset($_POST[$value_name])) $value = $_POST[$value_name]; break;
		case "COOKIE": if(isset($_COOKIE[$value_name])) $value = $_COOKIE[$value_name]; break;
		case "SESSION": if(isset($_SESSION[$value_name])) $value = $_SESSION[$value_name]; break;
		default: if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
	}
	$valueArray	= array("int" => intval($value), "str" => trim(strval($value)), "flo" => floatval($value), "dbl" => doubleval($value), "arr" => $value);
	foreach($valueArray as $key => $returnValue){
		if($data_type == $key){
			if($advance != 0){
				switch($advance){
					case 1:
						$returnValue = replaceMQ($returnValue);
						break;
					case 2:
						$returnValue = htmlspecialbo($returnValue);
						break;
				}
			}
			//Do số quá lớn nên phải kiểm tra trước khi trả về giá trị
			if((strval($returnValue) == "INF") && ($data_type != "str")) return 0;
			return $returnValue;
			break;
		}
	}
	return (intval($value));
}

function get_server_name(){
	$server = $_SERVER['SERVER_NAME'];
	if(strpos($server, "asiaqueentour.com") !== false) return "http://www.asiaqueentour.com";
	else return "http://" . $server . ":" . $_SERVER['SERVER_PORT'];
}

function htmlspecialbo($str){
	$arrDenied	= array('<', '>', '\"', '"');
	$arrReplace	= array('&lt;', '&gt;', '&quot;', '&quot;');
	$str = str_replace($arrDenied, $arrReplace, $str);
	return $str;
}

function javascript_writer($str){
	$mytextencode	= "";
	for($i=0; $i<strlen($str); $i++){
		$mytextencode	.= ord(substr($str, $i, 1)) . ",";
	}
	if($mytextencode != "") $mytextencode .= "32";
	return "<script language='javascript'>document.write(String.fromCharCode(" . $mytextencode . "));</script>";
}

function lang_path(){
	global $con_root_path;
	return $con_root_path;
}

function microtime_float(){
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}

function random(){
	$rand_value = "";
	$rand_value.= rand(1000,9999);
	$rand_value.= chr(rand(65,90));
	$rand_value.= rand(1000,9999);
	$rand_value.= chr(rand(97,122));
	$rand_value.= rand(1000,9999);
	$rand_value.= chr(rand(97,122));
	$rand_value.= rand(1000,9999);
	return $rand_value;
}

function redirect($url, $http=0, $redirect_parent=0){
	if($http == 0) $url = str_replace("://", "", $url);
	// Remove special char để tránh phá ngoặc
	$url = str_replace('"',"",$url);
	if($redirect_parent == 1) echo '<script language="javascript">window.parent.location.href="' . $url . '"</script>';
	else echo '<script language="javascript">window.location.href="' . $url . '"</script>';
	exit();
}

function redirectHeader($url, $http=0){
	if($http == 0) $url = str_replace("://", "", $url);
	Header( "HTTP/1.1 301 Moved Permanently" );
	Header( "Location: " . $url );
	exit();
}

function removeAccent($mystring){
	$marTViet=array(
		// Chữ thường
		"à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
		"è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
		"ì","í","ị","ỉ","ĩ",
		"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
		"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
		"ỳ","ý","ỵ","ỷ","ỹ",
		"đ","Đ","'",
		// Chữ hoa
		"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
		"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
		"Ì","Í","Ị","Ỉ","Ĩ",
		"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
		"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
		"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
		"Đ","Đ","'"
		);
	$marKoDau=array(
		/// Chữ thường
		"a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
		"e","e","e","e","e","e","e","e","e","e","e",
		"i","i","i","i","i",
		"o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
		"u","u","u","u","u","u","u","u","u","u","u",
		"y","y","y","y","y",
		"d","D","",
		//Chữ hoa
		"A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A",
		"E","E","E","E","E","E","E","E","E","E","E",
		"I","I","I","I","I",
		"O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O",
		"U","U","U","U","U","U","U","U","U","U","U",
		"Y","Y","Y","Y","Y",
		"D","D","",
		);
	return str_replace($marTViet, $marKoDau, $mystring);
}

function removeExcessSpace($string){
	$string	= preg_replace('/(\n|\r)+/', ' ', $string); // Xóa ký tự xuống dòng
	$string	= trim(preg_replace('/( )+/', ' ', $string)); // Xóa ký tự trắng thừa
	return $string;
}

function removeHTML($string){
	$string = preg_replace ('/<script.*?\>.*?<\/script>/si', ' ', $string);
	$string = preg_replace ('/<style.*?\>.*?<\/style>/si', ' ', $string);
	$string = preg_replace ('/<.*?\>/si', ' ', $string);
	$string = str_replace ('&nbsp;', ' ', $string);
	return $string;
}

function removeLink($string){
	$string = preg_replace ('/<a.*?\>/si', '', $string);
	$string = preg_replace ('/<\/a>/si', '', $string);
	return $string;
}

function replaceFCK($string, $type=0){
	$array_fck	= array ("&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Igrave;", "&Iacute;", "&Icirc;",
								"&Iuml;", "&ETH;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ugrave;", "&Uacute;", "&Yacute;", "&agrave;",
								"&aacute;", "&acirc;", "&atilde;", "&egrave;", "&eacute;", "&ecirc;", "&igrave;", "&iacute;", "&ograve;", "&oacute;",
								"&ocirc;", "&otilde;", "&ugrave;", "&uacute;", "&ucirc;", "&yacute;",
								);
	$array_text	= array ("À", "Á", "Â", "Ã", "È", "É", "Ê", "Ì", "Í", "Î",
								"Ï", "Ð", "Ò", "Ó", "Ô", "Õ", "Ù", "Ú", "Ý", "à",
								"á", "â", "ã", "è", "é", "ê", "ì", "í", "ò", "ó",
								"ô", "õ", "ù", "ú", "û", "ý",
								);
	if($type == 1) $string = str_replace($array_fck, $array_text, $string);
	else $string = str_replace($array_text, $array_fck, $string);
	return $string;
}

function replaceJS($text){
	$arr_str = array("\'", "'", '"', "&#39", "&#39;", chr(10), chr(13), "\n");
	$arr_rep = array(" ", " ", '&quot;', " ", " ", " ", " ");
	$text		= str_replace($arr_str, $arr_rep, $text);
	$text		= str_replace("    ", " ", $text);
	$text		= str_replace("   ", " ", $text);
	$text		= str_replace("  ", " ", $text);
	return $text;
}

function replace_keyword_search($keyword, $lower=1){
	if($lower == 1) $keyword	= mb_strtolower($keyword, "UTF-8");
	$keyword	= replaceMQ($keyword);
	$arrRep	= array("'", '"', "-", "+", "=", "*", "?", "/", "!", "~", "#", "@", "%", "$", "^", "&", "(", ")", ";", ":", "\\", ".", ",", "[", "]", "{", "}", "‘", "’", '“', '”');
	$keyword	= str_replace($arrRep, " ", $keyword);
	$keyword	= str_replace("  ", " ", $keyword);
	$keyword	= str_replace("  ", " ", $keyword);
	return $keyword;
}

function replaceMQ($text){
	$text	= str_replace("\'", "'", $text);
	$text	= str_replace("'", "''", $text);
	return $text;
}

function remove_magic_quote($string){
	$string	= str_replace("\'", "'", $string);
	$string	= str_replace('\"', '"', $string);
	$string	= str_replace('\&quot;', "&quot;", $string);
	$string	= str_replace("\\\\", "\\", $string);
	return $string;
}

function show_information_tabular($name, $value){
	$strReturn	= '<tr>
							<td class="tb_name">' . $name . ' :</td>
							<td class="tb_value">' . $value . '</td>
						</tr>';
	if($value != "") return $strReturn;
	else return "";
}

function tdt($variable){
	global $lang_display;
	if (isset($lang_display[$variable])){
		if (trim($lang_display[$variable]) == ""){
			return "#" . $variable . "#";
		}
		else{
			$arrStr	= array("\\\\'", '\"');
			$arrRep	= array("\\'", '"');
			return str_replace($arrStr, $arrRep, $lang_display[$variable]);
		}
	}
	else{
		return "_@" . $variable . "@_";
	}
}

function delete_order_to_recycle_bin($record_id){
	$db_insert	= new db_execute("INSERT IGNORE orders_recycle_bin (SELECT * FROM orders WHERE ord_id = " . $record_id . ")");
	unset($db_insert);
	$db_delete	= new db_execute("DELETE FROM orders WHERE ord_id = " . $record_id);
	unset($db_delete);
}

function restore_order_from_recycle_bin($record_id){
	$db_insert	= new db_execute("INSERT IGNORE orders (SELECT * FROM orders_recycle_bin WHERE ord_id = " . $record_id . ")");
	unset($db_insert);
	$db_delete	= new db_execute("DELETE FROM orders_recycle_bin WHERE ord_id = " . $record_id);
	unset($db_delete);
}

function delete_order_from_recycle_bin($record_id){
	$db_delete	= new db_execute("DELETE FROM orders_recycle_bin WHERE ord_id = " . $record_id);
	unset($db_delete);
}

// Thay thế mã số
function replaceMSCake($title_text){
	// Kiểm tra chiều dài chuỗi đầu vào
	$len = mb_strlen($title_text, "UTF-8");
	if ($len == 0) return $title_text;

	//Tìm dấu cách bên phải gần nhất
	$pos = mb_strrpos($title_text, " ", 1, "UTF-8");
	// Tìm đc bắt đầu check để replace
	if ($pos !== false){
		$ms = trim(mb_substr($title_text, $pos, $len - $pos, "UTF-8"));
		$title_text = trim(mb_substr($title_text, 0, $pos, "UTF-8"));

		if (is_numeric($ms) && strlen($ms) >= 3){
			$ms = "[MS: " . $ms . "]";
		}

		return $title_text . " " . $ms;
	}
	// Không tìm đc dấu cách return text luôn
	else return $title_text;
}
?>