<?
require_once("../security/security.php");

$module_id		= 6;
$module_name	= "Set sản phẩm";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table				= "set_multi";
$id_field				= "set_id";
$fs_fieldupload		= "set_picture";
$fs_filepath			= "../../set_pictures/";
$fs_extension			= "gif,jpg,jpe,jpeg,png";
$fs_filesize			= 800;
$height_small_image	= 200;
$width_small_image	= 200;
$height_medium_image	= 400;
$width_medium_image	= 400;
$fs_insert_logo		= 0;

$maxLastUpdate			= 20;

?>