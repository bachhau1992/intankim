<?
include("inc_security.php");

//Call class menu
$class_menu				= new menu();
$listAll					= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type = 'set' AND cat_id IN (" . $fs_category . ") AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

//Khai báo biến khi thêm mới
$after_save_data		= getValue("after_save_data", "str", "POST", "add.php");
$add						= "add.php";
$listing					= "listing.php";
$fs_title				= $module_name . " | Thêm mới";
$fs_action				= getURL();
$fs_redirect			= $after_save_data;
$fs_errorMsg			= "";

$category				= getValue("category");
//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$set_category_id		= getValue("set_category_id", "int", "POST", $category);
$set_name				= getValue("set_name", "str", "POST", "");
$set_picture_width	= 0;
$set_picture_height	= 0;

$width_small_image	= 600;
$height_small_image	= 360;
$set_teaser				= getValue("set_teaser", "str", "POST", "");
$set_description		= getValue("set_description", "str", "POST", "");
$set_hot					= getValue("set_hot", "int", "POST", 0);
$set_strdate			= getValue("set_strdate", "str", "POST", date("d/m/Y"));
$set_strtime			= getValue("set_strtime", "str", "POST", date("H:i:s"));
$set_date				= convertDateTime($set_strdate, $set_strtime);
$set_last_update		= time();
$set_rename				= removeAccent($set_name);
$set_active				= getValue("set_active", "int", "POST", 1);

//Get action variable for add new data
$set_temp				= getValue("set_temp", "str", "POST", random(), 1);
$action					= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	//Lấy dữ liệu kiểu checkbox
	$set_hot				= getValue("set_hot", "int", "POST", 0);
	$set_active			= getValue("set_active", "int", "POST", 0);

	// Check xem category có tồn tại hay ko
	$db_check			= new db_query("SELECT cat_id FROM categories_multi WHERE cat_type = 'set' AND cat_id = " . $set_category_id);
	if(mysql_num_rows($db_check->result) == 0){
		$fs_errorMsg	.= "&bull; Danh mục bạn chọn không tồn tại.<br />";
	}
	$db_check->close();
	unset($db_check);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	if(strpos($fs_category, $set_category_id . ",") === false){
		$fs_errorMsg .= "&bull; Bạn không được phép truy cập category này!<br />";
	}
	$myform->add("set_category_id", "set_category_id", 1, 1, 1, 1, "Bạn chưa chọn danh mục.", 0, "");
	$myform->add("set_name", "set_name", 0, 1, " ", 1, "Bạn chưa nhập tên set.", 0, "");
	$myform->add("set_picture_width", "set_picture_width", 1, 1, 0, 0, "", 0, "");
	$myform->add("set_picture_height", "set_picture_height", 1, 1, 0, 0, "", 0, "");
	$myform->add("set_rename", "set_rename", 0, 1, 0, 0, "", 0, "");
	$myform->add("set_teaser", "set_teaser", 0, 1, "", 0, "", 0, "");
	$myform->add("set_description", "set_description", 0, 1, "", 0, "", 0, "");
	$myform->add("set_hot", "set_hot", 1, 1, 0, 0, "", 0, "");
	$myform->add("set_date", "set_date", 1, 1, 0, 0, "", 0, "");
	$myform->add("set_last_update", "set_last_update", 1, 1, 0, 0, "", 0, "");
	$myform->add("set_active", "set_active", 1, 1, 0, 0, "", 0, "");
	$myform->add("set_temp", "set_temp", 0, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, $fs_insert_logo);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}

	if($fs_errorMsg == ""){

		if($filename != ""){
			//Upload new image
			$$fs_fieldupload = $filename;
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
			$upload->resize_image($fs_filepath, $filename, $width_small_image, $height_small_image, "small_", 1);
			list($set_picture_width, $set_picture_height)	= @getimagesize($fs_filepath . $filename);
		}//End if($filename != "")

		//Insert to database
		$myform->removeHTML(0);
		$db_insert	= new db_execute_return();
		$last_id		= $db_insert->db_execute($myform->generate_insert_SQL());
		unset($db_insert);

		if($last_id > 0){

			// Update more picture
			$db_update	= new db_execute("UPDATE set_pictures SET sp_product_id = " . $last_id . ", sp_temp = NULL WHERE sp_temp = '" . $set_temp . "'");
			unset($db_update);

			//Redirect after insert complate
			$fs_redirect .= "?category=" . $set_category_id;
			redirect($fs_redirect);

		}
		else{
			$fs_errorMsg .= "&bull; Không insert được vào database. Bạn hãy kiểm tra lại câu lệnh INSERT INTO.<br />";
		}

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../../js/jquery.min.js"></script>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=$listing?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("add", $fs_action, "post", "multipart/form-data", '');
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->select_db_multi("Danh mục", "set_category_id", "set_category_id", $listAll, "cat_id", "cat_name", $set_category_id, "Danh mục", 1, "", 1, 0, "", "")?>
<?=$form->text("Tên set", "set_name", "set_name", $set_name, "Tên set", 1, 250, "", 255, "", "", "")?>
<?=$form->getFile("Ảnh minh họa", "set_picture", "set_picture", "Ảnh minh họa", 0, 32, "", '<br />(Dung lượng tối đa <font color="#FF0000">' . $fs_filesize . ' Kb</font>)')?>
<?
$more_picture	= '<input type="button" class="form_button" value="Thêm ảnh" style="background:url(../images/button_3.gif)" onClick="more_picture(\'add\', \'' . $set_temp . '\')" />';
echo $form->create_control("", $more_picture);
?>
<?=$form->hidden("set_temp", "set_temp", $set_temp, "");?>
<?=$form->textarea("Tóm tắt", "set_teaser", "set_teaser", $set_teaser, "Tóm tắt tin", 0, 350, 70, "", "", "")?>
<?=$form->checkbox("Nổi bật", "set_hot", "set_hot", 1, $set_hot, "", 0, "", "")?>
<?=$form->text("Ngày cập nhật", "set_strdate" . $form->ec . "set_strtime", "set_strdate" . $form->ec . "set_strtime", $set_strdate . $form->ec . $set_strtime, "Ngày (dd/mm/yyyy)" . $form->ec . "Giờ (hh/mm/ss)", "0", "70" . $form->ec . "70", $form->ec, "10" . $form->ec . "10", " - ", $form->ec, "&nbsp; <i>(Ví dụ: dd/mm/yyyy - hh/mm/ss)</i>");?>
<?=$form->checkbox("Kích hoạt", "set_active", "set_active", 1, $set_active, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->close_table();?>
<?=$form->wysiwyg("Mô tả chi tiết", "set_description", $set_description, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->create_table();?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>