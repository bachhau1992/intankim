<?
include("inc_security.php");

//Declare prameter
$fs_table				= "set_pictures";
$id_field				= "sp_id";
$parent_id_field		= "sp_product_id";
$temp_field				= "sp_temp";
$fs_fieldupload		= "sp_picture";
$fs_filepath_fullsize= "../../set_pictures_more/";
$fs_filepath_small	= "../../set_pictures_more/small/";

$width_small_image	= 300;
$height_small_image	= 180;

//Khai báo biến khi thêm mới
$fs_title				= $module_name . " | Thêm ảnh";
$fs_action				= getURL();
$fs_redirect			= getURL(0,0,1,1,"r") . "&r=" . random();
$fs_errorMsg			= "";

// Variables temporary
$type						= getValue("type", "str", "GET", "");
$temp						= getValue("temp", "str", "GET", "");
$temp_id					= getValue("temp_id");

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$sp_product_id			= $temp_id;
$sp_name					= getValue("sp_name", "str", "POST", "");
$sp_picture_width		= 0;
$sp_picture_height	= 0;
$sp_order				= getValue("sp_order", "dbl", "POST", 1);
$sp_temp					= $temp;

//Get action variable for add new data
$action					= getValue("action", "str", "POST", "");
//Check $action for insert new data
if($action == "execute"){

	$record_id			= getValue("record_id", "int", "POST", 0);

	if($record_id > 0){

		$sqlWhere		= ($type == "add") ? " AND " . $temp_field . " = '" . $temp . "' " : " AND " . $parent_id_field . " = " . $temp_id . " ";
		$db_edit			= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id . $sqlWhere);
		if(mysql_num_rows($db_edit->result) == 0){
			$fs_errorMsg .= "&bull; Không tìm thấy dữ liệu, bạn hãy liên hệ với ban quản trị Website!<br />";
		}
		else{
			$edit	= mysql_fetch_assoc($db_edit->result);
			$sp_picture_width		= $edit["sp_picture_width"];
			$sp_picture_height	= $edit["sp_picture_height"];
			unset($db_edit);
		}

	}

	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	if($type == "edit" && $record_id == 0) $myform->add("sp_product_id", "sp_product_id", 1, 1, 1, 1, "Parent ID not found.", 0, "");
	$myform->add("sp_name", "sp_name", 0, 1, " ", 1, "Bạn chưa nhập tên ảnh.", 0, "");
	$myform->add("sp_picture_width", "sp_picture_width", 1, 1, 0, 0, "", 0, "");
	$myform->add("sp_picture_height", "sp_picture_height", 1, 1, 0, 0, "", 0, "");
	$myform->add("sp_order", "sp_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	if($type == "add" && $record_id == 0) $myform->add("sp_temp", "sp_temp", 0, 1, " ", 1, "Temporary value not found.", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath_fullsize, $fs_extension, $fs_filesize, $fs_insert_logo);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}

	if($fs_errorMsg == ""){

		if($filename != ""){

			$$fs_fieldupload = $filename;

			// Trong trường hợp edit
			if($record_id > 0){
				//Kiểm tra xem nếu có ảnh cũ thì xóa đi
				if($edit[$fs_fieldupload] != ""){
					$upload->delete_file($fs_filepath_fullsize, $edit[$fs_fieldupload]);
					$upload->delete_file($fs_filepath_small, $edit[$fs_fieldupload]);
				}
			}

			//Upload ảnh mới
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
			$upload->resize_image($fs_filepath_fullsize, $filename, $width_small_image, $height_small_image, "small_", $fs_filepath_small);
			list($sp_picture_width, $sp_picture_height)	= @getimagesize($fs_filepath_fullsize . $filename);

		}//End if($filename != "")

		// Execute query
		$myform->removeHTML(1);
		//echo $myform->generate_update_SQL($id_field, $record_id);
		if($record_id == 0) $db_execute = new db_execute($myform->generate_insert_SQL());
		else $db_execute = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_execute);

		//Redirect after insert complate
		redirect($fs_redirect);

	}//End if($fs_errorMsg == "")
	else{

		$arr_str	= array("&bull; ", "<br />");
		$arr_rep	= array("- ", "\\n");
		$fs_errorMsg= str_replace($arr_str, $arr_rep, $fs_errorMsg);
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
		echo '<script language="javascript">alert("Có những lỗi sau:\\n' . $fs_errorMsg . '")</script>';
		redirect($fs_redirect);

	}

}// End if($action == "execute")
?>
<?
// Search data
$id			= getValue("id");
$keyword		= getValue("keyword", "str", "GET", "", 1);
$sqlWhere	= ($type == "add") ? " AND " . $temp_field . " = '" . $temp . "' " : " AND " . $parent_id_field . " = " . $temp_id . " ";
//Tìm theo ID
if($id > 0)			$sqlWhere .= " AND sp_id = " . $id . " ";
//Tìm theo keyword
if($keyword != "")$sqlWhere .= " AND sp_name LIKE '%" . $keyword . "%' ";

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "sp_id ASC"; break;
	case 2: $sqlOrderBy = "sp_id DESC"; break;
	case 3: $sqlOrderBy = "sp_name ASC"; break;
	case 4: $sqlOrderBy = "sp_name DESC"; break;
	case 5: $sqlOrderBy = "sp_order ASC"; break;
	case 6: $sqlOrderBy = "sp_order DESC"; break;
	default:$sqlOrderBy = "sp_order ASC"; break;
}

//Get page break params
$page_size		= 10;
$page_prefix	= "Trang: ";
$normal_class	= "page";
$selected_class= "page_current";
$previous		= "<";
$next				= ">";
$first			= "<<";
$last				= ">>";
$break_type		= 1;//"1 => << < 1 2 [3] 4 5 > >>", "2 => < 1 2 [3] 4 5 >", "3 => 1 2 [3] 4 5", "4 => < >"
$url				= getURL(0,0,1,1,"page");
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM " . $fs_table . "
										 WHERE lang_id = " . $lang_id . $sqlWhere);
$listing_count	= mysql_fetch_assoc($db_count->result);
$total_record	= $listing_count["count"];
$current_page	= getValue("page", "int", "GET", 1);
if($total_record % $page_size == 0) $num_of_page = $total_record / $page_size;
else $num_of_page = (int)($total_record / $page_size) + 1;
if($current_page > $num_of_page) $current_page = $num_of_page;
if($current_page < 1) $current_page = 1;
$db_count->close();
unset($db_count);
//End get page break params
$db_listing		= new db_query("SELECT *
										 FROM " . $fs_table . "
										 WHERE lang_id = " . $lang_id . $sqlWhere . "
										 ORDER BY " . $sqlOrderBy . "
										 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
<script language="javascript" src="../js/tooltip.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Đóng cửa sổ" href="javascript:window.close()"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>close.gif" />Đóng</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("add", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text("Tên ảnh", "sp_name_0", "sp_name", $sp_name, "Tên ảnh", 1, 250, "", 255, "", "", "")?>
<?=$form->getFile("Ảnh minh họa", "sp_picture_0", "sp_picture", "Ảnh minh họa", 0, 32, "", '<br />(Dung lượng tối đa <font color="#FF0000">' . $fs_filesize . ' Kb</font>)');?>
<?=$form->text("Thứ tự", "sp_order_0", "sp_order", $sp_order, "Thứ tự", 2, 50, "", 255, "", "", "")?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
<? //Page break and search data?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
	<?	if($total_record > $page_size){?>
		<td nowrap="nowrap"><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
	<? }?>
		<td align="right">
			<table cellpadding="0" cellspacing="0">
			<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
				<tr>
					<td class="form_search" nowrap="nowrap">
						ID:
						<input title="ID" type="text" class="form_control" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:60px; text-align:right">&nbsp;
						Từ khóa:
						<input title="Từ khóa" type="text" class="form_control" id="keyword" name="keyword" value="<?=$keyword?>" maxlength="255" style="width:100px">&nbsp;
						<input type="hidden" name="type" value="<?=$type?>" />
						<input type="hidden" name="temp" value="<?=$temp?>" />
						<input type="hidden" name="temp_id" value="<?=$temp_id?>" />
						<input type="hidden" name="sort" value="<?=$sort?>" />
					</td>
					<td class="form_search" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?=$fs_imagepath?>search.gif" border="0"></td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<? //End page break and search data?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
	<tr class="table_title_3">
		<td>Stt.</td>
		<td nowrap="nowrap">
			<div>ID</div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>Ảnh</td>
		<td>
			<div>Tên ảnh</div>
			<div>
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>
			<div>Thứ tự</div>
			<div>
				<?=generate_sort("asc", 5, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 6, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>Lưu</td>
		<td>Xóa</td>
	</tr>
<?
//Call class form
$form = new form();
$form->class_form_name = "form_name_2";
?>
<?
//Đếm số thứ tự
$No = 0;
while($listing = mysql_fetch_assoc($db_listing->result)){
	$No++;
	$image	= ($listing["sp_picture"] != "") ? $fs_filepath_small . "small_" . $listing["sp_picture"] : $fs_no_image;
?>
	<?
	$form->create_form("quick_edit_" . $No, $fs_action, "post", "multipart/form-data");
	?>
	<tr id="tr_<?=$No?>" <?=$fs_change_bg?>>
		<td class="No"><?=$No?></td>
		<td align="right" class="text_normal_bold"><?=$listing["sp_id"]?></td>
		<td align="center">
			<div class="image_style">
			<? if($listing["sp_picture"] != ""){?>
				<img src="<?=$image?>" onMouseOver="showtip('<img src=\'<?=$fs_filepath_small?>small_<?=$listing["sp_picture"]?>\' />')" onMouseOut="hidetip()" onError="this.src='<?=$fs_error_image?>'" />
			<? }else{?>
				<img src="<?=$fs_no_image?>" />
			<? }?>
			</div>
		</td>
		<td align="center">
			<?=$form->create_table(2, 2, "");?>
			<?=$form->text("Tên ảnh", "sp_name_" . $No, "sp_name", $listing["sp_name"], "Tên ảnh", 1, 203, "", 255, "", "", "")?>
			<?=$form->getFile("Ảnh", "sp_picture_" . $No, "sp_picture", "Ảnh minh họa", "0", 23, "", "");?>
			<?=$form->close_table();?>
		</td>
		<td align="center"><input title="Thứ tự" type="text" class="form_control" id="sp_order_<?=$No?>" name="sp_order" value="<?=$listing["sp_order"]?>" style="text-align:right; width:40px" /></td>
		<td align="center"><input title="Lưu dữ liệu" type="image" hspace="5" src="<?=$fs_imagepath?>save.gif" onClick="MM_validateForm('sp_name_<?=$No?>','','R', 'sp_order_<?=$No?>','','RisNum'); return document.MM_returnValue" /></td>
		<td align="center"><img title="Xóa dữ liệu" hspace="5" src="<?=$fs_imagepath?>delete.gif" style="cursor:pointer" onClick="if(confirm('Bạn có muốn xóa dữ liệu này không?')){window.location.href='delete_picture.php?record_id=<?=$listing["sp_id"]?>&redirect=<?=base64_encode(getURL())?>'}" /></td>
	</tr>
	<?=$form->hidden("record_id_" . $No, "record_id", $listing["sp_id"], "");?>
	<?=$form->hidden("action_" . $No, "action", "execute", "");?>
	<?
	$form->close_form();
	?>
<?
}// End while($listing = mysql_fetch_assoc($db_listing->result))
?>
<?
unset($form);
?>
</table>
<? if($total_record > $page_size){?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
		<td><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
		<td align="right"><a title="Go to top" accesskey="T" class="top" href="#">Lên trên<img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>top.gif"></a></td>
	</tr>
</table>
<? }?>
</div>
</body>
</html>
<script language="javascript">self.moveTo((screen.width-document.body.clientWidth)/2, (screen.height-document.body.clientHeight)/2);</script>
<?
$db_listing->close();
unset($db_listing);
?>