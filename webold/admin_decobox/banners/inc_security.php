<?
require_once("../security/security.php");

$module_id	= 12;
$module_name= "Banner";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table				= "banners";
$id_field				= "ban_id";
$fs_fieldupload		= "ban_picture";
$fs_filepath			= "../../banners/";
$fs_extension			= "gif,jpg,jpe,jpeg,png,swf";
$fs_filesize			= 1024;
$width_small_image	= 120;
$height_small_image	= 120;
$fs_insert_logo		= 0;
//Array variable
$arrTarget				= array ("_blank"	=> "Trang mới",
										"_self"	=> "Hiện hành",
										);
$arrType					= array (1 => "Phía trên",
										2 => "Phía dưới",
										3 => "Đối tác",
										// 4 => "Phía dưới",
										//5 => "Giữa trang",
										);
?>