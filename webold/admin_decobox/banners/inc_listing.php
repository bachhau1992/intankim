<?
include("inc_security.php");

//Khai báo biến khi hiển thị danh sách
$fs_title		= $module_name . " | Danh sách";
$fs_action		= "listing.php" . getURL(0,0,0,1,"record_id");
$fs_redirect	= "listing.php" . getURL(0,0,0,1,"record_id|r") . "&r=" . random();
$fs_errorMsg	= "";

/*****----- Quick Edit -----*****/
$action			= getValue("action", "str", "POST", "");
if($action == "execute"){
	
	//Get data edit
	$record_id	= getValue("record_id", "int", "POST", 0);
	$db_edit		= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
	if(mysql_num_rows($db_edit->result) == 0){
		$fs_errorMsg .= "&bull; Không tìm thấy dữ liệu, bạn hãy liên hệ với ban quản trị Website!<br />";
	}
	else{

		$edit		= mysql_fetch_assoc($db_edit->result);
		unset($db_edit);
		
		//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
		$ban_name			= getValue("ban_name", "str", "POST", $edit["ban_name"]);
		$ban_width			= getValue("ban_width", "int", "POST", $edit["ban_width"]);
		$ban_height			= getValue("ban_height", "int", "POST", $edit["ban_height"]);
		$ban_type			= getValue("ban_type", "str", "POST", $edit["ban_type"]);
		$ban_link			= getValue("ban_link", "str", "POST", $edit["ban_link"]);
		$ban_target			= getValue("ban_target", "str", "POST", $edit["ban_target"]);
		$ban_order			= getValue("ban_order", "dbl", "POST", $edit["ban_order"]);
		$ban_hits			= getValue("ban_hits", "int", "POST", $edit["ban_hits"]);

		/*
		Call class form:
		1). Ten truong
		2). Ten form
		3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
		4). Noi luu giu data  0 : post, 1 : variable
		5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
		6). Du lieu nay co can thiet hay khong
		7). Loi dua ra man hinh
		8). Chi co duy nhat trong database
		9). Loi dua ra man hinh neu co duplicate
		*/
		$myform = new generate_form();
		//Add table insert data
		$myform->addTable($fs_table);
		$myform->add("ban_name", "ban_name", 0, 1, " ", 1, "Bạn chưa nhập tên quảng cáo.", 0, "");
		$myform->add("ban_width", "ban_width", 1, 1, 0, 0, "", 0, "");
		$myform->add("ban_height", "ban_height", 1, 1, 0, 0, "", 0, "");
		$myform->add("ban_type", "ban_type", 1, 1, 1, 1, "Bạn chưa chọn loại quảng cáo.", 0, "");
		$myform->add("ban_link", "ban_link", 0, 1, "", 0, "", 0, "");
		$myform->add("ban_target", "ban_target", 0, 1, "", 0, "", 0, "");
		$myform->add("ban_order", "ban_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
		//$myform->add("ban_hits", "ban_hits", 1, 1, 0, 0, "", 0, "");
		
		//Check form data
		$fs_errorMsg .= $myform->checkdata();
		
		//Get $filename
		$filename		= "";
		if($fs_errorMsg == ""){
			$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, $fs_insert_logo);
			$filename	= $upload->file_name;
			$fs_errorMsg .= $upload->warning_error;
		}
		
		if($fs_errorMsg == ""){
			
			if($filename != ""){
				$$fs_fieldupload = $filename;
				//Kiểm tra xem nếu có ảnh cũ thì xóa đi
				if($edit[$fs_fieldupload] != "") $upload->delete_file($fs_filepath, $edit[$fs_fieldupload]);
				//Upload ảnh mới
				$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
				//Nếu không phải file .swf thì mới resize
				if(getExtension($filename) != "swf") $upload->resize_image($fs_filepath, $filename, $width_small_image, $height_small_image, "small_");
			}//End if($filename != "")
			
			//Update to database
			$myform->removeHTML(0);
			$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
			unset($db_update);
			
		}//End if($fs_errorMsg == "")
		unset($myform);
	
	}
	
	if($fs_errorMsg != ""){
		$arr_str		= array("&bull; ", "<br />");
		$arr_rep		= array("- ", "\\n");
		$fs_errorMsg= str_replace($arr_str, $arr_rep, $fs_errorMsg);
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
		echo '<script language="javascript">alert("Có những lỗi sau:\\n' . $fs_errorMsg . '")</script>';
	}
	
	//Redirect
	redirect($fs_redirect);
	
}// End if($action == "execute")
/*****----- End Quick Edit -----*****/

$record_id	= getValue("record_id");

//Search data
$id			= getValue("id");
$keyword		= getValue("keyword", "str", "GET", "", 1);
$type			= getValue("type");
$sqlWhere	= "";
//Tìm theo ID
if($id > 0)	$sqlWhere .= " AND ban_id = " . $id . " ";
//Tìm theo keyword
if($keyword != ""){
	if(validateDate($keyword) == 1){
		$startTime	= convertDateTime($keyword, "00:00:00");
		$endTime		= convertDateTime($keyword, "23:59:59");
		$sqlWhere	.= " AND ban_date >= " . $startTime . " AND ban_date <= " . $endTime . " ";
	}
	else{
		$sqlWhere	.= " AND (ban_name LIKE '%" . $keyword . "%' OR ban_link LIKE '%" . $keyword . "%') ";
	}
}
//Tìm theo type
if($type > 0)		$sqlWhere .= " AND ban_type = " . $type . " ";

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "ban_type ASC, ban_name ASC"; break;
	case 2: $sqlOrderBy = "ban_type ASC, ban_name DESC"; break;
	case 3: $sqlOrderBy = "ban_type ASC, ban_order ASC"; break;
	case 4: $sqlOrderBy = "ban_type DESC, ban_order ASC"; break;
	case 5: $sqlOrderBy = "ban_type ASC, ban_order ASC"; break;
	case 6: $sqlOrderBy = "ban_type ASC, ban_order DESC"; break;
	case 7: $sqlOrderBy = "ban_type ASC, ban_hits ASC"; break;
	case 8: $sqlOrderBy = "ban_type ASC, ban_hits DESC"; break;
	case 9: $sqlOrderBy = "ban_type ASC, ban_date ASC"; break;
	case 10:$sqlOrderBy = "ban_type ASC, ban_date DESC"; break;
	default:$sqlOrderBy = "ban_type ASC, ban_order ASC"; break;
}

//Get page break params
$page_size		= 20;
$page_prefix	= "Trang: ";
$normal_class	= "page";
$selected_class= "page_current";
$previous		= "<";
$next				= ">";
$first			= "<<";
$last				= ">>";
$break_type		= 1;//"1 => << < 1 2 [3] 4 5 > >>", "2 => < 1 2 [3] 4 5 >", "3 => 1 2 [3] 4 5", "4 => < >"
$url				= getURL(0,0,1,1,"page");
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM " . $fs_table . "
										 WHERE " . $id_field . " <> " . $record_id . " AND lang_id = " . $lang_id . $sqlWhere);
$listing_count	= mysql_fetch_assoc($db_count->result);
$total_record	= $listing_count["count"];
$current_page	= getValue("page", "int", "GET", 1);
if($total_record % $page_size == 0) $num_of_page = $total_record / $page_size;
else $num_of_page = (int)($total_record / $page_size) + 1;
if($current_page > $num_of_page) $current_page = $num_of_page;
if($current_page < 1) $current_page = 1;
$db_count->close();
unset($db_count);
//End get page break params
$db_listing	= new db_query("SELECT *
									 FROM " . $fs_table . "
									 WHERE " . $id_field . " <> " . $record_id . " AND lang_id = " . $lang_id . $sqlWhere . "
									 ORDER BY " . $sqlOrderBy . "
									 LIMIT " . ($current_page-1) * $page_size . "," . $page_size);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
<script language="javascript" src="../js/tooltip.js"></script>
<script language="javascript" src="../js/AC_RunActiveContent.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?>: <font class="count"><?=format_number($total_record)?></font></div>
<div class="content_title" style="float:right"><a title="Thêm mới" href="add.php?type=<?=$type?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>add.gif" />Thêm mới</a></div>
</div>
<div align="center" class="content">
<? //Page break and search data?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
	<?	if($total_record > $page_size){?>
		<td nowrap="nowrap"><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
	<? }?>
		<td align="right">
			<table cellpadding="0" cellspacing="0">
			<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
				<tr>
					<td class="form_search" nowrap="nowrap">
						ID:
						<input title="ID" type="text" class="form_control" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:60px; text-align:right">&nbsp;
						Từ khóa:
						<input title="Từ khóa" type="text" class="form_control" id="keyword" name="keyword" value="<?=htmlspecialbo($keyword)?>" maxlength="255" style="width:100px">&nbsp;
						<select title="Loại quảng cáo" name="type" class="form_control">
							<option value="0">--[Chọn loại]--</option>
						<? foreach($arrType as $key => $value){?>
							<option value="<?=$key?>" <? if($key == $type){echo 'selected="selected"';}?>><?=$value?></option>
						<? }?>
						</select>
						<input type="hidden" name="sort" value="<?=$sort?>" />
					</td>
					<td class="form_search" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?=$fs_imagepath?>search.gif" border="0"></td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<? //End page break and search data?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
	<tr class="table_title_3">
		<td>Stt.</td>
		<td>Ảnh</td>
		<td>
			<div>Thông tin chung</div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>
			<div>Mở ra/ Loại</div>
			<div>
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>
			<div>Thứ tự</div>
			<div>
				<?=generate_sort("asc", 5, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 6, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td nowrap="nowrap">
			<div>Lượt xem</div>
			<div>
				<?=generate_sort("asc", 7, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 8, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>
			<div>Ngày tạo</div>
			<div>
				<?=generate_sort("asc", 9, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 10, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>Kích<br />hoạt</td>
		<td>Lưu</td>
		<td>Sửa</td>
		<td>Xóa</td>
	</tr>
<?
//Call class form
$form = new form();
$form->class_form_name = "form_name_2";
?>
<?
$ban_module	= 0;
$ban_type	= 0;
$record_id	= getValue("record_id", "int", "POST");
//Đếm số thứ tự
$No = ($current_page - 1) * $page_size;
while($listing = mysql_fetch_assoc($db_listing->result)){
	$No++;
	$preview_link	= $fs_preview . "banner_click.php?iBan=" . $listing["ban_id"];
	$ban_image		= ($listing["ban_picture"] != "") ? $fs_filepath . "small_" . $listing["ban_picture"] : $fs_no_image;
?>
	<?
	$form->create_form("quick_edit_" . $No, $fs_action, "post", "multipart/form-data");
	?>
	<?
	if($ban_type != $listing["ban_type"]){
		$ban_type = $listing["ban_type"];
	?>
		<tr class="table_title_2">
			<td colspan="12"><?=$arrType[$listing["ban_type"]]?></td>
		</tr>
	<?
	}
	?>
	<tr id="tr_<?=$No?>" <?=$fs_change_bg?>>
		<td class="No"><?=$No?></td>
		<td align="center">
			<div class="image_style">
			<? if($listing["ban_picture"] != ""){?>
				<? if(getExtension($listing["ban_picture"]) == "swf"){?>
					<script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','65','height','50','title','<?=$listing["ban_picture"]?>','src','<?=$fs_filepath?><?=substr($listing["ban_picture"], 0, strrpos($listing["ban_picture"], "."))?>','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent' );</script>
				<? }else{?>
					<a href="<?=$preview_link?>" target="_blank" onMouseOver="showtip('<img src=\'<?=$fs_filepath?><?=$listing["ban_picture"]?>\' />')" onMouseOut="hidetip()"><img src="<?=$ban_image?>" onError="this.src='<?=$fs_error_image?>'" /></a>
				<? }?>
			<? }else{?>
				<a title="Không có ảnh" href="<?=$preview_link?>" target="_blank"><img src="<?=$fs_no_image?>" /></a>
			<? }?>
			</div>
			<? if($listing["ban_picture"] != ""){?><div style="margin-top:3px"><a href="javascript:if(confirm('Bạn có muốn xóa banner này không?')){window.location.href='delete.php?type=picture&record_id=<?=$listing["ban_id"]?>&redirect=<?=base64_encode(getURL())?>'}">[Xóa ảnh]</a></div><? }?>
		</td>
		<td align="center">
			<?=$form->create_table(2, 2, "");?>
			<?=$form->text("Tên", "ban_name_" . $No, "ban_name", $listing["ban_name"], "Tên quảng cáo", 1, 175, "", 255, "", "", "")?>
			<?=$form->getFile("Ảnh", "ban_picture_" . $No, "ban_picture", "Ảnh minh họa", "0", 18, "", "");?>
			<?=$form->text("Link", "ban_link_" . $No, "ban_link", $listing["ban_link"], "Link quảng cáo", 0, 175, "", 255, "", "", "")?>
			<?=$form->close_table();?>
		</td>
		<td align="center">
			<?=$form->create_table(2, 2, "");?>
			<?=$form->select("Mở ra", "ban_target_" . $No, "ban_target", $arrTarget, $listing["ban_target"], "Mở ra", 0, 75, 1, 0, "", "")?>
			<?=$form->select("Loại", "ban_type_" . $No, "ban_type", $arrType, $listing["ban_type"], "Loại quảng cáo", 0, 75, 1, 0, "", "")?>
			<?=$form->text("Size", "ban_width_" . $No . $form->ec . "ban_height_" . $No, "ban_width" . $form->ec . "ban_height", $listing["ban_width"] . $form->ec . $listing["ban_height"], "Chiều rộng" . $form->ec . "Chiều cao", 0, 31 . $form->ec . 31, $form->ec, 4 . $form->ec . 4, " x ", $form->ec, "");?>
			<?=$form->close_table();?>
		</td>
		<td align="center"><input title="Thứ tự" type="text" class="form_control" id="ban_order_<?=$No?>" name="ban_order" value="<?=$listing["ban_order"]?>" style="text-align:right; width:40px" /></td>
		<td class="hits"><?=number_format($listing["ban_hits"])?></td>
		<td align="right">
			<div><?=date("d/m/Y", $listing["ban_date"])?></div>
			<div class="form_text_note"><?=date("H:i:s A", $listing["ban_date"])?></div>
		</td>
		<td align="center"><a href="quickset.php?type=active&record_id=<?=$listing["ban_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" src="<?=$fs_imagepath?>active_<?=$listing["ban_active"]?>.gif" /></a></td>
		<td align="center"><input title="Lưu dữ liệu" type="image" hspace="5" src="<?=$fs_imagepath?>save.gif" onClick="MM_validateForm('ban_name_<?=$No?>','','R'); return document.MM_returnValue" /></td>
		<td align="center"><a title="Sửa dữ liệu" href="edit.php?record_id=<?=$listing["ban_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" hspace="5" src="<?=$fs_imagepath?>edit.gif"></a></td>
		<td align="center"><img title="Xóa dữ liệu" hspace="5" src="<?=$fs_imagepath?>delete.gif" style="cursor:pointer" onClick="if(confirm('Bạn có muốn xóa dữ liệu này không?')){window.location.href='delete.php?record_id=<?=$listing["ban_id"]?>&redirect=<?=base64_encode(getURL())?>'}" /></td>
	</tr>
	<?=$form->hidden("record_id_" . $No, "record_id", $listing["ban_id"], "");?>
	<?=$form->hidden("action_" . $No, "action", "execute", "");?>
	<?
	$form->close_form();
	?>
<?
}// End while($listing = mysql_fetch_assoc($db_listing->result))
?>
<?
unset($form);
?>
</table>
<? if($total_record > $page_size){?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
		<td><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
		<td align="right"><a title="Go to top" accesskey="T" class="top" href="#">Lên trên<img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>top.gif"></a></td>
	</tr>
</table>
<? }?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>
<?
unset($db_listing);
?>