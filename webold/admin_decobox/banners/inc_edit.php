<?
include("inc_security.php");

//Khai báo biến khi thêm mới
$redirect			= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$after_save_data	= getValue("after_save_data", "str", "POST", $redirect);
$add					= base64_encode("add.php");
$listing				= $redirect;
$fs_title			= $module_name . " | Sửa đổi";
$fs_action			= getURL();
$fs_redirect		= $after_save_data;
$fs_redirect		= base64_decode($fs_redirect);
$fs_errorMsg		= "";

//Get data edit
$record_id			= getValue("record_id");
$record_id			= getValue("record_id", "int", "POST", $record_id);
$db_edit				= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
if(mysql_num_rows($db_edit->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$edit					= mysql_fetch_assoc($db_edit->result);
unset($db_edit);

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$ban_name			= getValue("ban_name", "str", "POST", $edit["ban_name"]);
$ban_width			= getValue("ban_width", "int", "POST", $edit["ban_width"]);
$ban_height			= getValue("ban_height", "int", "POST", $edit["ban_height"]);
$ban_type			= getValue("ban_type", "int", "POST", $edit["ban_type"]);
$ban_link			= getValue("ban_link", "str", "POST", $edit["ban_link"]);
$ban_target			= getValue("ban_target", "str", "POST", $edit["ban_target"]);
$ban_order			= getValue("ban_order", "dbl", "POST", $edit["ban_order"]);
$ban_hits			= getValue("ban_hits", "int", "POST", $edit["ban_hits"]);
$ban_strdate		= getValue("ban_strdate", "str", "POST", date("d/m/Y", $edit["ban_date"]));
$ban_strtime		= getValue("ban_strtime", "str", "POST", date("H:i:s", $edit["ban_date"]));
$ban_date			= convertDateTime($ban_strdate, $ban_strtime);
$ban_active			= getValue("ban_active", "int", "POST", $edit["ban_active"]);

//Get action variable for add new data
$action				= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){
	
	//Lấy dữ liệu kiểu checkbox
	$ban_active		= getValue("ban_active", "int", "POST", 0);
	
	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	$myform->add("ban_name", "ban_name", 0, 1, " ", 1, "Bạn chưa nhập tên quảng cáo.", 0, "");
	$myform->add("ban_width", "ban_width", 1, 1, 0, 0, "", 0, "");
	$myform->add("ban_height", "ban_height", 1, 1, 0, 0, "", 0, "");
	$myform->add("ban_type", "ban_type", 1, 1, 1, 1, "Bạn chưa chọn loại quảng cáo.", 0, "");
	$myform->add("ban_link", "ban_link", 0, 1, "", 0, "", 0, "");
	$myform->add("ban_target", "ban_target", 0, 1, "", 0, "", 0, "");
	$myform->add("ban_order", "ban_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	//$myform->add("ban_hits", "ban_hits", 1, 1, 0, 0, "", 0, "");
	$myform->add("ban_date", "ban_date", 1, 1, 0, 0, "", 0, "");
	$myform->add("ban_active", "ban_active", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();
	
	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, $fs_insert_logo);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}
	
	if($fs_errorMsg == ""){
		
		if($filename != ""){
			$$fs_fieldupload = $filename;
			//Kiểm tra xem nếu có ảnh cũ thì xóa đi
			if($edit[$fs_fieldupload] != "") $upload->delete_file($fs_filepath, $edit[$fs_fieldupload]);
			//Upload ảnh mới
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
			//Nếu không phải file .swf thì mới resize
			if(getExtension($filename) != "swf") $upload->resize_image($fs_filepath, $filename, $width_small_image, $height_small_image, "small_");
		}//End if($filename != "")
		
		//Insert to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_update);
		
		//Redirect after insert complate
		redirect($fs_redirect);
		
	}//End if($fs_errorMsg == "")
	unset($myform);
	
}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=base64_decode($listing)?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("edit", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->text("Tên quảng cáo", "ban_name", "ban_name", $ban_name, "Tên quảng cáo", 1, 250, "", 255, "", "", "")?>
<?=$form->getFile("Ảnh minh họa", "ban_picture", "ban_picture", "Ảnh minh họa", 0, 32, "", '<br />(Dung lượng tối đa <font color="#FF0000">' . $fs_filesize . ' Kb</font>)');?>
<?=$form->text("Kích thước", "ban_width" . $form->ec . "ban_height", "ban_width" . $form->ec . "ban_height", $ban_width . $form->ec . $ban_height, "Chiều rộng" . $form->ec . "Chiều cao", 0, 50 . $form->ec . 50, $form->ec, 4 . $form->ec . 4, " x ", $form->ec, "&nbsp; (Chiều rộng x Chiều cao)");?>
<?=$form->text("Liên kết", "ban_link", "ban_link", $ban_link, "Liên kết", 0, 250, "", 255, "", "", "")?>
<?=$form->select("Mở ra", "ban_target", "ban_target", $arrTarget, $ban_target, "Mở ra", 0, "", 1, 0, "", "")?>
<?=$form->select("Loại quảng cáo", "ban_type", "ban_type", $arrType, $ban_type, "Loại quảng cáo", 0, "", 1, 0, "", "")?>
<?=$form->text("Thứ tự", "ban_order", "ban_order", $ban_order, "Thứ tự", 2, 50, "", 255, "", "", "")?>
<? //$form->text("Lượt xem", "ban_hits", "ban_hits", $ban_hits, "Lượt xem", 2, 50, "", 255, "", "", "")?>
<?=$form->text("Ngày tạo", "ban_strdate" . $form->ec . "ban_strtime", "ban_strdate" . $form->ec . "ban_strtime", $ban_strdate . $form->ec . $ban_strtime, "Ngày (dd/mm/yyyy)" . $form->ec . "Giờ (hh:mm:ss)", 0, 70 . $form->ec . 70, $form->ec, 10 . $form->ec . 10, " - ", $form->ec, "&nbsp; <i>(Ví dụ: dd/mm/yyyy - hh:mm:ss)</i>");?>
<?=$form->checkbox("Kích hoạt", "ban_active", "ban_active", 1, $ban_active, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>