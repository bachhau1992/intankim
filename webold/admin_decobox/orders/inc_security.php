<?
require_once("../security/security.php");

$module_id	= 30;
$module_name= "Đơn hàng";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table		= "orders";
$id_field		= "ord_id";

// Array variable
$arrStatus		= array(0 => "Chưa xử lý", 1 => "Đã xử lý");
$arrTitle		= generate_array_variable("Name_title_LIST");
?>