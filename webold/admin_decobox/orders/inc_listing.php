<?
include("inc_security.php");

//Khai báo biến khi hiển thị danh sách
$fs_title		= $module_name . " | Danh sách";

//Search data
$id			= getValue("id");
$keyword		= getValue("keyword", "str", "GET", "", 1);
$status		= getValue("status", "int", "GET", -1);
$sqlWhere	= "";
//Tìm theo ID
if($id > 0)			$sqlWhere .= " AND ord_id = " . $id . " ";
//Tìm theo keyword
if($keyword != ""){
	if(validateDate($keyword) == 1){
		$startTime	= convertDateTime($keyword, "00:00:00");
		$endTime		= convertDateTime($keyword, "23:59:59");
		$sqlWhere	.= " AND ord_date >= " . $startTime . " AND ord_date <= " . $endTime . " ";
	}
	else{
		$sqlWhere	.= " AND (ord_name LIKE '%" . $keyword . "%' OR ord_address LIKE '%" . $keyword . "%' OR ord_email LIKE '%" . $keyword . "%') ";
	}
}
if($status > -1)	$sqlWhere .= " AND ord_status = " . $status . " ";

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "ord_name ASC"; break;
	case 2: $sqlOrderBy = "ord_name DESC"; break;
	case 3: $sqlOrderBy = "ord_date ASC"; break;
	case 4: $sqlOrderBy = "ord_date DESC"; break;
	default:$sqlOrderBy = "ord_status ASC, ord_date DESC"; break;
}

//Get page break params
$page_size		= 30;
$page_prefix	= "Trang: ";
$normal_class	= "page";
$selected_class= "page_current";
$previous		= "<";
$next				= ">";
$first			= "<<";
$last				= ">>";
$break_type		= 1;//"1 => << < 1 2 [3] 4 5 > >>", "2 => < 1 2 [3] 4 5 >", "3 => 1 2 [3] 4 5", "4 => < >"
$url				= getURL(0,0,1,1,"page");
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM " . $fs_table . "
										 WHERE lang_id = " . $lang_id . $sqlWhere);
$listing_count	= mysql_fetch_assoc($db_count->result);
$total_record	= $listing_count["count"];
$current_page	= getValue("page", "int", "GET", 1);
if($total_record % $page_size == 0) $num_of_page = $total_record / $page_size;
else $num_of_page = (int)($total_record / $page_size) + 1;
if($current_page > $num_of_page) $current_page = $num_of_page;
if($current_page < 1) $current_page = 1;
$db_count->close();
unset($db_count);
//End get page break params
$db_listing	= new db_query("SELECT *
									 FROM " . $fs_table . "
									 WHERE lang_id = " . $lang_id . $sqlWhere . "
									 ORDER BY " . $sqlOrderBy . "
									 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title"><?=$fs_title?>: <font class="count"><?=format_number($total_record)?></font></div>
</div>
<div align="center" class="content">
<? //Page break and search data?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
	<?	if($total_record > $page_size){?>
		<td nowrap="nowrap"><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
	<? }?>
		<td align="right">
			<table cellpadding="0" cellspacing="0">
			<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
				<tr>
					<td class="form_search" nowrap="nowrap">
						ID:
						<input title="ID" type="text" class="form_control" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:60px; text-align:right">&nbsp;
						Từ khóa:
						<input title="Từ khóa" type="text" class="form_control" id="keyword" name="keyword" value="<?=htmlspecialbo($keyword)?>" maxlength="255" style="width:100px">&nbsp;
						<select title="Hiển thị theo trạng thái" name="status" class="form_control">
							<option value="-1">--[Trạng thái]--</option>
						<?
						reset($arrStatus);
						foreach($arrStatus as $key => $value){
						?>
							<option value="<?=$key?>"<? if($key == $status){echo ' selected="selected"';}?>><?=$value?></option>
						<?
						}
						?>
						</select>
						<input type="hidden" name="sort" value="<?=$sort?>" />
					</td>
					<td class="form_search" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?=$fs_imagepath?>search.gif" border="0"></td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<? //End page break and search data?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
	<tr class="table_title_3">
		<td>Stt.</td>
		<td>Mã đơn hàng</td>
		<td>
			<div>Họ và tên</div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td width="25%">Số điện thoại</td>
		<? //<td>Tỉnh/ Thành phố</td>?>
		<td>Địa chỉ</td>
		<td>Email</td>
		<td>
			<div>Thời gian đặt đơn</div>
			<div>
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>Xem</td>
		<td>Xóa</td>
	</tr>
<?
/*$arrCity	= array();
$db_city	= new db_query("SELECT city_id, city_name FROM city ORDER BY city_order ASC, city_name ASC");
while($row = mysql_fetch_assoc($db_city->result)){
	$arrCity[$row["city_id"]] = $row["city_name"];
}*/
unset($db_city);
//Đếm số thứ tự
$No = ($current_page - 1) * $page_size;
while($listing = mysql_fetch_assoc($db_listing->result)){
	$No++;
	$bgcolor	= ($listing["ord_status"] == 0) ? "#FFCCCC" : "#FFFFFF";
?>
	<tr id="tr_<?=$No?>" <?=$fs_change_bg?>>
		<td id="No_<?=$No?>" class="No" bgcolor="<?=$bgcolor?>"><?=$No?></td>
		<td align="center"><?=$listing["ord_code"]?></td>
		<td align="center"><?=$listing["ord_name"]?></td>
		<td align="center"><?=$listing["ord_phone"]?></td>
		<? //<td align="center">?><? //if(isset($arrCity[$listing["ct_city_id"]])) echo $arrCity[$listing["ct_city_id"]];?><? //</td>?>
		<td align="center"><?=$listing["ord_address"]?></td>
		<td align="center"><?=$listing["ord_email"]?></td>
		<td align="center">
			<div><?=date("d/m/Y", $listing["ord_date"])?></div>
			<div class="form_text_note"><?=date("H:i:s A", $listing["ord_date"])?></div>
		</td>
		<td align="center"><img hspace="5" src="<?=$fs_imagepath?>view.gif" style="cursor:pointer" onClick="document.getElementById('No_<?=$No?>').style.background='#FFFFFF'; window.open('detail.php?record_id=<?=$listing["ord_id"]?>','','height=400,width=500,menubar=0,resizable=1,scrollbars=1,statusbar=0,titlebar=0,toolbar=0')" /></td>
		<td align="center"><img title="Xóa dữ liệu" hspace="5" src="<?=$fs_imagepath?>delete.gif" style="cursor:pointer" onClick="if(confirm('Bạn có muốn xóa dữ liệu này không?')){window.location.href='delete.php?record_id=<?=$listing["ord_id"]?>&redirect=<?=base64_encode(getURL())?>'}" /></td>
	</tr>
<?
}//End while($listing = mysql_fetch_assoc($db_listing->result))
?>
</table>
<? if($total_record > $page_size){?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
		<td><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
		<td align="right"><a title="Go to top" accesskey="T" class="top" href="#">Lên trên<img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>top.gif"></a></td>
	</tr>
</table>
<? }?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>
<?
unset($db_listing);
?>