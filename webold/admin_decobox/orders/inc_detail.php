<?
include("inc_security.php");

$fs_title	= $module_name . " | Chi tiết";

$record_id	= getValue("record_id");
$db_detail	= new db_query("SELECT *
									 FROM orders_product
									 	INNER JOIN orders ON (ord_id = op_order_id)
									 	INNER JOIN products_multi ON (pro_id = op_product_id)
									 WHERE op_order_id = " . $record_id);
if(mysql_num_rows($db_detail->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$arrDetailOrder = convert_result_set_2_array($db_detail->result);
$db_detail->close();
unset($db_detail);

$action		= getValue("action", "str", "POST", "");
$ord_note	= getValue("ord_note", "str", "POST", $arrDetailOrder[0]["ord_note"]);
$ord_status	= getValue("ord_status", "int", "POST", $arrDetailOrder[0]["ord_status"]);
if($action == "update"){
	// Update ct_status
	$db_update	= new db_execute("UPDATE " . $fs_table . " SET ord_status = " . $ord_status . ", ord_note = '" . replaceMQ($ord_note) . "', ord_last_update = " . time() . " WHERE " . $id_field . " = " . $record_id);
	unset($db_update);
}
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Đóng cửa sổ" href="javascript:window.close()"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>close.gif" />Đóng</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->class_form_name	= "form_name_3";
$form->create_table(3, 3, 'width="470"');
?>
<?=$form->create_control("Họ và tên",$arrDetailOrder[0]["ord_name"])?>
<?
/*$db_city = new db_query("SELECT city_name FROM city WHERE city_id = " . $detail["ct_city_id"]);
if($city = mysql_fetch_assoc($db_city->result)){
	echo $form->create_control("Tỉnh/ Thành phố", $city["city_name"]);
}*/
unset($db_city);
?>
<?=$form->create_control("Di động", $arrDetailOrder[0]["ord_phone"])?>
<?=$form->create_control("Email", $arrDetailOrder[0]["ord_email"])?>
<?=$form->create_control("Địa chỉ", $arrDetailOrder[0]["ord_address"])?>
<?=$form->create_control("Thời gian đặt", date("d/m/Y - H:i A", $arrDetailOrder[0]["ord_date"]))?>
<tr>
	<td class="form_name_3" colspan="2" style="text-align:left">Nội dung :</td>
</tr>
<tr>
	<td class="form_text" colspan="2" style="line-height:16px"><?=str_replace("\n", "<br />", $arrDetailOrder[0]["ord_detail"])?></td>
</tr>
<?
$form->close_table();
unset($form);
?>
<style type="text/css">
#box_product_order td{
	border: 1px solid #e5e5e5;
	text-align: center;
}
.order_control{
	margin-top: 10px;
}
</style>
<table cellpadding="3" class="table" id="box_product_order" style="width: 470px;">
	<tr class="table_title_3">
		<td>Mã đơn hàng</td>
		<td>Tên sản phẩm</td>
		<td>Giá</td>
		<td>Số lượng</td>
	</tr>
	<?
	foreach($arrDetailOrder as $kOrder => $vOrder){
	?>
	<tr>
		<td class="form_text"><?=$vOrder["ord_code"]?></td>
		<td class="form_text"><?=$vOrder["pro_name"]?></td>
		<td class="form_text"><?=($vOrder["pro_price"] > 0 ? format_number($vOrder["pro_price"]) . " VNĐ" : "Liên hệ")?></td>
		<td class="form_text"><?=$vOrder["op_quantity"]?></td>
	</tr>
	<?
	}
	?>
	<tr>
		<td colspan="3" style="text-align: right;">Tổng tiền</td>
		<td><b><?=format_number($arrDetailOrder[0]["ord_total_money"])?> VNĐ</b></td>
	</tr>
</table>
<div class="order_control">
	<form id="form_order_control" method="POST">
		<label class="form_name_3">Ghi chú</label>
		<textarea name="ord_note" style="width: 100%; padding: 10px; margin-bottom: 8px;"><?=$ord_note?></textarea>
		<select name="ord_status">
			<option value="-1">-- Chọn trang thái --</option>
			<?
			$arrStatus		= array(0 => "Chưa xử lý",
										  1 => "Đã xử lý");
			foreach($arrStatus as $key => $value) {
				echo '<option value="' . $key . '" ' . ($key == $ord_status ? ' selected="selected"' : '') . '>' . $value . '</option>';
			}
			?>
		</select> &nbsp;
		<input title="Lưu dữ liệu" src="../images/save.gif" type="image" style="vertical-align: middle;">
		<input type="hidden" name="action" value="update">
	</form>
</div>
</div>
</body>
</html>
<script language="javascript">self.moveTo((screen.width-document.body.clientWidth)/2, (screen.height-document.body.clientHeight)/2);</script>