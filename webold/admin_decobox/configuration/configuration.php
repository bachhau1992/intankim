<?
include("inc_security.php");

//Khai báo biến khi thêm mới
$fs_title				= "Cấu hình Website";
$fs_action				= getURL();
$fs_redirect			= getURL(0,0,1,1,"r") . "&r=" . random();
$fs_errorMsg			= "";

//Get data edit
$record_id				= $lang_id;
$db_edit					= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id);
if(mysql_num_rows($db_edit->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$edit						= mysql_fetch_assoc($db_edit->result);
unset($db_edit);

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$con_admin_email		= getValue("con_admin_email", "str", "POST", $edit["con_admin_email"]);
$con_site_title		= getValue("con_site_title", "str", "POST", $edit["con_site_title"]);
$con_background_style= getValue("con_background_style", "int", "POST", $edit["con_background_style"]);
$con_meta_keywords	= getValue("con_meta_keywords", "str", "POST", $edit["con_meta_keywords"]);
$con_meta_description= getValue("con_meta_description", "str", "POST", $edit["con_meta_description"]);
$con_currency			= getValue("con_currency", "str", "POST", $edit["con_currency"]);
$con_price_updating	= getValue("con_price_updating", "str", "POST", $edit["con_price_updating"]);
$con_root_path			= getValue("con_root_path", "str", "POST", $edit["con_root_path"]);
$con_facebook_code	= getValue("con_facebook_code", "str", "POST", $edit["con_facebook_code"]);
$con_register			= getValue("con_register", "int", "POST", $edit["con_register"]);
$con_company_name		= getValue("con_company_name", "str", "POST", $edit["con_company_name"]);
$con_address			= getValue("con_address", "str", "POST", $edit["con_address"]);
$con_phone				= getValue("con_phone", "str", "POST", $edit["con_phone"]);
$con_mobile				= getValue("con_mobile", "str", "POST", $edit["con_mobile"]);
$con_email				= getValue("con_email", "str", "POST", $edit["con_email"]);
$con_yahoo				= getValue("con_yahoo", "str", "POST", $edit["con_yahoo"]);
$con_skype				= getValue("con_skype", "str", "POST", $edit["con_skype"]);

//Cấu hình static
reset($arrStatic);
foreach($arrStatic as $key => $value){
	$$value				= getValue($value, "int", "POST", $edit[$value]);
}

//Get action variable for add new data
$action					= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	$con_hide_left_menu	= getValue("con_hide_left_menu", "int", "POST", 0);
	$con_register			= getValue("con_register", "int", "POST", 0);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	$myform->add("con_admin_email", "con_admin_email", 2, 1, "", 1, "Địa chỉ email của Admin không hợp lệ.", 0, "");
	$myform->add("con_site_title", "con_site_title", 0, 1, "", 0, "", 0, "");
	$myform->add("con_background_style", "con_background_style", 1, 1, 0, 0, "", 0, "");
	$myform->add("con_meta_keywords", "con_meta_keywords", 0, 1, "", 0, "", 0, "");
	$myform->add("con_meta_description", "con_meta_description", 0, 1, "", 0, "", 0, "");
	$myform->add("con_currency", "con_currency", 0, 1, "", 0, "", 0, "");
	$myform->add("con_price_updating", "con_price_updating", 0, 1, "", 0, "", 0, "");
	$myform->add("con_root_path", "con_root_path", 0, 1, "", 0, "", 0, "");
	$myform->add("con_facebook_code", "con_facebook_code", 0, 1, "", 0, "", 0, "");
	$myform->add("con_register", "con_register", 1, 1, 0, 0, "", 0, "");
	$myform->add("con_company_name", "con_company_name", 0, 1, "", 0, "", 0, "");
	$myform->add("con_address", "con_address", 0, 1, "", 0, "", 0, "");
	$myform->add("con_phone", "con_phone", 0, 1, "", 0, "", 0, "");
	$myform->add("con_mobile", "con_mobile", 0, 1, "", 0, "", 0, "");
	$myform->add("con_email", "con_email", 0, 1, "", 0, "", 0, "");
	$myform->add("con_yahoo", "con_yahoo", 0, 1, "", 0, "", 0, "");
	$myform->add("con_skype", "con_skype", 0, 1, "", 0, "", 0, "");

	//Cấu hình static
	reset($arrStatic);
	foreach($arrStatic as $key => $value){
		$myform->add($value, $value, 1, 1, 0, 0, "", 0, "");
	}

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, 0);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}

	if($fs_errorMsg == ""){

		if($filename != ""){
			$$fs_fieldupload = $filename;
			//Kiểm tra xem nếu có ảnh cũ thì xóa đi
			if($edit[$fs_fieldupload] != "") $upload->delete_file($fs_filepath, $edit[$fs_fieldupload]);
			//Upload ảnh mới
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
		}//End if($filename != "")

		//Insert to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_update);

		//Redirect after insert complate
		redirect($fs_redirect);

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("edit", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<tr><td align="center" colspan="2" class="text_link_bold">- Cấu hình chung -</td></tr>
<?=$form->text("Admin email", "con_admin_email", "con_admin_email", $con_admin_email, "Admin email", 1, 200, "", 255, "", "", "")?>
<?=$form->text("Tiêu đề Website", "con_site_title", "con_site_title", $con_site_title, "Tiêu đề Website", 0, 350, "", 255, "", "", "")?>
<?=$form->getFile("Chọn hình nền", "con_background", "con_background", "hình nền", 0, 32, "", '<br />(Dung lượng tối đa <span style="color:#FF0000">' . $fs_filesize . ' Kb)');?>
<?=$form->select("Vị trí hình nền", "con_background_style", "con_background_style", $arrBackground, $con_background_style, "vị trí hình nền", 0, "", 1, 0, "", "")?>
<?=$form->textarea("Meta Keyword", "con_meta_keywords", "con_meta_keywords", $con_meta_keywords, "Meta Keyword", 0, 350, 45, "", "", "")?>
<?=$form->textarea("Meta Description", "con_meta_description", "con_meta_description", $con_meta_description, "Meta Description", 0, 350, 70, "", "", "")?>
<?=$form->text("Đơn vị tiền tệ", "con_currency", "con_currency", $con_currency, "Đơn vị tiền tệ", 0, 50, "", 30, "", "", "")?>
<?=$form->text("Thông báo khi giá SP bằng 0", "con_price_updating", "con_price_updating", $con_price_updating, "Thông báo khi giá SP bằng 0", 0, 350, "", 255, "", "", "")?>
<?=$form->textarea("Facebook Code", "con_facebook_code", "con_facebook_code", $con_facebook_code, "Facebook Code", 0, 350, 70, "", "", "")?>
<?=$form->checkbox("Show đăng kí", "con_register", "con_register", 1, $con_register, "", 0, "", "")?>
<? //$form->text("Đường dẫn gốc", "con_root_path", "con_root_path", $con_root_path, "Đường dẫn gốc", 0, 200, "", 255, "", "", "")?>
<tr><td align="center" colspan="2" class="text_link_bold">- Thông tin liên hệ -</td></tr>
<?=$form->text("Tên Công ty", "con_company_name", "con_company_name", $con_company_name, "Tên Công ty", 0, 350, "", 255, "", "", "")?>
<?=$form->textarea("Địa chỉ", "con_address", "con_address", $con_address, "Địa chỉ", 0, 350, 58, "", "", "")?>
<?=$form->text("Điện thoại", "con_phone", "con_phone", $con_phone, "Điện thoại", 0, 350, "", 255, "", "", "")?>
<?=$form->text("Di động", "con_mobile", "con_mobile", $con_mobile, "Di động", 0, 350, "", 255, "", "", "")?>
<?=$form->text("Email", "con_email", "con_email", $con_email, "Email", 0, 350, "", 255, "", "", "")?>
<?=$form->text("Yahoo", "con_yahoo", "con_yahoo", $con_yahoo, "Yahoo", 0, 350, "", 255, "", "", "")?>
<?=$form->text("Skype", "con_skype", "con_skype", $con_skype, "Skype", 0, 350, "", 255, "", "", "")?>
<tr><td align="center" colspan="2" class="text_link_bold">- Cấu hình trang tĩnh -</td></tr>
<?
$db_static	= new db_query("SELECT cat_id, cat_name, sta_id, sta_title
									 FROM categories_multi, statics_multi
									 WHERE cat_id = sta_category_id AND cat_type = 'static'
									 ORDER BY cat_order ASC, cat_id ASC, sta_order ASC");
reset($arrStatic);
foreach($arrStatic as $key => $value){
	if(mysql_num_rows($db_static->result) > 0) mysql_data_seek($db_static->result, 0);
?>
<tr>
	<td class="form_name"><?=$key?> :</td>
	<td class="form_text">
		<select title="Chọn trang tĩnh" id="<?=$value?>" name="<?=$value?>" class="form_control">
			<option value="0">--[Chọn trang tĩnh]--</option>
			<?
			$cat_id = 0;
			while($row = mysql_fetch_assoc($db_static->result)){
				if($cat_id != $row["cat_id"]){
					$cat_id = $row["cat_id"];
					echo '<optgroup label="' . $row["cat_name"] . '"></optgroup>';
				}
				$selected = ($$value == $row["sta_id"]) ? ' selected="selected"' : '';
				echo '<option title="' . htmlspecialbo($row["sta_title"]) . '" value="' . $row["sta_id"] . '"' . $selected . '>&nbsp; &nbsp;|-- ' . cut_string($row["sta_title"], 55) . '</option>';
			}
			?>
		</select>
	</td>
</tr>
<?
}
unset($db_static);
?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>