<?
require_once("../security/security.php");

$module_id = 2;
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table			= "configuration";
$fs_fieldupload	= "con_background";
$fs_filepath		= "../../banners/";
$fs_extension		= "gif,jpg,jpe,jpeg,png";
$fs_filesize		= 800;
$id_field			= "con_id";
//Cấu hình static
$arrStatic		= array ("Liên hệ"			=> "con_static_contact",
								"Cuối trang"		=> "con_static_footer",
								"Home"				=> "con_static_home",
								);
// Chọn kiểu hiển thị background
$arrBackground	= array (0	=> "Không hiển thị",
								1	=> "Cố định",
								2	=> "Cuộn theo chuột"
								);
?>
