<?
require_once("../security/security.php");
include("template/config.php");
require_once("../../classes/database.php");
require_once("../../functions/functions.php");

$title		= "Administrator Login";
$username	= getValue("username", "str", "POST", "", 1);
$password	= getValue("password", "str", "POST", "", 1);
$action		= getValue("action", "str", "POST", "");

if($action == "login"){
	$user_id	= 0;
	$user_id = checkLogin($username, $password);
	if($user_id != 0){
		$isAdmin		= 0;
		$db_isadmin	= new db_query("SELECT adm_isadmin, lang_id FROM admin_user WHERE adm_id = " . $user_id);
		$row			= mysql_fetch_assoc($db_isadmin->result);
		if($row["adm_isadmin"] != 0) $isAdmin = 1;
		//Set SESSION
		$_SESSION["logged"]		= 1;
		$_SESSION["user_id"]		= $user_id;
		$_SESSION["userlogin"]	= $username;
		$_SESSION["password"]	= md5($password);
		$_SESSION["isAdmin"]		= $isAdmin;
		$_SESSION["lang_id"]		= $row["lang_id"];
		unset($db_isadmin);
	}
}

//Check logged
$logged = getValue("logged", "int", "SESSION", 0);
if($logged == "1"){
	redirect("../index.php");
}
echo '<!-- Code by: Mr. Tran - Yahoo: txnc2002, boy_infotech -->';
?>
<html>
<head>
<title>FinalStyle - Admin Control Pannel</title>
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.4)" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.4)" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style/login.css" rel="stylesheet" type="text/css">
</head>
<body>
<? /**********------------------- Begin Template V3 - Do not Modify -------------------**********/ ?>
<table height="100%" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<? /*****----- HEADER here -----*****/ ?>
			<table width="<?=$dt_form_width;?>" cellpadding="0" cellspacing="0">
				<tr height="<?=$dt_mid_top_height;?>">
					<? /*** Left _ top ***/ ?>
					<td width="<?=$dt_left_top_width;?>"><img src="<?=$dt_left_top_picture;?>"/></td>
					<? /*** Mid _ top - insert Title here ***/ ?>
					<td background="<?=$dt_mid_top_background;?>">
			<? /*---------- Title start here ----------*/ ?>
			
						<div class="form_title"><?=$title?></div>
			
			<? /*---------- Title end here ----------*/ ?>
					</td>
					<? /*** Right _ top ***/ ?>
					<td width="<?=$dt_right_top_width;?>"><img src="<?=$dt_right_top_picture;?>"/></td>
				</tr>
			</table>
			<? /*****----- End HEADER here -----*****/ ?>
			<? /*****----- BODY Here-----*****/ ?>
			<table height="200" width="<?=$dt_form_width;?>" cellpadding="0" cellspacing="0">
				<tr>
					<? /*** left _ mid ***/ ?>
					<td width="<?=$dt_left_mid_width;?>" background="<?=$dt_left_mid_background;?>"><img src="<?=$dt_left_mid_background;?>" /></td>
					<td align="center" bgcolor="<?=$dt_mid_mid_backcolor;?>">
<? /*----------..................... Start WEB CONTENT here ....................----------*/ ?>
<? /*-------------------------------------------------------------------------------------*/ ?>

<form name="login" action="login.php" method="post">
<table cellpadding="3" cellspacing="3">
	<tr>
		<td rowspan="2"><img src="style/icon.gif" border="0"></td>
		<td class="form_name" align="right">Username :</td>
		<td class="form_text"><input title="Username" id="usermane" name="username" type="text" class="form_control"></td>
	</tr>
	<tr>
		<td class="form_name" align="right">Password :</td>
		<td class="form_text"><input title="Password" id="password" name="password" type="password" class="form_control"></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td><input title="Login" name="Submit" type="image" src="style/button_login.gif"></td>
	</tr>
</table>
<input name="action" type="hidden" value="login">
</form>
<div align="center" class="form_text">Copyright &copy; 2006-<?=date("Y")?> <a class="finalstyle" href="http://www.finalstyle.com" target="_blank">FinalStyle</a>. All right reserved.</div>

<? /*-------------------------------------------------------------------------------------*/ ?>
<? /*----------...................... End WEB CONTENT here .....................----------*/ ?>
					</td>
					<td width="<?=$dt_right_mid_width;?>" background="<?=$dt_right_mid_background;?>"><img src="<?=$dt_right_mid_background;?>" /></td>
				</tr>
			</table>
			<? /*****----- End BODY here -----*****/ ?>
			<? /*****----- FOOTER here -----*****/ ?>
			<table width="<?=$dt_form_width;?>" cellpadding="0" cellspacing="0">
				<tr height="<?=$dt_mid_bottom_height;?>">
					<? /*** Left _ bottom ***/ ?>
					<td width="<?=$dt_left_bottom_width;?>"><img src="<?=$dt_left_bottom_picture;?>"/></td>
					<? /*** Mid _ bottom ***/ ?>
					<td background="<?=$dt_mid_bottom_background;?>"><img src="<?=$dt_mid_bottom_background;?>"/></td>
					<? /*** Right _ bottom ***/ ?>
					<td width="<?=$dt_right_bottom_width;?>"><img src="<?=$dt_right_bottom_picture;?>"/></td>
				</tr>
			</table>
			<? /*****----- End FOOTER here -----*****/ ?>
		</td>
	</tr>
</table>
<? /**********------------------- End Template V3 - Do not Modify -------------------**********/ ?>
</body>
</html>