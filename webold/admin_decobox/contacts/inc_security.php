<?
require_once("../security/security.php");

$module_id	= 30;
$module_name= "Liên hệ";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table		= "contacts";
$id_field		= "ct_id";

// Array variable
$arrStatus		= array(0 => "Chưa đọc", 1 => "Đã đọc");
$arrTitle		= generate_array_variable("Name_title_LIST");
?>