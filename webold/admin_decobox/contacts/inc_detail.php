<?
include("inc_security.php");

$fs_title	= $module_name . " | Chi tiết";

$record_id	= getValue("record_id");
$db_detail	= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
if(mysql_num_rows($db_detail->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$detail	= mysql_fetch_assoc($db_detail->result);
unset($db_detail);

// Update ct_status
if($detail["ct_status"] == 0){
	$db_update	= new db_execute("UPDATE " . $fs_table . " SET ct_status = 1 WHERE " . $id_field . " = " . $record_id);
	unset($db_update);
}
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Đóng cửa sổ" href="javascript:window.close()"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>close.gif" />Đóng</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->class_form_name	= "form_name_3";
$form->create_table(3, 3, 'width="470"');
?>
<?=$form->create_control("Họ và tên", $detail["ct_name"])?>
<?
/*$db_city = new db_query("SELECT city_name FROM city WHERE city_id = " . $detail["ct_city_id"]);
if($city = mysql_fetch_assoc($db_city->result)){
	echo $form->create_control("Tỉnh/ Thành phố", $city["city_name"]);
}*/
unset($db_city);
?>
<?=$form->create_control("Di động", $detail["ct_mobile"])?>
<?=$form->create_control("Email", $detail["ct_email"])?>
<?=$form->create_control("Thời gian gửi", date("d/m/Y - H:i A", $detail["ct_date"]))?>
<tr>
	<td class="form_name_3" colspan="2" style="text-align:left">Nội dung :</td>
</tr>
<tr>
	<td class="form_text" colspan="2" style="line-height:16px"><?=str_replace("\n", "<br />", $detail["ct_content"])?></td>
</tr>
<?
$form->close_table();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">self.moveTo((screen.width-document.body.clientWidth)/2, (screen.height-document.body.clientHeight)/2);</script>