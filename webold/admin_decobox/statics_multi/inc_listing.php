<?
include("inc_security.php");

//Call class menu
$class_menu		= new menu();
$listAll			= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type='static' AND cat_id IN (" . $fs_category . ") AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

//Khai báo biến khi hiển thị danh sách
$fs_title		= $module_name . " | Danh sách";
$fs_action		= "listing.php" . getURL(0,0,0,1,"record_id");
$fs_redirect	= "listing.php" . getURL(0,0,0,1,"record_id|r") . "&r=" . random();
$fs_errorMsg	= "";
$fs_alertMsg	= "";

/*****----- Quick Edit -----*****/
$action			= getValue("action", "str", "POST", "");
if($action == "execute"){

	//Get $record_id for edit data
	$arr_record_id	= getValue("record_id", "arr", "POST", "");
	if(is_array($arr_record_id)){

		//Loop array để update vào database
		for($i=0; $i<count($arr_record_id); $i++){

			$fs_errorMsg= "";

			//Lấy id của data cần sửa đổi
			$record_id	= intval($arr_record_id[$i]);

			//Lấy dữ liệu những trường sửa đổi
			$sta_category_id	= getValue("sta_category_id" . $record_id, "int", "POST", 0);
			$sta_title			= getValue("sta_title" . $record_id, "str", "POST", "");
			$sta_order			= getValue("sta_order" . $record_id, "dbl", "POST", 0);

			//Call class generate_form();
			$myform = new generate_form();
			//Add table insert data
			$myform->addTable($fs_table);
			if(strpos($fs_category, $sta_category_id . ",") === false){
				$fs_errorMsg .= "&bull; Bạn không được phép truy cập category này!<br />";
			}
			$myform->add("sta_category_id", "sta_category_id", 1, 1, 1, 1, "Bạn chưa chọn danh mục.", 0, "");
			$myform->add("sta_title", "sta_title", 0, 1, " ", 1, "Bạn chưa nhập tiêu đề.", 0, "");
			$myform->add("sta_order", "sta_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");

			//Check form data
			$fs_errorMsg .= $myform->checkdata();

			//Kiểm tra nếu có lỗi thì không update
			if($fs_errorMsg == ""){

				//Update to database
				$myform->removeHTML(1);
				$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
				unset($db_update);

			}//End if($fs_errorMsg == "")
			else{
				$fs_alertMsg .= "- Bạn không thay đổi được dữ liệu có ID = " . $record_id . ".\\n";
			}

			unset($myform);

		}//End for($i=0; $i<count($arr_record_id); $i++)

		//Nếu có data nào bị lỗi thì hiển thị thông báo alert message error rồi redirect
		if($fs_alertMsg != ""){
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
			echo '<script language="javascript">alert("Có những lỗi sau:\\n' . $fs_alertMsg . '")</script>';
			redirect($fs_redirect);
		}
		//Ngược lại thì redirect luôn
		else redirect($fs_redirect);

	}//End if(is_array($arr_record_id))

}// End if($action == "execute")
/*****----- End Quick Edit -----*****/

$record_id	= getValue("record_id");

//Search data
$id			= getValue("id");
$keyword		= getValue("keyword", "str", "GET", "", 1);
$category	= getValue("category");
$sqlWhere	= "";
//Tìm theo ID
if($id > 0)	$sqlWhere .= " AND sta_id = " . $id . " ";
//Tìm theo keyword
if($keyword != ""){
	if(validateDate($keyword) == 1){
		$startTime	= convertDateTime($keyword, "00:00:00");
		$endTime		= convertDateTime($keyword, "23:59:59");
		$sqlWhere	.= " AND sta_date >= " . $startTime . " AND sta_date <= " . $endTime . " ";
	}
	else{
		$sqlWhere	.= " AND (sta_title LIKE '%" . $keyword . "%') ";
	}
}
//Tìm theo category
if($category > 0){
	$db_cat	= new db_query("SELECT cat_all_child FROM categories_multi WHERE cat_id = " . $category);
	if($row	= mysql_fetch_assoc($db_cat->result)) $sqlWhere .= " AND cat_id IN (" . $row["cat_all_child"] . ") ";
	unset($db_cat);
}

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "sta_title ASC"; break;
	case 2: $sqlOrderBy = "sta_title DESC"; break;
	case 3: $sqlOrderBy = "sta_order ASC"; break;
	case 4: $sqlOrderBy = "sta_order DESC"; break;
	case 5: $sqlOrderBy = "sta_date ASC"; break;
	case 6: $sqlOrderBy = "sta_date DESC"; break;
	default:$sqlOrderBy = "sta_order ASC"; break;
}

//Get page break params
$page_size		= 20;
$page_prefix	= "Trang: ";
$normal_class	= "page";
$selected_class= "page_current";
$previous		= "<";
$next				= ">";
$first			= "<<";
$last				= ">>";
$break_type		= 1;//"1 => << < 1 2 [3] 4 5 > >>", "2 => < 1 2 [3] 4 5 >", "3 => 1 2 [3] 4 5", "4 => < >"
$url				= getURL(0,0,1,1,"page");
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM categories_multi, " . $fs_table . "
										 WHERE cat_id = sta_category_id AND cat_id IN (" . $fs_category . ") AND " . $id_field . " <> " . $record_id . " AND categories_multi.lang_id = " . $lang_id . $sqlWhere);
$listing_count	= mysql_fetch_assoc($db_count->result);
$total_record	= $listing_count["count"];
$current_page	= getValue("page", "int", "GET", 1);
if($total_record % $page_size == 0) $num_of_page = $total_record / $page_size;
else $num_of_page = (int)($total_record / $page_size) + 1;
if($current_page > $num_of_page) $current_page = $num_of_page;
if($current_page < 1) $current_page = 1;
$db_count->close();
unset($db_count);
//End get page break params
$db_listing	= new db_query("SELECT cat_id, cat_name, cat_type, sta_id, sta_category_id, sta_title, sta_order, sta_date
									 FROM categories_multi, " . $fs_table . "
									 WHERE cat_id = sta_category_id AND cat_id IN (" . $fs_category . ") AND " . $id_field . " <> " . $record_id . " AND categories_multi.lang_id = " . $lang_id . $sqlWhere . "
									 ORDER BY cat_order ASC, " . $sqlOrderBy . "
									 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?>: <font class="count"><?=format_number($total_record)?></font></div>
<div class="content_title" style="float:right"><a title="Thêm mới" href="add.php?category=<?=$category?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>add.gif" />Thêm mới</a></div>
</div>
<div align="center" class="content">
<? //Page break and search data?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
	<?	if($total_record > $page_size){?>
		<td nowrap="nowrap"><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
	<? }?>
		<td align="right">
			<table cellpadding="0" cellspacing="0">
			<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
				<tr>
					<td class="form_search" nowrap="nowrap">
						ID:
						<input title="ID" type="text" class="form_control" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:60px; text-align:right">&nbsp;
						Từ khóa:
						<input title="Từ khóa" type="text" class="form_control" id="keyword" name="keyword" value="<?=htmlspecialbo($keyword)?>" maxlength="255" style="width:100px">&nbsp;
						<select title="Hiển thị theo danh mục" name="category" class="form_control">
							<option value="">--[Tất cả các mục]--</option>
						<?
						for($i=0; $i<count($listAll); $i++){
							$db_count = new db_query("SELECT COUNT(*) AS count FROM " . $fs_table . " WHERE sta_category_id = " . $listAll[$i]["cat_id"]);
							$count	 = mysql_fetch_assoc($db_count->result);
							unset($db_count);
						?>
							<option title="<?=htmlspecialbo($listAll[$i]["cat_name"])?>" value="<?=$listAll[$i]["cat_id"]?>"<? if($listAll[$i]["cat_id"] == $category){echo ' selected="selected"';}?>>
							<?
							for($j=0; $j<$listAll[$i]["level"]; $j++) echo " |--";
							echo " " . $listAll[$i]["cat_name"] . " (" . $count["count"] . ")";
							?>
							</option>
						<?
						}
						?>
						</select>
						<input type="hidden" name="sort" value="<?=$sort?>" />
					</td>
					<td class="form_search" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?=$fs_imagepath?>search.gif" border="0"></td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<? //End page break and search data?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
	<tr class="table_title_3">
		<td>Stt.</td>
		<td>Danh mục</td>
		<td>
			<div>Tiêu đề</div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>Xem</td>
		<td>
			<div>Thứ tự</div>
			<div>
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>
			<div>Ngày tạo</div>
			<div>
				<?=generate_sort("asc", 5, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 6, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td>Lưu</td>
		<td>Sửa</td>
		<td>Xóa</td>
	</tr>
<?
//Call class form
$form = new form();
$form->class_form_name = "form_name_2";
$form->create_form("quick_edit", $fs_action, "post", "multipart/form-data");
?>
<?
$cat_id		= 0;
$record_id	= getValue("record_id", "int", "POST");
//Đếm số thứ tự
$No = ($current_page - 1) * $page_size;
while($listing = mysql_fetch_assoc($db_listing->result)){
	$No++;
	$preview_link	= generate_detail_url($listing["cat_type"], $listing["cat_name"], $listing["sta_title"], $listing["sta_id"]);
?>
	<?
	if($cat_id != $listing["cat_id"]){
		$cat_id = $listing["cat_id"];
	?>
		<tr class="table_title_2">
			<td colspan="10"><?=$listing["cat_name"]?></td>
		</tr>
	<?
	}
	?>
	<tr id="tr_<?=$No?>" <?=$fs_change_bg?>>
		<td class="No"><?=$No?></td>
		<td align="center">
			<select title="Danh mục" id="sta_category_id_<?=$No?>" name="sta_category_id<?=$listing["sta_id"]?>" class="form_control">
				<option value="">--[Danh mục]--</option>
			<? for($i=0; $i<count($listAll); $i++){?>
				<option title="<?=htmlspecialbo($listAll[$i]["cat_name"])?>" value="<?=$listAll[$i]["cat_id"]?>" <? if($listAll[$i]["cat_id"] == $listing["sta_category_id"]){echo 'selected="selected"';}?>>
				<?
				for($j=0; $j<$listAll[$i]["level"]; $j++) echo " |--";
				echo " " . $listAll[$i]["cat_name"];
				?>
				</option>
			<? }?>
			</select>
		</td>
		<td align="center"><input title="Tiêu đề" type="text" id="sta_title_<?=$No?>" name="sta_title<?=$listing["sta_id"]?>" value="<?=htmlspecialbo($listing["sta_title"])?>" class="form_control" style="width:250px" /></td>
		<td align="center"><a href="<?=$preview_link?>" target="_blank"><img border="0" hspace="5" src="<?=$fs_imagepath?>preview.gif"></a></td>
		<td align="center"><input title="Thứ tự" type="text" class="form_control" id="sta_order_<?=$No?>" name="sta_order<?=$listing["sta_id"]?>" value="<?=$listing["sta_order"]?>" style="text-align:right; width:40px" /></td>
		<td align="right">
			<div><?=date("d/m/Y", $listing["sta_date"])?></div>
			<div class="form_text_note"><?=date("H:i:s A", $listing["sta_date"])?></div>
		</td>
		<td align="center"><input title="Lưu dữ liệu" type="image" hspace="5" src="<?=$fs_imagepath?>save.gif" onClick="MM_validateForm('sta_category_id_<?=$No?>','','R', 'sta_title_<?=$No?>','','R'); return document.MM_returnValue" /></td>
		<td align="center"><a title="Sửa dữ liệu" href="edit.php?record_id=<?=$listing["sta_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" hspace="5" src="<?=$fs_imagepath?>edit.gif"></a></td>
		<td align="center"><img title="Xóa dữ liệu" hspace="5" src="<?=$fs_imagepath?>delete.gif" style="cursor:pointer" onClick="if(confirm('Bạn có muốn xóa dữ liệu này không?')){window.location.href='delete.php?record_id=<?=$listing["sta_id"]?>&redirect=<?=base64_encode(getURL())?>'}" /></td>
	</tr>
	<?=$form->hidden("record_id_" . $No, "record_id[]", $listing["sta_id"], "");?>
<?
}//End while($listing = mysql_fetch_assoc($db_listing->result))
?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_form();
unset($form);
?>
</table>
<? if($total_record > $page_size){?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
		<td style="white-space: nowrap;"><?=generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous, $next, $first, $last, $break_type)?></td>
		<td align="right"><a title="Go to top" accesskey="T" class="top" href="#">Lên trên<img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>top.gif"></a></td>
	</tr>
</table>
<? }?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>
<?
unset($db_listing);
?>