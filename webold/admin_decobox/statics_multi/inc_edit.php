<?
include("inc_security.php");

//Call class menu
$class_menu			= new menu();
$listAll				= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type='static' AND cat_id IN (" . $fs_category . ") AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

//Khai báo biến khi thêm mới
$redirect			= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$after_save_data	= getValue("after_save_data", "str", "POST", $redirect);
$add					= base64_encode("add.php");
$listing				= $redirect;
$fs_title			= $module_name . " | Sửa đổi";
$fs_action			= getURL();
$fs_redirect		= $after_save_data;
$fs_redirect		= base64_decode($fs_redirect);
$fs_errorMsg		= "";

//Get data edit
$record_id			= getValue("record_id");
$record_id			= getValue("record_id", "int", "POST", $record_id);
$db_edit				= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
if(mysql_num_rows($db_edit->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$edit					= mysql_fetch_assoc($db_edit->result);
unset($db_edit);

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$sta_category_id	= getValue("sta_category_id", "int", "POST", $edit["sta_category_id"]);
$sta_title			= getValue("sta_title", "str", "POST", $edit["sta_title"]);
$sta_type			= getValue("sta_type", "str", "POST", $edit["sta_type"], 1);
$sta_order			= getValue("sta_order", "dbl", "POST", $edit["sta_order"]);
$sta_picture		= getValue("sta_picture", "str", "POST", $edit["sta_picture"]);
$sta_picture_alt	= getValue("sta_picture_alt", "str", "POST", $edit["sta_picture_alt"]);
$sta_teaser			= getValue("sta_teaser", "str", "POST", $edit["sta_teaser"]);
$sta_description	= getValue("sta_description", "str", "POST", $edit["sta_description"]);
$sta_strdate		= getValue("sta_strdate", "str", "POST", date("d/m/Y", $edit["sta_date"]));
$sta_strtime		= getValue("sta_strtime", "str", "POST", date("H:i:s", $edit["sta_date"]));
$sta_date			= convertDateTime($sta_strdate, $sta_strtime);

//Get action variable for add new data
$action				= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	if(strpos($fs_category, $sta_category_id . ",") === false){
		$fs_errorMsg .= "&bull; Bạn không được phép truy cập category này!<br />";
	}
	$myform->add("sta_category_id", "sta_category_id", 1, 1, 1, 1, "Bạn chưa chọn danh mục.", 0, "");
	$myform->add("sta_title", "sta_title", 0, 1, " ", 1, "Bạn chưa nhập tiêu đề.", 0, "");
	$myform->add("sta_type", "sta_type", 0, 1, " ", 1, "Bạn chưa chọn loại trang tĩnh.", 0, "");
	$myform->add("sta_order", "sta_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	$myform->add("sta_picture", "sta_picture", 0, 1, "", 0, "", 0, "");
	$myform->add("sta_picture_alt", "sta_picture_alt", 0, 1, "", 0, "", 0, "");
	$myform->add("sta_teaser", "sta_teaser", 0, 1, "", 0, "", 0, "");
	$myform->add("sta_description", "sta_description", 0, 1, "", 0, "", 0, "");
	$myform->add("sta_date", "sta_date", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	if($fs_errorMsg == ""){

		//Insert to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_update);

		//Redirect after insert complate
		redirect($fs_redirect);

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../../js/jquery.min.js"></script>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=base64_decode($listing)?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("edit", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->select_db_multi("Danh mục", "sta_category_id", "sta_category_id", $listAll, "cat_id", "cat_name", $sta_category_id, "Danh mục", 1, "", 1, 0, "", "")?>
<?=$form->text("Tiêu đề", "sta_title", "sta_title", $sta_title, "Tiêu đề", 1, 250, "", 255, "", "", "")?>
<?=$form->select("Loại trang tĩnh", "sta_type", "sta_type", $arrStaticType, $sta_type, "Loại trang tĩnh", 1, "", 1, 0, "", "")?>
<?=$form->text("Thứ tự", "sta_order", "sta_order", $sta_order, "Thứ tự", 2, 50, "", 255, "", "", "")?>
<?=$form->text("Đường dẫn ảnh", "sta_picture", "sta_picture", $sta_picture, "Đường dẫn ảnh", 0, 350, "", 255, "", "", "")?>
<?=$form->text("Mô tả ảnh", "sta_picture_alt", "sta_picture_alt", $sta_picture_alt, "Đường dẫn ảnh", 0, 350, "", 255, "", "", "")?>
<?=$form->textarea("Tóm tắt", "sta_teaser", "sta_teaser", $sta_teaser, "Tóm tắt", 0, 350, 70, "", "", "")?>
<?=$form->text("Ngày tạo", "sta_strdate" . $form->ec . "sta_strtime", "sta_strdate" . $form->ec . "sta_strtime", $sta_strdate . $form->ec . $sta_strtime, "Ngày (dd/mm/yyyy)" . $form->ec . "Giờ (hh:mm:ss)", 0, 70 . $form->ec . 70, $form->ec, 10 . $form->ec . 10, " - ", $form->ec, "&nbsp; <i>(Ví dụ: dd/mm/yyyy - hh:mm:ss)</i>");?>
<?=$form->close_table();?>
<?=$form->wysiwyg("Mô tả chi tiết", "sta_description", $sta_description, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->create_table();?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>