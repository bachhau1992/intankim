<?
require_once("../security/security.php");

$module_id	= 11;
$module_name= "Trang tĩnh";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table		= "statics_multi";
$id_field		= "sta_id";
$break_page		= "{---break---}";
?>