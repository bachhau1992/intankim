<?
include("inc_security.php");

//Call class menu
$class_menu				= new menu();
$listAll					= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type = 'product' AND cat_id IN (" . $fs_category . ") AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

//Khai báo biến khi thêm mới
$after_save_data		= getValue("after_save_data", "str", "POST", "add.php");
$add						= "add.php";
$listing					= "listing.php";
$fs_title				= $module_name . " | Thêm mới";
$fs_action				= getURL();
$fs_redirect			= $after_save_data;
$fs_errorMsg			= "";

$category				= getValue("category");
//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$pro_category_id		= getValue("pro_category_id", "int", "POST", $category);
$pro_name				= getValue("pro_name", "str", "POST", "");
$pro_picture_width	= 0;
$pro_picture_height	= 0;

$width_small_image	= 710;
$height_small_image	= 710;
$pro_price				= getValue("pro_price", "dbl", "POST", 0);
$pro_teaser				= getValue("pro_teaser", "str", "POST", "");
$pro_title				= getValue("pro_title", "str", "POST", "");
$pro_meta_keyword		= getValue("pro_meta_keyword", "str", "POST", "");
$pro_meta_description= getValue("pro_meta_description", "str", "POST", "");
$pro_description		= getValue("pro_description", "str", "POST", "");
$pro_shipping_info	= getValue("pro_shipping_info", "str", "POST", "");
$pro_size				= getValue("pro_size", "str", "POST","");
$pro_color				= getValue("pro_color", "str", "POST","");
$pro_style				= getValue("pro_style", "str", "POST", "");
$pro_shipping			= getValue("pro_shipping", "str", "POST", "");
$pro_quantity			= getValue("pro_quantity", "int", "POST", 1);
$pro_hot					= getValue("pro_hot", "int", "POST", 0);
$pro_strdate			= getValue("pro_strdate", "str", "POST", date("d/m/Y"));
$pro_strtime			= getValue("pro_strtime", "str", "POST", date("H:i:s"));
$pro_date				= convertDateTime($pro_strdate, $pro_strtime);
$pro_last_update		= time();
$pro_rename				= removeAccent($pro_name);
$pro_active				= getValue("pro_active", "int", "POST", 1);

//Get action variable for add new data
$pro_temp				= getValue("pro_temp", "str", "POST", random(), 1);
$action					= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	//Lấy dữ liệu kiểu checkbox
	$pro_hot				= getValue("pro_hot", "int", "POST", 0);
	$pro_active			= getValue("pro_active", "int", "POST", 0);

	// Check xem category có tồn tại hay ko
	$db_check			= new db_query("SELECT cat_id FROM categories_multi WHERE cat_type = 'product' AND cat_id = " . $pro_category_id);
	if(mysql_num_rows($db_check->result) == 0){
		$fs_errorMsg	.= "&bull; Danh mục bạn chọn không tồn tại.<br />";
	}
	$db_check->close();
	unset($db_check);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	if(strpos($fs_category, $pro_category_id . ",") === false){
		$fs_errorMsg .= "&bull; Bạn không được phép truy cập category này!<br />";
	}
	$myform->add("pro_category_id", "pro_category_id", 1, 1, 1, 1, "Bạn chưa chọn danh mục.", 0, "");
	$myform->add("pro_name", "pro_name", 0, 1, " ", 1, "Bạn chưa nhập tên sản phẩm.", 0, "");
	$myform->add("pro_picture_width", "pro_picture_width", 1, 1, 0, 0, "", 0, "");
	$myform->add("pro_picture_height", "pro_picture_height", 1, 1, 0, 0, "", 0, "");
	$myform->add("pro_price", "pro_price", 3, 1, 0, 0, "", 0, "");
	$myform->add("pro_size", "pro_size", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_color", "pro_color", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_style", "pro_style", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_shipping", "pro_shipping", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_quantity", "pro_quantity", 1, 1, 0, 0, "", 0, "");
	$myform->add("pro_teaser", "pro_teaser", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_rename", "pro_rename", 0, 1, 0, 0, "", 0, "");
	$myform->add("pro_title", "pro_title", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_meta_keyword", "pro_meta_keyword", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_meta_description", "pro_meta_description", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_description", "pro_description", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_shipping_info", "pro_shipping_info", 0, 1, "", 0, "", 0, "");
	$myform->add("pro_hot", "pro_hot", 1, 1, 0, 0, "", 0, "");
	$myform->add("pro_date", "pro_date", 1, 1, 0, 0, "", 0, "");
	$myform->add("pro_last_update", "pro_last_update", 1, 1, 0, 0, "", 0, "");
	$myform->add("pro_active", "pro_active", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, $fs_insert_logo);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}

	if($fs_errorMsg == ""){

		if($filename != ""){
			//Upload new image
			$$fs_fieldupload = $filename;
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
			$upload->resize_image($fs_filepath, $filename, $width_small_image, $height_small_image, "small_", 1);
			list($pro_picture_width, $pro_picture_height)	= @getimagesize($fs_filepath . $filename);
		}//End if($filename != "")

		//Insert to database
		$myform->removeHTML(0);
		$db_insert	= new db_execute_return();
		$last_id		= $db_insert->db_execute($myform->generate_insert_SQL());
		unset($db_insert);

		if($last_id > 0){

			// Update more picture
			$db_update	= new db_execute("UPDATE product_pictures SET pp_product_id = " . $last_id . ", pp_temp = NULL WHERE pp_temp = '" . $pro_temp . "'");
			unset($db_update);

			// Insert vào bảng news_hits
			$db_insert	= new db_execute("INSERT IGNORE product_hits VALUES(" . $last_id . ", 0)");
			unset($db_insert);

			//Redirect after insert complate
			$fs_redirect .= "?category=" . $pro_category_id;
			redirect($fs_redirect);

		}
		else{
			$fs_errorMsg .= "&bull; Không insert được vào database. Bạn hãy kiểm tra lại câu lệnh INSERT INTO.<br />";
		}

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../../js/jquery.min.js"></script>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=$listing?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("add", $fs_action, "post", "multipart/form-data", '');
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->select_db_multi("Danh mục", "pro_category_id", "pro_category_id", $listAll, "cat_id", "cat_name", $pro_category_id, "Danh mục", 1, "", 1, 0, "", "")?>
<?=$form->text("Tên sản phẩm", "pro_name", "pro_name", $pro_name, "Tên sản phẩm", 1, 250, "", 255, "", "", "")?>
<?=$form->getFile("Ảnh minh họa", "pro_picture", "pro_picture", "Ảnh minh họa", 0, 32, "", '<br />(Dung lượng tối đa <font color="#FF0000">' . $fs_filesize . ' Kb</font>)')?>
<?
$more_picture	= '<input type="button" class="form_button" value="Thêm ảnh" style="background:url(../images/button_3.gif)" onClick="more_picture(\'add\', \'' . $pro_temp . '\')" />';
echo $form->create_control("", $more_picture);
?>
<?=$form->hidden("pro_temp", "pro_temp", $pro_temp, "");?>
<?
$price_text	= ($pro_price > 0 ? '<div id="price_text">' . format_number($pro_price) . '</div>' : '<div id="price_text" style="display:none"></div>');
?>
<?=$form->text("Giá sản phẩm", "pro_price", "pro_price", $pro_price, "Giá sản phẩm", 0, 100, "", 30, "", 'autocomplete="off" onkeyup="changePriceText(\'price_text\', this.value)"', ' VNĐ' . $price_text)?>
<?=$form->text("Kích thước", "pro_size", "pro_size", $pro_size, " Kích thước", 0, 250, "", 100, "", "", "")?>
<?=$form->text("Màu sắc", "pro_color", "pro_color", $pro_color, " Màu sắc", 0, 250, "", 100, "", "", "")?>
<?=$form->text("Phong cách", "pro_style", "pro_style", $pro_style, " Phong cách", 0, 250, "", 100, "", "", "")?>
<?=$form->text("Thời gian giao hàng", "pro_shipping", "pro_shipping", $pro_shipping, " Thời gian giao hàng", 0, 250, "", 100, "", "", "")?>
<?=$form->text("Số lượng", "pro_quantity", "pro_quantity", $pro_quantity, "Số lượng", 0, 50, "", 30, "", 'autocomplete="off"  onfocus="if(this.value==0)this.value=\'\'"', "")?>
<?=$form->checkbox("Nổi bật", "pro_hot", "pro_hot", 1, $pro_hot, "", 0, "", "")?>
<?=$form->textarea("Tóm tắt", "pro_teaser", "pro_teaser", $pro_teaser, "Tóm tắt tin", 0, 350, 70, "", "", "")?>
<?=$form->text("Title sản phẩm", "pro_title", "pro_title", $pro_title, " Title sản phẩm", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta keyword", "pro_meta_keyword", "pro_meta_keyword", $pro_meta_keyword, " Meta keyword", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta Description", "pro_meta_description", "pro_meta_description", $pro_meta_description, " Meta Description", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Ngày cập nhật", "pro_strdate" . $form->ec . "pro_strtime", "pro_strdate" . $form->ec . "pro_strtime", $pro_strdate . $form->ec . $pro_strtime, "Ngày (dd/mm/yyyy)" . $form->ec . "Giờ (hh/mm/ss)", "0", "70" . $form->ec . "70", $form->ec, "10" . $form->ec . "10", " - ", $form->ec, "&nbsp; <i>(Ví dụ: dd/mm/yyyy - hh/mm/ss)</i>");?>
<?=$form->checkbox("Kích hoạt", "pro_active", "pro_active", 1, $pro_active, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->close_table();?>
<?=$form->wysiwyg("Mô tả chi tiết", "pro_description", $pro_description, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->wysiwyg("Giao hàng và đổi trả", "pro_shipping_info", $pro_shipping_info, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->create_table();?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>