<?
include("inc_security.php");

//Khai báo biến khi thêm mới
$after_save_data	= getValue("after_save_data", "str", "POST", "add.php");
$add					= "add.php";
$listing				= "listing.php";
$fs_title			= $module_name . " | Thêm mới";
$fs_action			= getURL();
$fs_redirect		= $after_save_data;
$fs_errorMsg		= "";

$type					= getValue("type", "str", "GET", "news");
//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$cat_name			= getValue("cat_name", "str", "POST", "");
$cat_type			= getValue("cat_type", "str", "POST", $type);
$cat_order			= getValue("cat_order", "dbl", "POST", 1);
$cat_parent_id		= getValue("cat_parent_id", "int", "POST", 0);
$cat_title			= getValue("cat_title", "str", "POST", "");
$cat_meta_keyword	= getValue("cat_meta_keyword", "str", "POST", "");
$cat_meta_description= getValue("cat_meta_description", "str", "POST", "");
$cat_description	= getValue("cat_description", "str", "POST", "");
$cat_show			= getValue("cat_show", "int", "POST", 0);
$cat_active			= getValue("cat_active", "int", "POST", 1);

$class_menu			= new menu();
$listAll				= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type = '" . $cat_type . "' AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

//Get action variable for add new data
$action				= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	//Lấy control dạng checkbox
	$cat_active		= getValue("cat_active", "int", "POST", 0);
	$cat_show		= getValue("cat_show", "int", "POST", 0);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	$myform->add("cat_name", "cat_name", 0, 1, " ", 1, "Bạn chưa nhập tên danh mục.", 0, "");
	$myform->add("cat_type", "cat_type", 0, 1, " ", 1, "Bạn chưa chọn loại danh mục.", 0, "");
	$myform->add("cat_order", "cat_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	$myform->add("cat_parent_id", "cat_parent_id", 1, 1, 0, 0, "", 0, "");
	$myform->add("cat_title", "cat_title", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_meta_keyword", "cat_meta_keyword", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_meta_description", "cat_meta_description", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_description", "cat_description", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_show", "cat_show", 1, 1, 0, 0, "", 0, "");
	$myform->add("cat_active", "cat_active", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	if($fs_errorMsg == ""){

		//Insert to database
		$myform->removeHTML(0);
		$db_insert	= new db_execute_return();
		$last_id		= $db_insert->db_execute($myform->generate_insert_SQL());
		unset($db_insert);

		if($last_id > 0){
			//Update mnu_has_child cua parent_id
			if($cat_parent_id > 0){
				$db_update = new db_execute("UPDATE categories_multi SET cat_has_child = 1 WHERE cat_id = " . $cat_parent_id);
				unset($db_update);
			}

			// Update cat_all_child
			$db_update	= new db_execute("UPDATE categories_multi SET cat_all_child = '" . $last_id . "' WHERE cat_id = " . $last_id);
			unset($db_update);

			//Redirect after insert complate
			$fs_redirect .= "?type=" . $cat_type;
			redirect($fs_redirect);
		}
		else{
			echo "Khong insert dc data !";
			exit();
		}

		//Redirect after insert complate
		$fs_redirect .= "?type=" . $cat_type;
		redirect($fs_redirect);

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../../js/jquery.min.js"></script>
<script language="javascript" src="../js/library.js"></script>
<script language="javascript" src="../js/ajax.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=$listing?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$onChange	= "load_data('load_parent.php?type=' + this.value, 'content_loader')";
$form = new form();
$form->create_form("add", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->text("Tên danh mục", "cat_name", "cat_name", $cat_name, "Tên danh mục", 1, 250, "", 255, "", "", "")?>
<?=$form->select("Loại danh mục", "cat_type", "cat_type", $arrCategoryType, $cat_type, "Loại danh mục", 1, "", 1, 0, 'onChange="' . $onChange . '"', "")?>
<?=$form->text("Thứ tự", "cat_order", "cat_order", $cat_order, "Thứ tự", 2, 50, "", 5, "", "", "")?>
<tr>
	<td class="form_name">Danh mục cấp trên :</td>
	<td class="form_text">
		<div id="content_loader">
			<select title="Danh mục cấp trên" id="cat_parent_id" name="cat_parent_id" class="form_control">
				<option value="0">--[Danh mục cấp trên]--</option>
				<?
				for($i=0; $i<count($listAll); $i++){
					$selected = ($cat_parent_id == $listAll[$i]["cat_id"]) ? ' selected="selected"' : '';
					echo '<option title="' . htmlspecialbo($listAll[$i]["cat_name"]) . '" value="' . $listAll[$i]["cat_id"] . '"' . $selected . '>';
					for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
					echo ' ' . cut_string($listAll[$i]["cat_name"], 55) . '</option>';
				}
				?>
			</select>
		</div>
	</td>
</tr>
<?=$form->text("Meta title danh mục", "cat_title", "cat_title", $cat_title, " Meta title danh mục", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta keyword", "cat_meta_keyword", "cat_meta_keyword", $cat_meta_keyword, " Meta keyword", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta Description", "cat_meta_description", "cat_meta_description", $cat_meta_description, " Meta Description", 0, 250, "", 250, "", "", "")?>
<?=$form->checkbox("Kích hoạt", "cat_active", "cat_active", 1, $cat_active, "", 0, "", "")?>
<?=$form->checkbox("Show home", "cat_show", "cat_show", 1, $cat_show, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->close_table();?>
<?=$form->wysiwyg("Mô tả chi tiết", "cat_description", $cat_description, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->create_table();?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>