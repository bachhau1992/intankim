<?
include("inc_security.php");

$fs_title		= $module_name . " | Danh sách";
$fs_action		= "listing.php" . getURL(0,0,0,1,"record_id");
$fs_redirect	= "listing.php" . getURL(0,0,0,1,"record_id|r") . "&r=" . random();
$fs_errorMsg	= "";
$fs_alertMsg	= "";

/*****----- Quick Edit -----*****/
$action			= getValue("action", "str", "POST", "");
if($action == "execute"){

	//Get $record_id for edit data
	$arr_record_id	= getValue("record_id", "arr", "POST", "");
	if(is_array($arr_record_id)){

		//Loop array để update vào database
		for($i=0; $i<count($arr_record_id); $i++){

			$fs_errorMsg= "";

			//Lấy id của data cần sửa đổi
			$record_id	= intval($arr_record_id[$i]);

			//Lấy dữ liệu những trường sửa đổi
			$cat_name	= getValue("cat_name" . $record_id, "str", "POST", "");
			$cat_order	= getValue("cat_order" . $record_id, "dbl", "POST", 0);

			//Call class generate_form();
			$myform = new generate_form();
			//Add table insert data
			$myform->addTable($fs_table);
			$myform->add("cat_name", "cat_name", 0, 1, " ", 1, "Bạn chưa nhập tên danh mục.", 0, "");
			$myform->add("cat_order", "cat_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");

			//Check form data
			$fs_errorMsg .= $myform->checkdata();

			//Kiểm tra nếu có lỗi thì không update
			if($fs_errorMsg == ""){

				//Update to database
				$myform->removeHTML(1);
				$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
				unset($db_update);

			}//End if($fs_errorMsg == "")
			else{
				$fs_alertMsg .= "- Bạn không thay đổi được danh mục có ID = " . $record_id . ".\\n";
			}

			unset($myform);

		}//End for($i=0; $i<count($arr_record_id); $i++)

		//Nếu có data nào bị lỗi thì hiển thị thông báo alert message error rồi redirect
		if($fs_alertMsg != ""){
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
			echo '<script language="javascript">alert("Có những lỗi sau:\\n' . $fs_alertMsg . '"); window.location.href="' . $fs_redirect . '"</script>';
			exit();
		}
		//Ngược lại thì redirect luôn
		else redirect($fs_redirect);

	}//End if(is_array($arr_record_id))

}// End if($action == "execute")
/*****----- End Quick Edit -----*****/

//Search data
$id			= getValue("id");
$type			= getValue("type", "str", "GET", "", 1);
$sqlWhere	= "";
//Tìm theo ID
if($id > 0)			$sqlWhere .= " AND cat_id = " . $id . " ";
//Tìm theo type
if($type		!= "")$sqlWhere .= " AND cat_type = '" . $type . "' ";

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "cat_type, cat_order ASC"; break;
	case 2: $sqlOrderBy = "cat_type, cat_order DESC"; break;
	case 3: $sqlOrderBy = "cat_type, cat_name ASC"; break;
	case 4: $sqlOrderBy = "cat_type, cat_name DESC"; break;
	default:$sqlOrderBy = "cat_type, cat_order ASC"; break;
}

$class_menu		= new menu();
$listAll			= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "lang_id = " . $lang_id . $sqlWhere, "cat_id,cat_name,cat_type,cat_order,cat_show,cat_active", $sqlOrderBy, "cat_has_child", 0);
unset($class_menu);
$total_record	= count($listAll);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?>: <font class="count"><?=format_number($total_record)?></font></div>
<div class="content_title" style="float:right"><a title="Thêm mới" href="add.php?type=<?=$type?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>add.gif" />Thêm mới</a></div>
</div>
<div align="center" class="content">
<? //Page break and search data?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
		<td><input title="Refresh danh mục" type="button" class="form_button" value="Refresh" style="background:url(<?=$fs_imagepath?>button_3.gif) no-repeat" onClick="if(confirm('Bạn có muốn refresh lại tất cả danh mục không?')) window.location.href='refresh.php?redirect=<?=base64_encode(getURL())?>'" /></td>
		<td align="right">
			<table cellpadding="0" cellspacing="0">
			<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
				<tr>
					<td class="form_search" nowrap="nowrap">
						ID:
						<input title="ID" type="text" class="form_control" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:60px; text-align:right">&nbsp;
						Hiển thị:
						<select title="Hiển thị theo loại" name="type" class="form_control">
							<option value="">--[Tất cả]--</option>
						<? foreach($arrCategoryType as $key => $value){?>
							<option value="<?=$key?>" <? if($key == $type){echo 'selected="selected"';}?>><?=$value?></option>
						<? }?>
						</select>
						<input type="hidden" name="sort" value="<?=$sort?>" />
					</td>
					<td class="form_search" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?=$fs_imagepath?>search.gif" border="0"></td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<? //End page break and search data?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
	<tr class="table_title_3">
		<td>Stt.</td>
		<td>
			<div>Thứ tự | Danh mục</div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
				&nbsp;
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
				&nbsp; &nbsp;
			</div>
		</td>
		<td>Link dữ liệu</td>
		<td>Kích hoạt</td>
		<td>Show home</td>
		<td>Lưu</td>
		<td>Sửa</td>
		<td>Xóa</td>
	</tr>
<?
//Call class form
$form = new form();
$form->class_form_name = "form_name_2";
$form->create_form("quick_edit", $fs_action, "post", "multipart/form-data");
?>
<?
$cat_type	= "";
//Đếm số thứ tự
$No = 0;
for($i=0; $i<count($listAll); $i++){
	$No++;
	$link_data		= generate_type_url($listAll[$i]["cat_name"], $listAll[$i]["cat_id"]);
	$preview_link	= $fs_preview . "redirect.php?redirect=" . base64_encode($link_data);
?>
	<?
	if($cat_type != $listAll[$i]["cat_type"]){
		$cat_type = $listAll[$i]["cat_type"];
	?>
		<tr class="table_title_2">
			<td colspan="10"><?=$arrCategoryType[$listAll[$i]["cat_type"]]?></td>
		</tr>
	<?
	}
	?>
	<tr id="tr_<?=$No?>" <?=$fs_change_bg?>>
		<td class="No"><?=$No?></td>
		<td class="text_normal_red">
			<? for($j=0; $j<$listAll[$i]["level"]; $j++) echo "|--- ";?>
			<input title="Thứ tự" type="text" id="cat_order_<?=$No?>" name="cat_order<?=$listAll[$i]["cat_id"]?>" value="<?=$listAll[$i]["cat_order"]?>" class="form_control" style="width:20px; text-align:center<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" />
			<input title="Tên danh mục" type="text" id="cat_name_<?=$No?>" name="cat_name<?=$listAll[$i]["cat_id"]?>" value="<?=htmlspecialbo($listAll[$i]["cat_name"])?>" class="form_control" style="width:200px<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" />
		</td>
		<td><? if($listAll[$i]["cat_type"] != "static"){?><a title="<?=$link_data?>" href="<?=$preview_link?>" target="_blank" style="color:#0000FF"><?=cut_string($link_data, 40)?></a><? }?></td>
		<td align="center"><a href="quickset.php?type=active&record_id=<?=$listAll[$i]["cat_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["cat_active"]?>.gif" /></a></td>
		<td align="center"><a href="quickset.php?type=show&record_id=<?=$listAll[$i]["cat_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" src="<?=$fs_imagepath?>check_small_<?=$listAll[$i]["cat_show"]?>.gif" /></a></td>
		<td align="center"><input title="Lưu dữ liệu" type="image" hspace="5" src="<?=$fs_imagepath?>save.gif" onClick="MM_validateForm('cat_order_<?=$No?>','','RisNum', 'cat_name_<?=$No?>','','R'); return document.MM_returnValue" /></td>
		<td align="center"><img title="Sửa dữ liệu" hspace="5" src="<?=$fs_imagepath?>edit.gif" style="cursor:pointer" onClick="window.location.href='edit.php?record_id=<?=$listAll[$i]["cat_id"]?>&redirect=<?=base64_encode(getURL())?>'"></td>
		<td align="center"><img title="Xóa dữ liệu" hspace="5" src="<?=$fs_imagepath?>delete.gif" style="cursor:pointer" onClick="if(confirm('Bạn có muốn xóa dữ liệu này không?')){window.location.href='delete.php?record_id=<?=$listAll[$i]["cat_id"]?>&redirect=<?=base64_encode(getURL())?>'}" /></td>
	</tr>
	<?=$form->hidden("record_id_" . $No, "record_id[]", $listAll[$i]["cat_id"], "");?>
<?
}// End for($i=0; $i<count($listAll); $i++)
?>
	<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_form();
unset($form);
?>
</table>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>