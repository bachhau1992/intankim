<?
include("inc_security.php");

$record_id	= getValue("record_id");
$parent_id	= getValue("parent_id");
$type			= getValue("type", "str", "GET", "");

$class_menu	= new menu();
$listAll		= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type = '" . $type . "' AND cat_id <> " . $record_id . " AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);
?>
<select title="Danh mục cấp trên" id="cat_parent_id" name="cat_parent_id" class="form_control">
	<option value="0">--[Danh mục cấp trên]--</option>
	<?
	for($i=0; $i<count($listAll); $i++){
		$selected = ($parent_id == $listAll[$i]["cat_id"]) ? ' selected="selected"' : '';
		echo '<option title="' . htmlspecialbo($listAll[$i]["cat_name"]) . '" value="' . $listAll[$i]["cat_id"] . '"' . $selected . '>';
		for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
		echo ' ' . cut_string($listAll[$i]["cat_name"], 55) . '</option>';
	}
	?>
</select>