<?
include("inc_security.php");

//Khai báo biến khi thêm mới
$redirect			= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$after_save_data	= getValue("after_save_data", "str", "POST", $redirect);
$add					= base64_encode("add.php");
$listing				= $redirect;
$fs_title			= $module_name . " | Sửa đổi";
$fs_action			= getURL();
$fs_redirect		= $after_save_data;
$fs_redirect		= base64_decode($fs_redirect);
$fs_errorMsg		= "";

//Get data edit
$record_id			= getValue("record_id");
$record_id			= getValue("record_id", "int", "POST", $record_id);
$db_edit				= new db_query("SELECT * FROM categories_multi WHERE cat_id = " . $record_id . " AND lang_id = " . $lang_id);
if(mysql_num_rows($db_edit->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$edit					= mysql_fetch_assoc($db_edit->result);
unset($db_edit);

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$cat_name			= getValue("cat_name", "str", "POST", $edit["cat_name"]);
$cat_type			= getValue("cat_type", "str", "POST", $edit["cat_type"], 1);
$cat_order			= getValue("cat_order", "dbl", "POST", $edit["cat_order"]);
$cat_parent_id		= getValue("cat_parent_id", "int", "POST", $edit["cat_parent_id"]);
$cat_title			= getValue("cat_title", "str", "POST", $edit["cat_title"]);
$cat_meta_keyword	= getValue("cat_meta_keyword", "str", "POST", $edit["cat_meta_keyword"]);
$cat_meta_description= getValue("cat_meta_description", "str", "POST", $edit["cat_meta_description"]);
$cat_description	= getValue("cat_description", "str", "POST", $edit["cat_description"]);
$cat_show			= getValue("cat_show", "int", "POST", 0);
$cat_active			= getValue("cat_active", "int", "POST", $edit["cat_active"]);

$class_menu			= new menu();
$listAll				= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type = '" . $cat_type . "' AND cat_id <> " . $record_id . " AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

//Get action variable for add new data
$action				= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	//Lấy control dạng checkbox
	$cat_active		= getValue("cat_active", "int", "POST", 0);
	$cat_show		= getValue("cat_show", "int", "POST", 0);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	// Add table insert data
	$myform->addTable($fs_table);
	$myform->add("cat_name", "cat_name", 0, 1, " ", 1, "Bạn chưa nhập tên danh mục.", 0, "");
	$myform->add("cat_type", "cat_type", 0, 1, " ", 1, "Bạn chưa chọn loại danh mục.", 0, "");
	$myform->add("cat_order", "cat_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	$myform->add("cat_parent_id", "cat_parent_id", 1, 1, 0, 0, "", 0, "");
	$myform->add("cat_title", "cat_title", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_meta_keyword", "cat_meta_keyword", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_meta_description", "cat_meta_description", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_description", "cat_description", 0, 1, "", 0, "", 0, "");
	$myform->add("cat_show", "cat_show", 1, 1, 0, 0, "", 0, "");
	$myform->add("cat_active", "cat_active", 1, 1, 0, 0, "", 0, "");

	// Check form data
	$fs_errorMsg .= $myform->checkdata();

	if($fs_errorMsg == ""){

		// Update to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_update);

		// Update has child cua parent_id
		if($cat_parent_id > 0){
			$db_update = new db_execute("UPDATE " . $fs_table . " SET cat_has_child = 1 WHERE " . $id_field . " = " . $cat_parent_id);
			unset($db_update);
		}

		// Check xem cái cha cũ còn con hay không
		$db_check	= new db_query("SELECT " . $id_field . " FROM " . $fs_table . " WHERE cat_parent_id = " . $edit["cat_parent_id"]);
		if(mysql_num_rows($db_check->result) == 0){
			$db_update = new db_execute("UPDATE " . $fs_table . " SET cat_has_child = 0 WHERE " . $id_field . " = " . $edit["cat_parent_id"]);
			unset($db_update);
		}

		// Nếu có con và đổi type thì update type theo cấp cha
		if($edit["cat_has_child"] > 0 && $cat_type != $edit["cat_type"]){

			$class_menu	= new menu();
			$listAll		= $class_menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", $record_id, "lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
			unset($class_menu);

			// Danh sách update
			$list_id		= "";
			for($i=0; $i<count($listAll); $i++) $list_id	.= $listAll[$i]["cat_id"] . ",";
			$list_id		.= $record_id;

			$db_update	= new db_execute("UPDATE " . $fs_table . " SET cat_type = '" . $cat_type . "' WHERE cat_id IN(" . $list_id . ")");
			unset($db_update);

		}

		// Redirect after insert complate
		redirect($fs_redirect);

	}// End if($fs_errorMsg == "")
	unset($myform);

}// End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../../js/jquery.min.js"></script>
<script language="javascript" src="../js/library.js"></script>
<script language="javascript" src="../js/ajax.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=base64_decode($listing)?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$onChange	= "load_data('load_parent.php?record_id=" . $record_id . "&parent_id=" . $cat_parent_id . "&type=' + this.value, 'content_loader')";
$form = new form();
$form->create_form("edit", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->text("Tên danh mục", "cat_name", "cat_name", $cat_name, "Tên danh mục", 1, 250, "", 255, "", "", "")?>
<?=$form->select("Loại danh mục", "cat_type", "cat_type", $arrCategoryType, $cat_type, "Loại danh mục", 1, "", 1, 0, 'onChange="' . $onChange . '"', "")?>
<?=$form->text("Thứ tự", "cat_order", "cat_order", $cat_order, "Thứ tự", 2, 50, "", 5, "", "", "")?>
<tr>
	<td class="form_name">Danh mục cấp trên :</td>
	<td class="form_text">
		<div id="content_loader">
			<select title="Danh mục cấp trên" id="cat_parent_id" name="cat_parent_id" class="form_control">
				<option value="0">--[Danh mục cấp trên]--</option>
				<?
				for($i=0; $i<count($listAll); $i++){
					$selected = ($cat_parent_id == $listAll[$i]["cat_id"]) ? ' selected="selected"' : '';
					echo '<option title="' . htmlspecialbo($listAll[$i]["cat_name"]) . '" value="' . $listAll[$i]["cat_id"] . '"' . $selected . '>';
					for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
					echo ' ' . cut_string($listAll[$i]["cat_name"], 55) . '</option>';
				}
				?>
			</select>
		</div>
	</td>
</tr>
<?=$form->text("Meta title danh mục", "cat_title", "cat_title", $cat_title, " Meta title danh mục", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta keyword", "cat_meta_keyword", "cat_meta_keyword", $cat_meta_keyword, " Meta keyword", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta Description", "cat_meta_description", "cat_meta_description", $cat_meta_description, " Meta Description", 0, 250, "", 250, "", "", "")?>
<?=$form->checkbox("Kích hoạt", "cat_active", "cat_active", 1, $cat_active, "", 0, "", "")?>
<?=$form->checkbox("Show home", "cat_show", "cat_show", 1, $cat_show, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->close_table();?>
<?=$form->wysiwyg("Mô tả chi tiết", "cat_description", $cat_description, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->create_table();?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>