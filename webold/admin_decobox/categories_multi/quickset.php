<?
include("inc_security.php");

$type			= getValue("type", "str", "GET", "");
$record_id	= getValue("record_id");
$redirect	= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$redirect	= base64_decode($redirect);

$arrQuickSet= array("active"	=> "cat_active",
						  "show" 	=> "cat_show"
						  );
if(isset($arrQuickSet[$type])){
	$db_update	= new db_execute("UPDATE " . $fs_table . " SET " . $arrQuickSet[$type] . " = IF(" . $arrQuickSet[$type] . " = 1, 0, 1) WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
	unset($db_update);
}

redirect($redirect);
?>