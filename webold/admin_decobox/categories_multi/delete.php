<?
include("inc_security.php");

$record_id	= getValue("record_id");
$redirect	= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$redirect	= base64_decode($redirect);

$db_check	= new db_query("SELECT " . $id_field . ", cat_parent_id, cat_has_child FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
if($check = mysql_fetch_assoc($db_check->result)){
	
	if(checkDeleteCategory($record_id) == 1){
		
		// Xóa data
		$db_delete = new db_execute("DELETE FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
		unset($db_delete);
		
		// Nếu có con thì update parent_id của con thành parent_id của cha
		if($check["cat_has_child"] == 1){
			$db_update = new db_query("UPDATE " . $fs_table . " SET cat_parent_id = " . $check["cat_parent_id"] . " WHERE cat_parent_id = " . $check["cat_id"]);
			unset($db_update);
		}
		
	}
	else{
	
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
		echo '<script language="javascript">alert("Danh mục này đã có dữ liệu. Bạn hãy xóa dữ liệu trước rồi thử lại.")</script>';
	
	}
}
unset($db_check);

redirect($redirect);
?>