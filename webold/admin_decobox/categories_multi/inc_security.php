<?
require_once("../security/security.php");

$module_id	= 4;
$module_name= "Category";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table			= "categories_multi";
$id_field			= "cat_id";
?>