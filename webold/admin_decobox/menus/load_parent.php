<?
include("inc_security.php");

$record_id	= getValue("record_id");
$parent_id	= getValue("parent_id");
$type			= getValue("type", "int", "GET", 1);

$class_menu	= new menu();
$listAll		= $class_menu->getAllChild("menus", "mnu_id", "mnu_parent_id", 0, "mnu_type = " . $type . " AND mnu_id <> " . $record_id . " AND lang_id = " . $lang_id, "mnu_id,mnu_name,mnu_type", "mnu_order ASC,mnu_name ASC", "mnu_has_child", 0);
unset($class_menu);
?>
<select title="Menu cấp trên" id="mnu_parent_id" name="mnu_parent_id" class="form_control">
	<option value="0">--[Menu cấp trên]--</option>
	<?
	for($i=0; $i<count($listAll); $i++){
		$selected = ($parent_id == $listAll[$i]["mnu_id"]) ? ' selected="selected"' : '';
		echo '<option title="' . htmlspecialbo($listAll[$i]["mnu_name"]) . '" value="' . $listAll[$i]["mnu_id"] . '"' . $selected . '>';
		for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
		echo ' ' . cut_string($listAll[$i]["mnu_name"], 55) . '</option>';
	}
	?>
</select>