<?
require_once("../security/security.php");

$module_id	= 17;
$module_name= "Menu";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table	= "menus";
$id_field	= "mnu_id";
$arrTarget	= array ("_self"	=> "Hiện hành",
							"_blank"	=> "Trang mới",
							);
$arrType		= array (1 => "Phía trên",
							//2 => "Bên trái",
							//3 => "Bên phải",
							4 => "Phía dưới",
							);
?>