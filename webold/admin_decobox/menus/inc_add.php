<?
include("inc_security.php");

//Khai báo biến khi thêm mới
$after_save_data	= getValue("after_save_data", "str", "POST", "add.php");
$add					= "add.php";
$listing				= "listing.php";
$fs_title			= $module_name . " | Thêm mới";
$fs_action			= getURL();
$fs_redirect		= $after_save_data;
$fs_errorMsg		= "";

$type					= getValue("type", "int", "GET", 1);
if($type<1) $type	= 1;
//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$mnu_name			= getValue("mnu_name", "str", "POST", "");
$mnu_link			= getValue("mnu_link", "str", "POST", "");
$mnu_target			= getValue("mnu_target", "str", "POST", "");
$mnu_type			= getValue("mnu_type", "int", "POST", $type);
$mnu_order			= getValue("mnu_order", "dbl", "POST", 1);
$mnu_parent_id		= getValue("mnu_parent_id", "int", "POST", 0);
$mnu_defined		= getValue("mnu_defined", "str", "POST", "");
$mnu_active			= getValue("mnu_active", "int", "POST", 1);

$class_menu			= new menu();
$listAll				= $class_menu->getAllChild("menus", "mnu_id", "mnu_parent_id", 0, "mnu_type = " . $mnu_type . " AND lang_id = " . $lang_id, "mnu_id,mnu_name,mnu_type", "mnu_order ASC,mnu_name ASC", "mnu_has_child", 0);
unset($class_menu);

//Get action variable for add new data
$action				= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){
	
	//Lấy dữ liệu kiểu checkbox
	$mnu_active		= getValue("mnu_active", "int", "POST", 0);
	
	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	$myform->add("mnu_name", "mnu_name", 0, 1, " ", 1, "Bạn chưa nhập tên menu.", 0, "");
	$myform->add("mnu_link", "mnu_link", 0, 1, "", 0, "", 0, "");
	$myform->add("mnu_target", "mnu_target", 0, 1, "", 0, "", 0, "");
	$myform->add("mnu_type", "mnu_type", 1, 1, 1, 1, "Bạn chưa chọn loại menu.", 0, "");
	$myform->add("mnu_order", "mnu_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	$myform->add("mnu_parent_id", "mnu_parent_id", 1, 1, 0, 0, "", 0, "");
	$myform->add("mnu_defined", "mnu_defined", 0, 1, "", 0, "", 0, "");
	$myform->add("mnu_active", "mnu_active", 1, 1, 0, 0, "", 0, "");
	
	//Check form data
	$fs_errorMsg .= $myform->checkdata();
	if($fs_errorMsg == ""){
		
		//Insert to database
		$myform->removeHTML(0);
		$db_insert = new db_execute($myform->generate_insert_SQL());
		unset($db_insert);
		
		//Kiểm tra nếu $mnu_parent_id > 0 thì update trường mnu_has_child cho menu cha
		if($mnu_parent_id > 0){
			$db_update = new db_execute("UPDATE " . $fs_table . " SET mnu_has_child = 1 WHERE mnu_id = " . $mnu_parent_id);
			unset($db_update);
		}
		
		//Redirect after insert complate
		$fs_redirect .= "?type=" . $mnu_type;
		redirect($fs_redirect);
		
	}//End if($fs_errorMsg == "")
	unset($myform);
	
}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
<script language="javascript" src="../js/ajax.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=$listing?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$onChange	= "load_data('load_parent.php?type=' + this.value, 'content_loader')";
$form = new form();
$form->create_form("add", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->text("Tên menu", "mnu_name", "mnu_name", $mnu_name, "Tên menu", 1, 250, "", 255, "", "", "")?>
<?=$form->text("Liên kết", "mnu_link", "mnu_link", $mnu_link, "Liên kết", 0, 250, "", 255, "", "", '<img title="Tạo link cho menu" align="absmiddle" src="' . $fs_imagepath . 'create_link.gif" style="cursor:pointer; margin-left:5px" onClick="creat_link(\'mnu_link\')">')?>
<?=$form->select("Mở ra", "mnu_target", "mnu_target", $arrTarget, $mnu_target, "Mở ra", 0, "", 1, 0, "", "")?>
<?=$form->select("Loại menu", "mnu_type", "mnu_type", $arrType, $mnu_type, "Loại menu", 1, "", 1, 0, 'onChange="' . $onChange . '"', "")?>
<?=$form->text("Thứ tự", "mnu_order", "mnu_order", $mnu_order, "Thứ tự", 2, 50, "", 5, "", "", "")?>
<tr>
	<td class="form_name">Menu cấp trên :</td>
	<td class="form_text">
		<div id="content_loader">
			<select title="Menu cấp trên" id="mnu_parent_id" name="mnu_parent_id" class="form_control">
				<option value="0">--[Menu cấp trên]--</option>
				<?
				for($i=0; $i<count($listAll); $i++){
					$selected = ($mnu_parent_id == $listAll[$i]["mnu_id"]) ? ' selected="selected"' : '';
					echo '<option title="' . htmlspecialbo($listAll[$i]["mnu_name"]) . '" value="' . $listAll[$i]["mnu_id"] . '"' . $selected . '>';
					for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
					echo ' ' . cut_string($listAll[$i]["mnu_name"], 55) . '</option>';
				}
				?>
			</select>
		</div>
	</td>
</tr>
<?=$form->text("Menu defined", "mnu_defined", "mnu_defined", $mnu_defined, "Menu defined", 0, 250, "", 255, "", "", "")?>
<?=$form->checkbox("Kích hoạt", "mnu_active", "mnu_active", 1, $mnu_active, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>