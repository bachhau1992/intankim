<?
include("inc_security.php");

$redirect	= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$redirect	= base64_decode($redirect);

$class_menu	= new menu();
$listAll		= $class_menu->getAllChild("menus", "mnu_id", "mnu_parent_id", 0, 1, "mnu_id,mnu_name,mnu_link,mnu_target,mnu_type,mnu_order,mnu_defined,mnu_active", "mnu_order ASC,mnu_name ASC", "mnu_has_child", 1);
unset($class_menu);

redirect($redirect);
?>