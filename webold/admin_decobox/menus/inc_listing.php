<?
include("inc_security.php");

//Khai báo biến khi hiển thị danh sách
$fs_title		= $module_name . " | Danh sách";
$fs_action		= "listing.php" . getURL(0,0,0,1,"record_id");
$fs_redirect	= "listing.php" . getURL(0,0,0,1,"record_id|r") . "&r=" . random();
$fs_errorMsg	= "";
$fs_alertMsg	= "";

/*****----- Quick Edit -----*****/
$action			= getValue("action", "str", "POST", "");
if($action == "execute"){
	
	//Get $record_id for edit data
	$arr_record_id	= getValue("record_id", "arr", "POST", "");
	if(is_array($arr_record_id)){
		
		//Loop array để update vào database
		for($i=0; $i<count($arr_record_id); $i++){
			
			$fs_errorMsg= "";
			
			//Lấy id của data cần sửa đổi
			$record_id	= intval($arr_record_id[$i]);
			
			//Lấy dữ liệu những trường sửa đổi
			$mnu_name			= getValue("mnu_name" . $record_id, "str", "POST", "");
			$mnu_link			= getValue("mnu_link" . $record_id, "str", "POST", "");
			$mnu_target			= getValue("mnu_target" . $record_id, "str", "POST", "_self");
			$mnu_order			= getValue("mnu_order" . $record_id, "dbl", "POST", 0);
			$mnu_defined		= getValue("mnu_defined" . $record_id, "str", "POST", "");

			//Call class generate_form();
			$myform = new generate_form();
			//Add table insert data
			$myform->addTable($fs_table);
			$myform->add("mnu_name", "mnu_name", 0, 1, " ", 1, "Bạn chưa nhập tên menu.", 0, "");
			$myform->add("mnu_link", "mnu_link", 0, 1, "", 0, "", 0, "");
			$myform->add("mnu_target", "mnu_target", 0, 1, "", 0, "", 0, "");
			$myform->add("mnu_order", "mnu_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
			$myform->add("mnu_defined", "mnu_defined", 0, 1, "", 0, "", 0, "");
		
			//Check form data
			$fs_errorMsg .= $myform->checkdata();
			
			//Kiểm tra nếu có lỗi thì không update
			if($fs_errorMsg == ""){
				
				//Update to database
				$myform->removeHTML(1);
				$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
				unset($db_update);
				
			}//End if($fs_errorMsg == "")
			else{
				$fs_alertMsg .= "- Bạn không thay đổi được menu có ID = " . $record_id . ".\\n";
			}
			
			unset($myform);
			
			if($fs_errorMsg != ""){
				$arr_str		= array("&bull; ", "<br />");
				$arr_rep		= array("- ", "\\n");
				$fs_errorMsg= str_replace($arr_str, $arr_rep, $fs_errorMsg);
				echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
				echo '<script language="javascript">alert("Có những lỗi sau:\\n' . $fs_errorMsg . '")</script>';
			}
	
		}//End for($i=0; $i<count($arr_record_id); $i++)
		
		//Nếu có data nào bị lỗi thì hiển thị thông báo alert message error rồi redirect
		if($fs_alertMsg != ""){
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
			echo '<script language="javascript">alert("Có những lỗi sau:\\n' . $fs_alertMsg . '"); window.location.href="' . $fs_redirect . '"</script>';
			exit();
		}
		//Ngược lại thì redirect luôn
		else redirect($fs_redirect);
	
	}
	
}// End if($action == "execute")
/*****----- End Quick Edit -----*****/

//Search data
$id			= getValue("id");
$type			= getValue("type");
$sqlWhere	= "";
//Tìm theo ID
if($id > 0)			$sqlWhere .= " AND mnu_id = " . $id . " ";
// Tìm theo type
if($type		> 0)	$sqlWhere .= " AND mnu_type = " . $type . " ";

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "mnu_type ASC, mnu_order ASC"; break;
	case 2: $sqlOrderBy = "mnu_type ASC, mnu_order DESC"; break;
	case 3: $sqlOrderBy = "mnu_type ASC, mnu_name ASC"; break;
	case 4: $sqlOrderBy = "mnu_type ASC, mnu_name DESC"; break;
	default:$sqlOrderBy = "mnu_type ASC, mnu_order ASC"; break;
}

$class_menu		= new menu();
$listAll			= $class_menu->getAllChild("menus", "mnu_id", "mnu_parent_id", 0, "lang_id = " . $lang_id . $sqlWhere, "mnu_id,mnu_name,mnu_link,mnu_target,mnu_type,mnu_order,mnu_defined,mnu_active", $sqlOrderBy, "mnu_has_child", 0);
unset($class_menu);
$total_record	= count($listAll);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?>: <font class="count"><?=format_number($total_record)?></font></div>
<div class="content_title" style="float:right"><a title="Thêm mới" href="add.php?type=<?=$type?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>add.gif" />Thêm mới</a></div>
</div>
<div align="center" class="content">
<? //Page break and search data?>
<table width="98%" cellpadding="2" cellspacing="2">
	<tr>
		<td><input title="Refresh menu" type="button" class="form_button" value="Refresh" style="background:url(<?=$fs_imagepath?>button_3.gif) no-repeat" onClick="if(confirm('Bạn có muốn refresh lại tất cả menu không?')) window.location.href='refresh.php?redirect=<?=base64_encode(getURL())?>'" /></td>
		<td align="right">
			<table cellpadding="0" cellspacing="0">
			<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
				<tr>
					<td class="form_search" nowrap="nowrap">
						ID:
						<input title="ID" type="text" class="form_control" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:60px; text-align:right">&nbsp;
						Hiển thị: 
						<select title="Loại menu" name="type" class="form_control">
							<option value="0">--[Loại]--</option>
						<? foreach($arrType as $key => $value){?>
							<option value="<?=$key?>" <? if($key == $type){echo 'selected="selected"';}?>><?=$value?></option>
						<? }?>
						</select>
						<input type="hidden" name="sort" value="<?=$sort?>" />
					</td>
					<td class="form_search" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?=$fs_imagepath?>search.gif" border="0"></td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<? //End page break and search data?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
	<tr class="table_title_3">
		<td>Stt.</td>
		<td>
			<div>Thứ tự | Tên menu</div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
				&nbsp;
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
				&nbsp; &nbsp;
			</div>
		</td>
		<td>Liên kết</td>
		<td>Mở ra<br />cửa sổ</td>
		<td>Menu defined</td>
		<td>Kích<br />hoạt</td>
		<td>Lưu</td>
		<td>Sửa</td>
		<td>Xóa</td>
	</tr>
<?
//Call class form
$form = new form();
$form->class_form_name = "form_name_2";
$form->create_form("quick_edit", $fs_action, "post", "multipart/form-data");
?>
<?
$mnu_module	= 0;
$mnu_type	= 0;
//Đếm số thứ tự
$No = 0;
for($i=0; $i<count($listAll); $i++){
	$No++;
	$preview_link	= $listAll[$i]["mnu_link"];
?>
	<?
	if($mnu_type != $listAll[$i]["mnu_type"]){
		$mnu_type = $listAll[$i]["mnu_type"];
	?>
		<tr class="table_title_2">
			<td colspan="10"><? if(isset($arrType[$listAll[$i]["mnu_type"]])) echo $arrType[$listAll[$i]["mnu_type"]];?></td>
		</tr>
	<?
	}
	?>
	<tr id="tr_<?=$No?>" <?=$fs_change_bg?>>
		<td class="No"><?=$No?></td>
		<td class="text_normal_red" nowrap="nowrap">
			<? for($j=0; $j<$listAll[$i]["level"]; $j++) echo "|--- ";?>
			<input title="Thứ tự" type="text" id="mnu_order_<?=$No?>" name="mnu_order<?=$listAll[$i]["mnu_id"]?>" value="<?=$listAll[$i]["mnu_order"]?>" class="form_control" style="width:20px; text-align:center<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" />
			<input title="Tên menu" type="text" id="mnu_name_<?=$No?>" name="mnu_name<?=$listAll[$i]["mnu_id"]?>" value="<?=htmlspecialbo($listAll[$i]["mnu_name"])?>" class="form_control" style="width:120px<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" />
		</td>
		<td align="center" nowrap="nowrap">
			<input title="Liên kết" type="text" id="mnu_link_<?=$No?>" name="mnu_link<?=$listAll[$i]["mnu_id"]?>" value="<?=htmlspecialbo($listAll[$i]["mnu_link"])?>" class="form_control" style="width:210px; color:#0000FF<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" />
			<img title="Tạo link cho menu" align="absmiddle" src="<?=$fs_imagepath?>create_link.gif" style="cursor:pointer" onClick="creat_link('mnu_link_<?=$No?>')">
			</td>
		<td align="center">
			<select id="mnu_target_<?=$No?>" name="mnu_target<?=$listAll[$i]["mnu_id"]?>" class="form_control">
			<?
			reset($arrTarget);
			foreach($arrTarget as $key => $value){
			?>
				<option value="<?=$key?>"<? if($key == $listAll[$i]["mnu_target"]){echo ' selected="selected"';}?>><?=$value?></option>
			<?
			}
			?>
			</select>
		</td>
		<td align="center"><input title="Menu defined" type="text" id="mnu_defined_<?=$No?>" name="mnu_defined<?=$listAll[$i]["mnu_id"]?>" value="<?=htmlspecialbo($listAll[$i]["mnu_defined"])?>" class="form_control" style="width:180px; color:#FF0000<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" /></td>
		<td align="center"><a href="quickset.php?type=active&record_id=<?=$listAll[$i]["mnu_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["mnu_active"]?>.gif" /></a></td>
		<td align="center"><input title="Lưu dữ liệu" type="image" hspace="5" src="<?=$fs_imagepath?>save.gif" onClick="MM_validateForm('mnu_order_<?=$No?>','','RisNum', 'mnu_name_<?=$No?>','','R'); return document.MM_returnValue" /></td>
		<td align="center"><a title="Sửa dữ liệu" href="edit.php?record_id=<?=$listAll[$i]["mnu_id"]?>&redirect=<?=base64_encode(getURL())?>"><img border="0" hspace="5" src="<?=$fs_imagepath?>edit.gif"></a></td>
		<td align="center"><img title="Xóa dữ liệu" hspace="5" src="<?=$fs_imagepath?>delete.gif" style="cursor:pointer" onClick="if(confirm('Bạn có muốn xóa dữ liệu này không?')){window.location.href='delete.php?record_id=<?=$listAll[$i]["mnu_id"]?>&redirect=<?=base64_encode(getURL())?>'}" /></td>
	</tr>
	<?=$form->hidden("record_id_" . $No, "record_id[]", $listAll[$i]["mnu_id"], "");?>
<?
}// End for($i=0; $i<count($listAll); $i++)
?>
</table>
	<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>