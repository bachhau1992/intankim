<?
include("inc_security.php");
$fs_title = "Quản trị Website";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script type="text/javascript"></script>
</head>
<body>
<style type="text/css">
.bg_left_menu, .bg_left_menu_hover{
	cursor:pointer;
}
.bg_left_menu{
	background:url(../images/bgModule.gif) no-repeat;
}
.bg_left_menu_hover{
	background:url(../images/bgModule_hover.gif) no-repeat;
}
.left_menu{
	background:url(../images/arrow.gif) no-repeat 5px 8px;
	font-size:12px;
	color:#e86402;
	display:block;
	padding:4px 0px 5px 18px;
	text-decoration:none;
}
.left_menu_title, .left_menu_title_current{
	border-top:1px #46c646 solid;
	border-bottom:none;
	color:#333333;
	display:block;
	padding:6px 10px 6px 10px;
	font-size:12px;
	font-weight:bold;
}
.left_menu_title{
	background:url(../images/cong.gif) no-repeat right;
	background-color:#e5f7e5;
}
.left_menu_title_current{
	background:url(../images/tru.gif) no-repeat right;
	background-color:#aee6ae;
	color:#b00000;
}
.left_menu_title:hover, .left_menu_title_current:hover{
	background-color:#aee6ae;
	color:#333333;
}
.left_menu_hidden{
	height:0px;
	overflow:hidden;
}
.left_menu_show{
	height:auto;
}
</style>
<script language="javascript">
function show_left_menu(id){
	ob1 = document.getElementById(id + "_title");
	ob2 = document.getElementById(id + "_link");
	if(ob2.className == "left_menu_hidden"){
		ob1.className	= "left_menu_title_current";
		ob2.className	= "left_menu_show";
	}
	else{
		ob1.className	= "left_menu_title";
		ob2.className = "left_menu_hidden";
	}
}
</script>
<?
/*****-------- Tạo ra các Menu Module lấy từ bảng module --------*****/
$db_module = new db_query("SELECT * FROM modules ORDER BY mod_order ASC, mod_id ASC");
while($module = mysql_fetch_assoc($db_module->result)){
	if(checkAccessModule($module["mod_id"]) == 1 && file_exists("../" . $module["mod_path"] . "/inc_security.php")){
		echo createMenuModule($module["mod_name"], $module["mod_path"], $module["mod_action"], $module["mod_file"]);
	}
}
unset($db_menuModule);
/*****-------- Tạo ra các Menu Module không có trong bảng module --------*****/
if(checkAccessModule(1)	== 1 && file_exists("../admin_user/inc_security.php"))	echo createMenuModule("Admin - User Module", "admin_user", "Add Admin - User|Edit Admin - User", "add.php|listing.php");
if(checkAccessModule(2)	== 1 && file_exists("../configuration/inc_security.php"))echo createMenuModule("Configuration", "configuration", "Configuration Website", "configuration.php");
?>
</body>
</html>