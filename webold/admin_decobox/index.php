<?
session_start();
require_once("../functions/functions.php");

$login_path = "login/login.php";
if(!isset($_SESSION["logged"])){
	redirect($login_path);
}
else{
	if($_SESSION["logged"] != 1){
		redirect($login_path);
	}
}
echo '<!-- Code by: Mr. Tran - Yahoo: txnc2002, boy_infotech -->';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<title>FinalStyle - Admin Control Pannel</title>
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.4)" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.4)" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="javascript" src="js/service.js"></script>
</head>
<frameset rows="60,*" cols="*" frameborder="no" border="0" framespacing="0">
  <frame src="header/header.php" name="topFrame" scrolling="no" noresize="noresize">
  <frameset id="MainFrameSet" rows="*" cols="200,*" framespacing="0" frameborder="no" border="0">
    <frame src="modules/index.php" name="leftFrame" noresize="noresize" scrolling="auto">
    <frame src="modules/message.php" name="workFrame" noresize="noresize" scrolling="auto">
  </frameset>
</frameset>
<noframes><body></body></noframes>
</html>
<div style="display:none">Designed by: <a class="copyright" href="http://www.finalstyle.com" target="_blank">FinalStyle.com</a></div>