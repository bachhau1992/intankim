<?
require_once("../security/security.php");

$module_id	= 10;
$module_name= "Introduce";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table				= "introduces";
$id_field				= "int_id";
$fs_fieldupload		= "int_picture";
$fs_filepath			= "../../introduce_pictures/";
$fs_extension			= "gif,jpg,jpe,jpeg,png";
$fs_filesize			= 500;
$width_small_image	= 120;
$height_small_image	= 120;
$fs_insert_logo		= 0;
//Array variable
$arrTarget				= array ("_blank"	=> "Trang mới",
										"_self"	=> "Hiện hành",
										);
?>