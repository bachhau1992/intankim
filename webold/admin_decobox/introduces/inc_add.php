<?
include("inc_security.php");

//Khai báo biến khi thêm mới
$after_save_data		= getValue("after_save_data", "str", "POST", "add.php");
$add						= "add.php";
$listing					= "listing.php";
$fs_title				= $module_name . " | Thêm mới";
$fs_action				= getURL();
$fs_redirect			= $after_save_data;
$fs_errorMsg			= "";

$type						= getValue("type", "int", "GET", 1);
//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$int_name				= getValue("int_name", "str", "POST", "");
$int_picture_width	= 0;
$int_picture_height	= 0;
$width_larger_image	= 768;
$width_normal_image	= 300;
$width_small_image	= 30;
$height_larger_image	= 407;
$height_normal_image	= 159;
$height_small_image	= 16;
$int_link				= getValue("int_link", "str", "POST", "http://");
$int_target				= getValue("int_target", "str", "POST", "");
$int_order				= getValue("int_order", "dbl", "POST", 1);
$int_strdate			= getValue("int_strdate", "str", "POST", date("d/m/Y"));
$int_strtime			= getValue("int_strtime", "str", "POST", date("H:i:s"));
$int_date				= convertDateTime($int_strdate, $int_strtime);
$int_active				= getValue("int_active", "int", "POST", 1);

//Get action variable for execute
$action					= getValue("action", "str", "POST", "");
//Check $action for insert new data
if($action == "execute"){

	//Lấy dữ liệu kiểu checkbox
	$ban_active			= getValue("int_active", "int", "POST", 0);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	$myform->add("int_name", "int_name", 0, 1, " ", 1, "Bạn chưa nhập tên.", 0, "");
	$myform->add("int_picture_width", "int_picture_width", 1, 1, 0, 0, "", 0, "");
	$myform->add("int_picture_height", "int_picture_height", 1, 1, 0, 0, "", 0, "");
	$myform->add("int_link", "int_link", 0, 1, "", 0, "", 0, "");
	$myform->add("int_target", "int_target", 0, 1, "", 0, "", 0, "");
	$myform->add("int_order", "int_order", 3, 1, 0, 1, "Thứ tự phải lớn hơn hoặc bằng 0.", 0, "");
	$myform->add("int_date", "int_date", 1, 1, 0, 0, "", 0, "");
	$myform->add("int_active", "int_active", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, $fs_insert_logo);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}

	if($fs_errorMsg == ""){

		if($filename != ""){
			//Upload new image
			$$fs_fieldupload = $filename;
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
			$upload->resize_image($fs_filepath, $filename, $width_larger_image, $height_larger_image, "larger_");
			$upload->resize_image($fs_filepath, $filename, $width_normal_image, $height_normal_image, "normal_");
			$upload->resize_image($fs_filepath, $filename, $width_small_image, $height_small_image, "small_");
			list($int_picture_width, $int_picture_height)	= @getimagesize($fs_filepath . $filename);
		}//End if($filename != "")

		//Insert to database
		$myform->removeHTML(0);
		$db_insert = new db_execute($myform->generate_insert_SQL());
		unset($db_insert);

		//Redirect after insert complate
		redirect($fs_redirect);

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=$listing?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("add", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->text("Tên", "int_name", "int_name", $int_name, "Tên", 1, 250, "", 255, "", "", "")?>
<?=$form->getFile("Ảnh", "int_picture", "int_picture", "Ảnh", 0, 32, "", '<br />(Dung lượng tối đa <span style="color:#FF0000">' . $fs_filesize . ' Kb</span>, kích thước <span style="color:#FF0000">720x400</span>)');?>
<?=$form->text("Liên kết", "int_link", "int_link", $int_link, "Liên kết", 0, 250, "", 255, "", "", "")?>
<?=$form->select("Mở ra", "int_target", "int_target", $arrTarget, $int_target, "Mở ra", 0, "", 1, 0, "", "")?>
<?=$form->text("Thứ tự", "int_order", "int_order", $int_order, "Thứ tự", 2, 50, "", 255, "", "", "")?>
<?=$form->text("Ngày tạo", "int_strdate" . $form->ec . "int_strtime", "int_strdate" . $form->ec . "int_strtime", $int_strdate . $form->ec . $int_strtime, "Ngày (dd/mm/yyyy)" . $form->ec . "Giờ (hh:mm:ss)", 0, 70 . $form->ec . 70, $form->ec, 10 . $form->ec . 10, " - ", $form->ec, "&nbsp; <i>(Ví dụ: dd/mm/yyyy - hh:mm:ss)</i>");?>
<?=$form->checkbox("Kích hoạt", "int_active", "int_active", 1, $int_active, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>