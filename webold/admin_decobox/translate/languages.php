<?
include("inc_security.php");

$lg			= getValue("lg");
$db_language	= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $lg);
if(mysql_num_rows($db_language->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$language		= mysql_fetch_assoc($db_language->result);
$db_language->close();
unset($db_language);

$translate_file= "../../languages/translate.txt";
$language_file	= "../../languages/" . $array_lang[$lg][0] . ".php";

$fs_title		= "Translate " . $language["lang_name"];
$fs_action		= getURL();
$fs_redirect	= getURL(0,0,1,1,"r") . "&r=" . random(); 
$fs_errorMsg	= "";

if(!file_exists($language_file)){
	echo "Cannot find language file " . $language["lang_name"] . "!";
	exit();
}
require_once($language_file);

//Get action variable
$action			= getValue("action", "str", "POST", "");
if($action == "execute"){
	
	//read all translate variable
	$handle				= fopen($translate_file, "r");
	$contents			= fread($handle, filesize($translate_file));
	fclose($handle);
	$array_translate	= explode("\n", $contents);
	
	$arrStr	= array();
	$arrRep	= array();
	// Nếu magic_quotes_gpc = Off thì replace ' thành \' để chống phá ngoặc
	if(get_cfg_var("magic_quotes_gpc") != 1){
		$arrStr	= array("'");
		$arrRep	= array("\'");
	}

	//start file content
	$file_content = "<?\n" .
						 "\$lang_display = array(\n";
	for($i=0;$i<count($array_translate);$i++){
		$translate = getValue("translate" . $i, "str", "POST", "");
		$file_content .= "'" . trim($array_translate[$i]) . "' => '" . str_replace($arrStr, $arrRep, $translate) . "',\n";
	}
	$file_content.= ");";
	$file_content.= "\n?>";
	
	//save to text file
	$handle = fopen($language_file, "w");
	fwrite($handle, $file_content);
	fclose($handle);

	redirect($fs_action);
	
}
?>
<?
//read all translate variable
if(filesize($translate_file) > 0){
	$handle				= fopen($translate_file, "r");
	$contents			= fread($handle, filesize($translate_file));
	$array_translate	= explode("\n", $contents);
	fclose($handle);
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?>: <font class="count"><?=format_number(count($array_translate))?></font></div>
</div>
<div align="center" class="content">
<?
//Call class form
$form = new form();
$form->create_form("translate_language", $fs_action, "post", "multipart/form-data");
?>
<table class="table" border="1" bordercolor="#e5e3e6" cellpadding="3" cellspacing="0" width="98%">
<?
for ($i=0; $i<count($array_translate); $i++){
	$translated_text = "";
	if(isset($lang_display[trim($array_translate[$i])])) $translated_text = $lang_display[trim($array_translate[$i])];
?>
	<tr <?=$fs_change_bg?>>
		<td width="5%" class="No"<? if($translated_text == "") echo ' bgcolor="#FFCCCC"';?>><?=$i+1?></td>
		<td class="text" width="95%">
			<div style="margin-bottom:3px; color:#0033FF"><?=$array_translate[$i]?></div>
			<div><input type="text" name="translate<?=$i?>" value="<?=htmlspecialbo($translated_text)?>" class="form_control" style="width:95%" /></div>
		</td>
	</tr>
<?
}
?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
</table>
<?
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>
<?
}// End if(filesize($translate_file) > 0)
?>