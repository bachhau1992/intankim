<?
include("inc_security.php");

$fs_title		= "Thay đổi thông tin cá nhân";
$fs_form			= "edit_admin_profile";
$fs_action		= getURL();
$fs_redirect	= getURL(0,0,1,1,"r") . "&r=" . random(); 
$fs_errorMsg	= "";

//Get $record_id for edit data
$record_id		= getValue("user_id", "int", "SESSION");
//Get data edit
$db_edit_data	= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id);
if(mysql_num_rows($db_edit_data->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
else{
	$edit			= mysql_fetch_assoc($db_edit_data->result);
}

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$adm_name		= getValue("adm_name", "str", "POST", $edit["adm_name"]);
$adm_email		= getValue("adm_email", "str", "POST", $edit["adm_email"]);
$adm_address	= getValue("adm_address", "str", "POST", $edit["adm_address"]);
$adm_phone		= getValue("adm_phone", "str", "POST", $edit["adm_phone"]);
$adm_mobile		= getValue("adm_mobile", "str", "POST", $edit["adm_mobile"]);

//Get action variable for edit data
$action			= getValue("action", "str", "POST", "");
//Update profile
if($action == "update_profile"){
	
	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table
	$myform->addTable($fs_table);
	$myform->add("adm_name", "adm_name", 0, 1, " ", 1, "Bạn chưa nhập họ và tên.", 0, "");
	if($adm_email != $edit["adm_email"]){$myform->add("adm_email", "adm_email", 2, 1, "    ", 1, "Địa chỉ email không hợp lệ.", 1, "Email đã có trong database.");}
	$myform->add("adm_address", "adm_address", 0, 1, " ", 0, "Bạn chưa nhập địa chỉ.", 0, "");
	$myform->add("adm_phone", "adm_phone", 0, 1, " ", 0, "Bạn chưa nhập số điện thoại.", 0, "");
	$myform->add("adm_mobile", "adm_mobile", 0, 1, "", 0, "", 0, "");
	
	$fs_errorMsg .= $myform->checkdata();
	if($fs_errorMsg == ""){
	
		//Update to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL("adm_id", $record_id));
		unset($db_update);
		
		//Redirect after Update complate
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
		echo '<script language="javascript">alert("Thông tin của bạn đã được thay đổi thành công."); window.location.href="' . $fs_redirect . '"</script>';
		exit();
		
	}//End if($fs_errorMsg == "")
	
}//End if($action == "update")

//Lấy các biến khi update password
$fs_errorMsgPass		= "";

//Update Password
if($action == "update_password"){

	$adm_old_password		= getValue("adm_old_password", "str", "POST", "");
	$adm_password			= getValue("adm_password", "str", "POST", "");
	$adm_confirm_password= getValue("adm_confirm_password", "str", "POST", "");

	if(md5($adm_old_password) != $edit["adm_password"]){
		$fs_errorMsgPass .= "&bull; Mật khẩu cũ không chính xác.<br />";
	}
	
	$myform = new generate_form();
	//Add table
	$myform->addTable($fs_table);
	$myform->add("adm_password", "adm_password", 4, 1, "    ", 1, "Mật khẩu phải từ 4 ký tự trở lên.", 0, "");
	
	if($adm_password != $adm_confirm_password){
		$fs_errorMsgPass .= "&bull; Bạn xác nhận sai mật khẩu.<br />";
	}
	
	$fs_errorMsgPass .= $myform->checkdata();
	if($fs_errorMsgPass == ""){
		
		//Update to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL("adm_id", $record_id));
		unset($db_update);
		$_SESSION["password"] = md5($adm_password);
		
		//Redirect after Update complate
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
		echo '<script language="javascript">alert("Mật khẩu của bạn đã được thay đổi thành công."); window.location.href="' . $fs_redirect . '"</script>';
		exit();
		
	}
	
}
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
</div>
<div align="center" class="content">

<div class="text_link_bold">A - Thông tin cá nhân:</div>
<?
$form = new form();
$form->create_form("edit", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->text("Họ và tên", "adm_name", "adm_name", $adm_name, "Họ và tên", 1, 250, "", "255", "", "", "")?>
<?=$form->text("Email", "adm_email", "adm_email", $adm_email, "Email", 3, 250, "", "255", "", "", "")?>
<?=$form->text("Địa chỉ", "adm_address", "adm_address", $adm_address, "Địa chỉ", 0, 250, "", "255", "", "", "")?>
<?=$form->text("Điện thoại", "adm_phone", "adm_phone", $adm_phone, "Điện thoại", 0, 200, "", "255", "", "", "")?>
<?=$form->text("Di động", "adm_mobile", "adm_mobile", $adm_mobile, "Di động", 0, 200, "", "255", "", "", "")?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "update_profile", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>

<hr size="1" width="50%" style="border:1px #CCCCCC solid" />
<div class="text_link_bold">B - Thay đổi mật khẩu:</div>
<?
//Change password
$form = new form();
$form->create_form("edit_password", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsgPass)?>
<?=$form->password("Mật khẩu cũ", "adm_old_password", "adm_old_password", "", "Mật khẩu cũ", 1, 200, "", 255, "", "")?>
<?=$form->password("Mật khẩu mới", "adm_password", "adm_password", "", "Mật khẩu mới", 1, 200, "", 255, "", "")?>
<?=$form->password("Xác nhận mật khẩu", "adm_confirm_password", "adm_confirm_password", "", "Xác nhận mật khẩu", 1, 200, "", 255, "", "")?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "update_password", "")?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>

</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>