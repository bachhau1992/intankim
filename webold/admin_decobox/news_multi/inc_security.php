<?
require_once("../security/security.php");

$module_id		= 7;
$module_name	= "Tin tức";
//Check user login...
checkLogged();
//Check access module...
if(checkAccessModule($module_id) != 1) redirect($fs_denypath);

//Declare prameter when insert data
$fs_table				= "news_multi";
$id_field				= "new_id";
$fs_fieldupload		= "new_picture";
$fs_filepath			= "../../news_pictures/";
$fs_extension			= "gif,jpg,jpe,jpeg,png";
$fs_filesize			= 700;
$width_small_image	= 144;
$height_small_image	= 432;
$fs_insert_logo		= 0;
?>