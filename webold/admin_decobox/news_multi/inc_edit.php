<?
include("inc_security.php");

//Call class menu
$menu						= new menu();
$listAll					= $menu->getAllChild("categories_multi", "cat_id", "cat_parent_id", 0, "cat_type = 'news' AND cat_id IN (" . $fs_category . ") AND lang_id = " . $lang_id, "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($menu);

//Khai báo biến khi thêm mới
$redirect				= getValue("redirect", "str", "GET", base64_encode("listing.php"));
$after_save_data		= getValue("after_save_data", "str", "POST", $redirect);
$add						= base64_encode("add.php");
$listing					= $redirect;
$fs_title				= $module_name . " | Sửa đổi";
$fs_action				= getURL();
$fs_redirect			= $after_save_data;
$fs_redirect			= base64_decode($fs_redirect);
$fs_errorMsg			= "";
$width_small_image	= 300;
$height_small_image	= 300;

//Get data edit
$record_id				= getValue("record_id");
$record_id				= getValue("record_id", "int", "POST", $record_id);
$db_edit					= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $id_field . " = " . $record_id . " AND lang_id = " . $lang_id);
if(mysql_num_rows($db_edit->result) == 0){
	//Redirect if can not find data
	redirect($fs_error);
}
$edit						= mysql_fetch_assoc($db_edit->result);
unset($db_edit);


//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$new_category_id		= getValue("new_category_id", "int", "POST", $edit["new_category_id"]);
$new_title				= getValue("new_title", "str", "POST", $edit["new_title"]);
$new_picture_width	= $edit["new_picture_width"];
$new_picture_height	= $edit["new_picture_height"];
$new_teaser				= getValue("new_teaser", "str", "POST", $edit["new_teaser"]);
$new_meta_title		= getValue("new_meta_title", "str", "POST", $edit["new_meta_title"]);
$new_meta_keyword		= getValue("new_meta_keyword", "str", "POST", $edit["new_meta_keyword"]);
$new_meta_description= getValue("new_meta_description", "str", "POST", $edit["new_meta_description"]);
$new_description		= getValue("new_description", "str", "POST", $edit["new_description"]);
$new_latest				= getValue("new_latest", "int", "POST", 0);
$new_hot					= getValue("new_hot", "int", "POST", $edit["new_hot"]);
$new_strdate			= getValue("new_strdate", "str", "POST", date("d/m/Y", $edit["new_date"]));
$new_strtime			= getValue("new_strtime", "str", "POST", date("H:i:s", $edit["new_date"]));
$new_date				= convertDateTime($new_strdate, $new_strtime);
$new_active				= getValue("new_active", "int", "POST", $edit["new_active"]);

//Get action variable for add new data
$action					= getValue("action", "str", "POST", "");
//Check $action for execute
if($action == "execute"){

	//Lấy dữ liệu kiểu checkbox
	$new_latest			= getValue("new_latest", "int", "POST", 0);
	$new_hot				= getValue("new_hot", "int", "POST", 0);
	$new_active			= getValue("new_active", "int", "POST", 0);

	// Check xem category có tồn tại hay ko
	$db_check			= new db_query("SELECT cat_id FROM categories_multi WHERE cat_type = 'news' AND cat_id = " . $new_category_id);
	if(mysql_num_rows($db_check->result) == 0){
		$fs_errorMsg	.= "&bull; Danh mục bạn chọn không tồn tại.<br />";
	}
	$db_check->close();
	unset($db_check);

	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	$myform = new generate_form();
	//Add table insert data
	$myform->addTable($fs_table);
	if(strpos($fs_category, $new_category_id . ",") === false){
		$fs_errorMsg .= "&bull; Bạn không được phép truy cập category này!<br />";
	}
	$myform->add("new_category_id", "new_category_id", 1, 1, 1, 1, "Bạn chưa chọn danh mục tin.", 0, "");
	$myform->add("new_title", "new_title", 0, 1, " ", 1, "Bạn chưa nhập tiêu đề tin.", 0, "");
	$myform->add("new_picture_width", "new_picture_width", 1, 1, 0, 0, "", 0, "");
	$myform->add("new_picture_height", "new_picture_height", 1, 1, 0, 0, "", 0, "");
	$myform->add("new_teaser", "new_teaser", 0, 1, " ", 1, "Bạn chưa nhập tóm tắt tin.", 0, "");
	$myform->add("new_meta_title", "new_meta_title", 0, 1, "", 0, "", 0, "");
	$myform->add("new_meta_keyword", "new_meta_keyword", 0, 1, "", 0, "", 0, "");
	$myform->add("new_meta_description", "new_meta_description", 0, 1, "", 0, "", 0, "");
	$myform->add("new_description", "new_description", 0, 1, "", 0, "", 0, "");
	$myform->add("new_latest", "new_latest", 1, 1, 0, 0, "", 0, "");
	$myform->add("new_hot", "new_hot", 1, 1, 0, 0, "", 0, "");
	$myform->add("new_date", "new_date", 1, 1, 0, 0, "", 0, "");
	$myform->add("new_active", "new_active", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();

	//Get $filename
	$filename		= "";
	if($fs_errorMsg == ""){
		$upload		= new upload($fs_fieldupload, $fs_filepath, $fs_extension, $fs_filesize, $fs_insert_logo);
		$filename	= $upload->file_name;
		$fs_errorMsg .= $upload->warning_error;
	}
	if($fs_errorMsg == ""){

		if($filename != ""){
			//Xóa ảnh cũ đi
			if($edit[$fs_fieldupload] != "") $upload->delete_file($fs_filepath, $edit[$fs_fieldupload]);
			//Upload new image
			$$fs_fieldupload = $filename;
			$myform->add($fs_fieldupload, $fs_fieldupload, 0, 1, "", 0, "", 0, "");
			$upload->resize_image($fs_filepath, $filename, $width_small_image, $height_small_image, "small_", 1);
			list($new_picture_width, $new_picture_height)	= @getimagesize($fs_filepath . $filename);
		}//End if($filename != "")

		//Insert to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_update);

		//Redirect after insert complate
		redirect($fs_redirect);

	}//End if($fs_errorMsg == "")
	unset($myform);

}//End if($action == "execute")
?>
<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">@import "../css/FSportal.css";</style>
<script language="javascript" src="../../js/jquery.min.js"></script>
<script language="javascript" src="../js/library.js"></script>
</head>
<body>
<div class="bg_title_content">
<div class="content_title" style="float:left"><?=$fs_title?></div>
<div class="content_title" style="float:right"><a title="Quay về danh sách" href="<?=base64_decode($listing)?>"><img align="absmiddle" border="0" hspace="5" src="<?=$fs_imagepath?>list.gif" />Danh sách</a></div>
</div>
<div align="center" class="content">
<?
$form = new form();
$form->create_form("edit", $fs_action, "post", "multipart/form-data");
$form->create_table();
?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($fs_errorMsg)?>
<?=$form->select_db_multi("Danh mục tin", "new_category_id", "new_category_id", $listAll, "cat_id", "cat_name", $new_category_id, "Danh mục tin", 1, "", 1, 0, "", "")?>
<?=$form->text("Tiêu đề tin", "new_title", "new_title", $new_title, "Tiêu đề tin", 1, 250, "", 255, "", "", "")?>
<?=$form->getFile("Ảnh minh họa", "new_picture", "new_picture", "Ảnh minh họa", 0, 32, "", '<br />(Dung lượng tối đa <font color="#FF0000">' . $fs_filesize . ' Kb</font>)')?>
<? //$form->checkbox("Tùy chọn", "new_latest" . $form->ec . "new_hot", "new_latest" . $form->ec . "new_hot", "1" . $form->ec . "1", $new_latest . $form->ec . $new_hot, "Tin mới" . $form->ec . "Tin nổi bật", 0, $form->ec, "")?>
<?=$form->checkbox("Nổi bật", "new_hot", "new_hot", 1, $new_hot, "", 0, "", "")?>
<?=$form->textarea("Tóm tắt", "new_teaser", "new_teaser", $new_teaser, "Tóm tắt tin", 1, 350, 70, "", "", "")?>
<?=$form->text("Meta title tin", "new_meta_title", "new_meta_title", $new_meta_title, " Meta title tin", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta keyword", "new_meta_keyword", "new_meta_keyword", $new_meta_keyword, " Meta keyword", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Meta Description", "new_meta_description", "new_meta_description", $new_meta_description, " Meta Description", 0, 250, "", 250, "", "", "")?>
<?=$form->text("Ngày cập nhật", "new_strdate" . $form->ec . "new_strtime", "new_strdate" . $form->ec . "new_strtime", $new_strdate . $form->ec . $new_strtime, "Ngày (dd/mm/yyyy)" . $form->ec . "Giờ (hh/mm/ss)", "0", "70" . $form->ec . "70", $form->ec, "10" . $form->ec . "10", " - ", $form->ec, "&nbsp; <i>(Ví dụ: dd/mm/yyyy - hh/mm/ss)</i>");?>
<?=$form->checkbox("Kích hoạt", "new_active", "new_active", 1, $new_active, "", 0, "", "")?>
<?=$form->radio("Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", 0, $form->ec, "");?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->close_table();?>
<?=$form->wysiwyg("Mô tả chi tiết", "new_description", $new_description, "../wysiwyg_editor/", "99%", 450)?>
<?=$form->create_table();?>
<?=$form->button("submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "submit" . $form->ec . "reset", "Cập nhật" . $form->ec . "Làm lại", "Cập nhật" . $form->ec . "Làm lại", 'style="background:url(' . $fs_imagepath . 'button_1.gif) no-repeat"' . $form->ec . 'style="background:url(' . $fs_imagepath . 'button_2.gif)"', "");?>
<?=$form->hidden("action", "action", "execute", "");?>
<?
$form->close_table();
$form->close_form();
unset($form);
?>
</div>
</body>
</html>
<script language="javascript">ButtonLeftFrame();</script>