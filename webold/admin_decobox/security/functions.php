<?
function checkLogin($username, $password){
	$username	= replaceMQ($username);
	$password	= replaceMQ($password);
	$adm_id		= 0;
	$db_check	= new db_query("SELECT adm_id 
										 FROM admin_user
										 WHERE adm_loginname = '" . $username . "' AND adm_password = '" . md5($password) . "' AND adm_active = 1");
	if(mysql_num_rows($db_check->result) > 0){
		$check	= mysql_fetch_assoc($db_check->result);
		$adm_id	= $check["adm_id"];
		$db_check->close();
		unset($db_check);
		return $adm_id;
	}
	else{
		$db_check->close();
		unset($db_check);
		return 0;
	}
}

function checkLogged(){
	$fs_denypath = "../deny.html";
	if(!isset($_SESSION["logged"])){
		redirect($fs_denypath);
	}
	else{
		if($_SESSION["logged"] != 1){
			redirect($fs_denypath);
		}
	}
}

function checkLock(){
	$userlogin	= getValue("userlogin", "str", "SESSION", "", 1);
	if($userlogin == "lock") return 1;
	else return 0;
}

function checkAccessModule($module_id){
	
	$userlogin	= getValue("userlogin", "str", "SESSION", "", 1);
	$password	= getValue("password", "str", "SESSION", "", 1);
	$lang_id		= getValue("lang_id", "int", "SESSION", 1);
	
	$return		= 0;
	$db_module	= new db_query("SELECT adm_access_module, adm_isadmin
										 FROM admin_user
										 WHERE adm_loginname = '" . $userlogin . "' AND adm_password = '" . $password . "' AND adm_active = 1");
	
	//Check xem user co ton tai hay khong
	if($row = mysql_fetch_assoc($db_module->result)){
		if($row["adm_isadmin"] == 1) $return = 1;
		elseif(strpos($row["adm_access_module"], "[" . $module_id . "]") !== false) $return = 1;
	}
	unset($db_module);
	
	// Return value
	return $return;
	
}

function checkAccessCategory(){
	
	$userlogin	= getValue("userlogin", "str", "SESSION", "", 1);
	$password	= getValue("password", "str", "SESSION", "", 1);
	$lang_id		= getValue("lang_id", "int", "SESSION", 1);
	
	// Danh sách category đc phép truy cập
	$list_id		= "";
	$db_category= new db_query("SELECT adm_id, adm_isadmin, adm_access_category
										 FROM admin_user
										 WHERE adm_loginname = '" . $userlogin . "' AND adm_password='" . $password . "' AND adm_active = 1");
	
	//Check xem user co ton tai hay khong
	if($row = mysql_fetch_assoc($db_category->result)){
		
		//Neu column adm_isadmin = 1 thi get all category
		if($row["adm_isadmin"] == 1) {
			$db_getall = new db_query("SELECT cat_id FROM categories_multi");
			while($getall = mysql_fetch_assoc($db_getall->result)){
				$list_id .= $getall["cat_id"] . ",";
			}
			unset($db_getall);
		}
		else{
			preg_match_all('/\[(.*?)\]/is', $row["adm_access_category"], $matches);
			for($i=0; $i<count($matches[1]); $i++){
				$list_id	.= $matches[1][$i] . ",";
			}
		}
		
	}
	
	$db_category->close();
	unset($db_category);
		
	$list_id .= 0;
		
	return $list_id;
	
}

function checkDeleteCategory($record_id){
	$return = 1;
	$array_module	= array ("download"	=> array("downloads_multi", "dow_category_id"),
									"gallery"	=> array("galleries_multi", "gal_category_id"),
									"news"		=> array("news_multi", "new_category_id"),
									"product"	=> array("products_multi", "pro_category_id"),
									"static"		=> array("statics_multi", "sta_category_id"),
									);
	$db_module = new db_query("SELECT cat_type FROM categories_multi WHERE cat_id = " . $record_id);
	if($module = mysql_fetch_assoc($db_module->result)){
		if(isset($array_module[$module["cat_type"]])){
			$arrTemp	= $array_module[$module["cat_type"]];
			$db_check= new db_query("SELECT COUNT(*) AS count FROM " . $arrTemp[0] . " WHERE " . $arrTemp[1] . " = " . $record_id);
			$check	= mysql_fetch_assoc($db_check->result);
			if($check["count"] > 0) $return = 0;
			unset($db_check);
		}
	}
	unset($db_module);
	return $return;
}

function createMenuModule($name, $path, $action, $file){
	$imagepath	= '../images/';
	$menuModule = '<a id="' . $path . '_title" class="left_menu_title" href="javascript:show_left_menu(\'' . $path . '\')">' . $name . '</a>';
	$menuModule.= '<div id="' . $path . '_link" class="left_menu_hidden">';
	$arrAction 	= explode('|', $action);
	$arrFile 	= explode('|', $file);
	for($i=0; $i<count($arrAction); $i++){
		$menuModule	.= '<div class="bg_left_menu" onmouseover="this.className=\'bg_left_menu_hover\'" onmouseout="this.className=\'bg_left_menu\'">';
			$menuModule	.= '<div><a class="left_menu" href="../' . $path . '/' . $arrFile[$i] . '" target="workFrame">' . $arrAction[$i] . '</a></div>';
		$menuModule	.= '</div>';
	}
	$menuModule	.= '</div>';
	return $menuModule;
}
?>