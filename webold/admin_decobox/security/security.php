<?
session_start();
require_once("../security/functions.php");
require_once("../../classes/database.php");
require_once("../../classes/menu.php");
require_once("../../classes/form.php");
require_once("../../classes/generate_form.php");
require_once("../../classes/upload.php");
require_once("../../functions/functions.php");
require_once("../../functions/date_functions.php");
require_once("../../functions/file_functions.php");
require_once("../../functions/pagebreak.php");
require_once("../../functions/rewrite_functions.php");

$lang_id				= getValue("lang_id", "int", "SESSION", 1);
$array_lang			= array_language();
// Input language file
require_once("../../languages/" . $array_lang[$lang_id][0] . ".php");

//Set other config variable:
$fs_imagepath		= "../images/";
$fs_error_image	= "../images/error_image.gif";
$fs_no_image		= "../images/no_image.gif";
$fs_preview			= "../../" . lang_path();
$fs_error			= "../error.php";
$fs_denypath		= "../deny.html";
$fs_today			= getdate();
$fs_change_bg		= 'onMouseOver="this.style.background=\'#FFFFAA\'" onMouseOut="this.style.background=\'#FEFEFE\'"';
$fs_category		= checkAccessCategory();
$arrCategoryType	= array ("news"		=> "Tin tức",
									"product"	=> "Sản phẩm",
									// "page"		=> "Landing page",
									"static"		=> "Trang tĩnh",
									"set"			=> "Set sản phẩm",
									);

$arrStaticType	= array ("page"			=> "Trang tĩnh",
								"landing_page"	=> "Landing page",
								);

// Get config variable from database
$db_config = new db_query("SELECT con_root_path, con_mod_rewrite FROM configuration WHERE con_lang_id = " . $lang_id);
if($row = mysql_fetch_assoc($db_config->result)){
	while(list($data_field, $data_value) = each($row)){
		if(!is_int($data_field)){
			$$data_field = $data_value;
		}
	}
}
$db_config->close();
unset($db_config);

@file_put_contents("../../ipstore/" . ip2long($_SERVER["REMOTE_ADDR"]) . ".cfn", " ", FILE_APPEND | LOCK_EX);
?>