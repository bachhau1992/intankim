<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Mitssy.com - Những sản phẩm trang trí nội thất phong cách cho ngôi nhà đẹp. | Mitssy.com</title>
	<meta name="description" content="Mitssy.com mang những sản phẩm trang trí nội thất đậm dấu ấn phong cách nội thất đến ngôi nhà đẹp. Noi that, trang trí, trang tri, do trang tri, đồ trang trí, nội thất, đèn trang trí, kệ treo tường" />
	<meta name="keywords" content="mitssy, nội thất, noi that, trang trí nội thất, trang tri noi that, do trang tri, noi that mitssy, trang tri, trang tri nha,phong cách nhà đẹp, nội thất bếp, nội thất đep, nội thất phòng ngủ, nội thất phòng khách, phòng khách, phòng ngủ, phòng bếp, phòng bé, nhà đẹp,nha dep, đèn trang trí, kệ treo tường." />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="skin/frontend/rwdcs/default/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="skin/frontend/rwdcs/default/favicon.ico" type="image/x-icon" />

	<meta property="og:site_name" content="Ahometo" />
	<meta property="og:title" content="Mitssy.com - Những sản phẩm trang trí nội thất phong cách cho ngôi nhà đẹp. | Mitssy.com" />
	<meta property="og:type" content="website" />

	<meta property="og:image" content="skin/frontend/rwdcs/default/images/logo_facebook2.png" />

	<meta property="og:locale" content="vi_VN" />
	<meta property="og:description" content="Mitssy.com mang những sản phẩm trang trí nội thất đậm dấu ấn phong cách nội thất đến ngôi nhà đẹp. Noi that, trang trí, trang tri, do trang tri, đồ trang trí, nội thất, đèn trang trí, kệ treo tường" />

	<script>
		var BASE_URL="index.html";
	</script>

	<link href="skin/frontend/rwdcs/default/fonts/css/font-awesome.css" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="media/css/e17d2387042b630af6d7d4d38dc85ad7_1500029040-ssl.css" media="all" />
	<script type="text/javascript" src="media/js/bbfb99c6dc82e79a0bffcfc3aa5c9e35_1500029121.js"></script>
	<link rel="stylesheet" type="text/css" href="media/css/abc8cc4024f568186d34d332b34ebdea_1500029029-ssl.css" media="all" />

</head>
<body class=" cms-index-index cms-home">
	<div class="loading">
		<div class="thecube">
			<div class="cube c1"></div>
			<div class="cube c2"></div>
			<div class="cube c4"></div>
			<div class="cube c3"></div>
		</div>
	</div>
	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<div class="head_nav">
					<div class="promotion_bar">
						<div class="inner">
							<div class="promotion_text">
								<p style="margin: 0px;"><a style="color: #fff;" href="uu-dai-mua-le-hoi-2017.html"><b>ƯU Đ&Atilde;I L&Ecirc;N ĐẾN 30%</b>&nbsp;TRANG HO&Agrave;NG NH&Agrave; CỬA Đ&Oacute;N TẾT</a></p>            </div>
							</div>
						</div>
						<div class="top_bar">
							<div class="inner">
								<div class="ham_menu">
									<a href="javascript:void(0)">
										<i class="fa fa-2x fa-bars"></i>
									</a>
								</div>
								<div class="main_logo">
									<a href="index.html"><img src="media/newheader/mitssy_logo.png"  /></a>
								</div>
								<div class="tool_search">

									<form id="search_mini_form" action="https://www.mitssy.com/catalogsearch/result/" method="get" style='position:relative'>
										<div class="search_form_mini">
											<input id="" autocomplete="off" type="text" name="q" value="" class="input-text required-entry form-control" maxlength="128" placeholder="Bạn cần tìm gì ?" />
											<a id="submit_search" href="#" class="" >
												<span id='search_text'>TÌM</span>
												<span id='search_loading' style='font-size:15px'></span>
											</a>
											<a id="cancel_search" href="#" class="" >HUỶ</a>
										</div>
									</form>
									<div class="search-suggestion-wrapper sugges_search clearfix" id="SuggestSearch">
										<div class="search-suggestion clearfix">
											<ul class="suggestion-list with-image nolist "></ul>
										</div>
									</div>
									<div class="phone_text">
										<i class="fa fa-fw fa-phone"></i>(028) 668 46 460
									</div>
								</div>
								<div class="task_bar">
									<ul>
										<li>
											<a href="javascript:void(0)" id="ah_search"><i class="fa fa-fw fa-search"></i> </a>
										</li>
										<li>
											<div id="menu_account">
												<i class="fa fa-fw fa-user"></i>
												<div class="sub_menu_account">
													<div class="button_header">
														<a href="#form_popup_login" class="button button_quick_login">Đăng nhập</a>
														<span class="or_create"> hoặc <a href="customer/account/create/index.html">Tạo tài khoản mới</a></span>
													</div>
													<ul>
														<li><a href="customer/account/index.html">Thông tin tài khoản</a></li>
														<li><a href="sales/order/history/index.html">Quản lý đơn hàng</a></li>
														<li><a href="wishlist/index.html">Danh sách yêu thích</a></li>
													</ul>
													<div class="faq_support">Tư vấn và hỗ trợ: (028) 668 46 460</div>
												</div>
											</div>
											<div style="display:none">

												<div id="form_popup_login">
													<div class="logo_popup">
														<a href="index.html"><img src="media/newheader/mitssy_logo.png" alt="" /></a>
													</div><!--end .logo_popup-->
													<div class="body_popup">
														<form action="https://www.mitssy.com/mytheme/account/checklogin" data-redirect="customer/account/login/index.html"><input name="form_key" type="hidden" value="BEFjtIBXKHYHaBug" />
															<table width="100%">
																<tr>
																	<td width="140">Tài khoản</td>
																	<td><input placeholder="Email của bạn" class="input" type="text" name="txtusername" /></td>
																</tr>

																<tr>
																	<td width="140">Mật khẩu</td>
																	<td><input type="password" name="txtpassword" class="input" /></td>
																</tr>
																<tr class="noti_login">
																	<td width="140"></td>
																	<td>
																		<span class="login_result"></span>
																	</td>
																</tr>
																<tr>
																	<td width="140"></td>
																	<td>
																		<input type="checkbox" name="remember" /> Tự động đăng nhập cho lần sau
																	</td>
																</tr>
																<tr>
																	<td colspan="2">
																		<a href="#" rel="facebook-connect"  onclick="showLESocialPopup('https://graph.facebook.com/oauth/authorize?client_id=1475149059412569&amp;redirect_uri=https%3A%2F%2Fwww.mitssy.com%2Fle_sociallogin%2Ffacebook%2Fconnect%2F&amp;state=be156f21afc9265a5069938c1afd3794&amp;scope=email%2Cuser_birthday&amp;display=popup', 500, 270);" class="button_facebook">Đăng nhập bằng facebook</a>
																		<button href="#" class="button button_popup_login">Đăng nhập</button>
																	</td>
																</tr>

															</table>
														</form>
													</div><!--end .body_popup-->
													<div class="footer_popup">
														<i>* Quên mật khẩu ? Vui lòng <a href="customer/account/forgotpassword/index.html"> ấn vào đây</a></i>
													</div>

												</div><!--end #form_popup_login-->

											</div>
											<style>
												.fancybox-opened .fancybox-skin{ padding:0 !important}
											</style>

										</li>
										<li>
											<a href="javascript:void(0)"><i class="fa fa-fw fa-heart-o"></i> </a>
										</li>
									</ul>
								</div>
								<div class="cart_bar">
									<div class="cart_bar_item">


										<div class="cart">
											<div class="top-cart">
												<div class="top-cart1" id="hov_sp_cart">
													<p>
														<a href="checkout/cart.html">
															<i class="fa fa-shopping-cart"></i> <br/>
															<span class="gio_hang">Giỏ hàng</span>
														</a>
													</p>
													<div class="sp-cart"></div>
												</div>

												<div class="top-cart2" style="display: none">


													<div id="menu_account">
														<i class="fa fa-fw fa-user"></i>
														<div class="sub_menu_account">
															<div class="button_header">
																<a href="#form_popup_login" class="button button_quick_login">Đăng nhập</a>
																<span class="or_create"> hoặc <a href="customer/account/create/index.html">Tạo tài khoản mới</a></span>
															</div>
															<ul>
																<li><a href="customer/account/index.html">Thông tin tài khoản</a></li>
																<li><a href="sales/order/history/index.html">Quản lý đơn hàng</a></li>
																<li><a href="wishlist/index.html">Danh sách yêu thích</a></li>
															</ul>
															<div class="faq_support">Tư vấn và hỗ trợ: (028) 668 46 460</div>
														</div>
													</div>
													<div style="display:none">

														<div id="form_popup_login">
															<div class="logo_popup">
																<a href="index.html"><img src="media/newheader/mitssy_logo.png" alt="" /></a>
															</div><!--end .logo_popup-->

															<div class="body_popup">
																<form action="https://www.mitssy.com/mytheme/account/checklogin" data-redirect="customer/account/login/index.html"><input name="form_key" type="hidden" value="BEFjtIBXKHYHaBug" />
																	<table width="100%">
																		<tr>
																			<td width="140">Tài khoản</td>
																			<td><input placeholder="Email của bạn" class="input" type="text" name="txtusername" /></td>
																		</tr>

																		<tr>
																			<td width="140">Mật khẩu</td>
																			<td><input type="password" name="txtpassword" class="input" /></td>
																		</tr>
																		<tr class="noti_login">
																			<td width="140"></td>
																			<td>
																				<span class="login_result"></span>
																			</td>
																		</tr>
																		<tr>
																			<td width="140"></td>
																			<td>
																				<input type="checkbox" name="remember" /> Tự động đăng nhập cho lần sau
																			</td>
																		</tr>
																		<tr>
																			<td colspan="2">
																				<a href="#" rel="facebook-connect"  onclick="showLESocialPopup('https://graph.facebook.com/oauth/authorize?client_id=1475149059412569&amp;redirect_uri=https%3A%2F%2Fwww.mitssy.com%2Fle_sociallogin%2Ffacebook%2Fconnect%2F&amp;state=be156f21afc9265a5069938c1afd3794&amp;scope=email%2Cuser_birthday&amp;display=popup', 500, 270);" class="button_facebook">Đăng nhập bằng facebook</a>
																				<button href="#" class="button button_popup_login">Đăng nhập</button>
																			</td>
																		</tr>

																	</table>
																</form>
															</div><!--end .body_popup-->
															<div class="footer_popup">
																<i>* Quên mật khẩu ? Vui lòng <a href="customer/account/forgotpassword/index.html"> ấn vào đây</a></i>
															</div>

														</div><!--end #form_popup_login-->

													</div>
													<style>
														.fancybox-opened .fancybox-skin{ padding:0 !important}
													</style>


												</div>
											</div>
											<div class="bot-cart" style="display: none">


												<div class="bot-cart1">
													<p class="noact"><span class="price">0 đ</span></p> <!--add class 'act' for active-->
												</div>
												<div class="bot-cart2">
													<p class="noact"><a href="checkout/cart/index.html">thanh toán</a></p> <!--add class 'act' for active-->
												</div>

											</div>
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="menu_bar">
							<div class="inner">
								<div class="main_menu">
									<link media="all" href="skin/frontend/rwdcs/default/sm/megamenu/css/horizontal/black/megamenu.css" type="text/css" rel="stylesheet">
									<div class="sm_megamenu_wrapper_horizontal_menu sambar" id="sm_megamenu_menu596b74cb48f0e" data-sam="573373921500214475">
										<div class="sambar-inner">
											<a class="btn-sambar" data-sapi="collapse" href="#sm_megamenu_menu596b74cb48f0e">
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</a>
											<div class="mobi_menu">
												<span>DANH MỤC</span>
												<a id="close_menu" href="javascript:void(0)"><i class="fa fa-2x fa-close"></i></a>
											</div>
											<ul class="sm-megamenu-hover sm_megamenu_menu sm_megamenu_menu_black" data-jsapi="on">
												<li class="menu_57 other-toggle sm_megamenu_lv1 sm_megamenu_drop parent">
													<a class="drop_on_mobile  sm_megamenu_head sm_megamenu_drop " href="set-noi-that.html" id="sm_megamenu_57">Ý tưởng nội thất</a>
													<div class="sm_megamenu_dropdown_1column"><div class="sm_megamenu_content"><div class="drop_menu">    <div class="col_drop_menu"> <div class="drop_menu1">
														<ul>
															<li><a title="Phòng Khách" href="set-phong-khach.html">Phòng Khách</a></li>
														</ul>
													</div>

												</div>

											</div>
										</div>
									</div>

									<div class="sm-megamenu-child sm_megamenu_dropdown_2columns  ">
										<div class="sm_megamenu_col_2 lv_1 sm_megamenu_firstcolumn sm_megamenu_id57">
											<div class="sm_megamenu_col_6 lv_2 sm_megamenu_firstcolumn sm_megamenu_id105">
												<div class="sm_megamenu_head_item ">
													<div class="sm_megamenu_title">
														<a class="sm_megamenu_nodrop " href="set-phong-ngu.html"  >
															<span class="sm_megamenu_title_lv2">Phòng Ngủ</span>
														</a>
													</div>
												</div>
											</div>
											<div class="sm_megamenu_col_6 lv_2 sm_megamenu_firstcolumn  sm_megamenu_id107   ">
												<div class="sm_megamenu_head_item ">
													<div class="sm_megamenu_title">
														<a class="sm_megamenu_nodrop" href="set-phong-khach.html">
															<span class="sm_megamenu_title_lv2">Phòng Khách</span>
														</a>
													</div>
												</div>
											</div>
											<div class="sm_megamenu_col_6 lv_2 sm_megamenu_firstcolumn sm_megamenu_id106">
												<div class="sm_megamenu_head_item ">
													<div class="sm_megamenu_title">
														<a class="sm_megamenu_nodrop" href="set-nha-bep.html">
															<span class="sm_megamenu_title_lv2">Phòng Bếp</span>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="menu_29 other-toggle sm_megamenu_lv1 sm_megamenu_drop parent">
									<a class="drop_on_mobile  sm_megamenu_head sm_megamenu_drop " href="javascript:void(0)" id="sm_megamenu_29">Sản phẩm</a>

									<div class="sm-megamenu-child sm_megamenu_dropdown_6columns  ">
										<div class="sm_megamenu_col_6 lv_1 sm_megamenu_firstcolumn  sm_megamenu_id29   "><div class="sm_megamenu_col_1 lv_2 sm_megamenu_firstcolumn  sm_megamenu_id30   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="phong-khach.html"  ><span class="sm_megamenu_title_lv2">Phòng khách</span></a></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id67   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ghe-banh-va-don.html"  ><span class="sm_megamenu_title_lv3">Ghế Bành và Đôn</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id35   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="sofa.html"  ><span class="sm_megamenu_title_lv3">Sofa</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id36   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ban-sofa.html"  ><span class="sm_megamenu_title_lv3">Bàn</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id38   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="tu-ke-phong-khach.html"  ><span class="sm_megamenu_title_lv3">Tủ kệ</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id70   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="tu-giay.html"  ><span class="sm_megamenu_title_lv3">Tủ giày</span></a></div></div></div></div><div class="sm_megamenu_col_1 lv_2  sm_megamenu_id32   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="phong-n-b-p.html"  ><span class="sm_megamenu_title_lv2">Phòng ăn - bếp</span></a></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id44   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="bo-ban-an.html"  ><span class="sm_megamenu_title_lv3">Bộ bàn ăn</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id71   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ban-an.html"  ><span class="sm_megamenu_title_lv3">Bàn Ăn</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id64   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ghe-ban-an.html"  ><span class="sm_megamenu_title_lv3">Ghế bàn ăn</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id45   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="tu-ke.html"  ><span class="sm_megamenu_title_lv3">Tủ kệ</span></a></div></div></div></div><div class="sm_megamenu_col_1 lv_2  sm_megamenu_id31   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="phong-ngu.html"  ><span class="sm_megamenu_title_lv2">Phòng ngủ</span></a></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id40   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="giuong.html"  ><span class="sm_megamenu_title_lv3">Giường</span></a></div></div></div><div class="sm_megamenu_col_1 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id72   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ban-trang-diem.html"  ><span class="sm_megamenu_title_lv3">Bàn Trang Điểm</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id41   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="phong-ngu-tu-ke.html"  ><span class="sm_megamenu_title_lv3">Tủ kệ</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id42   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="guong.html"  ><span class="sm_megamenu_title_lv3">Gương</span></a></div></div></div></div><div class="sm_megamenu_col_1 lv_2  sm_megamenu_id33   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="phong-lam-viec.html"  ><span class="sm_megamenu_title_lv2">Phòng làm việc</span></a></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id47   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="tu-ke-121.html"  ><span class="sm_megamenu_title_lv3">Tủ kệ</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id48   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ban-ghe.html"  ><span class="sm_megamenu_title_lv3">Bàn ghế</span></a></div></div></div><div class="sm_megamenu_col_1 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id73   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ke-sach.html"  ><span class="sm_megamenu_title_lv3">Kệ Sách</span></a></div></div></div></div><div class="sm_megamenu_col_1 lv_2  sm_megamenu_id81   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="en.html"  ><span class="sm_megamenu_title_lv2">Đèn</span></a></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id82   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="en-ban.html"  ><span class="sm_megamenu_title_lv3">Đèn bàn</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id83   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="den-san.html"  ><span class="sm_megamenu_title_lv3">Đèn sàn</span></a></div></div></div></div><div class="sm_megamenu_col_1 lv_2  sm_megamenu_id74 sm_megamenu_right  "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="do-trang-tri-phong-khach.html"  ><span class="sm_megamenu_title_lv2">Đồ Trang Trí</span></a></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id75   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="tham-sofa.html"  ><span class="sm_megamenu_title_lv3">Thảm Sofa</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id76   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="tranh-treo-tuong.html"  ><span class="sm_megamenu_title_lv3">Tranh Treo Tường</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id77   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="goi-sofa.html"  ><span class="sm_megamenu_title_lv3">Gối Trang Trí</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id78   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="ke-trang-tri-phong-khach.html"  ><span class="sm_megamenu_title_lv3">Kệ Trang Trí</span></a></div></div></div><div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn  sm_megamenu_id79   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="binh-trang-tri-phong-khach.html"  ><span class="sm_megamenu_title_lv3">Bình Trang Trí</span></a></div></div></div></div></div>                            </div>
									</li>
									<li class="menu_109 other-toggle 						sm_megamenu_lv1 sm_megamenu_drop parent   ">

										<a class="drop_on_mobile  sm_megamenu_head sm_megamenu_drop "
										href="javascript:void(0)"                            id="sm_megamenu_109">

										Bộ Sưu Tập

									</a>

									<div class="sm-megamenu-child sm_megamenu_dropdown_6columns  ">
										<div class="sm_megamenu_col_6 lv_1 sm_megamenu_firstcolumn  sm_megamenu_id109   "><div class="sm_megamenu_col_6 lv_2  sm_megamenu_id110   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="the-new-wave-collection.html"  ><span class="sm_megamenu_title_lv2">BST The New Wave</span></a></div></div></div><div class="sm_megamenu_col_6 lv_2 sm_megamenu_firstcolumn  sm_megamenu_id111   "><div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="bo-suu-tap-riccardo.html"  ><span class="sm_megamenu_title_lv2">BST Riccardo</span></a></div></div></div></div>                            </div>
									</li>
									<li class="menu_80 other-toggle 						sm_megamenu_lv1 sm_megamenu_drop parent   ">

										<a class="drop_on_mobile  sm_megamenu_head sm_megamenu_drop "
										href="hang-moi.html"                            id="sm_megamenu_80">

										Hàng Mới

									</a>
									<div class="sm_megamenu_dropdown_1column"><div class="sm_megamenu_content"><div class="drop_menu">

									</div> </div> </div>

									<li class="menu_65 other-toggle 						sm_megamenu_lv1 sm_megamenu_drop parent   ">

										<a class="drop_on_mobile  sm_megamenu_head sm_megamenu_drop "
										href="javascript:void(0)"                            id="sm_megamenu_65">

										Nhà bếp

									</a>

									<div class="sm-megamenu-child sm_megamenu_dropdown_2columns  ">
										<div class="sm_megamenu_col_2 lv_1 sm_megamenu_firstcolumn  sm_megamenu_id65   ">
											<div class="sm_megamenu_col_6 lv_2 sm_megamenu_firstcolumn  sm_megamenu_id68   ">
												<div class="sm_megamenu_head_item ">	<div class="sm_megamenu_title"><a class="sm_megamenu_nodrop " href="nha-bep.html"  ><span class="sm_megamenu_title_lv2">Bếp của MITSSY</span></a></div></div></div></div>                            </div>
											</li>
											<li class="menu_54 other-toggle sm_megamenu_lv1 sm_megamenu_nodrop">

												<a class="  sm_megamenu_head sm_megamenu_nodrop " href="quy-trinh-tu-van9321.html?utm_source=mitssy&amp;utm_medium=menu&amp;utm_campaign=qttv" id="sm_megamenu_54">
													Quy trình tư vấn

												</a>

												<li class="menu_59 other-toggle sm_megamenu_lv1 sm_megamenu_nodrop">
													<a class="sm_megamenu_head sm_megamenu_nodrop " href="http://www.ahometo.com/cong-trinh" id="sm_megamenu_59">

														Dự án tiêu biểu

													</a>

													<li class="menu_63 other-toggle 						sm_megamenu_lv1 sm_megamenu_nodrop   ">

														<a class="  sm_megamenu_head sm_megamenu_nodrop "
														href="ve-mitssy.html"                            id="sm_megamenu_63">

														MITSSY

													</a>

												</ul>
											</div>
										</div>
										<!--End Module-->


									</div>
								</div>
							</div>
						</div>
						<div class="main-container col1-layout">
							<div class="one-con-wrapper">
								<div class="std"><div><div class="container-fluid">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">
												<div class="header home text-center slideshow-container">
													<ul class="slideshow" style="border:none;">
														<li>
															<a href="cong-trinh.html" target="_blank">
																<img class="img-responsive" src="media/bannerslider/l/u/luxcity.jpg" alt="" />
															</a>
														</li>
														<li>
															<a href="sofa.html" target="_self">
																<img class="img-responsive" src="media/bannerslider/s/u/summer-breeze-homepage-tubby-resized.jpg" alt="" />
															</a>
														</li>
														<li>
															<a href="giuong.html" target="_self">
																<img class="img-responsive" src="media/bannerslider/b/a/banner-website-gi_ng-tapered-t_ng-drap-01-resized.jpg" alt="" />
															</a>
														</li>
														<li>
															<a href="bo-ban-an.html" target="_self">
																<img class="img-responsive" src="media/bannerslider/b/_/b_n-_n-emily-banner-homepage-resized.jpg" alt="" />
															</a>
														</li>
														<li>
															<a href="bo-suu-tap-riccardo.html" target="_blank">
																<img class="img-responsive" src="media/bannerslider/r/i/riccardo_bannerweb-01-resize.jpg" alt="" />
															</a>
														</li>
													</ul>
													<i class="fa fa-angle-left slideshow-prev"></i>
													<i class="fa fa-angle-right slideshow-next"></i>
													<div class="slideshow-pager">

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>


							</div>
							<div>
								<section class="furniture_set_home none-pad">
									<section id="set" class="set-list alt">
										<div class="gray_line_bg">
											<div>Set nội thất tiêu biểu</div>
										</div>
										<div class="container">
											<div class="row">
												<h3 class="blue_title">DO CÁC HOME STYLIST CHUYÊN NGHIỆP PHỐI HỢP</h3>
												<div class="set-container">
													<div class="set-item col-md-4 col-sm-6">
														<div class="item-wrapper ">
															<div class="product-inner">
																<div class="image">
																	<a href="set-phong-khach-norway-9175.html" title="Phòng Khách Norway" class="product-image-set">
																		<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/600x360/9df78eab33525d08d6e5fb8d27136e95/n/o/norway.jpg" alt="Phòng Khách Norway">
																	</a>
																</div><!--end .image-->
																<div class="product-info">
																	<h4 class="product-name truncate">
																		<a href="set-phong-khach-norway-9175.html" title="Phòng Khách Norway">Phòng Khách Norway</a>
																	</h4>

																	<div class="price-wrapper">
																		<p class="price_km">
																			<span class="price">&nbsp;</span>
																		</p>
																	</div>

																</div>
															</div>
														</div>
													</div>
													<div class="set-item col-md-4 col-sm-6">
														<div class="item-wrapper ">
															<div class="product-inner">
																<div class="image">
																	<a href="set-phong-ngu-bristol.html" title="Phòng Ngủ Bristol Lớn" class="product-image-set">
																		<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/600x360/9df78eab33525d08d6e5fb8d27136e95/s/e/set-bristol.jpg" alt="Phòng Ngủ Bristol Lớn">
																	</a>
																</div><!--end .image-->
																<div class="product-info">
																	<h4 class="product-name truncate">
																		<a href="set-phong-ngu-bristol.html" title="Phòng Ngủ Bristol Lớn">Phòng Ngủ Bristol Lớn</a>
																	</h4>

																	<div class="price-wrapper">
																		<p class="price_km">
																			<span class="price">&nbsp;</span>
																		</p>
																	</div>

																</div>
															</div>
														</div>
													</div>
													<div class="set-item col-md-4 col-sm-6">
														<div class="item-wrapper ">
															<div class="product-inner">
																<div class="image">
																	<a href="set-phong-ngu-rachel-9322.html" title="Phòng Ngủ Rachel" class="product-image-set">
																		<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/600x360/9df78eab33525d08d6e5fb8d27136e95/m/i/mitssy_bed_the_art_rachel.jpg" alt="Phòng Ngủ Rachel">
																	</a>
																</div><!--end .image-->
																<div class="product-info">
																	<h4 class="product-name truncate">
																		<a href="set-phong-ngu-rachel-9322.html" title="Phòng Ngủ Rachel">Phòng Ngủ Rachel</a>
																	</h4>

																	<div class="price-wrapper">
																		<p class="price_km">
																			<span class="price">&nbsp;</span>
																		</p>
																	</div>

																</div>
															</div>
														</div>
													</div>
													<div class="set-item col-md-4 col-sm-6">
														<div class="item-wrapper ">
															<div class="product-inner">
																<div class="image">
																	<a href="set-phong-khach-norwich-9323.html" title="Phòng Khách Norwich" class="product-image-set">
																		<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/600x360/9df78eab33525d08d6e5fb8d27136e95/n/o/norwich---view-1.jpg" alt="Phòng Khách Norwich">
																	</a>
																</div><!--end .image-->
																<div class="product-info">
																	<h4 class="product-name truncate">
																		<a href="set-phong-khach-norwich-9323.html" title="Phòng Khách Norwich">Phòng Khách Norwich</a>
																	</h4>

																	<div class="price-wrapper">
																		<p class="price_km">
																			<span class="price">&nbsp;</span>
																		</p>
																	</div>

																</div>
															</div>
														</div>
													</div>
													<div class="col-md-8 col-sm-12">
														<a href="set-phong-khach.html">
															<img src="media/xem-them-01.jpg">
														</a>
													</div>
												</div>
											</div>
										</div>
									</section>
									<section id="related_product">
										<div class="gray_line_bg">
											<div>Sản phẩm nội thất tiêu biểu</div>
										</div>
										<div class="container no_border_bottom">
											<div class="row">
												<h3 class="blue_title">LỰA CHỌN TỪ MITSSY</h3>
												<div class="col-sm-12">
													<div class="related-product">

														<div class="flexslider">
															<ul class="slides">
																<li class="item">
																	<div class="item-wrapper ">
																		<div class="product-inner">
																			<div class="image">
																				<a href="sofa-2-cho-richie-mau-xam.html" title="Sofa 2 Chỗ Richie (màu xám)" class="product-image">
																					<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/m/i/mitssy__sofa_richie_xam_2_cho.jpg" alt="Sofa 2 Chỗ Richie (màu xám)">
																				</a>
																			</div><!--end .image-->
																			<div class="product-info" style="min-height: 41px;">
																				<h2 class="product-name truncate">
																					<a href="sofa-2-cho-richie-mau-xam.html" title="Sofa 2 Chỗ Richie (màu xám)">Sofa 2 Chỗ Richie (màu xám)</a>
																				</h2>
																				<div class="price-wrapper">
																					<p class="price_km"><span class="price">7.590.000 đ</span></p>
																				</div>
																				<div class="actions-ahometo" style="display: none">
																					<a onclick="setLocation('sofa-2-cho-richie-mau-xam.html')" href="#">
																						<div class="button">
																							<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																						</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</li>
																	<li class="item">
																		<div class="item-wrapper ">
																			<div class="product-inner">
																				<div class="image">
																					<a href="sofa-goc-peggy-nh-mau-xam-9359.html" title="Sofa Góc Peggy (nhỏ)" class="product-image">
																						<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/m/i/mitssy_sofa_peggy_goc_nho.jpg" alt="Sofa Góc Peggy (nhỏ)">
																					</a>
																				</div><!--end .image-->
																				<div class="product-info" style="min-height: 41px;">
																					<h2 class="product-name truncate">
																						<a href="sofa-goc-peggy-nh-mau-xam-9359.html" title="Sofa Góc Peggy (nhỏ)">Sofa Góc Peggy (nhỏ)</a>
																					</h2>
																					<div class="price-wrapper">
																						<p class="price_km"><span class="price">16.230.000 đ</span></p>
																					</div>
																					<div class="actions-ahometo" style="display: none">
																						<a onclick="setLocation('sofa-goc-peggy-nh-mau-xam-9359.html')" href="#">
																							<div class="button">
																								<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																							</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</li>
																		<li class="item">
																			<div class="item-wrapper ">
																				<div class="product-inner">
																					<div class="image">
																						<a href="ban-sofa-ashy.html" title="Bộ Bàn Ashy - Oak" class="product-image">
																							<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/a/s/ashy-oak.jpg" alt="Bộ Bàn Ashy - Oak">
																						</a>
																					</div><!--end .image-->
																					<div class="product-info" style="min-height: 41px;">
																						<h2 class="product-name truncate">
																							<a href="ban-sofa-ashy.html" title="Bộ Bàn Ashy - Oak">Bộ Bàn Ashy - Oak</a>
																						</h2>
																						<div class="price-wrapper">
																							<p class="price_km"><span class="price">4.575.000 đ</span></p>
																						</div>
																						<div class="actions-ahometo" style="display: none">
																							<a onclick="setLocation('ban-sofa-ashy.html')" href="#">
																								<div class="button">
																									<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																								</a>
																							</div>
																						</div>
																					</div>
																				</div>
																			</li>
																			<li class="item">
																				<div class="item-wrapper ">
																					<div class="product-inner">
																						<div class="image">
																							<a href="giuong-tapered.html" title="Giường Tapered" class="product-image">
																								<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/a/h/aht-g0004_1.jpg" alt="Giường Tapered">
																							</a>
																						</div><!--end .image-->
																						<div class="product-info" style="min-height: 41px;">
																							<h2 class="product-name truncate">
																								<a href="giuong-tapered.html" title="Giường Tapered">Giường Tapered</a>
																							</h2>
																							<div class="price-wrapper">
																								<p class="price_km"><span class="price">9.940.000 đ</span></p>
																							</div>
																							<div class="actions-ahometo" style="display: none">
																								<a onclick="setLocation('giuong-tapered.html')" href="#">
																									<div class="button">
																										<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																									</a>
																								</div>
																							</div>
																						</div>
																					</div>
																				</li>
																				<li class="item">
																					<div class="item-wrapper ">
																						<div class="product-inner">
																							<div class="image">
																								<a href="tu-quan-ao-6-ngan-modern.html" title="Tủ quần áo 6 ngăn Modern" class="product-image">
																									<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/a/h/aht-wedr17_4_1.jpg" alt="Tủ quần áo 6 ngăn Modern">
																								</a>
																							</div><!--end .image-->
																							<div class="product-info" style="min-height: 41px;">
																								<h2 class="product-name truncate">
																									<a href="tu-quan-ao-6-ngan-modern.html" title="Tủ quần áo 6 ngăn Modern">Tủ quần áo 6 ngăn Modern</a>
																								</h2>
																								<div class="price-wrapper">
																									<p class="price_km"><span class="price">6.400.000 đ</span></p>
																								</div>
																								<div class="actions-ahometo" style="display: none">
																									<a onclick="setLocation('tu-quan-ao-6-ngan-modern.html')" href="#">
																										<div class="button">
																											<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																										</a>
																									</div>
																								</div>
																							</div>
																						</div>
																					</li>
																					<li class="item">
																						<div class="item-wrapper ">
																							<div class="product-inner">
																								<div class="image">
																									<a href="giuong-acorn.html" title="Giường Acorn" class="product-image">
																										<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/u/n/untitled-1_1.png" alt="Giường Acorn">
																									</a>
																								</div><!--end .image-->
																								<div class="product-info" style="min-height: 41px;">
																									<h2 class="product-name truncate">
																										<a href="giuong-acorn.html" title="Giường Acorn">Giường Acorn</a>
																									</h2>
																									<div class="price-wrapper">
																										<p class="price_km"><span class="price">9.200.000 đ</span></p>
																									</div>
																									<div class="actions-ahometo" style="display: none">
																										<a onclick="setLocation('giuong-acorn.html')" href="#">
																											<div class="button">
																												<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																											</a>
																										</div>
																									</div>
																								</div>
																							</div>
																						</li>
																						<li class="item">
																							<div class="item-wrapper ">
																								<div class="product-inner">
																									<div class="image">
																										<a href="tu-quan-ao-6-ngan-acorn.html" title="Tủ quần áo 6 ngăn Acorn" class="product-image">
																											<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/m/i/mitssy_tu_quan_ao_6_ngan_acorn.jpg" alt="Tủ quần áo 6 ngăn Acorn">
																										</a>
																									</div><!--end .image-->
																									<div class="product-info" style="min-height: 41px;">
																										<h2 class="product-name truncate">
																											<a href="tu-quan-ao-6-ngan-acorn.html" title="Tủ quần áo 6 ngăn Acorn">Tủ quần áo 6 ngăn Acorn</a>
																										</h2>
																										<div class="price-wrapper">
																											<p class="price_km"><span class="price">7.350.000 đ</span></p>
																										</div>
																										<div class="actions-ahometo" style="display: none">
																											<a onclick="setLocation('tu-quan-ao-6-ngan-acorn.html')" href="#">
																												<div class="button">
																													<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																												</a>
																											</div>
																										</div>
																									</div>
																								</div>
																							</li>
																							<li class="item">
																								<div class="item-wrapper km">
																									<div class="product-inner">
																										<div class="image">
																											<a href="bo-ban-ghe-xep-ano.html" title="Bộ Bàn Ăn Ano" class="product-image">
																												<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/a/n/ano-_9_.jpg" alt="Bộ Bàn Ăn Ano">
																											</a>
																										</div><!--end .image-->
																										<div class="product-info" style="min-height: 41px;">
																											<h2 class="product-name truncate">
																												<a href="bo-ban-ghe-xep-ano.html" title="Bộ Bàn Ăn Ano">Bộ Bàn Ăn Ano</a>
																											</h2>
																											<div class="price-wrapper">
																												<p class="price_km"><span class="price">4.200.000 đ</span></p>
																												<span class="sale">-38%</span>
																												<p class="price"><span class="price">6.740.000 đ</span></p>
																											</div>
																											<div class="actions-ahometo" style="display: none">
																												<a onclick="setLocation('bo-ban-ghe-xep-ano.html')" href="#">
																													<div class="button">
																														<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																													</a>
																												</div>
																											</div>
																										</div>
																									</div>
																								</li>
																								<li class="item">
																									<div class="item-wrapper ">
																										<div class="product-inner">
																											<div class="image">
																												<a href="ke-sach-modular.html" title="Kệ sách Modular" class="product-image">
																													<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/m/i/mitssy_ke_sach_modular.png" alt="Kệ sách Modular">
																												</a>
																											</div><!--end .image-->
																											<div class="product-info" style="min-height: 41px;">
																												<h2 class="product-name truncate">
																													<a href="ke-sach-modular.html" title="Kệ sách Modular">Kệ sách Modular</a>
																												</h2>
																												<div class="price-wrapper">
																													<p class="price_km"><span class="price">8.200.000 đ</span></p>
																												</div>
																												<div class="actions-ahometo" style="display: none">
																													<a onclick="setLocation('ke-sach-modular.html')" href="#">
																														<div class="button">
																															<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																														</a>
																													</div>
																												</div>
																											</div>
																										</div>
																									</li>
																									<li class="item">
																										<div class="item-wrapper km">
																											<div class="product-inner">
																												<div class="image">
																													<a href="tu-giay-dep-dash-shirai.html" title="Tủ Giày Dép DASH - Shirai" class="product-image">
																														<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/250x250/9df78eab33525d08d6e5fb8d27136e95/h/n/hnm-1090.jpg" alt="Tủ Giày Dép DASH - Shirai">
																													</a>
																												</div><!--end .image-->
																												<div class="product-info" style="min-height: 41px;">
																													<h2 class="product-name truncate">
																														<a href="tu-giay-dep-dash-shirai.html" title="Tủ Giày Dép DASH - Shirai">Tủ Giày Dép DASH - Shirai</a>
																													</h2>
																													<div class="price-wrapper">
																														<p class="price_km"><span class="price">4.319.000 đ</span></p>
																														<span class="sale">-30%</span>
																														<p class="price"><span class="price">6.170.000 đ</span></p>
																													</div>
																													<div class="actions-ahometo" style="display: none">
																														<a onclick="setLocation('tu-giay-dep-dash-shirai.html')" href="#">
																															<div class="button">
																																<i class="fa fa-cart fa-shopping-cart"></i> Chọn mua</div>
																															</a>
																														</div>
																													</div>
																												</div>
																											</div>
																										</li>
																									</ul>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</section>
																		</section>

																		<section class="testi_section">
																			<div class="gray_line_bg" style="height: 6px">
																			</div>
																			<div class="container">
																				<h3 class="blue_title">CHÚNG TÔI <i class="fa fa-fw fa-heart"></i> MITSSY</h3>
																				<div id="testi">
																					<div class="testi_users">
																						<div data-for="user1" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/nguyen%20nhat%20vu.jpg" />
																						</div>
																						<div data-for="user2" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/van%20anh.jpg" />
																						</div>
																						<div data-for="user3" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/le%20chau%20bao.jpg" />
																						</div>
																						<div data-for="user4" class="img_user active">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/anh%20hung.jpg" />
																						</div>
																						<div data-for="user5" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/pham%20binh.jpg" />
																						</div>
																						<div data-for="user6" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/khanh%20nha.jpg" />
																						</div>
																						<div data-for="user7" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/nguyen%20thi%20huong.jpg" />
																						</div>
																						<div data-for="user8" class="img_user">
																							<div class="mask"></div>
																							<img src="media/wysiwyg/landingpage/sofa/testimonial/bui%20dinh%20chuong.jpg" />
																						</div>
																					</div>
																					<div class="testi_info">
																						<div class="info" id="user1">
																							<p>Ghế đẹp v&agrave; ngồi rất &ecirc;m, rất h&agrave;i l&ograve;ng về chất lượng của Ahometo. <br /> Bộ phận chăm s&oacute;c kh&aacute;ch h&agrave;ng rất nhiệt t&igrave;nh, chu đ&aacute;o, nếu c&oacute; dịp nhất định sẽ quay lại.</p>
																							<p class="name">Nguyễn Nhật Vũ</p>
																						</div>
																						<div class="info" id="user2">
																							<p>Sofa đẹp, ưng &yacute;. <br />Rất vui v&igrave; h&igrave;nh nh&agrave; m&igrave;nh đ&atilde; được quảng c&aacute;o tr&ecirc;n Facebook.</p>
																							<p class="name">T&ocirc; Thị V&acirc;n Anh</p>
																						</div>
																						<div class="info" id="user3">
																							<p>Sofa rất đẹp, m&agrave;u sắc ưng &yacute;</p>
																							<p class="name">L&ecirc; Ch&acirc;u Bảo</p>
																						</div>
																						<div class="info active" id="user4">
																							<p>Sofa rất ok v&agrave; kh&ocirc;ng kh&aacute;c h&igrave;nh ảnh, như những g&igrave; m&igrave;nh tưởng tượng, <br/> dịch vụ kh&aacute;ch h&agrave;ng kh&aacute; h&agrave;i l&ograve;ng.</p>
																							<p class="name">Hồ Hưng</p>
																						</div>
																						<div class="info" id="user5">
																							<p>T&igrave;nh cờ th&aacute;y sofa ở một shop kh&aacute;c, thấy xinh qu&aacute; n&ecirc;n m&igrave;nh hỏi th&igrave; biết đến AHOMETO.<br />Rất ưng &yacute; với sofa!</p>
																							<p class="name">Phạm Nguy&ecirc;n B&igrave;nh</p>
																						</div>
																						<div class="info" id="user6">
																							<p>T&ocirc;i mua ghế sofa tặng kh&aacute;ch, mọi thứ đều ưng &yacute;. Kh&aacute;ch của t&ocirc;i cũng rất th&iacute;ch.</p>
																							<p class="name">Kh&aacute;nh Nh&atilde;</p>
																						</div>
																						<div class="info" id="user7">
																							<p>T&ocirc;i c&oacute; thay đổi một ch&uacute;t so với mẫu ghế tr&ecirc;n website v&agrave; Ahometo đ&atilde; l&agrave;m theo đ&uacute;ng y&ecirc;u cầu, <br/>ghế rất đẹp, m&agrave;u sắc ok. Shipper rất nhiệt t&igrave;nh. T&ocirc;i rất h&agrave;i l&ograve;ng.</p>
																							<p class="name">Nguyễn Thị Hương</p>
																						</div>
																						<div class="info" id="user8">
																							<p>Ghế đẹp, m&igrave;nh rất h&agrave;i l&ograve;ng v&agrave; c&oacute; feedback lại với Ahometo.<br/> Cũng vui v&igrave; h&igrave;nh ghế của m&igrave;nh được xuất hiện tr&ecirc;n fanpage Ahometo. Nếu c&oacute; dịp nhất định sẽ quay lại.</p>
																							<p class="name">B&ugrave;i Đ&igrave;nh Chương</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</section>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<section class="set-footer" id='setQTTVFooter' style='display:none'>
														<div class="qttv-footer">
															<div class="col-md-8">
																<img src="media/lovely-living-room.jpg" />
															</div>
															<div  class="col-md-4">
																<div class="container">
																	<div id='registerQTTVHomeStylist' data-group='registerQTTVHomeStylist' data-href='/quy-trinh-tu-van.html' class="register-homestylist">
																		Hẹn Gặp Thiết Kế
																	</div>
																</div>
															</div>
														</div>
													</section>
													<div class="ad-bottom" style="">
														<a class="trigger-cp-cont" href="#campagin_popup">
															<p>none</p>    </a>
															<a href="#" class="close" >
																<i class="fa fa-times"></i>
															</a>
														</div>
														<div id="campagin_popup">
															<p>&nbsp;&nbsp;</p></div>
															<div class="cta soft" id='registerNowQTTVFooter' style='display:none;'>
																<div class="holder text-center">
																	<p class="text-center">
																		Bạn cần một chuyên gia tư vấn và giúp bạn hoàn thiện căn hộ?
																		<a class="btn btn-mid btn-orange-trans" data-href='/quy-trinh-tu-van.html'  data-group='registerQTTVHomeStylist'>Liên hệ ngay</a>
																	</p>
																</div>
															</div>
															<div class="footer">
																<div class="footer_bar">
																	<div class="container" style="width: 100%;">
																		<div class="general col-sm-6 col-md-3 col-lg-3 col-xs-12">
																			<div class="head">
																				<img src="skin/frontend/rwdc/default/images/logo_120.png">
																			</div>
																			<p class="sub">T&ecirc;n đơn vị: C&ocirc;ng Ty TNHH MITSSY</p>
																			<p class="sub">Địa chỉ: 294 Nguyễn Trọng Tuyển, Phường 1, Quận T&acirc;n B&igrave;nh, TP. HCM<br />Số giấy chứng nhận đăng k&yacute; kinh doanh: 0314 031 369 Do Sở kế hoạch đầu tư TP. HCM Cấp ng&agrave;y 26/09/2016. Đăng k&yacute; thay đổi lần thứ nhất 23/12/2016</p>
																			<p class="sub">&copy; Copyright 2016 Mitssy</p>            </div>
																			<div class="ho-tro col-sm-6 col-md-3 col-lg-3 col-xs-12 clearfix">
																				<div class="head phone">
																					<p class="tel"><a href="tel:02866846460"><i class="fa fa-phone"></i>(028) 668 46 460</a   ></p>
																					<p class="sub">(9:00 - 21:00, từ thứ hai đến chủ nhật)</p>
																				</div>
																				<p class="title">HỖ TRỢ KH&Aacute;CH H&Agrave;NG</p>
																				<p class="item"><a href="cau-hoi-thuong-gap.html">C&acirc;u hỏi thường gặp (FAQ)</a></p>
																				<p class="item"><a href="chinh-sach-giao-hang.html">Ch&iacute;nh s&aacute;ch giao h&agrave;ng</a></p>
																				<p class="item"><a href="chinh-sach-doi-tra.html">Ch&iacute;nh s&aacute;ch đổi trả</a></p>
																				<p class="item"><a href="bao-hanh-san-pham.html">Ch&iacute;nh s&aacute;ch bảo h&agrave;nh</a></p>
																				<p class="item"><a href="dieu-khoan-bao-mat.html">Điều khoản bảo mật</a></p>
																				<p class="item"><a href="doi-tac.html">Đối t&aacute;c kinh doanh</a></p>            </div>
																				<div class="doi-tac col-sm-6 col-md-3 col-lg-3 col-xs-12  clearfix">
																					<div class="head">
																						<p><a href="cdn-cgi/l/email-protection.html#563e333a3a39163b3f2225252f7835393b"><i class="fa fa-envelope"></i> <span class="__cf_email__" data-cfemail="aac2cfc6c6c5eac7c3ded9d9d384c9c5c7">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
																					</div>
																					<p class="title">VỀ MITSSY</p>
																					<p class="item"><a href="ve-mitssy.html">Giới thiệu về Mitssy</a></p>
																					<p class="item"><a href="quy-trinh-tu-van.html">Quy tr&igrave;nh tư vấn</a></p>
																					<p class="item"><a href="cong-trinh.html">Dự &aacute;n ti&ecirc;u biểu</a></p>
																					<p class="item"><a href="tim-hieu-san-pham-noi-that-va-vat-dung-trang-tri.html">Sản phẩm ở Mitssy</a></p>
																					<p class="item"><a href="vi-sao-ban-nen-chon-ahometo.html">V&igrave; sao chọn Mitssy?</a></p>
																					<p class="item"><a href="tuyen-dung.html">Tuyển dụng</a></p>            </div>
																					<div class="facebook col-sm-6 col-md-3 col-lg-3 col-xs-12  clearfix">
																						<div class="head social">
																							<ul>
																								<li><a target="_blank" class="fa fa-facebook-square fa-2x" href="https://www.facebook.com/Mitssy.HomeStylist/"></a></li>
																								<li><a target="_blank"  class="fa fa-instagram fa-2x" href="https://www.instagram.com/mitssy.homestylist/"></a></li>
																							</ul>                </div>
																							<p class="sub">Được chứng nhận</p>
																							<p><a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=29483" target="_blank"><img alt="" src="media/wysiwyg/asset/bo-cong-thuong.png" height="45" width="119" /></a></p>
																							<p class="sub">C&aacute;ch thức thanh to&aacute;n</p>
																							<p><img alt="" src="media/wysiwyg/campaign/go/thanhtoan.png" /></p>
																							<p><span id="cdSiteSeal1">
																								<script type="text/javascript" src="http://tracedseals.starfieldtech.com/siteseal/get?scriptId=cdSiteSeal1&amp;cdSealType=Seal1&amp;sealId=55e4ye7y7mb73934294e6d21fecd94ccj1y7mb7355e4ye7aba4ace4431734ba3"></script>
																							</span></p>            </div>
																						</div>
																					</div><!--end .footer_bar-->
																					<div class="footer-bottom">
																						<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyC76OnjQgnYrXSb0xwmOvr1-TBdBvS5wHE'></script>
																						<div style='overflow:hidden;height:220px;width:100%;'>
																							<div id='gmap_canvas' style='height:220px;width:100%;'></div>
																							<div>
																								<small><a href="http://www.embedgooglemaps.com/en/">Generate your map here, quick and easy! Give your customers directions									Get found</a></small>
																							</div>
																							<style>
																								#gmap_canvas img{max-width:none!important;background:none!important}
																								.gm-style-iw p {
																									line-height: 25px;
																									margin: 0px;
																								}
																								.gm-style-iw p.title {
																									margin-bottom: 10px;
																								}
																							</style>
																						</div>
																						<script type='text/javascript'>
																							function init_map(){
																								var myOptions = {zoom:15,center:new google.maps.LatLng(10.798336,106.6666073),mapTypeId: google.maps.MapTypeId.ROADMAP};
																								map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
																								marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(10.798336,106.6666073)});
																								infowindow = new google.maps.InfoWindow({content:'<p class="title"><strong>SHOW ROOM CHÍNH THỨC</strong></p><p class="add">294 Nguyễn Trọng Tuyển, Hồ Chí Minh, Vietnam</p>'});
																								google.maps.event.addListener(marker, 'click', function(){
																									infowindow.open(map,marker);
																								});
																								infowindow.open(map,marker);
																								setTimeout(function(){
																									map.panBy(0,-80);
																								}, 300);

																							}
																							google.maps.event.addDomListener(window, 'load', init_map);

																							jQuery.noConflict()
																							(
																								function($) {
																									if(typeof registerQTTVHomeStylist === "function"){
																										var registerQTTVContainer = $("[data-group='registerQTTVHomeStylist']");
																										var url = $(registerQTTVContainer).attr("data-href");
																										url = registerQTTVHomeStylist($, url);
																										if(url !== ''){
																											var anchorRegisterQTTV = document.createElement("a");
																											$(anchorRegisterQTTV).attr("href", url);
																											$(registerQTTVContainer).wrap(anchorRegisterQTTV);
																										} else {
																											$(registerQTTVContainer).click(function(){
																												if(registerQTTVHomeStylist !== undefined){
																													registerQTTVHomeStylist($, $(this).attr("data-href"));
																												}
																											});
																										}
																									}


																								}
																								);
																							</script>
																						</div><!--end .footer_bar-->
																					</div><!--end .footer-->

																					<section>
																					</script>
																				</div>
																			</div>
																		</body>
																		</html>
