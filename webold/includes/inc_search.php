<?
$page_prefix		= 'Trang';
$current_page		= getValue('page', 'int', '', 1);
$page_size			= 12;
$normal_class		= 'page';
$selected_class	= 'page selected';

$db_count	= new db_query("SELECT COUNT(*) AS count
									 FROM products_multi
									 	INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_active = 1 AND cat_type = 'product')
									 WHERE pro_active = 1 AND pro_name LIKE '%" . replaceMQ($kSearch) . "%'
									 OR pro_price LIKE '%" . replaceMQ($kSearch) . "%'
									 OR pro_rename LIKE '%" . replaceMQ($kSearch) . "%'
									 OR pro_description LIKE '%" . replaceMQ($kSearch) . "%'");
$total_record = 0;
if($search = mysql_fetch_assoc($db_count->result)) $total_record = $search['count'];
$db_count->close();
unset($db_count);

$num_of_page 		= (int)($total_record / $page_size) + 1;
if($current_page <= 0) $current_page = 1;
if($current_page > $num_of_page) $current_page = $num_of_page;
$url					= generate_search_url('search/' . str_replace(' ', '+', $kSearch));

$db_search = new db_query("SELECT *
									FROM products_multi
										INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_active = 1 AND cat_type = 'product')
									WHERE pro_name LIKE '%" . replaceMQ($kSearch) . "%'
									OR pro_price LIKE '%" . replaceMQ($kSearch) . "%'
									OR pro_rename LIKE '%" . replaceMQ($kSearch) . "%'
									OR pro_description LIKE '%" . replaceMQ($kSearch) . "%'
									ORDER BY pro_date DESC
									LIMIT " . (($current_page - 1) * $page_size . ',' . $page_size));

?>
<div class="one-con-wrapper">
	<div class='col-main'>
		<div class='container'>
			<div class="page-title category-title">
				<h1>Kết quả search cho từ khóa "<?=$kSearch?>"</h1>
			</div>
			<script>
				var total_product="<?=$total_record?>";
			</script>
			<div class="category-products" data-group='productList'>
				<ul class="products-grid">
				<?
				if($total_record > 0){
					while($row_search = mysql_fetch_assoc($db_search->result)){
						$url_detail_search = generate_detail_url($row_search["cat_type"], $row_search["cat_name"], $row_search["pro_name"], $row_search["pro_id"]);
					?>
					<li class="item col-md-3 col-lg-3 colg-sm-6 col-xs-12">
						<div class="product-inner" style='position:relative;'>
							<?
							if($row_search["pro_date"] < time() + 7*86400) echo '<img src="' . $con_css_path . 'tag-1.png" style="position:absolute;right:0px;top:0px;z-index:4">';
							?>
							<!-- Image -->
							<div class="image">
								<a href="<?=$url_detail_search?>" title="<?=$row_search['pro_name']?>" class="product-image" data-hhref="<?=$url_detail_search?>">
									<img id="<?=$url_detail_search?>" src="<?=$path_picture_small . $row_search['pro_picture']?>" data-group="imageHover" data-src-hover="" alt="<?=$row_search['pro_name']?>" />
								</a>
							</div>
							<!--end .image-->

							<!-- Product info -->
							<div class="product-info">
								<h2 class="product-name truncate"><a href="<?=$url_detail_search?>" title="<?=$row_search['pro_name']?>"><?=$row_search["pro_name"]?></a></h2>
								<div class="price-box def">
									<span id="product-price-<?=$row_search['pro_id']?>">
										<p class="new_price">
											<span class="price"><?=($row_search["pro_price"] > 0 ? format_number($row_search["pro_price"]) . " VNĐ" : "Liên hệ")?></span>
										</p>
									</span>
								</div>
							</div>
							<!-- End product info -->
						</div>
						<!-- End product inner -->
					</li>
					<?
					}// End foreach($arrTypeProduct as $key => $row_search)
					?>
				</ul>
				<?
				echo '<div class="pagination">';
					if($total_record > $page_size) echo '<div>' . generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous='←', $next='→', $first='', $last='', $break_type='1', $obj_response='',$page_space=5, $page_rewrite=1) . '</div>';
				echo '</div>';
				}// End if(count($arrTypeProduct) > 0)
				else{
					echo '<p class="alert alert-danger">Không có kết quả nào!!!</p>';
				}
				?>
				<br clear="all" />
			</div>
		</div>
	</div>
</div>