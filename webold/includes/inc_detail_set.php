<div class="one-con-wrapper">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>survey1446.css">
	<div class="product-view product-view-set">
		<div class="product-essential container-fluid">
			<form action="https://www.mitssy.com/checkout/cart/add/uenc/aHR0cHM6Ly93d3cubWl0c3N5LmNvbS9waG9uZy1raGFjaC1ub3Jmb2xrLmh0bWw_X19fU0lEPVU,/product/9334/form_key/BEFjtIBXKHYHaBug/" method="post" id="product_addtocart_form">
				<div class="row">

					<!-- Media -->
					<div class="product-img-box col-sm-8" id="set_thumb_image">
						<div>
							<img id="main_set_image" src="<?=$path_set_picture . $rowDetail['set_picture']?>" style="height: auto;"/>
						</div>
						<div class="flexslider">
							<ul id="thumblist" class="clearfix slides">
								<li>
									<a href="javascript:void(0)" class="product_thumb" data-image="<?=$path_set_picture . $rowDetail['set_picture']?>">
										<img id="image-0" src="<?=$path_set_picture_small . $rowDetail['set_picture']?>" />
									</a>
								</li>
								<?
								$db_pic_thumb = new db_query("SELECT * FROM set_pictures WHERE sp_product_id =" . $rowDetail["set_id"]);
								while($rowPic = mysql_fetch_assoc($db_pic_thumb->result)){
									?>
									<li>
										<a href="javascript:void(0)" class="product_thumb" data-image="/set_pictures_more/<?=$rowPic['sp_picture']?>">
											<img id="image-0" src="/set_pictures_more/small_<?=$rowPic['sp_picture']?>" />
										</a>
									</li>
									<?
								}
								$db_pic_thumb->close();
								unset($db_pic_thumb);
								?>
							</ul>
						</div>
					</div>
					<script type="text/javascript">
					jQuery.noConflict()(function ($) {
						$('#set_thumb_image').find('.product_thumb').click(function(){
							var imgUrl=$(this).data('image');
							$('#set_thumb_image').find("#main_set_image").attr('src',imgUrl);
						})
					});
					</script>
					<div class="right-detail-ahometo-product col-sm-4">
						<div class="product-name">
							<h1 class="tit_sg"><?=$rowDetail["set_name"]?></h1>
						</div>
						<div class="product-shop">
							<div class="extra-info" style="width: 100%; max-width: 100%; margin: 10px 0;"><?=$rowDetail["set_teaser"]?></div>
						</div>

						<div class="bot_right">
							<div class="add-to-cart-wrapper">
								<!-- Call to action -->
								<div class="p-benefit">
									Để tùy chỉnh và hoàn thiện căn hộ của bạn theo set nội thất này, xin nhấn vào nút "Hẹn gặp thiết kế" nói chuyện trực tiếp với Home Stylist.
								</div>
								<div class='form-group'>
									<div class="register-dk" style='text-align:center;'>
										<a id="set_reg" class="res-button btn btn-lg text-uppercase " href="/process_contact.html">HẸN GẶP THIẾT KẾ</a>
									</div>
								</div>
								<!-- End call to action -->
								<div class='form-group'>
									<div class='text-center'>
										<a href='/process_contact.html'>Tìm hiểu quy trình tư vấn thiết kế</a>
									</div>
									<div class='text-center' style='margin-top:10px;'>
										<div class='col-sm-4'>
											<div>
												<img style='max-width:50px;margin:0px auto;' src='<?=$con_css_path?>person.jpg'/>
											</div>
											<div>
												Tư vấn <br/> cụ thể
											</div>
										</div>
										<div class='col-sm-4'>
											<div>
												<img style='max-width:50px;margin:0px auto;' src='<?=$con_css_path?>clock.png'/>
											</div>
											<div>
												Báo giá <br/> và thiết kế nhanh
											</div>
										</div>
										<div class='col-sm-4'>
											<div>
												<img style='max-width:50px;margin:0px auto;' src='<?=$con_css_path?>baogianhanh.png'/>
											</div>
											<div>
												Chỉnh sửa <br/> trực tiếp
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End add to cart -->
						</div>
						<!--end .bot_right-->
					</div>
					<!-- End right-detail-ahometo-product -->
				</div>
				<!-- End row -->
			</form>
		</div>
		<div class="social_block" style="display: block;">
			<div class="left_block">
				<strong>Chia sẻ ngay</strong><br/>
				<br/>
				<a href="javascript:void(0)"><i class="fa fa-2x fa-facebook-square"></i></a>
				<a href="javascript:void(0)"><i class="fa fa-2x fa-instagram"></i></a>
			</div>
			<div class="right_block"><?=$rowDetail["set_description"]?></div>
		</div>
		<?/*
		<div class="container">
			<!-- Product in set -->
			<div class="main-product-view">
				<div class="col-sm-12">
					<script type="text/javascript">var totalProductInSet=3</script>
					<div class="gray_line_bg">
						<div>Sản phẩm trong set</div>
					</div>
					<table class="item_in_set table table-hover" style="display: inline-block;overflow: auto;">
						<tr class="set_row head_row">
							<td class="set_cell stt">STT</td>
							<td class="set_cell image">Hình ảnh</td>
							<td class="set_cell name">Tên sản phẩm</td>
							<td class="set_cell dimenssion">Kích thước</td>
							<td class="set_cell quantity">Số lượng</td>
							<td class="set_cell price">Giá</td>
						</tr>
						<tr class="set_row">
							<td class="set_cell stt">1</td>
							<td class="set_cell image">
								<a href="ban-sofa-diana-8320.html" class="product-image">
									<img style="width:100%;" src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/400x/9df78eab33525d08d6e5fb8d27136e95/d/i/diana-coffee.jpg"  alt="Bàn Diana" />
								</a>
							</td>
							<td class="set_cell name"><a  class="product-image" href="ban-sofa-diana-8320.html" >Bàn Diana</a></td>
							<td class="set_cell dimenssion">Cao 43cm x Dài 98cm x Sâu 60cm</td>
							<td class="set_cell quantity">0</td>
							<td class="set_cell price">0 đ</td>
						</tr>
						<tr class="set_row">
							<td class="set_cell stt">2</td>
							<td class="set_cell image">
								<a href="bo-ban-an-ghe-dai-nana.html" class="product-image">
									<img style="width:100%;" src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/400x/9df78eab33525d08d6e5fb8d27136e95/a/h/aht-mcsnnb.jpg"  alt="Bộ Bàn Ăn Ghế Dài NANA" />
								</a>
							</td>
							<td class="set_cell name"><a  class="product-image" href="bo-ban-an-ghe-dai-nana.html" >Bộ Bàn Ăn Ghế Dài NANA</a></td>
							<td class="set_cell dimenssion">D1300 - R800 - C740 mm</td>
							<td class="set_cell quantity">0</td>
							<td class="set_cell price">0 đ</td>
						</tr>
						<tr class="set_row">
							<td class="set_cell stt">3</td>
							<td class="set_cell image">
								<a href="sofa-3-cho-candi-9329.html" class="product-image">
									<img style="width:100%;" src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/thumbnail/400x/9df78eab33525d08d6e5fb8d27136e95/1/3/13694316_1747815998797306_1971074628_o.jpg"  alt="Sofa 3 Chỗ Candi" />
								</a>
							</td>
							<td class="set_cell name"><a  class="product-image" href="sofa-3-cho-candi-9329.html" >Sofa 3 Chỗ Candi</a></td>
							<td class="set_cell dimenssion">W209 x D82 x H73 cm</td>
							<td class="set_cell quantity">0</td>
							<td class="set_cell price">0 đ</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- End product-essential -->
		*/?>
		<section class="furniture_set_home">
			<section id="set" class="set-list alt">
				<div class="container">
					<div class="row">
						<div class="gray_line_bg">
							<div>Set nội thất tương tự</div>
						</div>
						<div class="set-container">
							<?
							$db_set_related   = new db_query("SELECT *
																		 FROM set_multi
																			INNER JOIN categories_multi ON (cat_id = set_category_id AND cat_type = 'set' AND cat_active = 1)
																		 WHERE set_active = 1 AND set_id != " . $rowDetail['set_id'] . " AND set_category_id = " . $rowDetail['set_category_id'] . "
																		 ORDER BY set_date DESC
																		 LIMIT 4");
							if(mysql_num_rows($db_set_related->result) > 0){
								while($row_set_related = mysql_fetch_assoc($db_set_related->result)){
									$url_set_related = generate_detail_url($row_set_related["cat_type"], $row_set_related["cat_name"], $row_set_related["set_name"], $row_set_related["set_id"]);
								?>
								<div class="set-item col-md-6">
									<div class="item-wrapper ">
										<div class="product-inner">
											<div class="image">
												<a href="<?=$url_set_related?>" title="<?=$row_set_related['set_name']?>" class="product-image-set">
													<img src="<?=$path_set_picture_small . $row_set_related['set_picture']?>" alt="<?=$row_set_related['set_name']?>">
												</a>
											</div><!--end .image-->
											<div class="product-info">
												<h4 class="product-name truncate">
													<a href="<?=$url_set_related?>" title="<?=$row_set_related['set_name']?>"><?=$row_set_related['set_name']?></a>
												</h4>
											</div>
										</div>
									</div>
								</div>
								<?
								}// End while($row_product_related = mysql_fetch_assoc($db_product_related->result))
							}// End if(mysql_num_rows($db_product_related->result) > 0)
							else{
								echo '<p class="alert alert-danger">Không có sản phẩm nào liên quan!</p>';
							}
							$db_set_related->close();
							unset($db_set_related);
							?>
						</div>
					</div>
				</div>
			</section>
		</section>
	</div>
</div>