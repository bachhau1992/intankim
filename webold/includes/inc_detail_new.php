<style type="text/css">
#step_list .container p{
	text-align: justify;
}
#step_list h1{
	width:100%;
	float:left;
	text-transform: none;
	padding: 30px 15px;
	margin-bottom: 0;
	border-top: 1px solid #CCC;
}
</style>
<div class="one-con-wrapper">
	<div class="std"><div>
		<div id='p_process'>
			<!-- Repsonsive -->
			<section id="step_list" class="step-list none-pad">
				<h1 class="text-center"><?=$rowDetail["new_title"]?></h1>
				<div class="container basic"><?=$rowDetail["new_description"]?></div>
			</section>
			<style type="text/css">
			.about_us_content .content_block .content_block_decor{
				width: 100%;
				display: inline-block;
				margin-top: 20px;
			}
			.about_us_content .content_block .content_block_decor .decor_img{
				width: 100%;
			}
			.about_us_content .content_block .content_block_decor .decor_img img {
				height: auto;
				width: 100%;
			}
			.about_us_content .content_block .content_block_decor .decor_content .decor_title{
				margin: 15px 0;
				text-align: left;
				font-weight: bold;
				font-size: 14px;
			}
			.about_us_content .content_block .content_block_decor .decor_content .decor_title a:hover{
				color: #00beb4;
			}
			.about_us_content .content_block {
				margin-top: 20px;
				max-width: 1170px;
				margin: 0 auto;
				overflow: hidden;
			}
			.decor_content p{
				text-align: justify;
				font-size: 12px;
			}
			</style>
			<div class="about_us_content">
				<div class='gray_line_bg'>
					<div class='title' style="font-size: 20px">Tin tức khác</div>
				</div>
				<div class="content_block">
					<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						<div class="row">
							<div class="content_block_decor">
								<?
								$db_news_lq   = new db_query("SELECT *
																		FROM news_multi
																			INNER JOIN categories_multi ON (cat_id = new_category_id AND cat_type = 'news' AND cat_active = 1)
																		WHERE new_active = 1 AND new_id != " . $iData . "
																		ORDER BY new_date DESC
																		LIMIT 4");
								while($row_new_lq = mysql_fetch_assoc($db_news_lq->result)){
									$url_detail_lq 		= generate_detail_url($row_new_lq["cat_type"], $row_new_lq["cat_name"], $row_new_lq["new_title"], $row_new_lq["new_id"]);
								?>
								<div class="decor col-sm-3">
									<div class="decor_img">
										<a href="<?=$url_detail_lq?>" title="<?=$row_new_lq['new_title']?>"><img class="img-responsive" alt="<?=$row_new_lq['new_title']?>" src="<?=$path_new_small . $row_new_lq["new_picture"]?>" /></a>
									</div>
									<div class="decor_content">
										<div class="decor_title"><a href="<?=$url_detail_lq?>" title="<?=$row_new_lq['new_title']?>"><?=$row_new_lq['new_title']?></a></div>
										<p><?=cut_string($row_new_lq['new_teaser'], 75)?></p>
									</div>
								</div>
								<?
								}// End while($row_new_lq = mysql_fetch_assoc($db_news_lq->result))
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>