<?
$page_prefix		= 'Trang';
$current_page		= getValue('page', 'int', '', 1);
$page_size			= 12;
$normal_class		= 'page';
$selected_class	= 'page selected';
$url					= generate_type_url($row_type["cat_name"], $row_type["cat_id"]);

$db_count_product = new db_query("SELECT COUNT(*) AS count
											 FROM set_multi
											 WHERE set_active = 1
											 AND set_category_id IN (" . $listProId . ")");
$total_record = 0;
if($row_count = mysql_fetch_assoc($db_count_product->result)) $total_record = $row_count['count'];
$db_count_product->close();
unset($db_count_product);

$num_of_page 		= (int)($total_record / $page_size) + 1;
if($current_page <= 0) $current_page = 1;
if($current_page > $num_of_page) $current_page = $num_of_page;

$db_type_product 	= new db_query("SELECT *
											 FROM set_multi
											 WHERE set_active = 1
											 AND set_category_id IN (" . $listProId . ")
											 ORDER BY set_date DESC
											 LIMIT " . (($current_page - 1) * $page_size . ',' . $page_size));
$arrTypeProduct 	= convert_result_set_2_array($db_type_product->result);
?>
<div class="main container">
	<div class="col-main">
		<div class='col-main'>
			<div class='container'>
				<div class="page-title category-title">
					<h1><?=$row_type["cat_name"]?></h1>
				</div>
				<div class="category-products">
					<div class="toolbar arrange">
						<div class="pager"></div>
					</div>
					<ul class="set-list row" id="listproduct">
					<?
					if(count($arrTypeProduct) > 0){
						foreach($arrTypeProduct as $key => $value){
							$url_detail = generate_detail_url($row_type["cat_type"], $row_type["cat_name"], $value["set_name"], $value["set_id"]);
						?>
						<li class="set-item col-sm-6">
							<div class="item-wrapper ">
								<div class="product-inner">
									<div class="image">
										<a href="<?=$url_detail?>" title="<?=$value['set_name']?>" class="product-image-set">
											<img data-name="" data-set="" src="<?=$path_set_picture_small . $value['set_picture']?>" alt="<?=$value['set_name']?>">
										</a>
										<a data-name="" data-id="" data-set="" class="see_more" href="<?=$url_detail?>">
											Xem chi tiết
										</a>
									</div><!--end .image-->
									<div class="product-info">
										<h4 class="product-name truncate">
											<a href="<?=$url_detail?>" title="<?=$value['set_name']?>"><?=$value['set_name']?></a>
										</h4>
									</div>
								</div>
							</div>
						</li>
						<?
					}// End foreach($arrTypeProduct as $key => $value)
					?>
					</ul>
					<?
					echo '<div class="pagination">';
						if($total_record > $page_size) echo '<div>' . generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous='←', $next='→', $first='', $last='', $break_type='1', $obj_response='',$page_space=5, $page_rewrite=1) . '</div>';
					echo '</div>';
					}// End if(count($arrTypeProduct) > 0)
					else{
						echo '<p class="alert alert-danger">Danh mục bạn chọn chưa có sản phẩm nào!!!</p>';
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var total_product="<?=$total_record?>";
</script>