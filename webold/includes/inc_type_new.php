<?
$page_prefix		= 'Trang';
$current_page		= getValue('page', 'int', '', 1);
$page_size			= 10;
$normal_class		= 'page';
$selected_class	= 'page selected';
$url	= generate_type_url($row_type["cat_name"], $row_type["cat_id"]);

$db_count_new = new db_query("SELECT COUNT(*) AS count
										FROM news_multi
										WHERE new_active = 1 AND new_category_id = " . $iCat);
$total_record = 0;
if($row_count = mysql_fetch_assoc($db_count_new->result)) $total_record = $row_count['count'];
$db_count_new->close();
unset($db_count_new);

$num_of_page 		= (int)($total_record / $page_size) + 1;
if($current_page <= 0) $current_page = 1;
if($current_page > $num_of_page) $current_page = $num_of_page;

?>
<div class="one-con-wrapper">
	<div class="std">
		<p>
			<style>
			.product-border{
				width:70px;margin:0px auto;border-color:#494949;padding:0px;padding-bottom:20px;
			}
			.collection-product{
				width:300px !important;
				margin:0px auto;
			}

			.btn-view{
				margin-top:20px;
				background-color:transparent;
				color:#00BEB4 !important;
				border: 1px solid #00BEB4;
			}
			.text-desc{
				text-align: justify;
				padding-bottom: 20px;
			}
			</style>
			<div id='p_process'>
				<!-- Repsonsive -->
				<section id="step_list" class=" none-pad">
					<div class="page-title category-title">
						<h1 style="text-align: center; font-weight: bold;">Tin tức</h1>
					</div>
					<div class="container basic text-center" style='margin-bottom:20px;'>
						<?
						$db_new = new db_query("SELECT *
														FROM news_multi
														WHERE new_active = 1 AND new_category_id = " . $iCat . "
														ORDER BY new_date DESC
														LIMIT " . (($current_page - 1) * $page_size . ',' . $page_size));
						while($row_new = mysql_fetch_assoc($db_new->result)){
							$url_detail_new = generate_detail_url($row_type["cat_type"], $row_type["cat_name"], $row_new["new_title"], $row_new["new_id"]);
						?>
						<div class="row step-2" style="margin-bottom: 30px;border-top:1px solid #CCC;padding-top:30px">
							<div class="col-sm-5 col-xs-12">
								<img class='collection-product' src="<?=$path_new_small . $row_new['new_picture']?>" style="margin-bottom: 20px;" />
							</div>
							<div class="col-sm-6">
								<div class="qt-title" style="margin: 20px 0;">
									<h3 class="title" style="text-transform: none; font-weight: bold;">
										<a href="<?=$url_detail_new?>" title="<?=$row_new['new_title']?>"><?=$row_new['new_title']?></a>
									</h3>
								</div>
								<div class="hiw-step-copy">
									<p class='text-desc'><?=$row_new["new_teaser"]?></p>
									<a href='<?=$url_detail_new?>'>
										<span class='btn-view'>
											Xem thêm
										</span>
									</a>
								</div>
							</div>
						</div>
						<?
						}// End while($row_new = mysql_fetch_assoc($db_new->result))
						$db_new->close();
						unset($db_new);
						?>
					</div>
					<?
					echo '<div class="pagination">';
						if($total_record > $page_size) echo '<div>' . generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous='←', $next='→', $first='', $last='', $break_type='1', $obj_response='',$page_space=5, $page_rewrite=1) . '</div>';
					echo '</div>';
					?>
				</section>
			</div>
		</p>
	</div>
</div>
