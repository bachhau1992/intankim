<div class="main container">
	<div class="col-main">
		<div id="messages_product_view"></div>
		<div class="product-view">
			<div class="product-essential">
				<div class="row">
					<div class="product-img-box col-sm-6">
						<a href="ghe-banh-va-don.html" class="back_to_category">
							<i class="fa fa-long-arrow-left"></i>Danh mục
						</a>
						<div>
							<img id="main_product_image" src="<?=$path_picture . $rowDetail['pro_picture']?>" style="height: auto;"/>
						</div>
						<div class="flexslider">
							<ul id="thumblist" class="clearfix slides">
								<li>
									<a href="javascript:void(0)" class="product_thumb" data-image="<?=$path_picture . $rowDetail['pro_picture']?>">
										<img id="image-0" src="<?=$path_picture_small . $rowDetail['pro_picture']?>" />
									</a>
								</li>
								<?
								$db_pic_thumb = new db_query("SELECT * FROM product_pictures WHERE pp_product_id =" . $rowDetail["pro_id"]);
								while($rowPic = mysql_fetch_assoc($db_pic_thumb->result)){
									?>
									<li>
										<a href="javascript:void(0)" class="product_thumb" data-image="<?=$path_picture_more . $rowPic['pp_picture']?>">
											<img id="image-0" src="<?=$path_picture_more_small . $rowPic['pp_picture']?>" />
										</a>
									</li>
									<?
								}
								$db_pic_thumb->close();
								unset($db_pic_thumb);
								?>
							</ul>
						</div>
					</div>
					<div class="right-detail-ahometo-product col-sm-6">
						<div class="product-name">
							<h1 class="tit_sg"><?=$rowDetail["pro_name"]?></h1>
						</div>
						<div class="product-shop-fullpage">
							<div class="short-description">
								<div class="std"><?=$rowDetail["pro_teaser"]?></div>
							</div>
							<table class="tb_sg">
								<tbody>
									<tr>
										<td class="f_col">Kích thước</td>
										<td class="s_col"><?=$rowDetail["pro_size"]?></td>
										</tr>
										<tr>
											<td class="f_col">Màu sắc</td>
											<td class="s_col"><?=$rowDetail["pro_color"]?></td>
										</tr>
									</tbody>
								</table>
								<div class="price_sale clearfix">
									<div class="select_quality">
										<select id="product_quality" name="qty" data-value="1" onchange="changeQuantity($j(this))">
											<?
											for ($i=1; $i <= $rowDetail["pro_quantity"]; $i++){
												echo '<option value="' . $i . '">' . $i . '</option>';
											}
											?>
										</select>
										<i class="fa fa-2x fa-caret-down icon"></i>
										<div class="quality_number">
											<div class="title">Số lượng</div>
											<div class="number" id="product_number">1</div>
										</div>
									</div>
									<div class="price_display right_pr_sl def">
										<div class="price-box def">
											<span id="product-price-<?=$rowDetail['pro_id']?>">
												<p class="new_price">
													<span class="price"><?=($rowDetail["pro_price"] > 0 ? format_number($rowDetail["pro_price"]) . " VNĐ" : "Liên hệ")?></span>
												</p>
											</span>
										</div>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="bot_right">
								<div class="clear"></div>
								<div class="price-qty-ahometo">
									<div class="price-info"></div>
								</div>
								<div class="add-to-cart-wrapper">
									<p class="p-benefit">
										<i class="fa fa-check"></i> <?=$rowDetail["pro_shipping"]?>
									</p>
									<div class="action">
										<div class="pur-wrapper clearfix">
											<div class="chose_sg box-button" style="margin-bottom: 20px">
												<a id="add_cart" class="add_to_cart" href="javascript:;" data-value="1" onclick="add_to_cart(<?=$rowDetail['pro_id']?>, $j(this))">
													<p>
														<i class="fa fa-shopping-cart"></i>
														<span>Chọn mua</span>
													</p>
												</a>
												<?/*
												<script type="text/javascript">
												jQuery.noConflict()(
													function($) {

														var iPro			= <?=$rowDetail["pro_id"]?>;
														$(".add_to_cart").click(function(){
															quantity 	= parseInt($(this).attr("data-value"));
															if(quantity <= 0 || isNaN(quantity)){ alert("Số lượng phải lớn hơn 0."); return false; }

															$.get("/ajax/add_to_cart.php?iPro=" + iPro + "&quantity=" + quantity, function(data){
																if(data != ""){
																	$("#gio_hang").append('<span class="total_product">' + data + '</span>');
																}
															});
														})
													}
												);
												</script>
												*/?>
											</div>
										</div>
									</div>
									<div class="clear"></div>
									<p class="sdt_sg clear">* Nếu bạn cần hỗ trợ: Hãy gọi  <span>(028) 668 46 460</span></p>
								</div>
								<div class="block">
									<i class="fa fa-2x fa-truck"></i><br/>
									Miễn phí giao hàng
								</div>
								<div class="block">
									<i class="fa fa-2x fa-refresh"></i><br/>
									Miễn phí đổi trả
								</div>
								<div class="block">
									<i class="fa fa-2x fa-clock-o"></i><br/>
									Giao hàng trong 14 ngày
								</div>
							</div>
							<!--end .bot_right-->
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
				<div class="tab_sg">
					<div role="tabpanel">
						<div class="social_block">
							<div class="left_block">
								<strong>Chia sẻ ngay</strong><br/>
								<br/>
								<a href="javascript:void(0)"><i class="fa fa-2x fa-facebook-square"></i></a>
								<a href="javascript:void(0)"><i class="fa fa-2x fa-instagram"></i></a>
							</div>
							<div class="right_block"><?=$rowDetail["pro_description"]?></div>
						</div>
						<div class="product_info">
							<div class="gray_line_bg">
								<div>
									<a id="slide_info" class="slide_close" href="javascript:void(0)">
										<i class="fa fa-fw fa-minus-circle"></i> Thông tin sản phẩm
									</a>
								</div>
							</div>
							<table class="table attr-table" id="collapse_info">
								<tr>
									<td> Màu sắc</td>
									<td> <?=$rowDetail["pro_color"]?></td>
								</tr>
								<tr>
									<td> Kích thước</td>
									<td> <?=$rowDetail["pro_size"]?></td>
								</tr>
								<tr>
									<td> Phong cách</td>
									<td> <?=$rowDetail["pro_style"]?></td>
								</tr>
							</table>
						</div>
						<div class="shipping_info">
							<div class="gray_line_bg">
								<div>
									<a id="slide_shipping" class="slide_open" href="javascript:void(0)">
										<i class="fa fa-fw fa-minus-circle"></i> Giao nhận và đổi trả
									</a>
								</div>
							</div>
							<div class="shiping_info_content" id="collapse_shipping" style="display: block"><?=$rowDetail["pro_shipping_info"]?></div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="block block-related clearfix">
							<div class="gray_line_bg">
								<div>Sản phẩm tương tự</div>
							</div>
							<div class="block-content">
								<ol class="mini-products-list" id="block-related">
									<?
									$db_product_related  = new db_query("SELECT *
																					 FROM products_multi
																						INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_type = 'product' AND cat_active = 1)
																					 WHERE pro_active = 1 AND pro_id != " . $rowDetail['pro_id'] . " AND pro_category_id = " . $rowDetail['pro_category_id'] . "
																					 ORDER BY pro_date DESC
																					 LIMIT 4");
									if(mysql_num_rows($db_product_related->result) > 0){
										while($row_product_related = mysql_fetch_assoc($db_product_related->result)){
											$url_product_related = generate_detail_url($row_product_related["cat_type"], $row_product_related["cat_name"], $row_product_related["pro_name"], $row_product_related["pro_id"]);
										?>
									<li class="item col-xs-3">
										<div class="sp_follow1">
											<div class="in_sp_follow1">
												<div class="product">
													<div class="img_spflow">
														<a href="<?=$url_product_related?>" title="<?=$row_product_related['pro_name']?>" class="product-image">
															<img src="<?=$path_picture_small . $row_product_related["pro_picture"]?>" alt="<?=$row_product_related['pro_name']?>"/>
														</a>
													</div><!--end .img_spflow-->
													<div class="product-details">
														<p class="tit_folow truncate">
															<a title="<?=$row_product_related['pro_name']?>" href="<?=$url_product_related?>"><?=$row_product_related['pro_name']?></a>
														</p>
														<div class="price-box def">
															<span id="product-price-<?=$row_product_related['pro_id']?>-related">
																<p class="new_price">
																	<span class="price"><?=($row_product_related["pro_price"] > 0 ? format_number($row_product_related["pro_price"]) . " VNĐ" : "Liên hệ")?></span>
																</p>
															</span>
														</div>
													</div>
												</div>
											</div><!--end .in_sp_follow1-->
										</div><!--end .sp_follow1-->
									</li>
									<?
										}// End while($row_product_related = mysql_fetch_assoc($db_product_related->result))
									}// End if(mysql_num_rows($db_product_related->result) > 0)
									else{
										echo '<p class="alert alert-danger">Không có sản phẩm nào liên quan!</p>';
									}
									?>
								</ol>
								<div class="clear"></div>
							</div>
						</div>
						<?/*
						<div class="bot_right_tab">
							<h3 class="subtitle">Sản phẩm bạn vừa xem</h3>
							<div class="justwatch" id="recent_viewed">
								<div class="watch1">
									<div class="img_watch1">
										<a href="ban-updown-10626.html" title="Bàn Louis - Gold" class="product-image">
											<img src="../ahometo.s3.amazonaws.com/catalog/product/cache/1/small_image/60x60/9df78eab33525d08d6e5fb8d27136e95/l/o/louis---gold.jpg" width="60" height="60" alt="Bàn Louis - Gold" />
										</a>
									</div><!--end .img_watch1-->
									<a class="removeitem"><!-- --></a>
								</div><!--end .watch1-->
								<div class="clear"></div>
							</div><!--end .justwatch-->
						</div>
						<style>
							#recent_viewed{ padding:10px}
							#recent_viewed .watch1{ width:60px; margin-bottom:5px; margin-right:6px}
							#recent_viewed .watch1:hover .removeitem{ display:block}
							#recent_viewed .watch1:nth-child(3n){ margin-right:6px}
							#recent_viewed .img_watch1{ width:60px}
							#recent_viewed .watch1 img{ border:1px solid #999; border-radius:5px;-moz-border-radius:5px;-khtml-border-radius:5px; width:58px; height:58px}
						</style>
						<script>
							jQuery(document).ready(function(){
								jQuery(".removeitem").each(function(){
									jQuery(this).click(function(){
										cfm=confirm("Bạn có chắc chắn không ?");
										if(cfm)
										{
											jQuery(this).closest('div').remove();
										}
										return false;
									});
								});
							});
						</script>
						*/?>
					</div>
					<!--end .tab_sg-->
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>