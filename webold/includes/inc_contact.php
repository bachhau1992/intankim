<style type="text/css">
#calendar_form .error{
	top: 35px;
	color: #F00;
}
</style>
<div class="one-con-wrapper">
	<div class="col-sm-12">
		<div class="std">
			<div id="p_process">
				<div class="banner banner-background" style="background-image: url('<?=$con_css_path?>qttv/qttvbaner.png');">
					<div class='banner_block col-md-8 col-lg-9 col-sm-6 hidden-xs'>
						<div class='banner_text text-center'>
							<h2 style="font-size: 36px;">
								Tận hưởng phút giây riêng tư gia đình
							</h2>
							<p style="font-size: 21px;">
								Chỉ trong 3 tuần bao gồm thiết kế, thi công và lắp đặt
							</p>
						</div>
					</div>
					<div id="error_show"></div>
					<div  class="form_block register-dk col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<form class="material-input" id="calendar_form" method="post">
							<h3>
								Thiết Kế Cùng Decobox
							</h3>
							<div class="form-group">
								<input class="input-text required-entry" name="billing_name" id="billing_name" type="text">
								<span class="bar"></span>
								<label>Tên của bạn</label>
							</div>
							<div class="form-group">
								<input class="input-text required-entry validate-number" name="billing_phone" id="billing_phone" type="text">
								<span class="bar"></span>
								<label>Số điện thoại</label>
							</div>
							<div class="form-group">
								<input class="input-text required-entry validate-email" name="billing_email" id="billing_email" type="text">
								<span class="bar"></span>
								<label>Địa chỉ email</label>
							</div>
							<div class="form-group">
								<input class="input-text required-entry" name="billing_project" id="billing_project" type="text">
								<span class="bar"></span>
								<label>Dự án căn hộ</label>
							</div>
							<div class="form-group" style="margin:0;" id="btn_control">
								<button class="res-button btn btn-lg text-uppercase" type="submit" id="btn_submit">Hẹn gặp thiết kế</button>
								<input id="action" name="action" type="hidden" value="send">
							</div>
						</form>
					</div>
				</div>
				<?
				$db_static_contact	= new db_query("SELECT *
																 FROM statics_multi
																 WHERE sta_id = " . $con_static_contact);
				if($row = mysql_fetch_assoc($db_static_contact->result)) echo $row["sta_description"];
				$db_static_contact->close();
				unset($db_static_contact);
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?=$con_js_path?>jquery.validate.js"></script>
<script type="text/javascript">
jQuery.noConflict()(function ($) {
	$(function(){
		$("#calendar_form").validate({
			rules: {
				billing_name: {
						required: true,
						minlength: 3
					},
				billing_phone: {
						required: true,
						number: true
					},
				billing_email: {
					required: true,
					email: true
					},
				billing_project: {
						required: true
					}
			},
			messages: {
				billing_name: {
					required: "Vui lòng nhập họ và tên của bạn!",
					minlength: "Họ tên bạn tối thiểu 3 kí tự!"
					},
				billing_phone: {
						required: "Vui lòng nhập số điện thoại của bạn!",
						number: "Số điện thoại không đúng!"
					},
				billing_email: "Vui lòng nhập email của bạn!",
				billing_project: "Vui lòng nhập tên dự án!"
			},
			submitHandler: function(form) {

				var name				= $("#billing_name").val();
				var email			= $("#billing_email").val();
				var phone			= $("#billing_phone").val();
				var project_name	= $("#billing_project").val();
				var action			= $("#action").val();

				$.ajax({
					url: '/ajax/contact_form.php',
					type: 'POST',
					data: {'ct_name':name, 'ct_email':email, 'ct_phone':phone, 'project_name':project_name, 'ct_action':action},
					success: function(data){
						if(data == 'success'){
							$("#btn_submit").hide();
							$("#btn_control").html('<p class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> &nbsp; Gửi tin thành công!</p>');
						}else{
							$('#error_show').hide().html(data).slideDown("slow");
						}
					}
				});

				return false;
			}
		});
	})
});
</script>
