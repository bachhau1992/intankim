<!-- FontAwesome Css -->
<link rel="icon" href="<?=$con_css_path?>favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<?=$con_css_path?>favicon.ico" type="image/x-icon" />

<meta property="og:site_name" content="Ahometo" />
<meta property="og:title" content="Decobox.vn - Những sản phẩm trang trí nội thất phong cách cho ngôi nhà đẹp." />
<meta property="og:type" content="website" />

<meta property="og:image" content="<?=$con_css_path?>logo_facebook2.png" />

<meta property="og:locale" content="vi_VN" />
<meta property="og:description" content="Decobox.vn mang những sản phẩm trang trí nội thất đậm dấu ấn phong cách nội thất đến ngôi nhà đẹp. Noi that, trang trí, trang tri, do trang tri, đồ trang trí, nội thất, đèn trang trí, kệ treo tường" />

<link href="<?=$con_css_path?>font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>css_default_two.css" media="all" />
<link media="all" href="<?=$con_css_path?>megamenu/css/horizontal/black/megamenu.css" type="text/css" rel="stylesheet">
<link media="all" href="<?=$con_css_path?>css_theme.css" type="text/css" rel="stylesheet">

<script type="text/javascript">
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W3HQMVD');
</script>

<script type="text/javascript" src="<?=$con_js_path?>jquery.min.js"></script>

<script type="text/javascript">
var siteConfig	= {
	con_ajax_path	: "<?=$con_ajax_path?>",
	con_css_path	: "<?=$con_css_path?>",
	con_redirect	: "<?=$con_redirect?>",
	con_root_path	: "<?=$con_root_path?>"
};
</script>