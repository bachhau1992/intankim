<div class="head_nav">
	<div class="top_bar">
		<div class="inner">
			<div class="ham_menu">
				<a href="javascript:void(0)">
					<i class="fa fa-2x fa-bars"></i>
				</a>
			</div>
			<div class="main_logo">
				<a href="/"><img src="<?=$con_css_path?>mitssy_logo.png"  /></a>
			</div>
			<div class="tool_search">
				<form id="form_search" style='position:relative'>
					<div class="search_form_mini">
						<input id="" autocomplete="off" type="text" name="keyword" value="" class="text_search input-text required-entry form-control" maxlength="128" placeholder="Bạn cần tìm gì ?" />
						<a id="submit_search" href="javascript:;" class="" >
							<span id='search_text'>TÌM</span>
							<span id='search_loading' style='font-size:15px'></span>
						</a>
						<a id="cancel_search" href="#" class="" >HUỶ</a>
					</div>
				</form>
				<div class="phone_text">
					<i class="fa fa-fw fa-phone"></i><?=$con_mobile . " - " . $con_phone?>
				</div>
			</div>
			<div class="task_bar">
				<ul>
					<li>
						<a href="javascript:void(0)" id="ah_search"><i class="fa fa-fw fa-search"></i> </a>
						<?
						if(getURL(0,0,1,0) == "checkout.php" || getURL(0,0,1,0) == "checkout_success.php"){
						?>
						<script type="text/javascript">
							$('#ah_search').click(function(){
								$('.phone_text').hide();
								$('#form_search').css({'display':'block'});
								setTimeout(function(){
									$('#form_search').css({width:'100%'});
									$('.search_form_mini > input').focus();
								},100);
							});

							$('.search_form_mini > input').focusout(function(e){
								$('#form_search').css({width:'0%'});
								setTimeout(function(){
									$('#form_search').css({display:'none'});
									$('.phone_text').show();
								},300);
							});

							$('#cancel_search').click(function(){
								$('#form_search').css({'width':'0%'});
								setTimeout(function(){
									$('#form_search').css({'display':'none'});
									$('.phone_text').show();
								},300);
							});

							$(function(){
								$("#form_search").submit(function(){
									var tag = $('.text_search').val().replace(/ /g, "+");
									if($.trim(tag) == "") alert("Bạn chưa nhập từ khóa");
									else window.location = '/search/' + tag + '.sps';
									return false;
								});
							});
						</script>
						<?
						}
						else{
						?>
						<script type="text/javascript">
							jQuery.noConflict()(function ($) {
								$('#ah_search').click(function(){
									$('.phone_text').hide();
									$('#form_search').css({'display':'block'});
									setTimeout(function(){
										$('#form_search').css({width:'100%'});
										$('.search_form_mini > input').focus();
									},100);
								});

								$('.search_form_mini > input').focusout(function(e){
									$('#form_search').css({width:'0%'});
									setTimeout(function(){
										$('#form_search').css({display:'none'});
										$('.phone_text').show();
									},300);
								});

								$('#cancel_search').click(function(){
									$('#form_search').css({'width':'0%'});
									setTimeout(function(){
										$('#form_search').css({'display':'none'});
										$('.phone_text').show();
									},300);
								});

								$(function(){
									$("#form_search").submit(function(){
										var tag = $('.text_search').val().replace(/ /g, "+");
										if($.trim(tag) == "") alert("Bạn chưa nhập từ khóa");
										else window.location = '/search/' + tag + '.sps';
										return false;
									});
								});
							});
						</script>
						<?
						}
						?>
					</li>
					<li>
						<div id="menu_account">
							<i class="fa fa-fw fa-user"></i>
							<div class="sub_menu_account">
								<div class="button_header">
									<a href="#form_popup_login" class="button button_quick_login">Đăng nhập</a>
									<span class="or_create"> hoặc <a href="customer/account/create/index.html">Tạo tài khoản mới</a></span>
								</div>
								<ul>
									<li><a href="customer/account/index.html">Thông tin tài khoản</a></li>
									<li><a href="sales/order/history/index.html">Quản lý đơn hàng</a></li>
								</ul>
								<div class="faq_support">Tư vấn và hỗ trợ: (028) 668 46 460</div>
							</div>
						</div>
						<div style="display:none">
							<div id="form_popup_login">
								<div class="logo_popup">
									<a href="/"><img src="<?=$con_css_path?>mitssy_logo.png" alt="" /></a>
								</div><!--end .logo_popup-->
								<div class="body_popup">
									<form action="https://www.mitssy.com/mytheme/account/checklogin" data-redirect="customer/account/login/index.html"><input name="form_key" type="hidden" value="BEFjtIBXKHYHaBug" />
										<table width="100%">
											<tr>
												<td width="140">Tài khoản</td>
												<td><input placeholder="Email của bạn" class="input" type="text" name="txtusername" /></td>
											</tr>
											<tr>
												<td width="140">Mật khẩu</td>
												<td><input type="password" name="txtpassword" class="input" /></td>
											</tr>
											<tr class="noti_login">
												<td width="140"></td>
												<td>
													<span class="login_result"></span>
												</td>
											</tr>
											<tr>
												<td width="140"></td>
												<td>
													<input type="checkbox" name="remember" /> Tự động đăng nhập cho lần sau
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<button href="#" class="button button_popup_login">Đăng nhập</button>
												</td>
											</tr>
										</table>
									</form>
								</div><!--end .body_popup-->
							</div><!--end #form_popup_login-->
						</div>
						<style>
							.fancybox-opened .fancybox-skin{ padding:0 !important}
						</style>
					</li>
				</ul>
			</div>
			<div class="cart_bar">
				<div class="cart_bar_item">
					<div class="cart">
						<div class="top-cart">
							<div class="top-cart1" id="hov_sp_cart">
								<p>
									<a href="/shopping_cart.html">
									<?
									$myCart 		= getValue("mycart", "str", "COOKIE", "");
									$arrCart 	= base64_decode($myCart);
									$arrCart 	= json_decode($arrCart, 1);
									$totalCart	= 0;
									if(count($arrCart) > 0){
										foreach ($arrCart as $kTotal => $vTotal){
											$totalCart += intval($vTotal["quantity"]);
										}
									}
									?>
										<i class="fa fa-shopping-cart"></i> <br/>
										<span id="gio_hang" class="gio_hang">Giỏ hàng</span>
										<?
										if($totalCart > 0) echo '<span class="total_product" id="total_product">' . $totalCart . '</span>';
										?>
									</a>
								</p>
								<div class="sp-cart"></div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu_bar">
		<div class="inner">
			<div class="main_menu">
				<div class="sm_megamenu_wrapper_horizontal_menu sambar" id="sm_megamenu_menu596b74fb95ca0" data-sam="9382240911500214523">
					<div class="sambar-inner">
						<a class="btn-sambar" data-sapi="collapse" href="#sm_megamenu_menu596b74fb95ca0">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="mobi_menu">
							<span>DANH MỤC</span>
							<a id="close_menu" href="javascript:void(0)"><i class="fa fa-2x fa-close"></i></a>
						</div>
						<ul class="sm-megamenu-hover sm_megamenu_menu sm_megamenu_menu_black" data-jsapi="on">
							<?
							$db_menu = new db_query("SELECT * FROM menus WHERE mnu_active = 1 AND mnu_type = 1 AND mnu_parent_id = 0 ORDER BY mnu_order ASC");
							while($row = mysql_fetch_assoc($db_menu->result)){
								$mnu_link	= $row["mnu_link"] == "" ? 'javascript:;' : $row["mnu_link"];
								$class = ($_SERVER['REQUEST_URI'] == $row["mnu_link"] ? ' active' : '');
								if($row["mnu_has_child"] == 1){
									echo '<li class="menu_' . $row["mnu_id"] . ' other-toggle sm_megamenu_lv1 sm_megamenu_drop parent"><a class="drop_on_mobile sm_megamenu_head sm_megamenu_drop" href="javascript:void(0)" id="sm_megamenu_' . $row["mnu_id"] . '">' . $row["mnu_name"] . '</a>';
									$db_menu1 = new db_query("SELECT * FROM menus WHERE mnu_active = 1 AND mnu_type = 1 AND mnu_parent_id = " . $row["mnu_id"] . " ORDER BY mnu_order ASC");
									if(mysql_num_rows($db_menu1->result) > 0){
										echo '<div class="sm-megamenu-child sm_megamenu_dropdown_6columns"><div class="sm_megamenu_col_6 lv_1 sm_megamenu_firstcolumn sm_megamenu_id' . $row["mnu_id"] . '">';
										while($row1 = mysql_fetch_assoc($db_menu1->result)){
											$mnu_link1	= $row1["mnu_link"] == "" ? 'javascript:;' : $row1["mnu_link"];
											$class1 = ($_SERVER['REQUEST_URI'] == $row1["mnu_link"] ? ' active' : '');
											if($row1["mnu_has_child"] == 1){
												echo '<div class="sm_megamenu_col_1 lv_2 sm_megamenu_firstcolumn sm_megamenu_id' . $row1["mnu_id"] . '">
															<div class="sm_megamenu_head_item">
																<div class="sm_megamenu_title">
																	<a class="sm_megamenu_nodrop" href="' . $mnu_link1 . '"><span class="sm_megamenu_title_lv2">' . $row1["mnu_name"] . '</span></a>
																</div>
															</div>';
															$db_menu2 = new db_query("SELECT * FROM menus WHERE mnu_active = 1 AND mnu_type = 1 AND mnu_parent_id = " . $row1["mnu_id"] . " ORDER BY mnu_order ASC");
															if(mysql_num_rows($db_menu2->result) > 0){
																while($row2 = mysql_fetch_assoc($db_menu2->result)){
																	$mnu_link2	= $row2["mnu_link"] == "" ? 'javascript:;' : $row2["mnu_link"];
																	echo '<div class="sm_megamenu_col_6 lv_3 sm_megamenu_firstcolumn sm_megamenu_id' . $row2["mnu_id"] . '">
																				<div class="sm_megamenu_head_item">
																					<div class="sm_megamenu_title">
																						<a class="sm_megamenu_nodrop" href="' . $mnu_link2 . '"><span class="sm_megamenu_title_lv3">' . $row2["mnu_name"] . '</span></a>
																					</div>
																				</div>
																			</div>';
																}
															}
												echo '</div>';
											}
											else{
												echo '<div class="sm_megamenu_col_1 lv_2 sm_megamenu_firstcolumn  sm_megamenu_id' . $row1["mnu_id"] . '">
															<div class="sm_megamenu_head_item ">
																<div class="sm_megamenu_title">
																	<a class="sm_megamenu_nodrop" href="' . $mnu_link1 . '"><span class="sm_megamenu_title_lv2">' . $row1["mnu_name"] . '</span></a>
																</div>
															</div>
														</div>';
											}
										}
										echo '</div></div>';
									}
									echo '</li>';
								}
								else{
									echo '<li class="menu_' . $row["mnu_id"] . ' other-toggle sm_megamenu_lv1 sm_megamenu_nodrop">
												<a class="sm_megamenu_head sm_megamenu_nodrop" href="' . $mnu_link . '" id="sm_megamenu_' . $row["mnu_id"] . '">' . $row["mnu_name"] . '</a>
											</li>';
								}
							}
							$db_menu->close();
							unset($db_menu);
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>