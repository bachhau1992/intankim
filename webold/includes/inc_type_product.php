<?
$page_prefix		= 'Trang';
$current_page		= getValue('page', 'int', '', 1);
$page_size			= 12;
$normal_class		= 'page';
$selected_class	= 'page selected';
$url					= generate_type_url($row_type["cat_name"], $row_type["cat_id"]);

$db_count_product = new db_query("SELECT COUNT(*) AS count
											 FROM products_multi
											 WHERE pro_active = 1
											 AND pro_category_id IN (" . $listProId . ")");
$total_record = 0;
if($row_count = mysql_fetch_assoc($db_count_product->result)) $total_record = $row_count['count'];
$db_count_product->close();
unset($db_count_product);

$num_of_page 		= (int)($total_record / $page_size) + 1;
if($current_page <= 0) $current_page = 1;
if($current_page > $num_of_page) $current_page = $num_of_page;

//Lọc
$cat_filter		= getValue("cat_filter");
$title_filter	= "Tất cả";
$db_type_child 	= new db_query("SELECT *
											 FROM categories_multi
											 WHERE cat_active = 1 AND cat_type = 'product' AND cat_has_child = 0 AND cat_id IN (" . $listProId . ")"
											 );
$arrTypeChild	= convert_result_set_2_array($db_type_child->result);

$arrTypeFilter	= array(0 => array("cat_id" => 0, "cat_name" => "Tất cả"));
$arrType2		= array();
foreach ($arrTypeChild as $key => $value) {
	$arrType2[$key + 1] = $value;
	if($cat_filter == $value["cat_id"]) $title_filter = $value["cat_name"];
}
$arrTypeFilter = $arrTypeFilter + $arrType2;
if($cat_filter > 0) $listProId = $cat_filter;

//Sắp xếp
$arrSort		= array (0 	=> "Tất cả",
							1	=> "Tăng dần",
							2	=> "Giảm dần"
							);
$sort			= getValue("sort");
$sqlOrderBy	= "";
if($sort > 0){
	switch ($sort) {
		case 1:
			$sqlOrderBy = "pro_price ASC, ";
			break;

		case 2:
			$sqlOrderBy = "pro_price DESC, ";
			break;
	}
}

$db_type_product 	= new db_query("SELECT *
											 FROM products_multi
											 WHERE pro_active = 1
											 AND pro_category_id IN (" . $listProId . ")
											 ORDER BY " . $sqlOrderBy . "pro_date DESC
											 LIMIT " . (($current_page - 1) * $page_size . ',' . $page_size));
$arrTypeProduct 	= convert_result_set_2_array($db_type_product->result);

?>
<div class="one-con-wrapper">
	<div class='col-main'>
		<div class='container'>
			<div class="page-title category-title">
				<h1><?=$row_type["cat_name"]?></h1>
			</div>
			<div id="sorter"></div>
			<div class="block-filter" >
				<div class="col-sm-4 col-xs-12 col-md-4 col-lg-4 text-center" style="margin-top: 20px;float: right;text-align: right;">
					<h3>Hiển thị <span id="product_total"><?=$total_record?></span> sản phẩm</h3>
				</div>
				<div class="filter_block_wrapper col-sm-8 col-xs-12 col-md-8 col-lg-8">
					<?
					if($arrTypeChild[0]["cat_id"] != $row_type["cat_id"]){
					?>
					<div class="filter_block">
						<div class="dropdown">
							<button style="width:100%;" type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
								<span style="text-align: left;"><b>Danh mục:</b> <?=$title_filter?></span>
								<span class="caret" style="text-align: right;"></span>
							</button>
							<ul class="dropdown-menu" id="cat_filter">
								<?
								//Lọc
								foreach ($arrTypeFilter as $kFil => $vFil){
									$linkFilter		= $url;
									$queryStr		= getURL(0,0,0,1,"iCat|nCat|page|cat_filter") . ($vFil["cat_id"] > 0 ? '&cat_filter=' . $vFil["cat_id"] : '');
									if($queryStr != "?") $linkFilter	.= str_replace("?&", "?", $queryStr);
									if($vFil["cat_id"] == $cat_filter){
										$linkFilter = $url . getURL(0,0,0,1,"iCat|nCat|page|cat_filter");
										echo '<li>
													<a href="javascript:;" class="small" data-url="' . $linkFilter . '">
														<input checked="checked" type="radio" name="cat_filter"/><span class="label_filter">&nbsp; ' . $vFil["cat_name"] . '</span>
													</a>
												</li>';
									}
									else{
										echo '<li>
													<a href="javascript:;" class="small" data-url="' . $linkFilter . '">
														<input type="radio" name="cat_filter"/><span class="label_filter">&nbsp; ' . $vFil["cat_name"] . '</span>
													</a>
												</li>';
									}
								}
								?>
							</ul>
						</div>
					</div>
					<?
					}// End if($arrTypeChild[0]["cat_id"] != $row_type["cat-id"])
					?>
					<div class="filter_block">
						<div class="dropdown">
							<button style="width:100%;" type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
								<span style="text-align: left;"><b>Giá:</b> <?=$arrSort[$sort]?></span>
								<span class="caret" style="text-align: right;"></span>
							</button>
							<ul class="dropdown-menu" id="sort_price">
								<?
								//Sắp xếp
								foreach($arrSort as $kSort => $vSort){
									$linkSort		= $url;
									$queryStr		= getURL(0,0,0,1,"iCat|nCat|page|sort") . ($kSort > 0 ? '&sort=' . $kSort : '');
									if($queryStr != "?") $linkSort	.= str_replace("?&", "?", $queryStr);
									if($kSort == $sort){
										$linkSort = $url . getURL(0,0,0,1,"iCat|nCat|page|sort");
										echo '<li>
													<a href="javascript:;" class="small" data-url="' . $linkSort . '">
														<input checked type="radio" name="price"/><span class="label_filter">&nbsp; ' . $vSort . '</span>
													</a>
												</li>';
									}
									else{
										echo '<li>
													<a href="javascript:;" class="small" data-url="' . $linkSort . '">
														<input type="radio" name="price"/><span class="label_filter">&nbsp; ' . $vSort . '</span>
													</a>
												</li>';
									}
								}
								?>
							</ul>
							<script type="text/javascript">
							jQuery.noConflict()(function ($) {
								$(function(){
									$(".filter_block_wrapper").find("li a").click(function(){
										var url_sort = $(this).attr("data-url");
										window.location.href = url_sort;
									})
								})
							});
							</script>
						</div>
					</div>
				</div>
			</div>
			<script>
				var total_product="<?=$total_record?>";
			</script>
			<div class="category-products" data-group='productList'>
				<ul class="products-grid">
				<?
				if(count($arrTypeProduct) > 0){
					foreach($arrTypeProduct as $key => $value){
						$url_detail = generate_detail_url($row_type["cat_type"], $row_type["cat_name"], $value["pro_name"], $value["pro_id"]);
					?>
					<li class="item col-md-3 col-lg-3 colg-sm-6 col-xs-12">
						<div class="product-inner" style='position:relative;'>
							<?
							if($value["pro_date"] < time() + 7*86400) echo '<img src="' . $con_css_path . 'tag-1.png" style="position:absolute;right:0px;top:0px;z-index:4">';
							?>
							<!-- Image -->
							<div class="image">
								<a href="<?=$url_detail?>" title="<?=$value['pro_name']?>" class="product-image" data-hhref="<?=$url_detail?>">
									<img id="<?=$url_detail?>" src="<?=$path_picture_small . $value['pro_picture']?>" data-group="imageHover" data-src-hover="" alt="<?=$value['pro_name']?>" />
								</a>
							</div>
							<!--end .image-->

							<!-- Product info -->
							<div class="product-info">
								<h2 class="product-name truncate"><a href="<?=$url_detail?>" title="<?=$value['pro_name']?>"><?=$value["pro_name"]?></a></h2>
								<div class="price-box def">
									<span id="product-price-<?=$value['pro_id']?>">
										<p class="new_price">
											<span class="price"><?=($value["pro_price"] > 0 ? format_number($value["pro_price"]) . " VNĐ" : "Liên hệ")?></span>
										</p>
									</span>
								</div>
							</div>
							<!-- End product info -->
						</div>
						<!-- End product inner -->
					</li>
					<?
					}// End foreach($arrTypeProduct as $key => $value)
					?>
				</ul>
				<?
				echo '<div class="pagination">';
					if($total_record > $page_size) echo '<div>' . generatePageBar($page_prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $previous='←', $next='→', $first='', $last='', $break_type='1', $obj_response='',$page_space=5, $page_rewrite=1) . '</div>';
				echo '</div>';
				}// End if(count($arrTypeProduct) > 0)
				else{
					echo '<p class="alert alert-danger">Danh mục bạn chọn chưa có sản phẩm nào!!!</p>';
				}
				?>
				<br clear="all" />
			</div>
		</div>
	</div>
</div>