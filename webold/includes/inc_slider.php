<div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<div class="header home text-center slideshow-container">
						<ul class="slideshow" style="border:none;">
							<?
							$db_slide	= new db_query("SELECT * FROM introduces WHERE int_active = 1 ORDER BY int_order ASC LIMIT 6");
							$arrSlide	= convert_result_set_2_array($db_slide->result);
							$db_slide->close();
							unset($db_slide);
							if(count($arrSlide) > 0){
								foreach($arrSlide as $row){
									?>
									<li>
										<a href="<?=$row['int_link']?>" target="_blank">
											<img class="img-responsive" src="introduce_pictures/<?=$row['int_picture']?>" alt="" />
										</a>
									</li>
									<?
								}
							}// End if(count($arrSlide) > 0)
							?>
						</ul>
						<i class="fa fa-angle-left slideshow-prev"></i>
						<i class="fa fa-angle-right slideshow-next"></i>
						<div class="slideshow-pager"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>