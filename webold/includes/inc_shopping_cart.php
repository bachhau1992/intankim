<?
// Lấy list id sản phẩm trong giỏ hàng
$arrList = array();
if($totalCart > 0){
	foreach($arrCart as $key => $value){
		$arrList[] = $key;
	}
	$listProductId = convert_array_to_list($arrList);

	$db_product_cart = new db_query("SELECT cat_id, cat_name, cat_type, pro_id, pro_name, pro_picture, pro_price, pro_size, pro_quantity
		FROM products_multi
		INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_active = 1 AND cat_type = 'product')
		WHERE pro_active = 1 AND pro_id IN (" . $listProductId . ")");
	$arrProductCart = convert_result_set_2_array($db_product_cart->result, "pro_id");
	$db_product_cart->close();
	unset($db_product_cart);
}
?>
<div class="main container">
	<div class="col-main">
		<?
		if($totalCart > 0){
		?>
		<div class="cont_order display-single-price">
			<div class="page-title title-buttons">
				<div class="row">
					<div class="col-sm-12">
						<div class="head_order mg_b7">
							<h1>Giỏ hàng</h1>
						</div>
					</div>
				</div>
				<div class="wrap row">
					<div class="left_order pg_b col-sm-8">
						<div class="cart-left">
							<table id="shopping-cart-table" class="tb_cart cart-table data-table">
								<colgroup>
								<col width="1">
								<col width="1">
								<col width="1">
								<col width="1">
								<col width="1">
							</colgroup>
							<thead>
								<tr class="first last">
									<td class="col1" rowspan="1"><span class="nobr">Sản phẩm</span></td>
									<td class="col2" rowspan="1">&nbsp;</td>
									<td class="col3" colspan="1">Giá</td>
									<td rowspan="1" class="col4">Số lượng</td>
									<td class="col5 last"></td>
								</tr>
							</thead>
							<tbody>
								<?
								$totalProMoney 	= 0;
								$totalTempMoney 	= 0;
								$quantity 			= 0;
								foreach($arrProductCart as $kCart => $vCart){
									$url_detail = generate_detail_url($vCart["cat_type"], $vCart["cat_name"], $vCart["pro_name"], $vCart["pro_id"]);
									$quantity = $arrCart[$vCart["pro_id"]]["quantity"];
									$totalProMoney 	= $vCart["pro_price"]*$quantity;
									$totalTempMoney 	+=  $vCart["pro_price"]*$quantity;
									?>
									<tr class="first odd" id="item_<?=$kCart?>" data-price="<?=$vCart["pro_price"]*$quantity?>">
										<td class="col1">
											<div class="img_cart">
												<a href="<?=$url_detail?>" title="<?=$vCart['pro_name']?>" class="product-image" data-hhref="<?=$url_detail?>">
													<img src="<?=$path_picture_small . $vCart['pro_picture']?>" alt="<?=$vCart['pro_name']?>">
												</a>
											</div>
										</td>
										<td class="col2">
											<p><?=$vCart["pro_name"]?></p>
											<div>
												- <font>Kích thước</font>: <span><?=$vCart['pro_size']?></span>
											</div>
											<p></p>
										</td>
										<td class="col3" data-rwd-label="Giá" data-rwd-tax-label="Excl. Tax">
											<span class="cart-price">
												<div class="price-box def">
													<span id="product-price-8636">
														<p class="new_price">
															<span class="price"><?=($vCart["pro_price"] > 0 ? format_number($vCart["pro_price"]) . " VNĐ" : 0)?></span>
														</p>
													</span>
													<span class="total_money_product" id="totalProMoney_<?=$vCart['pro_id']?>" style="display: none;"><?=$totalProMoney?></span>
												</div>
											</span>
										</td>
										<td class="product-cart-actions col4" data-rwd-label="Số lượng">
											<select name="quantity" class="form-control" onchange="update_cart(<?=$vCart["pro_id"]?>, $j(this)); return false;">
												<?
												for($i=1;$i<=$vCart["pro_quantity"];$i++){
													$selected = ($i == $quantity ? 'selected = "selected"' : '');
													echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
												}
												?>
											</select>
										</td>
										<td class="col5 last">
											<a href="javascript:;" onclick="delete_cart(<?=$vCart["pro_id"]?>); return false;" class="btn-remove btn-remove2"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?
								}// End foreach($arrCart as $key => $value)
								?>
							</tbody>
						</table>
					</div>
					<p class="continue"><a href="/"><i class="fa fa-caret-left"></i> Tiếp tục mua hàng</a></p>

				</div>
				<!--end .left_order-->
				<div id="cartRightColumn">
					<div class="right_order col-sm-4">
						<div class="in_right_oeder">
							<div class="head_r_order">
								Thông tin thanh toán
							</div>
							<div class="right_order3">
								<p class="p_right_order">Tổng tiền <span class="total"><span class="price" id="totalTempMoney"><?=format_number($totalTempMoney)?> VNĐ</span></span></p>
							</div>
							<input onclick="window.location='checkout.html';" type="submit" value="HOÀN THÀNH" class="final_total" id="" name="">
							<div class="clear"></div>
							<div class="support_order">
								<p class="p_or1">Hỗ trợ khách hàng</p>
								<p class="p_or2"><?=$con_phone?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end .wrap-->
		</div>
		<?
		}else{
		?>
		<div class="page-title">
			<h1>Giỏ hàng chưa có sản phẩm</h1>
		</div>
		<div class="cart-empty">
			<p>Không có sản phẩm nào</p>
			<p><a href="/">Click vào đây</a> để tiếp tục mua hàng.</p>
		</div>
		<?
		}
		?>
		<div class="clear"></div>
	</div>
</div>