<?
require_once('../classes/mailer/class.phpmailer.php');

// Lấy list id sản phẩm trong giỏ hàng
if(isset($arrCart) && count($arrCart) > 0){

	$arrList = array();
	foreach($arrCart as $key => $value){
		$arrList[] = $key;
	}
	$listProductId = convert_array_to_list($arrList);

	$db_product_cart = new db_query("SELECT pro_id, pro_name, pro_price, pro_size, pro_picture
												FROM products_multi
												INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_active = 1 AND cat_type = 'product')
												WHERE pro_active = 1 AND pro_id IN (" . $listProductId . ")");
	$arrProductCart = convert_result_set_2_array($db_product_cart->result, "pro_id");
	$db_product_cart->close();
	unset($db_product_cart);

	//Xử lí và lưu thông tin đơn hàng
	$action 				= getValue("action", "str", "POST", "");
	$ord_total_money	= getValue("total_money", "int", "POST", 0);
	$full_name			= getValue("billing_name", "str", "POST", "");
	$email 				= getValue("billing_email", "str", "POST", "");
	$ord_date			= time();
	$ord_user_id 		= $myuser->u_id;
	$ord_code 			= $full_name . "_" . date("Ymd");
	$str_product_mail = "";
	$errorMsg 			= "";

	$myform = new generate_form();
	$myform->add("ord_name","billing_name",0,0,"",1,translate_text("Vui lòng nhập họ tên của bạn!"),0,"");
	$myform->add("ord_company","billing_company",0,0,"",0,"",0,"");
	$myform->add("ord_detail","order_comments",0,0,"",0,"",0,"");
	$myform->add("ord_address","billing_address",0,0,"",1,translate_text("Vui lòng nhập địa chỉ của bạn!"),0,"");
	$myform->add("ord_phone","billing_phone",0,0,"",1,translate_text("Vui lòng nhập số điện thoại của bạn!"),0,"");
	$myform->add("ord_email","billing_email",2,0,"",1,translate_text("Vui lòng nhập đúng định dạng email!"),0,"");
	$myform->add("ord_date","ord_date",1,1,"",0,"",0,"");
	$myform->add("ord_user_id","ord_user_id",1,1,"",0,"",0,"");
	$myform->add("ord_total_money","ord_total_money",1,1,"",0,"",0,"");
	$myform->add("ord_code","ord_code",0,1,"",0,"",0,"");
	$myform->addTable("orders");

	if($action == "checkout"){
		$querystr = $myform->generate_insert_SQL();
		$errorMsg .= $myform->checkdata();

		if($errorMsg == ""){
			$db_excute 	= new db_execute_return();
			$last_id 	= $db_excute->db_execute($querystr);

			if($last_id > 0){

				if(isset($arrProductCart) && count($arrProductCart) > 0){
					foreach($arrProductCart as $kPro => $vPro){
						$quantity 	= $arrCart[$vPro["pro_id"]]["quantity"];

						// Ghi vào bảng chi tiết đơn hàng
						$sqlInsert 	= "INSERT INTO orders_product (op_order_id, op_product_id, op_quantity, op_price, op_date) VALUES(" . $last_id . ", " . $vPro["pro_id"] . ", " . $quantity . ", " . $vPro['pro_price'] . ", " . $ord_date . ")";
						$db_insert	= new db_execute($sqlInsert);
						unset($db_insert);

						$str_product_mail .= '<tr>
														<td>' . $vPro['pro_name'] . '</td>
														<td>' . format_number($vPro['pro_price']) . ' VNĐ</td>
														<td>' . $quantity . '</td>
													</tr>';
					}


					// Nếu đặt hàng thành công thì xóa giỏ hàng
					$cartData = json_encode($arrCart);
					setcookie("mycart", $cartData, time() - 3600, "/", null, null, 1);

					// Gửi mail
					$subject    	= "Thông tin đơn hàng từ Greenlineshanoi.vn";

					// HTML email starts here
					$message  = '<html><body>';
					$message .= '<table cellpadding="0" cellspacing="0" border="0" dir="LTR" style="background-color:#f0f7fc;border:1px solid #a5cae4;border-radius:5px;">';
					$message .= '<tbody>';
					$message .= '<tr>
										<td style="background-color:#7ab8b5;padding:5px 10px;border-bottom:1px solid #a5cae4;border-top-left-radius:4px;border-top-right-radius:4px;font-family:Helvetica,Arial,sans-serif;font-size:11px;line-height:1.231">
											<a href="#" style="color:#FFFFFF;text-decoration:none" target="_blank">Greenlineshanoi.vn</a>
										</td>
									</tr>';
					$message .= '<tr>
										<td style="background-color:#fcfcff;padding:1em;color:#141414;font-family:Helvetica,Arial,sans-serif;font-size:13px;line-height:1.231">
											<p style="margin-top:0">Chào ' . $full_name . ', chúng tôi gửi bạn thông tin đơn hàng đặt tại
											<a href="http://www.greenlineshanoi.vn/" style="color:#176093;text-decoration:none" target="_blank">Greenlineshanoi.vn</a></p>
											<h2 style="font-size:14pt;font-weight:normal;margin:10px 0 4px">Thông tin đơn hàng ' . $ord_code . '</h2>
											<div style="color:#333;font-size:12px;margin:4px 0 10px">
												<table cellpadding="0" cellspacing="0" border="1">
													<thead>
														<tr>
															<th>Tên sản phẩm</th>
															<th>Đơn giá</th>
															<th>Số lượng</th>
														</tr>
													</thead>
													<tbody>
														' . $str_product_mail . '
														<tr>
															<td colspan="2"><b style="color: #7ab8b5">Tổng tiền:</b></td>
															<td><b style="color: #7ab8b5">' . format_number($ord_total_money) . ' VNĐ</b></td>
														</tr>
													</tbody>
												</table>
											</div>
											<p>Cám ơn bạn đã mua hàng!</p>
										</td>
									</tr>';
					$message .= '<tr>
										<td style="background-color:#f0f7fc;padding:5px 10px;border-top:1px solid #d7edfc;border-bottom-left-radius:4px;border-bottom-right-radius:4px;text-align:right;font-family:Helvetica,Arial,sans-serif;font-size:11px;line-height:1.231">
										</td>
									</tr>';
					$message .= '</tbody></table>';
					$message .= '</body></html>';

					// HTML email ends here
					$mail = new PHPMailer(true);
					$mail->IsSMTP();
					$mail->isHTML(true);
					// $mail->SMTPDebug  = 2;
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "ssl";
					$mail->Host       = "ns89136.dotvndns.vn";
					$mail->Port       = 465;
					$mail->AddAddress($email);
					$mail->Username   ="administrator@greenlineshanoi.vn";
					$mail->Password   ="greenlineshanoi2017";
					$mail->SetFrom('administrator@greenlineshanoi.vn','Thông tin đơn hàng từ Greenlineshanoi.vn');
					$mail->AddReplyTo('administrator@greenlineshanoi.vn','Thông tin đơn hàng từ Greenlineshanoi.vn');
					$mail->Subject    = $subject;
					$mail->Body 		= $message;
					$mail->AltBody    = $message;
					if(!$mail->Send()) {
					    echo "Mailer Error: " . $mail->ErrorInfo;
					 } else {
					    // echo "Message has been sent";
					 }

					redirect("/checkout_success.html");
				}
			}
		}
	}
}// End if(isset($arrCart) && count($arrCart) > 0)
?>
<style type="text/css">
.tb_cart tbody td.col1 img{
	width: 100%;
	max-width: 180px;
	margin: 0 auto;
}
.tb_cart thead td {
	vertical-align: middle;
	font-size: 16px;
	background: #f7f7f7;
}
.cart-table th, .cart-table td, .cart-table tbody td{
	vertical-align: middle;
}
.data-table td, .data-table th{
	text-align: center;
}
.cart-table tr{
	border-bottom: 0;
}
.total{
	color: #7ab8b5;
	font-size: 20px;
	float: right;
	font-weight: bold;
}
</style>
<div class="main container">
	<div class="col-main">
		<div class="cont_order">
			<div class="main">
				<div class="page-title title-buttons">
					<div class="head_order clearfix">
						<h1 style="text-align: center; width: 100%;">Thanh toán</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<form id="one-step-checkout-form" method="post">
							<div class="wrap">
								<div class="left_order">
									<div class="address-information address-info-3-columns">
										<p class="hd">
											<span>1</span>Cung cấp thông tin địa chỉ thanh toán
										</p>
										<div class="clear"></div>
										<div class="content_order">
											<table class="tb_order">
												<tbody>
													<tr>
														<td class="col1">Họ và tên<span class="required"></span></td>
														<td class="col2">
															<input type="text" placeholder="Họ và tên" id="billing_name" name="billing_name" class="ho required-entry" size="50">
														</td>
													</tr>
													<tr>
														<td class="col1">Địa chỉ email<span class="required"></span></td>
														<td class="col2">
															<input type="text" placeholder="Email" id="billing_email" name="billing_email" value="" title="Địa chỉ email" class="input-text required-entry" size="50">
														</td>
													</tr>
													<tr>
														<td class="col1">Điện thoại<span class="required"></span></td>
														<td class="col2">
															<input type="text" placeholder="Điện thoại" id="billing_phone" name="billing_phone" value="" title="Điện thoại" class="input-text required-entry" size="50">
														</td>
													</tr>
													<tr>
														<td class="col1">Địa chỉ<span class="required"></span></td>
														<td class="col2">
															<input style="margin-bottom:10px;" placeholder="Địa chỉ" type="text" title="Địa chỉ" name="billing_address" id="billing_address" value="" class="required-entry input-text" size="50">
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<p class="hd no_ra"><span><i class="fa fa-pencil"></i></span>Ghi chú cho Decobox (nếu có)</p>
										<div class="note">
											<textarea id="onestepcheckout_comment" placeholder="Tôi muốn Decobox..." name="order_comments" col="15"></textarea>
											<div class="clear"></div>
										</div>
										<div class="in_right_oeder_ack">
											<p class="hd">
												<span>2</span>Thông tin thanh toán
											</p>
											<div>
												<div id="checkout-review-load" class="default-box checkout-review-load">
													<div id="checkout-review-table-wrapper">
														<table id="shopping-cart-table" class="tb_cart cart-table data-table">
															<colgroup>
															<col width="1">
															<col width="1">
															<col width="1">
															<col width="1">
														</colgroup>
														<thead>
															<tr class="first last">
																<td class="col1" rowspan="1"><span class="nobr">Sản phẩm</span></td>
																<td class="col3" colspan="1"></td>
																<td class="col3" colspan="1">Giá</td>
																<td rowspan="1" class="col4">Số lượng</td>
															</tr>
														</thead>
														<tbody>
															<?
															$totalProMoney 	= 0;
															$totalTempMoney 	= 0;
															$quantity 			= 0;
															foreach($arrProductCart as $kCart => $vCart){
																$quantity = $arrCart[$vCart["pro_id"]]["quantity"];
																$totalProMoney 	= $vCart["pro_price"]*$quantity;
																$totalTempMoney 	+=  $vCart["pro_price"]*$quantity;
																?>
																<tr class="first odd" id="item_<?=$kCart?>" data-price="<?=$vCart["pro_price"]*$quantity?>">
																	<td class="col1" align="center">
																		<div class="img_cart">
																			<a href="javascript:;" title="<?=$vCart['pro_name']?>" class="product-image">
																				<img src="<?=$path_picture_small . $vCart['pro_picture']?>" alt="<?=$vCart['pro_name']?>">
																			</a>
																		</div>
																	</td>
																	<td class="col2">
																		<p><?=$vCart["pro_name"]?></p>
																		<div>
																			- <font>Kích thước</font>: <span><?=$vCart['pro_size']?></span>
																		</div>
																		<p></p>
																	</td>
																	<td class="col2">
																		<span class="cart-price">
																			<div class="price-box def">
																				<span id="product-price-8636">
																					<p class="new_price">
																						<span class="price"><?=($vCart["pro_price"] > 0 ? format_number($vCart["pro_price"]) . " VNĐ" : 0)?></span>
																					</p>
																				</span>
																			</div>
																		</span>
																	</td>
																	<td class="col2"><?=$quantity?></td>
																</tr>
																<?
															}// End foreach($arrCart as $key => $value)
															?>
																<tr>
																	<td></td>
																	<td></td>
																	<td><span class="total">Tổng tiền:</span></td>
																	<td><span class="total"><span class="price" id="totalTempMoney"><?=format_number($totalTempMoney)?> VNĐ</span></span></td>
																</tr>
														</tbody>
													</table>
													<div class="button-set clearfix button-onestepcheckout">
														<input type="submit" value="HOÀN THÀNH" class="final_total" name="">
														<input type="hidden" id="checkout" name="action" value="checkout">
														<input type="hidden" id="total_money" name="total_money" value="<?=$totalTempMoney?>">
													</div>
												</div>
											</div>
										</div><!--end .in_right_oeder-->
									</div><!--end .left_order-->
								</div><!--end .wrap-->
							</form>
						</div>
					</div>
				</div><!--end .main-->
			</div>
		</div>
	</div>
</div><!--end .cont_order-->
<script type="text/javascript" src="<?=$con_js_path?>jquery.validate.js"></script>
<script type="text/javascript">
$(function(){

	$("#one-step-checkout-form").validate({
		rules: {
			billing_name: {
					required: true,
					minlength: 6
				},
			billing_address: "required",
			billing_phone: {
					required: true,
					number: true
				},
			billing_email: {
				required: true,
				email: true
				}
		},
		messages: {
			billing_name: {
				required: "Vui lòng nhập họ và tên của bạn!",
				minlength: "Họ tên bạn tối thiểu 6 kí tự!"
				},
			billing_address: "Vui lòng nhập địa chỉ của bạn!",
			billing_phone: {
					required: "Vui lòng nhập số điện thoại của bạn!",
					number: "Số điện thoại không đúng!"
				},
			billing_email: "Vui lòng nhập email của bạn!"
		},
		submitHandler: function(form) {
			$(form).submit();
			return false;
		}
	});
})
</script>