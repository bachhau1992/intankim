<?
// Lấy sản phẩm hot
$db_set  = new db_query("SELECT *
								 FROM set_multi
									INNER JOIN categories_multi ON (cat_id = set_category_id AND cat_type = 'set' AND cat_active = 1)
								 WHERE set_active = 1 AND set_hot = 1
								 ORDER BY set_date DESC
								 LIMIT 4");
$arrSetHot	= convert_result_set_2_array($db_set->result);
$db_set->close();
unset($db_set);

?>
<div>
	<section class="furniture_set_home none-pad">
		<?
		if(count($arrSetHot) > 0){
		?>
		<section id="set" class="set-list alt">
			<div class="gray_line_bg">
				<div>Set nội thất tiêu biểu</div>
			</div>
			<div class="container">
				<div class="row">
					<h3 class="blue_title">DO CÁC HOME STYLIST CHUYÊN NGHIỆP PHỐI HỢP</h3>
					<div class="set-container">
						<?
						foreach ($arrSetHot as $kHot => $vHot){
							$url_detail = generate_detail_url($vHot["cat_type"], $vHot["cat_name"], $vHot["set_name"], $vHot["set_id"]);
							?>
						<div class="set-item col-md-4 col-sm-6">
							<div class="item-wrapper ">
								<div class="product-inner">
									<div class="image">
										<a href="<?=$url_detail?>" title="<?=$vHot['set_name']?>" class="product-image-set">
											<img src="<?=$path_set_picture_small . $vHot['set_picture']?>" alt="<?=$vHot['set_name']?>">
										</a>
									</div><!--end .image-->
									<div class="product-info">
										<h4 class="product-name truncate">
											<a href="<?=$url_detail?>" title="<?=$vHot['set_name']?>"><?=$vHot['set_name']?></a>
										</h4>
									</div>
								</div>
							</div>
						</div>
						<?
						}// End foreach ($arrProHot as $kHot => $vHot)
						?>
						<div class="col-md-8 col-sm-12">
							<a href="set-phong-khach.html">
								<img src="<?=$con_css_path?>xem-them-01.jpg">
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?
		}//End if(count($arrSetHot) > 0)

		// Lấy sản phẩm mới
		$db_product_new  = new db_query("SELECT *
													FROM products_multi
														INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_type = 'product' AND cat_active = 1)
													WHERE pro_active = 1
													ORDER BY pro_date DESC
													LIMIT 10");
		$arrProNews	= convert_result_set_2_array($db_product_new->result);
		$db_product_new->close();
		unset($db_product_new);

		if(count($arrProNews) > 0){
		?>
		<section id="related_product">
			<div class="gray_line_bg">
				<div>Sản phẩm nội thất nổi bật</div>
			</div>
			<div class="container no_border_bottom">
				<div class="row">
					<h3 class="blue_title">LỰA CHỌN TỪ DECOBOX</h3>
					<div class="col-sm-12">
						<div class="related-product">
							<div class="flexslider">
								<ul class="slides">
								<?
								foreach ($arrProNews as $kNews => $vNews){
									$url_detail = generate_detail_url($vNews["cat_type"], $vNews["cat_name"], $vNews["pro_name"], $vNews["pro_id"]);
									?>
									<li class="item">
										<div class="item-wrapper ">
											<div class="product-inner">
												<?
												if($vNews["pro_date"] < time() + 7*86400) echo '<img src="' . $con_css_path . 'tag-1.png" style="position:absolute;right:0px;top:0px;z-index:4;width:auto;">';
												?>
												<div class="image">
													<a href="<?=$url_detail?>" title="<?=$vNews['pro_name']?>" class="product-image">
														<img src="<?=$path_picture_small . $vNews['pro_picture']?>" alt="<?=$vNews['pro_name']?>">
													</a>
												</div><!--end .image-->
												<div class="product-info" style="min-height: 41px;">
													<h2 class="product-name truncate">
														<a href="<?=$url_detail?>" title="<?=$vNews['pro_name']?>"><?=$vNews['pro_name']?></a>
													</h2>
													<div class="price-wrapper">
														<p class="price_km"><span class="price"><?=format_number($vNews["pro_price"])?> VNĐ</span></p>
													</div>
												</div>
											</div>
										</div>
									</li>
									<?
								}// End foreach ($arrProNews as $kHot => $vHot)
								?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?
		}//End if(count($arrProNews) > 0)
		?>
	</section>
	<?/*
	<section class="testi_section">
		<div class="gray_line_bg" style="height: 6px"></div>
		<?
		$db_static_home	= new db_query("SELECT *
													 FROM statics_multi
													 WHERE sta_id = " . $con_static_home);
		if($row = mysql_fetch_assoc($db_static_home->result)) echo $row["sta_description"];
		$db_static_home->close();
		unset($db_static_home);
		?>
	</section>
	*/?>
	<style type="text/css">
	.ld_image{
		-o-border-radius: 50%;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		border-radius: 50%;
		display: inline-block;
		overflow: hidden;
		padding: 10px;
		float: left;
		-o-box-shadow: 0 0 3px 1px rgba(0,0,0,0.08);
		-moz-box-shadow: 0 0 3px 1px rgba(0,0,0,0.08);
		-webkit-box-shadow: 0 0 3px 1px rgba(0,0,0,0.08);
		box-shadow: 0 0 3px 1px rgba(0,0,0,0.08);
		background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(242,242,242,1) 100%);
	}
	.ld_image img{
		border: 1px solid #fff;
		-o-border-radius: 50%;
		-webkit-border-radius: 50%;
		-moz-border-radius: 50%;
		border-radius: 50%;
		display: block;
		height: 120px;
		overflow: hidden;
		width: 120px;
		float: left;
		max-width: 120px
	}
	.ld_content{
		padding: 20px 0 15px;
	}
	.ld_author{
		color: #999;
	}
	#love_decobox .flex-control-nav{
		display: none;
	}
	</style>
	<div id="love_decobox">
		<div class='gray_line_bg'>
			<div class='title'>Chúng tôi <i class="fa fa-fw fa-heart" style="color: #7ab8b5;"></i> Decobox</div>
		</div>
		<div class="container">
			<div class='banner-background'>
				<div class="flexslider">
					<div class='flex-viewport'>
						<?
						$db_static_home	= new db_query("SELECT *
																	 FROM statics_multi
																	 WHERE sta_id = " . $con_static_home);
						if($row = mysql_fetch_assoc($db_static_home->result)) echo $row["sta_description"];
						$db_static_home->close();
						unset($db_static_home);
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style type="text/css">
	#about_us_home{
		max-width: 1170px;
		margin: 0 auto;
	}
	#about_us_home img{
		margin: 0px auto;
		max-width: 130px;
	}
	</style>
	<div id="about_us_home">
		<div class='gray_line_bg'>
			<div class='title'>Đối tác của Decobox</div>
		</div>
		<div class="content_block partner">
			<div class='banner-background'>
				<div class="flexslider">
					<div class='flex-viewport'>
						<ul class="slides">
							<?
							// $db_gallery	= new db_query("SELECT * FROM banners WHERE ban_active = 1 AND ban_type = 3 ORDER BY ban_order ASC LIMIT 9");
							// $arrGallery	= convert_result_set_2_array($db_gallery->result);
							// $db_gallery->close();
							// unset($db_gallery);
							?>
							<li>
								<div class='col-sm-4'>
									<img src='<?=$con_css_path?>doitac/hafele.png' width='100%' style='height:auto'/>
								</div>
								<div class='col-sm-4'>
									<img src='<?=$con_css_path?>doitac/hettich.png' width='100%' style='height:auto'/>
								</div>
								<div class='col-sm-4'>
									<img src='<?=$con_css_path?>doitac/malloca.png' width='100%' style='height:auto'/>
								</div>
							</li>
							<li>
								<div class='col-sm-4'>
									<img src='<?=$con_css_path?>doitac/blum.png' width='100%' style='height:auto'/>
								</div>
								<div class='col-sm-4'>
									<img src='<?=$con_css_path?>doitac/ancuong.png' width='100%' style='height:auto'/>
								</div>
								<div class='col-sm-4'>
									<img src='<?=$con_css_path?>doitac/acacia.png' width='100%' style='height:auto'/>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	jQuery.noConflict()(function ($) {
		function fixSize() {
			$('.flexslider').flexslider({
				animation: "slide"
			});
			$(".flex-prev").html("");
			$(".flex-next").html("");
		}
		fixSize();
		$(window).resize(function () {
			fixSize();
		});
	});
	</script>
</div>