/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : fs_mitssy

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2017-08-04 03:13:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `adm_id` int(11) NOT NULL AUTO_INCREMENT,
  `adm_loginname` varchar(100) DEFAULT NULL,
  `adm_password` varchar(100) DEFAULT NULL,
  `adm_name` varchar(255) DEFAULT NULL,
  `adm_email` varchar(255) DEFAULT NULL,
  `adm_address` varchar(255) DEFAULT NULL,
  `adm_phone` varchar(255) DEFAULT NULL,
  `adm_mobile` varchar(255) DEFAULT NULL,
  `adm_access_module` varchar(255) DEFAULT NULL,
  `adm_access_category` varchar(255) DEFAULT NULL,
  `adm_date` int(11) DEFAULT '0',
  `adm_isadmin` tinyint(1) DEFAULT '0',
  `adm_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`adm_id`),
  KEY `adm_date` (`adm_date`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', 'admin', '16d7a4fca7442dda3ad93c9a726597e4', 'Trần Ngọc Tuấn', 'txnc2002@yahoo.ca', '51 Lê Đại Hành - Hai Bà Trưng - Hà Nội', '(84-04) 784 7135 - (84-04) 219 2996', '095 330 8125', null, null, '0', '1', '1', '1');
INSERT INTO `admin_user` VALUES ('2', 'txnc2002', 'e10adc3949ba59abbe56e057f20f883e', 'Trần Ngọc Tuấn', 'txnc2002@yahoo.com', '', '', '', '[4][7][11]', '[1][2]', '1236848791', '0', '1', '1');

-- ----------------------------
-- Table structure for banners
-- ----------------------------
DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_name` varchar(255) DEFAULT NULL,
  `ban_picture` varchar(255) DEFAULT NULL,
  `ban_width` int(11) DEFAULT '0',
  `ban_height` int(11) DEFAULT '0',
  `ban_link` text,
  `ban_target` varchar(255) DEFAULT '_blank',
  `ban_type` tinyint(3) DEFAULT '1',
  `ban_order` double DEFAULT '0',
  `ban_hits` int(11) DEFAULT '0',
  `ban_date` int(11) DEFAULT '0',
  `ban_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ban_id`),
  KEY `ban_order` (`ban_order`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of banners
-- ----------------------------
INSERT INTO `banners` VALUES ('1', 'Banner Footer', 'jtn1501762783.jpg', '0', '0', 'http://', '_blank', '2', '1', '0', '1501762768', '1', '1');
INSERT INTO `banners` VALUES ('2', 'Banner Top', 'vim1501762817.jpg', '0', '0', 'http://', '_blank', '1', '2', '0', '1501762783', '1', '1');
INSERT INTO `banners` VALUES ('3', 'Đối tác 1', 'mme1501762846.png', '0', '0', 'http://', '_blank', '3', '3', '0', '1501762818', '1', '1');
INSERT INTO `banners` VALUES ('4', 'Đối tác 2', 'gzp1501762867.png', '0', '0', 'http://', '_blank', '3', '4', '0', '1501762846', '1', '1');
INSERT INTO `banners` VALUES ('5', 'Đối tác 3', 'nsh1501762881.png', '0', '0', 'http://', '_blank', '3', '5', '0', '1501762867', '1', '1');
INSERT INTO `banners` VALUES ('6', 'Đối tác 4', 'qkd1501762895.png', '0', '0', 'http://', '_blank', '3', '6', '0', '1501762881', '1', '1');
INSERT INTO `banners` VALUES ('7', 'Đối tác 5', 'hnl1501762910.png', '0', '0', 'http://', '_blank', '3', '7', '0', '1501762895', '1', '1');
INSERT INTO `banners` VALUES ('8', 'Đối tác 6', 'ght1501762925.png', '0', '0', 'http://', '_blank', '3', '9', '0', '1501762910', '1', '1');

-- ----------------------------
-- Table structure for categories_multi
-- ----------------------------
DROP TABLE IF EXISTS `categories_multi`;
CREATE TABLE `categories_multi` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) DEFAULT NULL,
  `cat_type` varchar(20) DEFAULT NULL,
  `cat_order` int(5) DEFAULT '0',
  `cat_parent_id` int(11) DEFAULT '0',
  `cat_title` varchar(255) DEFAULT NULL,
  `cat_meta_keyword` varchar(255) DEFAULT NULL,
  `cat_meta_description` varchar(255) DEFAULT NULL,
  `cat_description` text,
  `cat_has_child` int(1) DEFAULT '0',
  `cat_all_child` varchar(255) DEFAULT NULL,
  `cat_show` tinyint(1) DEFAULT '0',
  `cat_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`cat_id`),
  KEY `cat_order` (`cat_order`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of categories_multi
-- ----------------------------
INSERT INTO `categories_multi` VALUES ('1', 'Phòng khách', 'product', '1', '0', null, '', '', '', '1', '2,3,1', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('2', 'Ghế bành và đôn', 'product', '1', '1', null, '', '', '', '0', '2', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('3', 'Sofa', 'product', '2', '1', null, '', '', '', '0', '3', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('4', 'Trang tĩnh', 'static', '2', '0', null, '', '', '', '0', '4', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('5', 'Tin Tức', 'news', '3', '0', null, '', '', '', '0', '5', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('6', 'Ý tưởng nội thất', 'set', '4', '0', null, '', '', '', '1', '6', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('7', 'Thiết kế các phòng', 'set', '5', '6', null, '', '', '', '0', '7', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('8', 'Các không gian nội thất', 'set', '6', '6', null, '', '', '', '0', '8', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('9', 'Thiết kế căn hộ', 'set', '7', '0', '', '', '', '', '0', '9', '0', '1', '1');
INSERT INTO `categories_multi` VALUES ('10', 'Dự án tiêu biểu', 'set', '8', '0', '', '', '', '', '0', '10', '0', '1', '1');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(30) DEFAULT NULL,
  `city_order` double DEFAULT '1',
  `city_parent_id` int(11) DEFAULT '0',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`city_id`),
  KEY `city_order` (`city_order`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('4', 'Bà Rịa - Vũng Tàu', '4', '0', '1');
INSERT INTO `city` VALUES ('7', 'Bắc Giang', '7', '0', '1');
INSERT INTO `city` VALUES ('6', 'Bắc Kạn', '6', '0', '1');
INSERT INTO `city` VALUES ('5', 'Bạc Liêu', '5', '0', '1');
INSERT INTO `city` VALUES ('8', 'Bắc Ninh', '8', '0', '1');
INSERT INTO `city` VALUES ('9', 'Bến Tre', '9', '0', '1');
INSERT INTO `city` VALUES ('11', 'Bình Định', '11', '0', '1');
INSERT INTO `city` VALUES ('10', 'Bình Dương', '10', '0', '1');
INSERT INTO `city` VALUES ('12', 'Bình Phước', '12', '0', '1');
INSERT INTO `city` VALUES ('13', 'Bình Thuận', '13', '0', '1');
INSERT INTO `city` VALUES ('14', 'Cà Mau', '14', '0', '1');
INSERT INTO `city` VALUES ('16', 'Cần Thơ', '16', '0', '1');
INSERT INTO `city` VALUES ('15', 'Cao Bằng', '15', '0', '1');
INSERT INTO `city` VALUES ('17', 'Đà Nẵng', '17', '0', '1');
INSERT INTO `city` VALUES ('18', 'Đắk Lắk', '18', '0', '1');
INSERT INTO `city` VALUES ('21', 'Đồng Nai', '21', '0', '1');
INSERT INTO `city` VALUES ('22', 'Đồng Tháp', '22', '0', '1');
INSERT INTO `city` VALUES ('23', 'Gia Lai', '23', '0', '1');
INSERT INTO `city` VALUES ('24', 'Hà Giang', '24', '0', '1');
INSERT INTO `city` VALUES ('25', 'Hà Nam', '25', '0', '1');
INSERT INTO `city` VALUES ('1', 'Hà Nội', '1', '0', '1');
INSERT INTO `city` VALUES ('26', 'Hà Tây', '26', '0', '1');
INSERT INTO `city` VALUES ('27', 'Hà Tĩnh', '27', '0', '1');
INSERT INTO `city` VALUES ('28', 'Hải Dương', '28', '0', '1');
INSERT INTO `city` VALUES ('29', 'Hải Phòng', '29', '0', '1');
INSERT INTO `city` VALUES ('31', 'Hòa Bình', '31', '0', '1');
INSERT INTO `city` VALUES ('32', 'Hưng Yên', '32', '0', '1');
INSERT INTO `city` VALUES ('33', 'Khánh Hòa', '33', '0', '1');
INSERT INTO `city` VALUES ('34', 'Kiên Giang', '34', '0', '1');
INSERT INTO `city` VALUES ('35', 'Kon Tum', '35', '0', '1');
INSERT INTO `city` VALUES ('36', 'Lai Châu', '36', '0', '1');
INSERT INTO `city` VALUES ('37', 'Lâm Đồng', '37', '0', '1');
INSERT INTO `city` VALUES ('38', 'Lạng Sơn', '38', '0', '1');
INSERT INTO `city` VALUES ('39', 'Lào Cai', '39', '0', '1');
INSERT INTO `city` VALUES ('40', 'Long An', '40', '0', '1');
INSERT INTO `city` VALUES ('41', 'Nam Định', '41', '0', '1');
INSERT INTO `city` VALUES ('42', 'Nghệ An', '42', '0', '1');
INSERT INTO `city` VALUES ('43', 'Ninh Bình', '43', '0', '1');
INSERT INTO `city` VALUES ('44', 'Ninh Thuận', '44', '0', '1');
INSERT INTO `city` VALUES ('45', 'Phú Thọ', '45', '0', '1');
INSERT INTO `city` VALUES ('46', 'Phú Yên', '46', '0', '1');
INSERT INTO `city` VALUES ('47', 'Quảng Bình', '47', '0', '1');
INSERT INTO `city` VALUES ('48', 'Quảng Nam', '48', '0', '1');
INSERT INTO `city` VALUES ('49', 'Quảng Ngãi', '49', '0', '1');
INSERT INTO `city` VALUES ('50', 'Quảng Ninh', '50', '0', '1');
INSERT INTO `city` VALUES ('51', 'Quảng Trị', '51', '0', '1');
INSERT INTO `city` VALUES ('52', 'Sóc Trăng', '52', '0', '1');
INSERT INTO `city` VALUES ('53', 'Sơn La', '53', '0', '1');
INSERT INTO `city` VALUES ('54', 'Tây Ninh', '54', '0', '1');
INSERT INTO `city` VALUES ('55', 'Thái Bình', '55', '0', '1');
INSERT INTO `city` VALUES ('56', 'Thái Nguyên', '56', '0', '1');
INSERT INTO `city` VALUES ('57', 'Thanh Hóa', '57', '0', '1');
INSERT INTO `city` VALUES ('58', 'Thừa Thiên - Huế', '58', '0', '1');
INSERT INTO `city` VALUES ('59', 'Tiền Giang', '59', '0', '1');
INSERT INTO `city` VALUES ('2', 'Tp. Hồ Chí Minh', '2', '0', '1');
INSERT INTO `city` VALUES ('60', 'Trà Vinh', '60', '0', '1');
INSERT INTO `city` VALUES ('61', 'Tuyên Quang', '61', '0', '1');
INSERT INTO `city` VALUES ('62', 'Vĩnh Long', '62', '0', '1');
INSERT INTO `city` VALUES ('63', 'Vĩnh Phúc', '63', '0', '1');
INSERT INTO `city` VALUES ('64', 'Yên Bái', '64', '0', '1');
INSERT INTO `city` VALUES ('3', 'An Giang', '3', '0', '1');
INSERT INTO `city` VALUES ('19', 'Đắc Nông', '19', '0', '1');
INSERT INTO `city` VALUES ('30', 'Hậu Giang', '30', '0', '1');
INSERT INTO `city` VALUES ('20', 'Điện Biên', '20', '0', '1');

-- ----------------------------
-- Table structure for configuration
-- ----------------------------
DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `con_admin_email` varchar(255) DEFAULT NULL,
  `con_site_title` varchar(255) DEFAULT NULL,
  `con_background` varchar(255) DEFAULT NULL,
  `con_background_style` int(3) DEFAULT '0',
  `con_meta_keywords` text,
  `con_meta_description` text,
  `con_currency` varchar(255) NOT NULL DEFAULT 'VND',
  `con_price_updating` varchar(255) DEFAULT '0',
  `con_root_path` varchar(255) DEFAULT '/',
  `con_mod_rewrite` tinyint(1) DEFAULT '1',
  `con_facebook_code` text,
  `con_register` tinyint(1) DEFAULT '0',
  `con_company_name` varchar(255) DEFAULT NULL,
  `con_address` text,
  `con_phone` varchar(255) DEFAULT NULL,
  `con_mobile` varchar(255) DEFAULT NULL,
  `con_email` varchar(255) DEFAULT NULL,
  `con_yahoo` varchar(255) DEFAULT NULL,
  `con_skype` varchar(255) DEFAULT NULL,
  `con_static_contact` int(11) DEFAULT '0',
  `con_static_footer` int(11) DEFAULT '0',
  `con_static_home` int(11) DEFAULT '0',
  `con_lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`con_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of configuration
-- ----------------------------
INSERT INTO `configuration` VALUES ('1', 'imhung1179@gmail.com', 'Decobox.vn - Những sản phẩm trang trí nội thất phong cách cho ngôi nhà đẹp.', 'ghw1397821777.png', '2', 'nội thất, noi that, trang trí nội thất, trang tri noi that, do trang tri,  trang tri, trang tri nha,phong cách nhà đẹp, nội thất bếp, nội thất đep, nội thất phòng ngủ, nội thất phòng khách, phòng khách, phòng ngủ, phòng bếp, phòng bé, nhà đẹp,nha dep, đèn trang trí, kệ treo tường.', 'Greenlineshanoi.vn mang những sản phẩm trang trí nội thất đậm dấu ấn phong cách nội thất đến ngôi nhà đẹp. Noi that, trang trí, trang tri, do trang tri, đồ trang trí, nội thất, đèn trang trí, kệ treo tường', 'VNĐ', 'Liên hệ: 091.200.5327', '/', '1', '', '1', 'Decobox.vn - Công ty thiết kế nội thất', 'Tầng 4, 19 An Trạch, Q. Đống Đa, Hà Nội', '(024) 7300 8687', '09 020 929 36', 'info@decobox.vn', '21.0274902,105.8267348', '', '19', '17', '16', '1');

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `ct_id` int(11) NOT NULL AUTO_INCREMENT,
  `ct_name` varchar(255) DEFAULT NULL,
  `ct_mobile` varchar(255) DEFAULT NULL,
  `ct_email` varchar(255) DEFAULT NULL,
  `ct_content` text,
  `ct_date` int(11) DEFAULT '0',
  `ct_status` tinyint(1) DEFAULT '0',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ct_id`),
  KEY `ct_date` (`ct_date`),
  KEY `ct_status` (`ct_status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES ('1', 'hungnv', '0963453437', 'imhung1179@gmail.com', 'Mua nha chung cu', '1501676268', '1', '1');
INSERT INTO `contacts` VALUES ('2', 'dasdad', '0963453437', 'imhung1179@gmail.com', 'Nha chung cu', '1501677123', '0', '1');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `cus_id` int(11) NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(255) DEFAULT NULL,
  `cus_phone` varchar(255) DEFAULT NULL,
  `cus_address` varchar(255) DEFAULT NULL,
  `cus_information` text,
  `cus_date` int(11) DEFAULT '0',
  `cus_hide` tinyint(1) DEFAULT '0',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`cus_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customers
-- ----------------------------

-- ----------------------------
-- Table structure for introduces
-- ----------------------------
DROP TABLE IF EXISTS `introduces`;
CREATE TABLE `introduces` (
  `int_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_name` varchar(255) DEFAULT NULL,
  `int_picture` varchar(255) DEFAULT NULL,
  `int_picture_width` int(11) DEFAULT NULL,
  `int_picture_height` int(11) DEFAULT NULL,
  `int_link` text,
  `int_target` varchar(255) DEFAULT NULL,
  `int_order` double DEFAULT '0',
  `int_date` int(11) DEFAULT NULL,
  `int_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`int_id`),
  KEY `int_order` (`int_order`),
  KEY `int_active` (`int_active`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of introduces
-- ----------------------------
INSERT INTO `introduces` VALUES ('18', 'Slide 1', 'yzy1500973065.jpg', '3570', '1283', 'http://', '_blank', '1', '1500973049', '1', '1');
INSERT INTO `introduces` VALUES ('19', 'Slide 2', 'gaf1500973080.jpg', '2917', '1042', 'http://', '_blank', '2', '1500973069', '1', '1');
INSERT INTO `introduces` VALUES ('20', 'Slide 3', 'jkg1500973092.jpg', '2917', '1042', 'http://', '_blank', '3', '1500973083', '1', '1');

-- ----------------------------
-- Table structure for ip_denyconnect
-- ----------------------------
DROP TABLE IF EXISTS `ip_denyconnect`;
CREATE TABLE `ip_denyconnect` (
  `ip` int(11) unsigned NOT NULL DEFAULT '0',
  `time` int(11) DEFAULT '0',
  KEY `ip` (`ip`),
  KEY `time` (`time`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of ip_denyconnect
-- ----------------------------

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `lang_id` int(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(32) DEFAULT NULL,
  `lang_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', 'vn', 'Tiếng Việt');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `mnu_id` int(11) NOT NULL AUTO_INCREMENT,
  `mnu_name` varchar(255) DEFAULT NULL,
  `mnu_link` text,
  `mnu_target` varchar(10) DEFAULT '_self',
  `mnu_type` tinyint(3) DEFAULT '1',
  `mnu_order` double DEFAULT '0',
  `mnu_parent_id` int(11) DEFAULT '0',
  `mnu_has_child` int(1) DEFAULT '0',
  `mnu_defined` varchar(255) DEFAULT NULL,
  `mnu_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`mnu_id`),
  KEY `mnu_order` (`mnu_order`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'Ý tưởng nội thất', '', '_self', '1', '1', '0', '1', '', '1', '1');
INSERT INTO `menus` VALUES ('3', 'Sản phẩm', '', '_self', '1', '2', '0', '1', '', '1', '1');
INSERT INTO `menus` VALUES ('4', 'Phòng khách', '/phong-khach/1.html', '_self', '1', '2.1', '3', '1', '', '1', '1');
INSERT INTO `menus` VALUES ('5', 'Ghế bành và đôn', '/ghe-banh-va-don/2.html', '_self', '1', '2.2', '4', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('6', 'Đèn', '', '_self', '1', '2.3', '3', '1', '', '1', '1');
INSERT INTO `menus` VALUES ('7', 'Đèn bàn', '', '_self', '1', '2.4', '6', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('8', 'Đèn sàn', '', '_self', '1', '2.5', '6', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('10', 'Dự án tiêu biểu', '/du-an-tieu-bieu/10.html', '_self', '1', '5', '0', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('11', 'Decobox', '/gioi-thieu/18.shtml', '_self', '1', '6', '0', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('12', 'Sofa', '/sofa/3.html', '_self', '1', '2.6', '4', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('13', 'Quy trình tư vấn', '/process_contact.html', '_self', '1', '4', '0', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('16', 'Thiết kế căn hộ', '/thiet-ke-can-ho/9.html', '_self', '1', '3', '0', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('14', 'Thiết kế các phòng', '/thiet-ke-cac-phong/7.html', '_self', '1', '1.1', '1', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('15', 'Các không gian nội thất', '/cac-khong-gian-noi-that/8.html', '_self', '1', '1.2', '1', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('17', 'Hỗ trợ khách hàng', '', '_self', '4', '1', '0', '1', '', '1', '1');
INSERT INTO `menus` VALUES ('18', 'Chính sách giao hàng', '/chinh-sach-giao-hang/21.shtml', '_self', '4', '1.1', '17', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('19', 'Chính sách đổi trả', '', '_self', '4', '1.2', '17', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('20', 'Bảo hành sản phẩm', '', '_self', '4', '1.3', '17', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('21', 'Điều khoản bảo mật', '', '_self', '4', '1.4', '17', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('22', 'Câu hỏi thường gặp', '', '_self', '4', '1.5', '17', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('23', 'Về Decobox', '', '_self', '4', '2', '0', '1', '', '1', '1');
INSERT INTO `menus` VALUES ('24', 'Giới thiệu', '/gioi-thieu/18.shtml', '_self', '4', '2.1', '23', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('25', 'Thiết kế căn hộ', '/thiet-ke-can-ho/9.html', '_self', '4', '2.2', '23', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('26', 'Quy trình tư vấn', '/process_contact.html', '_self', '4', '2.3', '23', '0', '', '1', '1');
INSERT INTO `menus` VALUES ('27', 'Dự án tiêu biểu', '/du-an-tieu-bieu/10.html', '_self', '4', '2.4', '23', '0', '', '1', '1');

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `mod_id` int(11) NOT NULL DEFAULT '0',
  `mod_name` varchar(100) DEFAULT NULL,
  `mod_path` varchar(255) DEFAULT NULL,
  `mod_action` varchar(255) DEFAULT NULL,
  `mod_file` varchar(255) DEFAULT NULL,
  `mod_order` double DEFAULT NULL,
  PRIMARY KEY (`mod_id`),
  KEY `mod_order` (`mod_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('17', 'Menu - Module', 'menus', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '10');
INSERT INTO `modules` VALUES ('4', 'Category - Module', 'categories_multi', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '1');
INSERT INTO `modules` VALUES ('12', 'Banner - Module', 'banners', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '7');
INSERT INTO `modules` VALUES ('7', 'Tin tức - Module', 'news_multi', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '3');
INSERT INTO `modules` VALUES ('11', 'Trang tĩnh - Module', 'statics_multi', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '8');
INSERT INTO `modules` VALUES ('5', 'Set - Module', 'set_multi', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '5');
INSERT INTO `modules` VALUES ('10', 'Introduce - Module', 'introduces', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '6');
INSERT INTO `modules` VALUES ('6', 'Sản phẩm - Module', 'products_multi', 'Thêm mới|Sửa đổi', 'add.php|listing.php', '2');
INSERT INTO `modules` VALUES ('31', 'Đơn hàng - Module', 'orders', 'Danh sách', 'listing.php', '4');
INSERT INTO `modules` VALUES ('30', 'Liên hệ - Module', 'contacts', 'Danh sách', 'listing.php', '9');

-- ----------------------------
-- Table structure for news_hits
-- ----------------------------
DROP TABLE IF EXISTS `news_hits`;
CREATE TABLE `news_hits` (
  `nh_id` int(11) NOT NULL DEFAULT '0',
  `nh_hits` int(11) DEFAULT '0',
  PRIMARY KEY (`nh_id`),
  KEY `nh_hits` (`nh_hits`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_hits
-- ----------------------------
INSERT INTO `news_hits` VALUES ('1', '0');
INSERT INTO `news_hits` VALUES ('2', '0');
INSERT INTO `news_hits` VALUES ('3', '0');
INSERT INTO `news_hits` VALUES ('4', '0');
INSERT INTO `news_hits` VALUES ('5', '0');
INSERT INTO `news_hits` VALUES ('12', '0');
INSERT INTO `news_hits` VALUES ('13', '0');
INSERT INTO `news_hits` VALUES ('14', '0');
INSERT INTO `news_hits` VALUES ('15', '0');
INSERT INTO `news_hits` VALUES ('16', '0');
INSERT INTO `news_hits` VALUES ('17', '0');

-- ----------------------------
-- Table structure for news_multi
-- ----------------------------
DROP TABLE IF EXISTS `news_multi`;
CREATE TABLE `news_multi` (
  `new_id` int(11) NOT NULL AUTO_INCREMENT,
  `new_category_id` int(11) DEFAULT NULL,
  `new_tag_id` varchar(255) DEFAULT NULL,
  `new_title` varchar(255) DEFAULT NULL,
  `new_picture` varchar(225) DEFAULT NULL,
  `new_picture_width` int(11) DEFAULT '0',
  `new_picture_height` int(11) DEFAULT '0',
  `new_meta_title` varchar(255) DEFAULT NULL,
  `new_meta_keyword` varchar(255) DEFAULT NULL,
  `new_meta_description` varchar(255) DEFAULT NULL,
  `new_teaser` text,
  `new_description` mediumtext,
  `new_latest` tinyint(1) DEFAULT '0',
  `new_hot` tinyint(1) DEFAULT '0',
  `new_date` int(11) DEFAULT '0',
  `new_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`new_id`),
  KEY `new_category_id` (`new_category_id`),
  KEY `new_date` (`new_date`),
  KEY `new_latest` (`new_latest`),
  KEY `new_hot` (`new_hot`),
  KEY `new_active` (`new_active`),
  KEY `new_tag_id` (`new_tag_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of news_multi
-- ----------------------------
INSERT INTO `news_multi` VALUES ('16', '5', null, 'Phong cách nào phù hợp nhất khi thiết kế nội nhà chung cư diện tích nhỏ?', 'aoh1501217304.jpg', '649', '649', '', '', '', 'Dù đang sở hữu căn hộ chung cư diện tích nhỏ, hẹp nhưng bạn vẫn hoàn toàn có thể thể hiện phong cách và sáng tạo để tạo nên vẻ đẹp cho không gian sống hoàn hảo, đáng ngưỡng mộ. Tất cả sẽ trở lên vô cùng dễ dàng với thiết kế nhà chung cư diện tích nhỏ MoreHome.', '<p>Dù đang sở hữu căn hộ chung cư diện tích nhỏ, hẹp nhưng bạn vẫn hoàn toàn có thể thể hiện phong cách và sáng tạo để tạo nên vẻ đẹp cho không gian sống hoàn hảo, đáng ngưỡng mộ. Tất cả sẽ trở lên vô cùng dễ dàng với thiết kế nhà chung cư diện tích nhỏ MoreHome.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"400\" src=\"/upload_images/images/thiet-ke-noi-that.jpg\" width=\"600\" /></p>\r\n\r\n<p>Thiết kế nội thất căn hộ nhỏ thật sự khó khăn hơn rất nhiều so với các căn hộ diện tích vừa phải và rộng rãi vì không có nhiều không gian để bạn sáng tạo, bố trí nhiều đồ đạc. Mà trái lại, bạn cần phải rút gọn tối đa, sắp xếp khéo léo nhất để tạo được sự thoáng đãng cho toàn bộ căn hộ mà vẫn phải đảm bảo mọi nhu cầu của các thành viên. Do đó, phong cách đơn giản, tiết chế các chi tiết rườm rà chính là lựa chọn tối ưu nhất. Những món đồ sử dụng trong thiết kế nhà chung cư nhỏ cấu tạo đơn giản, các đường nét rõ ràng, không cầu kỳ, khoa trương nhưng ẩn chứa vẻ đẹp tinh tế, độc đáo.</p>\r\n\r\n<p><strong>Thiết kế nội thất chung cư nhỏ đẹp cho từng vị trí</strong></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"500\" src=\"/upload_images/images/thiet-ke-phong-khach-chung-cu-dep.jpg\" width=\"800\" /></p>\r\n\r\n<p>Phòng bếp nhỏ có ưu điểm là tạo được không khí ấm cúng, cảm giác xum họp gia đình. Giải pháp tốt nhất cho chiều ngang hẹp đó là sử dụng thiết kế kệ và tủ bếp hình chữ U. Ưu điểm của loại kệ bếp này là tiết kiệm diện tích vì lối đi ở giữa nên rất tiện lợi. Bát đĩa, đồ dùng nhà bếp được sắp xếp gọn gàng trong tủ, tủ lạnh và lò vi sóng cũng được bố trí ngay cạnh. Gam màu trắng chủ đạo mở rộng không gian, khiến cho màu gỗ càng trở nên nổi bật.</p>\r\n\r\n<p>Thiết kế nhà chung cư nhỏ không thể bỏ qua phòng khách, nơi để gia chủ đón tiếp các vị khách đến chơi nhà và thể hiện được phong cách, niềm tự hào của bản thân. Một bộ bàn ghế sopha màu sắc trang nhã với những chi tiết đơn giản cũng đủ để căn phòng sang trọng và nổi bật. Trong phòng khách bạn có thể bầy thêm kệ và tivi, một số khung hình trang trí trên nên sơn tường trắng tinh khôi. Với những không gian hẹp, màu sắc nhẹ nhàng là phù hợp nhất, bạn không nên sử dụng các màu rực rỡ vì dễ làm gia tăng không khí ngột ngạt, cảm giác chật hẹp.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"358\" src=\"/upload_images/images/img2.jpg\" width=\"600\" /></p>\r\n\r\n<p>Mặc dù nhỏ nhưng bạn vẫn có thể sử dụng vách ngăn để phân chia các không gian phòng khách và phòng ăn. Để tăng thêm tính thẩm mỹ, bạn nên sử dụng tấm ngăn có họa tiết trang trí bắt mắt hoặc biến tấm ngăn thành một kệ đồ nhỏ nhắn, tiện dụng.</p>\r\n\r\n<p>Hãy đưa thiên nhiên đến với không gian sống của bạn qua những chậu cây cảnh, cây bonsai xanh biếc nhằm tạo không khí trong lành, tâm hồn thư thái nhất. Nếu còn đang băn khoăn lựa chọn các thiết kế nhà chung cư nhỏ đẹp phù hợp với căn hộ của gia đình mình, các bạn hãy liên hệ tới MoreHome qua hotline: 0975 438 686 để được tư vấn và hướng dẫn chi tiết.</p>', '0', '0', '1501217052', '1', '1');
INSERT INTO `news_multi` VALUES ('17', '5', null, 'Những tuyệt chiêu thiết kế nội thất chung cư 45m2 đẹp và hợp lý', 'vlv1501225023.jpg', '600', '600', 'Những tuyệt chiêu thiết kế nội thất chung cư 45m2 đẹp và hợp lý', '', '', 'Mua nhà chung cư hiện thời là giải pháp kinh tế nhất cho các hộ gia đình. Những căn hộ với diện tích nhỏ với diện tích khoảng 45m2, 50m2 là lựa chọn tối ưu cho những gia đình trẻ hoặc những người không có kinh tế cao.', '<p>Mua nhà chung cư hiện thời là giải pháp kinh tế nhất cho các hộ gia đình. Những căn hộ với diện tích nhỏ với diện tích khoảng 45m2, 50m2 là lựa chọn tối ưu cho những gia đình trẻ hoặc những người không có kinh tế cao. Tuy nhiên với diện tích căn hộ nhỏ như vậy thì vấn đề đặt ra là làm sao để thiết kế nội thất hợp lý nhưng vẫn phải đảm bảo đầy đủ công năng thì không hề dễ dàng. Nội thất Morehome là đơn vị chuyên thiết kế và thi công nội thất với nhiều năm kinh nghiệm sẽ gợi ý cho bạn những tuyệt chiêu dưới đây để có thể thiết kế nội thất căn hộ chung cư 45m2 một cách hợp lý và khoa học.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"400\" src=\"/upload_images/images/thiet-ke-noi-that-can-ho-masteri%20(1)_1.jpg\" width=\"600\" /></p>\r\n\r\n<p><strong>Về màu sắc:</strong><br />\r\nVì diện tích căn hộ chỉ 45m2 khá nhỏ nên thông thường ta nên sử dụng gam màu sáng sẽ khiến không gian trở nên rộng rãi, thoáng đãng hơn như màu trắng, sữa, kem, vàng kem&hellip;.Tuy nhiên cũng không nên lạm dụng gam màu sáng quá nhiều sẽ khiến cả căn hộ trở nên lạnh lẽo, đơn điệu. Thay vào đó bạn có thể tạo điểm nhấn bằng những sắc màu chấm phá nổi bật để tạo nên sức sống và cá tính cho căn phòng.</p>\r\n\r\n<p>Ngoài ra, bạn cũng có thể sử dụng những gam màu trung tính để làm mọi thứ trông sạch sẽ, ngăn nắp tại nhà bếp, phòng ăn. Đối với khu vực nhà tắm, vệ sinh, ngoài việc sử dụng đá ốp tường sáng màu tạo sự mở rộng cho không gian thì bạn cũng có thể trang trí bằng những loại đá tương tự nhưng được thêm vào nhiều họa tiết nhỏ đem lại sự tươi mát mà vẫn độc đáo.&nbsp;&nbsp;<br />\r\nMàu sắc phòng ngủ có thể chọn những tông màu như tím hay xanh đậm để khiến căn phòng trông ấm áp và cá tính hơn.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"386\" src=\"/upload_images/images/thiet-ke-noi-that-can-ho-masteri%20(5).jpg\" width=\"600\" /></p>\r\n\r\n<p><strong>Về không gian</strong><br />\r\n&nbsp;Không gian trong thiết kế chung cư 45m2 thường là không gian mở với phòng khách bếp - ăn liên thông nhau tạo sự thông thoáng, tăng diện tích sử dụng và cảm giác căn hộ như rộng hơn bởi với diện tích hạn chế như vậy thì việc phân chia phòng là rất khó thực hiện. Phòng bếp liền kề luôn với phòng khách, tránh có nhiều tường sẽ hạn chế không gian và không nhất thiết phải có quầy bar hay bàn đảo. Không gian bếp nên nhỏ gọn, đơn giản.<br />\r\nVới phòng ngủ, để có không gian riêng tư trong 1 diện tích nhỏ thì bạn nên sử dụng vách ngăn như vách ngăn kính hoặc gỗ để vừa tạo nên sự thông thoáng mà vẫn đảm bảo sự riêng tư.</p>\r\n\r\n<p><strong>Về nội thất</strong><br />\r\nNội thất sử dụng trong thiết kế căn hộ chung cư 45m2 phải hướng đến sự tối giản, giảm bớt các đồ đạc không cần thiết nhưng phải hài hòa với cấu trúc căn hộ về màu sắc, hình dáng, kích thước và vị trí hợp lý.</p>\r\n\r\n<p>Bên cạnh đó, hãy lưu ý đến chức năng, công dụng và mục đích sử dụng của đồ vật. Bạn nên lựa chọn những loại vật dụng, đồ nội thất có màu sắc trang nhã, ít màu để tô điểm cho cùng một không gian trong căn nhà.</p>\r\n\r\n<p>Những vật dụng đơn giản với những chi tiết, hoa văn nhã nhặn, tinh tế, đơn giản sẽ giúp không gian của bạn gọn gàng và ngăn nắp hơn. Không nên lựa chọn những đồ nội thất cầu kì, rườm rà bởi chúng chỉ khiến cho căn phòng nhỏ thêm chật chội và rối rắm hơn.</p>\r\n\r\n<p>Một mẹo thông minh khi chọn nội thất chính là sử dụng những đồ vật &ldquo;đa di năng&rdquo;, những món đồ nội thất thông minh, tích hợp nhiều tiện ích trong một như:&nbsp; một chiếc giường có gầm gồm các ngăn kéo chứa đồ, băng đọc sách vừa là kệ sách vừa là ghế ngồi, bàn uống nước cũng đồng thời là bàn làm việc hay chiếc giường tầng, giường thông minh, giường gấp&hellip; đều là những gợi ý đắt giá giúp bạn tiết kiệm không gian mà vẫn đảm bảo tiện ích cuộc sống.&nbsp; &nbsp;</p>\r\n\r\n<p>Ngoài ra, bạn cũng có thể sử dụng chiếc gương có khổ lớn phù hợp với diện tích tường nhà để mở rộng diện tích. Chiếc gương không chỉ là trợ thủ đắc lực mà còn là vật dụng trang trí đẹp mắt và hữu dụng.</p>', '0', '0', '1501224826', '1', '1');
INSERT INTO `news_multi` VALUES ('15', '5', null, 'Giải pháp tuyệt vời khi thiết kế bếp chung cư nhỏ', 'lam1501214774.jpg', '560', '397', 'Giải pháp tuyệt vời khi thiết kế bếp chung cư nhỏ', '', '', 'Những căn hộ với diện tích khiêm tốn luôn khiến bạn phải bối rối không biết bố trí sao cho ngăn nắp, hợp lý mà vẫn đảm bảo được công năng và không gian thoải mái cho các thành viên trong gia đình.', '<p>Những căn hộ với diện tích khiêm tốn luôn khiến bạn phải bối rối không biết bố trí sao cho ngăn nắp, hợp lý mà vẫn đảm bảo được công năng và không gian thoải mái cho các thành viên trong gia đình. Đặc biệt là khu phòng bếp &ndash; không gian nấu nướng và sinh hoạt chung đòi hỏi phải có sự đầu tư kỹ lưỡng. Hiểu được điều này, MoreHome sẽ đưa đến cho bạn những gợi ý hữu ích trong thiết kế bếp chung cư nhỏ ngay dưới đây.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"434\" src=\"/upload_images/images/image001_1.jpg\" width=\"600\" /></p>\r\n\r\n<p><strong>Lưu ý khi thiết kế bếp chung cư nhỏ</strong><br />\r\nThiết kế nội thất chung cư diện tích nhỏ khó khăn hơn rất nhiều với các căn hộ diện tích rộng bởi sự nhỏ hẹp về không gian rất khó để bạn bố trí đầy đủ các đồ cần thiết mà không bị ngột ngạt, bừa bộn. Một điều hiển nhiên là căn bếp nhỏ không thể có nhiều không gian để sử dụng, không những thế còn có nhiều góc chết rất khó xử lý.</p>\r\n\r\n<p>Chính vì vậy khi thiết kế căn hộ chung cư diện tích nhỏ nói chung cũng như cho phòng bếp, người thiết kế phải thật sự khéo léo, biết cách tận dụng mọi khoảng không và sự tinh tế khi biến những góc tưởng như không thể làm gì trở lên hữu ích và độc đáo.</p>\r\n\r\n<p><strong>Giải pháp thiết kế bếp căn hộ chung cư nhỏ hoàn hảo nhất</strong><br />\r\nPhần kệ bếp và tủ bếp luôn là điểm chú ý đầu tiên trong căn phòng. Với diện tích nhỏ, bố trí kệ hình chữ U hoặc kệ dài 2 tầng là phù hợp nhất. Tất cả mọi tính năng đều vẫn đầy đủ như chậu nước, bếp ga hoặc bếp điện, tủ bếp, nếu căn phòng quá nhỏ thì đồ nội thất cũng cần thiết kế nhỏ tương ứng. Tủ lạnh, lò vi sóng được khéo léo lắp đặt kết hợp với tủ để không phát sinh thêm diện tích sử dụng.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"518\" src=\"/upload_images/images/image003_1.jpg\" width=\"600\" /></p>\r\n\r\n<p>Một khu vực để đặt bàn ăn không quá rộng rãi nên sử dụng những chiếc bàn tròn nhỏ xinh là lựa chọn thông minh. Bàn tròn thích hợp với các góc nhỏ nên được sử dụng phổ biến trong thiết kế nội thất chung cư nhỏ. Không chỉ thế chúng còn giúp các thành viên xích lại gần nhau hơn, tạo không khí thân mật, ấm cúng cho gia đình. Tuy nhiên với các căn hộ có chiều dài lớn, chiều rộng hẹp, bạn có thể dùng bàn ăn dài để đảm bảo sự cân đối, hài hòa và phù hợp hơn với diện tích của từng căn nhà.</p>\r\n\r\n<p>Khi thiết kế bếp chung cư nhỏ cũng đừng quên việc trang trí để tạo nên vẻ đẹp và thể hiện phong cách của gia chủ. Một ô cửa sổ nhỏ vừa lấy ánh sáng thiên nhiên cho căn phòng bừng sáng, vừa là nơi để&nbsp; bạn đặt những chậu hoa nhỏ nhắn, tô điểm cho không gian thêm xinh tươi, dễ chịu.</p>\r\n\r\n<p>Để đảm bảo có được một thiết kế bếp chung cư nhỏ ấn tượng, hoàn hảo với chính căn hộ của bạn, hãy liên hệ với Morehome ngay qua hotline: 0975 438 686 để các chuyên gia thiết kế có thể giúp đỡ và cho bạn nhiều lời khuyên hữu ích.</p>', '0', '0', '1501214643', '1', '1');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `ord_id` int(11) NOT NULL AUTO_INCREMENT,
  `ord_user_id` int(11) DEFAULT '0',
  `ord_code` varchar(255) DEFAULT NULL,
  `ord_detail` varchar(255) DEFAULT NULL,
  `ord_company` varchar(255) DEFAULT NULL,
  `ord_name` varchar(100) DEFAULT NULL,
  `ord_address` varchar(100) DEFAULT NULL,
  `ord_email` varchar(100) DEFAULT NULL,
  `ord_phone` varchar(100) DEFAULT NULL,
  `ord_date` int(11) DEFAULT '0',
  `ord_last_update` int(11) DEFAULT '0',
  `ord_note` text,
  `ord_status` int(1) DEFAULT '0',
  `ord_total_money` double DEFAULT '0',
  `ord_active` tinyint(3) DEFAULT '0',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ord_id`),
  KEY `ord_user_id` (`ord_user_id`) USING BTREE,
  KEY `ord_code` (`ord_code`) USING BTREE,
  KEY `ord_status` (`ord_status`) USING BTREE,
  KEY `ord_date` (`ord_date`) USING BTREE,
  KEY `ord_active` (`ord_active`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('17', '-1', 'hungnv_20170804', 'Mua hang', '', 'hungnv', 'Ha Noi', 'imhung1179@gmail.com', '0963453437', '1501786293', '1501788948', 'Đã xử lý', '1', '6000000', '0', '1');

-- ----------------------------
-- Table structure for orders_product
-- ----------------------------
DROP TABLE IF EXISTS `orders_product`;
CREATE TABLE `orders_product` (
  `op_order_id` int(11) NOT NULL DEFAULT '0',
  `op_product_id` int(11) NOT NULL DEFAULT '0',
  `op_price` double DEFAULT '0',
  `op_quantity` int(11) DEFAULT '0',
  `op_date` int(11) DEFAULT '0',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`op_order_id`,`op_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders_product
-- ----------------------------
INSERT INTO `orders_product` VALUES ('17', '5', '3000000', '2', '1501786293', '1');

-- ----------------------------
-- Table structure for products_multi
-- ----------------------------
DROP TABLE IF EXISTS `products_multi`;
CREATE TABLE `products_multi` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_category_id` int(11) DEFAULT '0',
  `pro_name` varchar(255) DEFAULT NULL,
  `pro_picture` varchar(225) DEFAULT NULL,
  `pro_picture_width` int(11) DEFAULT '0',
  `pro_picture_height` int(11) DEFAULT '0',
  `pro_price` double DEFAULT '0',
  `pro_size` varchar(100) DEFAULT NULL,
  `pro_color` varchar(255) DEFAULT NULL,
  `pro_style` varchar(255) DEFAULT NULL,
  `pro_shipping` varchar(255) DEFAULT NULL,
  `pro_quantity` int(11) DEFAULT '0',
  `pro_teaser` text,
  `pro_rename` varchar(255) DEFAULT NULL,
  `pro_title` varchar(255) DEFAULT NULL,
  `pro_meta_keyword` varchar(255) DEFAULT NULL,
  `pro_meta_description` varchar(255) DEFAULT NULL,
  `pro_description` mediumtext,
  `pro_shipping_info` mediumtext,
  `pro_hot` tinyint(1) DEFAULT '0',
  `pro_date` int(11) DEFAULT '0',
  `pro_last_update` int(11) DEFAULT NULL,
  `pro_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`pro_id`),
  KEY `pro_category_id` (`pro_category_id`),
  KEY `pro_date` (`pro_date`),
  KEY `pro_hot` (`pro_hot`),
  KEY `pro_active` (`pro_active`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of products_multi
-- ----------------------------
INSERT INTO `products_multi` VALUES ('1', '2', 'Ghế Bench Acorn', 'ucu1501065796.jpg', '1000', '1000', '4290000', 'Dài 110.8 x Rộng 41 x Cao 59 cm', 'Nâu', 'Default', 'Giao hàng tận nơi, miễn phí đổi trả', '15', 'Mang phong cách Mid-Century, chân ghế Bench Acorn làm từ gỗ sồi kết hợp thùng tủ từ gỗ MDF.', 'Ghe Bench Acorn', '', '', '', '<p>Về công năng, ghế Bench Acorn vừa là ghế ngồi vừa là tủ lưu trữ đồ; ngoài ra, kích thước bench Acorn nhỏ gọn giúp bạn dễ dàng di chuyển để bố trí trong không gian sống.</p>\r\n\r\n<p>Về thẩm mỹ, ghế Bench Acorn có kiểu dáng cổ điển, hộc tủ làm bằng MDF và chân chống được làm bằng gỗ sồi tự nhiên chắc chắn. Đặc biệt với tổng thể tinh tế, hài hòa, Bench Acorn sẽ là trợ thủ đắc lực giúp bạn tô điểm làm đẹp thêm cho không gian sống.</p>', '<p style=\"text-align: center;\"><strong>Thời gian giao hàng</strong></p>\r\n\r\n<p style=\"text-align: center;\">Thời gian giao hàng: Khu vực Hồ Chí Minh: 7 &ndash; 10 ngày. Khu vực Hà Nội: 14 &ndash; 17 ngày. Các khu vực khác sẽ báo cụ thể ngay sau khi đặt hàng.</p>\r\n\r\n<p style=\"text-align: center;\">Ngoài ra đối với sofa có yêu cầu vải đặc biệt cần nhập khẩu, thời gian giao hàng sẽ cộng thêm 5 &ndash; 7 ngày.</p>\r\n\r\n<p style=\"text-align: center;\"><strong>Giao hàng tận nhà.</strong></p>\r\n\r\n<p style=\"text-align: center;\">Giao hàng toàn quốc.</p>\r\n\r\n<p style=\"text-align: center;\">Hàng hóa được vận chuyển bằng đường bộ đến tận nhà khách hàng.</p>\r\n\r\n<p style=\"text-align: center;\">Thời gian nhận hàng sẽ được thông báo trước ( từ thứ 2 đến thứ 7) tùy vào vùng miền.</p>\r\n\r\n<p style=\"text-align: center;\">Đơn vị giao hàng sẽ gọi cho bạn trước 4 tiếng.</p>\r\n\r\n<p style=\"text-align: center;\">Món hàng bạn đặt sẽ được vận chuyển tới căn phòng bạn chọn.</p>\r\n\r\n<p style=\"text-align: center;\">Miễn phí vận chuyển đối với khách hàng tại nội thành khu vực Hồ Chí Minh.</p>\r\n\r\n<p style=\"text-align: center;\"><strong>Chính sách đổi trả hàng đã mua</strong></p>\r\n\r\n<p style=\"text-align: center;\">Chúng tôi rất tự hào về chất lượng nội thất của Mitssy. Tuy nhiên trong quá trình nhận hàng nếu gặp phải bất kì lý do gì do lỗi trong quá trình sản xuất, chúng tôi đảm bảo sẽ tiến hàng thực hiện các biện pháp khắc phục lỗi (kể cả đổi trả hàng) cho đến khi quý khách thực sự hài lòng.</p>\r\n\r\n<p style=\"text-align: center;\">LƯU Ý: chúng tôi sẽ không nhận hoàn trả những đơn hàng về nệm, nội thất đã được thiết kế riêng như in chữ, bị hư hỏng do khách hàng. Chính sách đổi trả cũng sẽ không áp dụng cho nhưng đơn hàng giảm giá.</p>', '1', '1501065511', '1501076582', '1', '1');
INSERT INTO `products_multi` VALUES ('2', '2', 'Ghế Studio', 'mzv1501067622.jpg', '1000', '1000', '5000000', 'W650 D650 H770 MM', 'Xanh, đỏ', '', 'Giao hàng tận nơi, miễn phí đổi trả', '20', 'Khung ghế bằng gỗ được đẽo gọt tỉ mỉ, tăng thẩm mĩ cho không gian xung quanh. Màu: Xanh, đỏ', 'Ghe Studio', '', '', '', '<p>Ghế Studio lấy cảm hứng từ chiếc ghế huyền thoại mang tên Peacock được thiết kế từ năm 1947 bởi Hans Wegner.</p>\r\n\r\n<p>Khung ghế được làm hoàn toàn bằng chất liệu gỗ tự nhiên, nhưng bạn sẽ không khỏi ngạc nhiên vì độ thanh mảnh và mềm mại của sản phẩm, các chi tiết đều đều được tính toán kỹ càng để tạo độ mỏng nhưng khả năng chịu lực hoàn toàn đáp ứng được một cách tối ưu.</p>\r\n\r\n<p>Ghế gỗ Studio phù hợp với nhiều không gian đặc biệt dành để thư giãn đọc sách.</p>', '<p style=\"text-align: center;\"><strong>Thời gian giao hàng</strong></p>\r\n\r\n<p style=\"text-align: center;\">Thời gian giao hàng: Khu vực Hồ Chí Minh: 7 &ndash; 10 ngày. Khu vực Hà Nội: 14 &ndash; 17 ngày. Các khu vực khác sẽ báo cụ thể ngay sau khi đặt hàng.</p>\r\n\r\n<p style=\"text-align: center;\">Ngoài ra đối với sofa có yêu cầu vải đặc biệt cần nhập khẩu, thời gian giao hàng sẽ cộng thêm 5 &ndash; 7 ngày.</p>\r\n\r\n<p style=\"text-align: center;\"><strong>Giao hàng tận nhà.</strong></p>\r\n\r\n<p style=\"text-align: center;\">Giao hàng toàn quốc.</p>\r\n\r\n<p style=\"text-align: center;\">Hàng hóa được vận chuyển bằng đường bộ đến tận nhà khách hàng.</p>\r\n\r\n<p style=\"text-align: center;\">Thời gian nhận hàng sẽ được thông báo trước ( từ thứ 2 đến thứ 7) tùy vào vùng miền.</p>\r\n\r\n<p style=\"text-align: center;\">Đơn vị giao hàng sẽ gọi cho bạn trước 4 tiếng.</p>\r\n\r\n<p style=\"text-align: center;\">Món hàng bạn đặt sẽ được vận chuyển tới căn phòng bạn chọn.</p>\r\n\r\n<p style=\"text-align: center;\">Miễn phí vận chuyển đối với khách hàng tại nội thành khu vực Hồ Chí Minh.</p>\r\n\r\n<p style=\"text-align: center;\"><strong>Chính sách đổi trả hàng đã mua</strong></p>\r\n\r\n<p style=\"text-align: center;\">Chúng tôi rất tự hào về chất lượng nội thất của Mitssy. Tuy nhiên trong quá trình nhận hàng nếu gặp phải bất kì lý do gì do lỗi trong quá trình sản xuất, chúng tôi đảm bảo sẽ tiến hàng thực hiện các biện pháp khắc phục lỗi (kể cả đổi trả hàng) cho đến khi quý khách thực sự hài lòng.</p>\r\n\r\n<p style=\"text-align: center;\">LƯU Ý: chúng tôi sẽ không nhận hoàn trả những đơn hàng về nệm, nội thất đã được thiết kế riêng như in chữ, bị hư hỏng do khách hàng. Chính sách đổi trả cũng sẽ không áp dụng cho nhưng đơn hàng giảm giá.</p>', '1', '1501067448', '1501076576', '1', '1');
INSERT INTO `products_multi` VALUES ('3', '3', 'Sofa 2 chỗ Rounded', 'jfe1501067878.jpg', '1000', '1000', '6000000', 'W165 x D89 x H84', 'Xám', 'Modern', 'Giao hàng: 14 - 16 ngày (vải Việt Nam), 20 - 25 ngày (vải nhập khẩu)', '10', '', 'Sofa 2 cho Rounded', '', '', '', '<p>Sofa Rounded đơn giản nhưng vẫn tinh tế với hàng nút được đơm cẩn thận cùng đệm dày êm mang đến sự phóng khoáng và tiện nghi cho không gian sinh hoạt.</p>', '<p style=\"text-align: center;\"><strong>Thời gian giao hàng</strong></p>\r\n\r\n<p style=\"text-align: center;\">Thời gian giao hàng: Khu vực Hồ Chí Minh: 7 &ndash; 10 ngày. Khu vực Hà Nội: 14 &ndash; 17 ngày. Các khu vực khác sẽ báo cụ thể ngay sau khi đặt hàng.</p>\r\n\r\n<p style=\"text-align: center;\">Ngoài ra đối với sofa có yêu cầu vải đặc biệt cần nhập khẩu, thời gian giao hàng sẽ cộng thêm 5 &ndash; 7 ngày.</p>\r\n\r\n<p style=\"text-align: center;\"><strong>Giao hàng tận nhà.</strong></p>\r\n\r\n<p style=\"text-align: center;\">Giao hàng toàn quốc.</p>\r\n\r\n<p style=\"text-align: center;\">Hàng hóa được vận chuyển bằng đường bộ đến tận nhà khách hàng.</p>\r\n\r\n<p style=\"text-align: center;\">Thời gian nhận hàng sẽ được thông báo trước ( từ thứ 2 đến thứ 7) tùy vào vùng miền.</p>\r\n\r\n<p style=\"text-align: center;\">Đơn vị giao hàng sẽ gọi cho bạn trước 4 tiếng.</p>\r\n\r\n<p style=\"text-align: center;\">Món hàng bạn đặt sẽ được vận chuyển tới căn phòng bạn chọn.</p>\r\n\r\n<p style=\"text-align: center;\">Miễn phí vận chuyển đối với khách hàng tại nội thành khu vực Hồ Chí Minh.</p>\r\n\r\n<p style=\"text-align: center;\"><strong>Chính sách đổi trả hàng đã mua</strong></p>\r\n\r\n<p style=\"text-align: center;\">Chúng tôi rất tự hào về chất lượng nội thất của Mitssy. Tuy nhiên trong quá trình nhận hàng nếu gặp phải bất kì lý do gì do lỗi trong quá trình sản xuất, chúng tôi đảm bảo sẽ tiến hàng thực hiện các biện pháp khắc phục lỗi (kể cả đổi trả hàng) cho đến khi quý khách thực sự hài lòng.</p>\r\n\r\n<p style=\"text-align: center;\">LƯU Ý: chúng tôi sẽ không nhận hoàn trả những đơn hàng về nệm, nội thất đã được thiết kế riêng như in chữ, bị hư hỏng do khách hàng. Chính sách đổi trả cũng sẽ không áp dụng cho nhưng đơn hàng giảm giá.</p>', '1', '1501067718', '1501076569', '1', '1');
INSERT INTO `products_multi` VALUES ('4', '3', 'Sofa 2 chỗ Peggy', 'kld1501131704.jpg', '1000', '1000', '8000000', 'w1600 x d900 x h860', 'Xám, đỏ, xanh', 'Modern', 'Giao hàng: 14 - 16 ngày (vải Việt Nam), 20 - 25 ngày (vải nhập khẩu)', '5', 'Sofa Peggy với hàng nút nhấn retro cổ điển và đường nét thẳng thớm hiện đại, dễ dàng phối hợp với mọi không gian.', 'Sofa 2 cho Peggy', '', '', '', '<p>Sofa Peggy mang phong cách Mid-Cetury với hàng nút ở lưng tựa - đệm ngồi cùng chân gỗ cao tiện tròn. Ở sofa Peggy, người ta tìm thấy sự gặp gỡ thú vị giữa tinh thần cổ điển và nét đẹp hiện đại. Đó là vì sự kết hợp liền mạch giữa kiểu dáng đơn giản với những chi tiết nhỏ của phong cách Retro như đường chỉ ống khéo léo bo quanh thân ghế, những hàng nút nhấn ở gối tựa lưng và nệm ngồi.</p>\r\n\r\n<p>Dòng sofa Peggy gồm 4 loại: sofa 2 chỗ, 3 chỗ và sofa góc nhỏ, sofa góc lớn.</p>', '<p><strong>Thời gian giao hàng</strong></p>\r\n\r\n<p>Thời gian giao hàng: Khu vực Hồ Chí Minh: 7 &ndash; 10 ngày. Khu vực Hà Nội: 14 &ndash; 17 ngày. Các khu vực khác sẽ báo cụ thể ngay sau khi đặt hàng.</p>\r\n\r\n<p>Ngoài ra đối với sofa có yêu cầu vải đặc biệt cần nhập khẩu, thời gian giao hàng sẽ cộng thêm 5 &ndash; 7 ngày.</p>\r\n\r\n<p><strong>Giao hàng tận nhà.</strong></p>\r\n\r\n<p>Giao hàng toàn quốc.</p>\r\n\r\n<p>Hàng hóa được vận chuyển bằng đường bộ đến tận nhà khách hàng.</p>\r\n\r\n<p>Thời gian nhận hàng sẽ được thông báo trước ( từ thứ 2 đến thứ 7) tùy vào vùng miền.</p>\r\n\r\n<p>Đơn vị giao hàng sẽ gọi cho bạn trước 4 tiếng.</p>\r\n\r\n<p>Món hàng bạn đặt sẽ được vận chuyển tới căn phòng bạn chọn.</p>\r\n\r\n<p>Miễn phí vận chuyển đối với khách hàng tại nội thành khu vực Hồ Chí Minh.</p>\r\n\r\n<p><strong>Chính sách đổi trả hàng đã mua</strong></p>\r\n\r\n<p>Chúng tôi rất tự hào về chất lượng nội thất của Mitssy. Tuy nhiên trong quá trình nhận hàng nếu gặp phải bất kì lý do gì do lỗi trong quá trình sản xuất, chúng tôi đảm bảo sẽ tiến hàng thực hiện các biện pháp khắc phục lỗi (kể cả đổi trả hàng) cho đến khi quý khách thực sự hài lòng.</p>\r\n\r\n<p>LƯU Ý: chúng tôi sẽ không nhận hoàn trả những đơn hàng về nệm, nội thất đã được thiết kế riêng như in chữ, bị hư hỏng do khách hàng. Chính sách đổi trả cũng sẽ không áp dụng cho nhưng đơn hàng giảm giá.</p>', '1', '1501131555', '1501131704', '1', '1');
INSERT INTO `products_multi` VALUES ('5', '2', 'Ghế Lexi', 'set1501136829.jpg', '1000', '1000', '3000000', 'Dài 715mm cao 650mm rộng 710mm', 'Xám', 'Natural', 'Giao hàng: 14 - 16 ngày (vải Việt Nam)', '6', 'Khung ghế gỗ sồi uốn', 'Ghe Lexi', '', '', '', '<p>Đôn dài Gillesy mang nét cổ điển từ nệm giựt lún đến màu chân nâu sậm. Bên cạnh đó, Gillesy vẫn có cá tính riêng với kết cấu chân 4 góc cá tính.</p>', '<p><strong>Thời gian giao hàng</strong></p>\r\n\r\n<p>Thời gian giao hàng: Khu vực Hồ Chí Minh: 7 &ndash; 10 ngày. Khu vực Hà Nội: 14 &ndash; 17 ngày. Các khu vực khác sẽ báo cụ thể ngay sau khi đặt hàng.</p>\r\n\r\n<p>Ngoài ra đối với sofa có yêu cầu vải đặc biệt cần nhập khẩu, thời gian giao hàng sẽ cộng thêm 5 &ndash; 7 ngày.</p>\r\n\r\n<p><strong>Giao hàng tận nhà.</strong></p>\r\n\r\n<p>Giao hàng toàn quốc.</p>\r\n\r\n<p>Hàng hóa được vận chuyển bằng đường bộ đến tận nhà khách hàng.</p>\r\n\r\n<p>Thời gian nhận hàng sẽ được thông báo trước ( từ thứ 2 đến thứ 7) tùy vào vùng miền.</p>\r\n\r\n<p>Đơn vị giao hàng sẽ gọi cho bạn trước 4 tiếng.</p>\r\n\r\n<p>Món hàng bạn đặt sẽ được vận chuyển tới căn phòng bạn chọn.</p>\r\n\r\n<p>Miễn phí vận chuyển đối với khách hàng tại nội thành khu vực Hồ Chí Minh.</p>\r\n\r\n<p><strong>Chính sách đổi trả hàng đã mua</strong></p>\r\n\r\n<p>Chúng tôi rất tự hào về chất lượng nội thất của Mitssy. Tuy nhiên trong quá trình nhận hàng nếu gặp phải bất kì lý do gì do lỗi trong quá trình sản xuất, chúng tôi đảm bảo sẽ tiến hàng thực hiện các biện pháp khắc phục lỗi (kể cả đổi trả hàng) cho đến khi quý khách thực sự hài lòng.</p>\r\n\r\n<p>LƯU Ý: chúng tôi sẽ không nhận hoàn trả những đơn hàng về nệm, nội thất đã được thiết kế riêng như in chữ, bị hư hỏng do khách hàng. Chính sách đổi trả cũng sẽ không áp dụng cho nhưng đơn hàng giảm giá.</p>', '1', '1501136693', '1501136829', '1', '1');

-- ----------------------------
-- Table structure for product_hits
-- ----------------------------
DROP TABLE IF EXISTS `product_hits`;
CREATE TABLE `product_hits` (
  `ph_id` int(11) NOT NULL DEFAULT '0',
  `ph_hits` int(11) DEFAULT '0',
  PRIMARY KEY (`ph_id`),
  KEY `nh_hits` (`ph_hits`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_hits
-- ----------------------------
INSERT INTO `product_hits` VALUES ('1', '0');
INSERT INTO `product_hits` VALUES ('2', '0');
INSERT INTO `product_hits` VALUES ('3', '0');
INSERT INTO `product_hits` VALUES ('4', '0');
INSERT INTO `product_hits` VALUES ('5', '0');
INSERT INTO `product_hits` VALUES ('6', '0');
INSERT INTO `product_hits` VALUES ('7', '0');

-- ----------------------------
-- Table structure for product_pictures
-- ----------------------------
DROP TABLE IF EXISTS `product_pictures`;
CREATE TABLE `product_pictures` (
  `pp_id` int(11) NOT NULL AUTO_INCREMENT,
  `pp_product_id` int(11) DEFAULT '0',
  `pp_name` varchar(255) DEFAULT NULL,
  `pp_picture` varchar(255) DEFAULT NULL,
  `pp_picture_width` int(11) DEFAULT '0',
  `pp_picture_height` int(11) DEFAULT '0',
  `pp_order` double DEFAULT '0',
  `pp_temp` char(50) DEFAULT NULL,
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`pp_id`),
  KEY `pp_product_id` (`pp_product_id`),
  KEY `pp_order` (`pp_order`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of product_pictures
-- ----------------------------
INSERT INTO `product_pictures` VALUES ('6', '2', 'ST1', 'ezo1501067497.jpg', '1000', '1000', '1', null, '1');
INSERT INTO `product_pictures` VALUES ('7', '3', 'SFR01', 'xru1501067794.jpg', '1000', '1000', '1', null, '1');
INSERT INTO `product_pictures` VALUES ('5', '1', 'BA01', 'lkc1501066752.jpg', '1000', '1000', '1', null, '1');
INSERT INTO `product_pictures` VALUES ('8', '1', 'BA02', 'gvp1501070393.jpg', '1000', '1000', '2', null, '1');
INSERT INTO `product_pictures` VALUES ('9', '4', 'SFPG01', 'ykn1501131600.jpg', '1000', '1000', '1', null, '1');
INSERT INTO `product_pictures` VALUES ('10', '4', 'SFPG02', 'nud1501131612.jpg', '1000', '1000', '2', null, '1');
INSERT INTO `product_pictures` VALUES ('11', '0', 'canho1', 'bjq1501761302.jpg', '1200', '720', '1', '5010V6257s7588w6436', '1');

-- ----------------------------
-- Table structure for set_multi
-- ----------------------------
DROP TABLE IF EXISTS `set_multi`;
CREATE TABLE `set_multi` (
  `set_id` int(11) NOT NULL AUTO_INCREMENT,
  `set_category_id` int(11) NOT NULL DEFAULT '0',
  `set_name` varchar(255) NOT NULL,
  `set_rename` varchar(255) DEFAULT NULL,
  `set_picture` varchar(255) DEFAULT NULL,
  `set_picture_width` int(11) DEFAULT '0',
  `set_picture_height` int(11) DEFAULT '0',
  `set_teaser` varchar(255) DEFAULT NULL,
  `set_description` text,
  `set_temp` varchar(255) DEFAULT NULL,
  `set_date` int(11) DEFAULT '0',
  `set_last_update` int(11) DEFAULT '0',
  `set_hot` tinyint(1) DEFAULT '0',
  `set_active` tinyint(1) DEFAULT '1',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`set_id`),
  KEY `set_category_id` (`set_category_id`) USING BTREE,
  KEY `set_date` (`set_date`) USING BTREE,
  KEY `set_active` (`set_active`) USING BTREE,
  KEY `set_hot` (`set_hot`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_multi
-- ----------------------------
INSERT INTO `set_multi` VALUES ('2', '7', 'Phòng ngủ Bennie cho bé', 'Phong ngu Bennie cho be', 'rrm1501631956.jpg', '1200', '720', '<p>Chuẩn phòng khách Masteri 2 phòng ngủ 71m2<br />\r\nDiện tích: 5m x4m</p>', null, '7636M5548s9907h4927', '1501631899', '1501631956', '1', '1', '1');
INSERT INTO `set_multi` VALUES ('3', '7', 'Phòng khách Norfolk', 'Phong khach Norfolk', 'czh1501632239.jpg', '1200', '720', '<p>Chuẩn phòng khách Masteri 2 phòng ngủ 71m2<br />\r\nDiện tích: 5m x4m</p>', null, '7168A2138a4697e9889', '1501632202', '1501632239', '1', '1', '1');
INSERT INTO `set_multi` VALUES ('4', '8', 'Phòng khách Elena', 'Phong khach Elena', 'rqq1501632297.jpg', '1200', '720', '<p>Chuẩn phòng khách Masteri 2 phòng ngủ 71m2<br />\r\nDiện tích: 5m x4m</p>', null, '6908V7593x3861s2147', '1501632239', '1501632297', '1', '1', '1');
INSERT INTO `set_multi` VALUES ('5', '8', 'Phòng khách Norwich', 'Phong khach Norwich', 'gvi1501632332.jpg', '0', '0', 'Chuẩn phòng khách Masteri 2 phòng ngủ 71m2 - Diện tích: 5m x4m', '<p><strong>Why we like this</strong><br />\r\nThis content wrote in English, This content wrote in English,This content wrote in English,This content wrote in English,This content wrote in English,This content wrote in English,This content wrote in English,This content wrote in English,</p>', '9653N6235h4888k2622', '1501632297', '1501634819', '1', '1', '1');
INSERT INTO `set_multi` VALUES ('6', '9', 'Căn hộ Luxcity 72m2', 'Can ho Luxcity 72m2', 'hio1501761474.jpg', '1200', '720', '', '<p>Những đường nét uốn cong gợi cảm kết hợp cùng màu Rose Gold - màu sắc được các công nương rất ưa thích trong thời kỳ Victorian vì toát lên được vẻ lịch thiệp, quý phái - đã tạo cho Louis Rose Gold một vẻ ngoài ăn nhập theo xu hướng đương đại.</p>', '4272S6308h3840o3161', '1501761321', '1501761474', '0', '1', '1');
INSERT INTO `set_multi` VALUES ('7', '9', 'Căn hộ The Art 60m2', 'Can ho The Art 60m2', 'mcz1501761545.jpg', '1200', '720', '', '<p>Bàn sofa Louis được giới hạn trong hai màu nổi bật trên dòng chảy nội thất đương đại: Rose Gold và Gold. Bàn Louis Gold không mang đến sự lãng mạn như Rose Gold nhưng bật lên vẻ sang trọng. Chất liệu sắt sơn Epoxy đảm bảo cho màu sắc nguyên bản của bàn Louis được giữ trọn vẹn.</p>', '6727L5997w2186g5666', '1501761475', '1501761545', '0', '1', '1');
INSERT INTO `set_multi` VALUES ('8', '10', 'Căn hộ Masteri 70m2 - Chị Lan', 'Can ho Masteri 70m2 - Chi Lan', 'dwk1501761796.jpg', '1000', '664', '', '<p>Góc bo tròn của bàn Randy là ưu điểm được đánh giá cao nhất. Ngoài yếu tố thẩm mỹ, góc bàn mềm mại đem lại sự an toàn cho những gia đình có trẻ em. Khung bàn được cải tiến với phần nối giữa hai khung chân chữ U thay cho khung hộp truyền thống tạo nét mới lạ và chắc chắn.</p>', '6605P4218g4200u7641', '1501761719', '1501761796', '0', '1', '1');
INSERT INTO `set_multi` VALUES ('9', '10', 'Căn Hộ Ehome 80m2 - Anh Tùng', 'Can Ho Ehome 80m2 - Anh Tung', 'hqc1501761924.jpg', '1000', '563', '', '<p>Phát triển từ dòng bàn Box Frame vốn được ưa chuộng tại Mitssy, bàn Marvin với hai thanh đỡ chịu lực cho mặt bàn đá cùng khung chân sắt sơn tĩnh điện có độ kháng trầy cao đẩy giá trị công năng của Marble lên một nấc so với bàn Box Frame cũ.</p>', '1842D2747j8587q8358', '1501761797', '1501761924', '0', '1', '1');

-- ----------------------------
-- Table structure for set_pictures
-- ----------------------------
DROP TABLE IF EXISTS `set_pictures`;
CREATE TABLE `set_pictures` (
  `sp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_product_id` int(11) DEFAULT '0',
  `sp_name` varchar(255) DEFAULT NULL,
  `sp_picture` varchar(255) DEFAULT NULL,
  `sp_picture_width` int(11) DEFAULT '0',
  `sp_picture_height` int(11) DEFAULT '0',
  `sp_order` double DEFAULT '0',
  `sp_temp` char(50) DEFAULT NULL,
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`sp_id`),
  KEY `pp_product_id` (`sp_product_id`),
  KEY `pp_order` (`sp_order`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of set_pictures
-- ----------------------------
INSERT INTO `set_pictures` VALUES ('1', '2', 'BN01', 'mqw1501631930.jpg', '1200', '720', '1', null, '1');
INSERT INTO `set_pictures` VALUES ('2', '6', 'luxcity1', 'sjr1501761396.jpg', '1200', '720', '1', null, '1');
INSERT INTO `set_pictures` VALUES ('3', '8', 'chilan', 'wpp1501761775.jpg', '1000', '664', '1', null, '1');
INSERT INTO `set_pictures` VALUES ('4', '9', 'anhtung', 'dzu1501761909.jpg', '1200', '710', '1', null, '1');

-- ----------------------------
-- Table structure for statics_multi
-- ----------------------------
DROP TABLE IF EXISTS `statics_multi`;
CREATE TABLE `statics_multi` (
  `sta_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sta_category_id` int(11) DEFAULT '0',
  `sta_type` varchar(255) DEFAULT NULL,
  `sta_title` varchar(255) DEFAULT NULL,
  `sta_order` double DEFAULT '0',
  `sta_picture` varchar(255) DEFAULT NULL,
  `sta_picture_alt` varchar(255) DEFAULT NULL,
  `sta_teaser` text,
  `sta_description` text,
  `sta_date` int(11) DEFAULT '0',
  `lang_id` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`sta_id`),
  KEY `sta_category_id` (`sta_category_id`),
  KEY `sta_order` (`sta_order`),
  KEY `sta_date` (`sta_date`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of statics_multi
-- ----------------------------
INSERT INTO `statics_multi` VALUES ('19', '4', 'page', 'Quy trình tư vấn', '4', '', '', '', '<!-- Repsonsive -->\r\n<section class=\"step-list none-pad\" id=\"step_list\">\r\n<h1 class=\"text-center\" style=\"width:100%;float:left;text-transform: none;\">Quy trình hoàn thiện căn hộ</h1>\r\n\r\n<div class=\"container basic text-center\">\r\n<div class=\"row step-1\" style=\"margin-bottom: 20px;\">\r\n<div class=\"col-sm-6 visible-xs step_img\"><img alt=\"\" height=\"861\" src=\"/upload_images/images/qttv-1.jpg\" width=\"861\" /></div>\r\n\r\n<div class=\"col-sm-6\">\r\n<div class=\"qt-title\">\r\n<h3 class=\"title text-uppercase\">Phong cách<br />\r\ncủa bạn là gì?</h3>\r\n</div>\r\n\r\n<div class=\"hiw-step-copy\">\r\n<p>Định hình phong cách nội thất của bạn một cách đơn giản và thú vị với bài trắc nghiệm của Mitssy.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-offset-1 col-sm-5 hidden-xs step_img\"><img alt=\"\" height=\"861\" src=\"/upload_images/images/qttv-1.jpg\" width=\"861\" /></div>\r\n</div>\r\n\r\n<div class=\"row step-2\" style=\"margin-bottom: 20px;\">\r\n<div class=\"col-sm-5 col-xs-12 step_img\"><img alt=\"\" height=\"300\" src=\"/upload_images/images/qttv-2.jpg\" width=\"300\" /></div>\r\n\r\n<div class=\"col-sm-6 col-sm-offset-1\">\r\n<div class=\"qt-title\">\r\n<h3 class=\"title text-uppercase\">Từ phong cách<br />\r\nđến giải pháp</h3>\r\n</div>\r\n\r\n<div class=\"hiw-step-copy\">\r\n<p>Chỉ trong 2 tiếng nói chuyện với Home Stylist, bạn sẽ nhận được gói giải pháp sơ bộ bao gồm: thiết kế (với sản phẩm có tại Mitssy) và báo giá.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row step-3\" style=\"margin-bottom: 20px;\">\r\n<div class=\"col-sm-6 visible-xs step_img\"><img alt=\"\" height=\"300\" src=\"/upload_images/images/qttv-3.jpg\" width=\"300\" /></div>\r\n\r\n<div class=\"col-sm-6\">\r\n<div class=\"qt-title\">\r\n<h3 class=\"title text-uppercase\">Hiện thực hóa giải pháp</h3>\r\n</div>\r\n\r\n<div class=\"hiw-step-copy\">\r\n<p>Giúp bạn có cái nhìn trực quan hơn qua bản vẽ 3D và nhận báo giá chi tiết dựa theo giải pháp.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-5 col-sm-offset-1 hidden-xs step_img\"><img alt=\"\" height=\"300\" src=\"/upload_images/images/qttv-3.jpg\" width=\"300\" /></div>\r\n</div>\r\n\r\n<div class=\"row step-4\" style=\"margin-bottom: 20px;\">\r\n<div class=\"col-sm-5 step_img\"><img alt=\"\" height=\"300\" src=\"/upload_images/images/qttv-4.jpg\" width=\"300\" /></div>\r\n\r\n<div class=\"col-sm-6 col-sm-offset-1\">\r\n<div class=\"qt-title\">\r\n<h3 class=\"title text-uppercase\">Thi công và lắp đặt</h3>\r\n</div>\r\n\r\n<div class=\"hiw-step-copy\">\r\n<p>Tiến độ của công trình luôn được đảm bảo bằng việc Mitssy sẽ cập nhật cho bạn hàng tuần qua Email.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row step-5\" style=\"margin-bottom: 20px;\">\r\n<div class=\"col-sm-6 visible-xs step_img\"><img alt=\"\" height=\"300\" src=\"/upload_images/images/qttv-6.jpg\" style=\"height: auto;\" width=\"300\" /></div>\r\n\r\n<div class=\"col-sm-6\">\r\n<div class=\"qt-title\">\r\n<h3 class=\"title text-uppercase\">Căn hộ đã sẵn sàng<br />\r\nchào đón bạn</h3>\r\n</div>\r\n\r\n<div class=\"hiw-step-copy\">\r\n<p>Cùng gia đình tận hưởng không gian mới riêng tư và ấm cúng theo cách riêng của mình.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-5 col-sm-offset-1 hidden-xs step_img\"><img alt=\"\" height=\"300\" src=\"/upload_images/images/qttv-6.jpg\" style=\"height: auto;\" width=\"300\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"love-us\">\r\n<div class=\"success-stories-text text-center\">\r\n<h2 class=\"heading\">NHỮNG KHÁCH HÀNG CỦA MITSSY</h2>\r\n\r\n<p class=\"sub-heading\">Hơn 150 khách hàng hài lòng. Hơn 150 cuộc sống mới bắt đầu<br />\r\nVà đây là câu chuyện của họ. Còn bạn đã sẵn sàng chưa?<br />\r\n#Bắt Đầu Cuộc Sống Mới Với Mitssy</p>\r\n</div>\r\n\r\n<div class=\"story\">\r\n<div class=\"circle-img-box\">\r\n<div class=\"box-content looks-story\">\r\n<div class=\"circle-img\"><img alt=\"\" class=\"img-responsive\" height=\"250\" src=\"/upload_images/images/hih_1.jpg\" width=\"250\" /></div>\r\n\r\n<div class=\"circle-img-box-text\">\r\n<h4 class=\"name\">phạm hồng tâm</h4>\r\n\r\n<h5 class=\"city\">Masteri</h5>\r\n\r\n<h2 class=\"heading\">Bếp và tủ quần áo</h2>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"success-stories-img\">\r\n<div class=\"img-left\"><img alt=\"\" class=\"img-responsive\" height=\"300\" src=\"/upload_images/images/hih_2.jpg\" width=\"500\" /></div>\r\n\r\n<div class=\"img-right\"><img alt=\"\" class=\"img-responsive\" height=\"300\" src=\"/upload_images/images/hih_3.jpg\" width=\"500\" /></div>\r\n</div>\r\n\r\n<p class=\"text-center quot\">&quot;Tôi thực sự hài lòng về sự nhiệt tình cũng như kiến thức chuyên môn tốt của Home Stylist ở Mitssy. Anh ấy giúp chúng tôi chọn bếp, tư vấn tủ quần áo hợp gu hợp thói quen sử dụng của hai vợ chồng.&quot;</p>\r\n</div>\r\n</div>\r\n</section>', '1501672052', '1');
INSERT INTO `statics_multi` VALUES ('18', '4', 'page', 'Giới thiệu', '3', '', '', '', '<div id=\"p_about_us\">\r\n<div class=\"banner\">\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"673\" src=\"/upload_images/images/banner.png\" width=\"1366\" /></p>\r\n</div>\r\n\r\n<div class=\"about_us_content\">\r\n<div class=\"gray_line_bg\">\r\n<div class=\"title\">Decobox &amp; Chuyện về Home Stylist</div>\r\n</div>\r\n\r\n<p>Làm việc trong lĩnh vực nội thất từ năm 2012. <span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">Decobox</span>nhận ra quá trình hoàn thiện nội thất cho ngôi nhà luôn mất rất nhiều thời gian và công sức. Nhất là khi nhu cầu khẳng định bản thân với những người Việt trẻ trong thời buổi hội nhập quốc tế ngày càng đa diện - và đã dần mở rộng sang lĩnh vực nội thất.<br />\r\n<br />\r\nHiểu được điều đó&nbsp;Decobox tự hào là đơn vị bán lẻ &amp; thi công nội thất đầu tiên ở Việt Nam mang đến khái niệm <b>&quot;Home Stylist&quot;</b>.<br />\r\n<br />\r\nHãy tưởng tượng ngôi nhà của bạn là một cô gái. &quot;Furniture Designer&quot; sẽ thiết kế và may quần áo còn &quot;Interior Architect&quot; sẽ vẽ lên một bản chân dung lý tưởng.<br />\r\n<br />\r\nKhác với họ. <b>&quot;Home Stylist&quot;</b> là người tư vấn, chọn mẫu quần áo phù hợp và phối thêm trang sức, giày dép. Ngoài ra <b>&quot;Home Stylist&quot;</b> còn có thể điều chỉnh số đo quần áo hoặc thay thế chất liệu vải vóc khi cần, để cô thể hiện cá tính mình một cách tự tin nhất!<br />\r\n<br />\r\n&quot;Cô gái&quot; của bạn đã sẵn sàng cho một diện mạo mới cùng <b>Home Stylist</b> của&nbsp;Decobox chưa?</p>\r\n</div>\r\n\r\n<div class=\"about_us_content\">\r\n<div class=\"gray_line_bg\">\r\n<div class=\"title\">Sản phẩm đặc trưng</div>\r\n</div>\r\n\r\n<div class=\"content_block\">\r\n<div class=\"col-md-12 col-lg-12 col-xs-12 col-sm-12\">\r\n<div class=\"row\">\r\n<div class=\"content_block_decor\">\r\n<div class=\"decor text-center\">\r\n<div class=\"decor_img\"><img alt=\"\" class=\"img-responsive\" height=\"197\" src=\"/upload_images/images/Sofa.png\" width=\"197\" /></div>\r\n\r\n<div class=\"decor_content\">\r\n<div class=\"decor_title\">Sofa</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"decor text-center\">\r\n<div class=\"decor_img\"><img alt=\"\" class=\"img-responsive\" height=\"196\" src=\"/upload_images/images/Bed(1).png\" width=\"196\" /></div>\r\n\r\n<div class=\"decor_content\">\r\n<div class=\"decor_title\">Giường</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"decor text-center\">\r\n<div class=\"decor_img\"><img alt=\"\" class=\"img-responsive\" height=\"197\" src=\"/upload_images/images/Bo%20ban%20an.png\" width=\"197\" /></div>\r\n\r\n<div class=\"decor_content\">\r\n<div class=\"decor_title\">Bộ bàn ăn</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"decor text-center\">\r\n<div class=\"decor_img\"><img alt=\"\" class=\"img-responsive\" height=\"197\" src=\"/upload_images/images/Fit-in.png\" width=\"197\" /></div>\r\n\r\n<div class=\"decor_content\">\r\n<div class=\"decor_title\">Nội thất Fit-In</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '1501213651', '1');
INSERT INTO `statics_multi` VALUES ('16', '4', 'page', 'Home', '1', '', '', '', '<div class=\"container\">\r\n<h3 class=\"blue_title\">CHÚNG TÔI\r\n<i class=\"fa fa-fw fa-heart\"></i>\r\nDECOBOX</h3>\r\n\r\n<div id=\"testi\">\r\n<div class=\"testi_users\">\r\n<div class=\"img_user\" data-for=\"user1\">\r\n<div class=\"mask\">&nbsp;</div>\r\n<img alt=\"\" height=\"500\" src=\"/upload_images/images/nguyen%20nhat%20vu.jpg\" width=\"500\" /></div>\r\n\r\n<div class=\"img_user active\" data-for=\"user2\">\r\n<div class=\"mask\">&nbsp;</div>\r\n<img alt=\"\" height=\"500\" src=\"/upload_images/images/van%20anh.jpg\" width=\"498\" /></div>\r\n\r\n<div class=\"img_user\" data-for=\"user3\">\r\n<div class=\"mask\">&nbsp;</div>\r\n<img alt=\"\" height=\"500\" src=\"/upload_images/images/le%20chau%20bao.jpg\" width=\"500\" /></div>\r\n</div>\r\n\r\n<div class=\"testi_info\">\r\n<div class=\"info\" id=\"user1\">\r\n<p>Ghế đẹp và ngồi rất êm, rất hài lòng về chất lượng của Ahometo.<br />\r\nBộ phận chăm sóc khách hàng rất nhiệt tình, chu đáo, nếu có dịp nhất định sẽ quay lại.</p>\r\n\r\n<p class=\"name\">Nguyễn Nhật Vũ</p>\r\n</div>\r\n\r\n<div class=\"info active\" id=\"user2\">\r\n<p>Sofa đẹp, ưng ý.<br />\r\nRất vui vì hình nhà mình đã được quảng cáo trên Facebook.</p>\r\n\r\n<p class=\"name\">Tô Thị Vân Anh</p>\r\n</div>\r\n\r\n<div class=\"info\" id=\"user3\">\r\n<p>Sofa rất đẹp, màu sắc ưng ý</p>\r\n\r\n<p class=\"name\">Lê Châu Bảo</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '1501207563', '1');
INSERT INTO `statics_multi` VALUES ('17', '4', 'page', 'Footer', '2', '', '', '', '<div class=\"general col-sm-6 col-md-3 col-lg-3 col-xs-12\">\r\n<div class=\"head\"><img alt=\"\" height=\"60\" src=\"/upload_images/images/logo_120(1).png\" width=\"180\" /></div>\r\n\r\n<p class=\"sub\">Tên đơn vị: Công ty Cổ phần Đầu tư và Xây dựng Greenlines Hà Nội</p>\r\n\r\n<p class=\"sub\">Tầng 4, 19 An Trạch, Q. Đống Đa, Hà Nội</p>\r\n\r\n<p class=\"sub\">Giấy chứng nhận đăng ký kinh doanh: 0107795126.</p>\r\n\r\n<p class=\"sub\">Do Sở kế hoạch và đầu tư Hà Nội cấp ngày 07/04/2017</p>\r\n\r\n<p class=\"sub\">&copy; Copyright 2016 Binweb</p>\r\n</div>\r\n\r\n<div class=\"ho-tro col-sm-6 col-md-3 col-lg-3 col-xs-12 clearfix\">\r\n<div class=\"head phone\">\r\n<p class=\"tel\"><a href=\"tel:(024) 7300 8687\">\r\n<i class=\"fa fa-phone\"></i>\r\n(024) 7300 8687</a></p>\r\n\r\n<p class=\"tel\"><a href=\"tel:09 020 929 36\">\r\n<i class=\"fa fa-phone\"></i>\r\n09 020 929 36</a></p>\r\n</div>\r\n\r\n<p class=\"title\">HỖ TRỢ KHÁCH HÀNG</p>\r\n\r\n<p class=\"item\"><a href=\"cau-hoi-thuong-gap.html\">Câu hỏi thường gặp (FAQ)</a></p>\r\n\r\n<p class=\"item\"><a href=\"chinh-sach-giao-hang.html\">Chính sách giao hàng</a></p>\r\n\r\n<p class=\"item\"><a href=\"chinh-sach-doi-tra.html\">Chính sách đổi trả</a></p>\r\n\r\n<p class=\"item\"><a href=\"bao-hanh-san-pham.html\">Chính sách bảo hành</a></p>\r\n\r\n<p class=\"item\"><a href=\"dieu-khoan-bao-mat.html\">Điều khoản bảo mật</a></p>\r\n\r\n<p class=\"item\"><a href=\"doi-tac.html\">Đối tác kinh doanh</a></p>\r\n</div>\r\n\r\n<div class=\"doi-tac col-sm-6 col-md-3 col-lg-3 col-xs-12  clearfix\">\r\n<div class=\"head\">\r\n<p><a href=\"cdn-cgi/l/email-protection.html#563e333a3a39163b3f2225252f7835393b\">\r\n<i class=\"fa fa-envelope\"></i>\r\n<span class=\"__cf_email__\" data-cfemail=\"aac2cfc6c6c5eac7c3ded9d9d384c9c5c7\">info@decobox.vn</span> </a></p>\r\n</div>\r\n\r\n<p class=\"title\">VỀ DECOBOX</p>\r\n\r\n<p class=\"item\"><a href=\"ve-mitssy.html\">Giới thiệu về Decobox</a></p>\r\n\r\n<p class=\"item\"><a href=\"quy-trinh-tu-van.html\">Quy trình tư vấn</a></p>\r\n\r\n<p class=\"item\"><a href=\"cong-trinh.html\">Dự án tiêu biểu</a></p>\r\n\r\n<p class=\"item\"><a href=\"tim-hieu-san-pham-noi-that-va-vat-dung-trang-tri.html\">Sản phẩm ở Decobox</a></p>\r\n\r\n<p class=\"item\"><a href=\"vi-sao-ban-nen-chon-ahometo.html\">Vì sao chọn Decobox?</a></p>\r\n\r\n<p class=\"item\"><a href=\"tuyen-dung.html\">Tuyển dụng</a></p>\r\n</div>\r\n\r\n<div class=\"facebook col-sm-6 col-md-3 col-lg-3 col-xs-12  clearfix\">\r\n<div class=\"head social\">\r\n<ul>\r\n	<li><a href=\"https://www.facebook.com/decobox.vn\" target=\"_blank\">\r\n	<i class=\"fa fa-facebook-square fa-2x\"></i>\r\n	</a></li>\r\n	<li><a href=\"https://www.instagram.com/your.decobox/\" target=\"_blank\">\r\n	<i class=\"fa fa-instagram fa-2x\"></i>\r\n	</a></li>\r\n</ul>\r\n</div>\r\n</div>', '1501207895', '1');
INSERT INTO `statics_multi` VALUES ('21', '4', 'landing_page', 'Chính sách giao hàng', '5', '', '', '', '<div class=\"policy-content\">\r\n<div class=\"policy-content-item\" data-content-name=\"policy_content_1\" data-group=\"policy_content\">\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Mitssy tính phí giao hàng như thế nào?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Phí giao hàng được tính dựa trên khối lượng/ thể tích của sản phẩm và được áp dụng tùy theo khu vực với mức phí khác nhau.<br />\r\nMitssy hỗ trợ 100% phí vận chuyển đối với đơn hàng ở tp. Hồ Chí Minh (Trừ các khu vực: q.12, q. Hóc Môn, huyện Củ Chi, Cần Giờ, Bình Chánh, q. 9, q. Thủ Đức)</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Khi nào tôi nhận được sản phẩm?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<div data-group=\"content_expand\" style=\"display: none;\">\r\n<p>Sau khi quý khách đã đặt hàng thành công, Mitssy sẽ gọi điện thoại cho quý khách xác nhận đơn hàng. Khi đơn hàng được xuất kho sẽ được giao đến quý khách trong khoảng thời gian sau đây</p>\r\nThời gian giao hàng\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<th>Địa chỉ giao hàng</th>\r\n			<th>Hàng có sẵn</th>\r\n			<th>Hàng đặt</th>\r\n		</tr>\r\n		<tr>\r\n			<td>Hồ Chí Minh</td>\r\n			<td>3 - 4 ngày làm việc</td>\r\n			<td>12 - 16 ngày làm việc</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Hà Nội</td>\r\n			<td>7 - 10 ngày làm việc</td>\r\n			<td>23 - 25 ngày làm việc</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Khu vực khác</td>\r\n			<td>Thời gian dự kiến tùy từng địa phương</td>\r\n			<td>Thời gian dự kiến tùy từng địa phương</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Làm sao để tôi biết được chính xác khi nào tôi nhận được hàng?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Trước khi giao hàng, Mitssy sẽ liên hệ với quý khách để hẹn ngày và thời gian giao hàng.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Tôi cần sản phẩm gấp, bắt buộc phải giao trong ngày có được không?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Thời gian giao hàng tùy thuộc vào sản phẩm đặt hàng và lịch giao hàng của công ty. Mọi yêu cầu cần xử lý gấp, xin vui lòng gọi số Hotline 028-668-46460. Mitssy sẽ hồi đáp những kiến nghị của khách hàng trong giờ hành chính.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Mitssy có giao hàng vào chủ nhật hoặc ngày lễ không?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Mitssy luôn hỗ trợ quý khách để sản phẩm được giao nhanh nhất. Tuy nhiên, Mitssy sẽ tính thêm phí đối với những đơn hàng giao sau 18:00 hoặc giao vào Chủ nhật hoặc ngày lễ tết<br />\r\n10% giá trị đơn hàng: đối với đơn hàng dưới 4.000.000đ<br />\r\n5% giá trị đơn hàng: đối với đơn hàng trên 4.000.000đ</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Mitssy giao hàng đến lúc tôi đi vắng thì làm thế nào?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Quý khách có thể nhờ người khác nhận hàng giúp, tuy nhiên Mitssy sẽ cần một thư ủy quyền với chữ ký xác nhận của quý khách, trong đó nêu rõ về người được ủy quyền nhận đơn hàng.<br />\r\n<br />\r\nNgười được ủy quyền sẽ có trách kiểm tra và kí vào đơn hàng xác nhận sản phẩm ở trong điều kiện tốt, trong trường hợp người ủy quyền không hài lòng về sản phẩm hãy ghi chú vào đơn hàng và thông báo cho Mitssy.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Có khu vực nào mà Mitssy không giao hàng đến không?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Mitssy có thể giao hàng đến hầu hết các tỉnh thành và khu vực trong nước.<br />\r\n<br />\r\nNgoài các chành xe giao hàng uy tín, Mitssy còn liên kết với các nhà vận chuyển lớn khác như Hỏa Xa, Bưu điện Việt Nam, Giao Hàng Nhanh, và Viettel post để đảm bảo hàng hóa luôn được lưu thông nhanh chóng và đảm bảo.<br />\r\n<br />\r\nNgoài ra, quý khách cũng có thể đề xuất với Mitssy chành xe giao hàng quen thuộc để Mitssy đảm bảo hỗ trợ tốt nhất.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Tôi có thể xem hoặc mua trực tiếp tại Showroom Mitssy không?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Hiện tại, Mitssy chỉ trưng bày sản phẩm mới tại Showroom. Tất cả các sản phẩm khác đều ở kho và xưởng sản xuất, quý khách vui lòng chọn sản phẩm trên Website, hoặc đến văn phòng để xem chất lượng chung và được tư vấn kỹ hơn về sản phẩm.<br />\r\n<br />\r\nĐịa chỉ Showroom Mitssy: 294 Nguyễn Trọng Tuyển, p.1, q. Tân Bình, tp. Hồ Chí Minh.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Làm thế nào để đăng ký dịch vụ lắp đặt?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Dịch vụ lắp đặt sẽ mặc định được cho vào giỏ hàng của quý khách khi đặt hàng các sản phẩm đang được hỗ trợ lắp đặt như: Tranh, kệ sách, tủ sách, tủ tivi.<br />\r\n<br />\r\nTrong trường hợp quý khách không muốn sử dụng dịch vụ lắp đặt, có thể liên hệ trực tiếp tới Mitssy qua số Hotline: 028 - 668 - 46460. Mitssy sẽ hồi đáp những kiến nghị của khách hàng trong giờ hành chính.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Mitssy thu phí lắp đặt như thế nào?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<div data-group=\"content_expand\" style=\"display: none;\">\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<th>STT</th>\r\n			<th>Sản phẩm</th>\r\n			<th>Giá</th>\r\n		</tr>\r\n		<tr>\r\n			<td>1</td>\r\n			<td>Tranh Canvas/ Tranh PP</td>\r\n			<td>50,0000vnđ/ sản phẩm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2</td>\r\n			<td>Kệ sách</td>\r\n			<td>50,0000vnđ/ sản phẩm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>3</td>\r\n			<td>Giường</td>\r\n			<td>Miễn phí</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4</td>\r\n			<td>Tủ quần áo</td>\r\n			<td>Miễn phí</td>\r\n		</tr>\r\n		<tr>\r\n			<td>5</td>\r\n			<td>Sản phẩm thuộc dòng hàng Shirai - nhập khẩu Nhật Bản</td>\r\n			<td>Miễn phí</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Sản phẩm lắp đặt rồi có còn được đổi trả không?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Mitssy vẫn sẽ hỗ trợ đổi trả bình thường theo chính sách nếu sản phẩm phát sinh lỗi kỹ thuật.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Chất lượng lắp đặt không tốt thì sao?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Quý khách vui lòng kiểm tra chất lượng thi công trước khi ký xác nhận hoàn tất dịch vụ. Tuy nhiên, nếu việc lắp đặt ảnh hưởng đến chất lượng sản phẩm, Quý khách vui lòng liên hệ đến Mitssy qua số Hotline: 028 - 668 - 46460 để được hỗ trợ cụ thể. Mitssy sẽ hồi đáp những kiến nghị của khách hàng trong giờ hành chính.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Tôi đã chọn lắp đặt, nhưng sau khi nhận hàng không cần lắp đặt nữa, tôi có được hoàn phí lắp đặt không?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Mitssy rất tiếc sẽ không hỗ trợ việc hoàn phí khi đơn hàng đã được giao thành công.</p>\r\n</div>\r\n\r\n<div class=\"policy-content-row\" data-group=\"policy_content_row\">\r\n<h4>Chính sách đồng kiểm là gì?\r\n<i class=\"policy-icon-drop fa fa-chevron-down\" data-group=\"policy_expand\" data-status=\"0\">&nbsp;</i>\r\n</h4>\r\n\r\n<p data-group=\"content_expand\" style=\"display: none;\">Trước khi chấp nhận và thanh toán cho một đơn hàng, quý khách có quyền yêu cầu kiểm tra sản phẩm có đúng với thông tin đặt hàng hay không. Bao gồm: số lượng, chủng loại, màu sắc, hình thức, tính nguyên vẹn của hàng hóa.<br />\r\nKhi đã kiểm tra sản phẩm xong, quý khách vui lòng kí vào đơn hàng xác nhận sản phẩm ở trong điều kiện tốt. Trong trường hợp quý khách không hài lòng về sản phẩm hãy ghi chú vào đơn hàng và thông báo cho Mitssy.</p>\r\n</div>\r\n</div>\r\n</div>', '1501781222', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `use_id` int(11) NOT NULL AUTO_INCREMENT,
  `use_login` varchar(100) NOT NULL,
  `use_email` varchar(100) DEFAULT NULL,
  `use_password` varchar(50) DEFAULT NULL,
  `use_security` varchar(255) DEFAULT NULL,
  `use_date` int(11) DEFAULT '0',
  `use_active` tinyint(2) DEFAULT '1',
  `lang_id` int(11) DEFAULT '0',
  PRIMARY KEY (`use_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'hungnv', 'imhung1179@gmail.com', '6dd9f8b7397a65c744000591fdc1b6ff', null, '1495185142', '1', '1');
INSERT INTO `users` VALUES ('2', 'linhnv', 'linhnv@gmail.com', '6dd9f8b7397a65c744000591fdc1b6ff', null, '1495437695', '1', '1');
INSERT INTO `users` VALUES ('3', 'hungnv2', 'hungnv2@gmail.com', '6dd9f8b7397a65c744000591fdc1b6ff', null, '1495438379', '1', '1');
