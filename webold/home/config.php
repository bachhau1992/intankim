<?
$con_lang_id			= 1;
require_once("../classes/database.php");
require_once("../classes/generate_form.php");
require_once("../classes/user.php");
require_once("../classes/form.php");
require_once("../classes/menu.php");
require_once("../functions/functions.php");
require_once("../functions/file_functions.php");
require_once("../functions/date_functions.php");
require_once("../functions/rewrite_functions.php");
require_once("../functions/pagebreak.php");
require_once("../functions/translate.php");

// Variable configurations
require_once("../includes/inc_config.php");

// Khai báo những biến dùng chung
$con_root_path			= "/";
$con_ajax_path			= "/ajax/";
$con_css_path			= "/css/";
$con_js_path			= "/js/";
$con_redirect			= base64_encode($_SERVER['REQUEST_URI']);
$con_file_version		= "?v=20130712";
$con_default_keyword	= "Tìm kiếm...";
$path_picture_more_medium = "/product_pictures_more/medium_";
$path_picture_more_small = "/product_pictures_more/small_";
$path_picture_more  	= "/product_pictures_more/";
$path_picture_normal = "/product_pictures/normal_";
$path_picture_small 	= "/product_pictures/small_";
$path_picture 		 	= "/product_pictures/";
$path_new_small		= "/news_pictures/small_";
$path_set_picture_small = "/set_pictures/small_";
$path_set_picture 	= "/set_pictures/";

// Array background
$arrBackground			= array("bg_bounce_toy.png", "bg_elephant.png", "bg_knit_toy.png", "bg_owls.png", "bg_rabbit.png");
$con_background		= $arrBackground[array_rand($arrBackground)];

// Thanh navigate ở header
$header_navigate		= "";

// Lấy trước những biến hay sử dụng
$iCat						= getValue("iCat");
$iData 					= getValue("iData");
$iTag 					= getValue("iTag");

$myuser 					= new user();

// Check xem trình duyệt là IE6 hay IE7
$isIE						= (strpos(@$_SERVER['HTTP_USER_AGENT'], "MSIE") !== false ? 1 : 0);
$isIE6					= (strpos(@$_SERVER['HTTP_USER_AGENT'], "MSIE 6") !== false ? 1 : 0);
$isIE7					= (strpos(@$_SERVER['HTTP_USER_AGENT'], "MSIE 7") !== false ? 1 : 0);
$isIElowVersion		= ($isIE6 || $isIE7 ? 1 : 0);
?>