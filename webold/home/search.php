<?
require_once("config.php");
ob_start("clearSpaceBuffer");

$kSearch				= getValue("keyword", "str", "GET", "", "");
if($kSearch == "") exit("Bạn chưa nhập từ khóa");
$con_site_title 	= "Kết quả search của từ khóa " . $kSearch . " - Greenlineshanoi.vn";

?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$con_site_title?></title>
	<meta name="keywords" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_keywords))?>" />
	<meta name="description" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_description))?>" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>da94653532597a1703228c9ac44eb8d2_1500029040-ssl.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>3e20fe578765a28cb3b2b5e201cfac18_1500029051-ssl.css" media="all" />
	<? include("../includes/inc_css_javascript.php");?>
	<script type="text/javascript" src="<?=$con_js_path?>b7178d64aee94a33b44399d81a54122d_1500029145.js"></script>
	<script type="text/javascript" src="<?=$con_js_path?>functions_main.js"></script>
</head>
<body class=" cms-index-index cms-home">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<? include("../includes/inc_header.php"); ?>
				<div class="main-container col1-layout">
				<? include("../includes/inc_search.php"); ?>
				</div>
			</div>
			<? include("../includes/inc_footer.php"); ?>
		</div>
	</body>
</body>
</html>
<?
ob_end_flush();
?>