<?
require_once("config.php");
ob_start("clearSpaceBuffer");
?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$con_site_title?></title>
	<meta name="keywords" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_keywords))?>" />
	<meta name="description" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_description))?>" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>css_default_one.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>3e20fe578765a28cb3b2b5e201cfac18_1500029051-ssl.css" media="all" />
	<? include("../includes/inc_css_javascript.php");?>
	<script type="text/javascript" src="<?=$con_js_path?>js_default_one.js"></script>
	<script type="text/javascript" src="<?=$con_js_path?>functions_main.js"></script>
</head>
<body class="cms-index-index cms-home">

<noscript>
<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>

	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<? include("../includes/inc_header.php"); ?>
				<div class="main-container col1-layout">
					<div class="one-con-wrapper">
						<div class="std">
							<?
							include("../includes/inc_slider.php");
							include("../includes/inc_home.php");
							?>
						</div>
					</div>
				</div>
			</div>
			<? include("../includes/inc_footer.php"); ?>
		</div>
	</body>
</body>
</html>
<?
ob_end_flush();
?>
