<?
require_once("config.php");
ob_start("clearSpaceBuffer");

$db_news   = new db_query("SELECT *
									FROM news_multi
										INNER JOIN categories_multi ON (cat_id = new_category_id AND cat_type = 'news' AND cat_active = 1)
									WHERE new_active = 1 AND new_id = " . $iData);
if(mysql_num_rows($db_news->result) == 0) exit("Tin tức không tồn tại");
$rowDetail = mysql_fetch_assoc($db_news->result);
$db_news->close();
unset($db_news);

$con_site_title 		= $rowDetail["new_meta_title"] != "" ? $rowDetail["new_meta_title"] : $con_site_title;
$con_meta_keywords 	= $rowDetail["new_meta_keyword"] != "" ? $rowDetail["new_meta_keyword"] : $con_meta_keywords;
$con_meta_description= $rowDetail["new_meta_description"] != "" ? $rowDetail["new_meta_description"] : $con_meta_description;

?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$con_site_title?></title>
	<meta name="keywords" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_keywords))?>" />
	<meta name="description" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_description))?>" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>3e20fe578765a28cb3b2b5e201cfac18_1500029051-ssl.css" media="all" />
	<? include("../includes/inc_css_javascript.php");?>
	<script type="text/javascript" src="<?=$con_js_path?>8e3e7a5d3f2932ff2b79174f02ce647e_1500029114.js"></script>
	<script type="text/javascript" src="<?=$con_js_path?>functions_main.js"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="wrapper">
		<div class="page">
			<? include("../includes/inc_header.php"); ?>
			<div class="main-container col1-layout">
			<?
			include("../includes/inc_detail_new.php");
			?>
			</div>
		</div>
		<? include("../includes/inc_footer.php"); ?>
	</body>
</body>
</html>
<?
ob_end_flush();
?>