<?
require_once("config.php");
ob_start("clearSpaceBuffer");
?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Thanh toán - Greenlineshanoi.vn - Những sản phẩm trang trí nội thất phong cách cho ngôi nhà đẹp.</title>
	<meta name="description" content="Greenlineshanoi.vn mang những sản phẩm trang trí nội thất đậm dấu ấn phong cách nội thất đến ngôi nhà đẹp. Noi that, trang trí, trang tri, do trang tri, đồ trang trí, nội thất, đèn trang trí, kệ treo tường" />
	<meta name="keywords" content="mitssy, nội thất, noi that, trang trí nội thất, trang tri noi that, do trang tri, noi that mitssy, trang tri, trang tri nha,phong cách nhà đẹp, nội thất bếp, nội thất đep, nội thất phòng ngủ, nội thất phòng khách, phòng khách, phòng ngủ, phòng bếp, phòng bé, nhà đẹp,nha dep, đèn trang trí, kệ treo tường." />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>checkout.css" media="all" />
	<? include("../includes/inc_css_javascript.php");?>
	<script type="text/javascript" src="<?=$con_js_path?>functions_main.js"></script>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<? include("../includes/inc_header.php"); ?>
				<div class="main-container col1-layout">
				<?
				include("../includes/inc_checkout.php");
				?>
				</div>
			</div>
			<? include("../includes/inc_footer.php"); ?>
		</div>
	</body>
</body>
</html>
<?
ob_end_flush();
?>