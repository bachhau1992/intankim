<? include("initsession.php");?>
<?php
header("Content-type: image/png");
$code		= "";
if(isset($_SESSION["session_security_code"])) $code = $_SESSION["session_security_code"];
$im 		= imagecreate(55, 24);
// white background and blue text
$bg 		= imagecolorallocate($im, 255, 225, 98);
$color	= imagecolorallocate($im, 76, 76, 76);
imagestring($im, 5, 10, 4, $code, $color);
imagepng($im);
?>