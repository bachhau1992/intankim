<?
require_once("config.php");
ob_start("clearSpaceBuffer");

$db_type	= new db_query("SELECT * FROM categories_multi WHERE cat_active = 1 AND cat_id = " . $iCat);
if(mysql_num_rows($db_type->result) == 0) exit("Trang khong ton tai");

$row_type = mysql_fetch_assoc($db_type->result);
$db_type->close();
unset($db_type);

$listProId					= $row_type["cat_all_child"];
$cat_link					= "";
$con_site_title			= $row_type["cat_title"] != "" ? $row_type["cat_title"] : $con_site_title;
$con_meta_keywords		= $row_type["cat_meta_keyword"] != "" ? $row_type["cat_meta_keyword"] : $con_meta_keywords;
$con_meta_description	= $row_type["cat_meta_description"] != "" ? $row_type["cat_meta_description"] : $con_meta_description;

?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$con_site_title?></title>
	<meta name="keywords" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_keywords))?>" />
	<meta name="description" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_description))?>" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>da94653532597a1703228c9ac44eb8d2_1500029040-ssl.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>3e20fe578765a28cb3b2b5e201cfac18_1500029051-ssl.css" media="all" />
	<? include("../includes/inc_css_javascript.php");?>
	<script type="text/javascript" src="<?=$con_js_path?>b7178d64aee94a33b44399d81a54122d_1500029145.js"></script>
	<script type="text/javascript" src="<?=$con_js_path?>functions_main.js"></script>
</head>
<body class=" cms-index-index cms-home">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<? include("../includes/inc_header.php"); ?>
				<div class="main-container col1-layout">
				<?
				if($row_type["cat_type"] == "news") include("../includes/inc_type_new.php");
				else if($row_type["cat_type"] == "set") include("../includes/inc_type_set.php");
				else include("../includes/inc_type_product.php");
				?>
				</div>
			</div>
			<? include("../includes/inc_footer.php"); ?>
		</div>
	</body>
</body>
</html>
<?
ob_end_flush();
?>