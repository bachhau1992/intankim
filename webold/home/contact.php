<?
require_once("config.php");
ob_start("clearSpaceBuffer");
?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$con_site_title?></title>
	<meta name="keywords" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_keywords))?>" />
	<meta name="description" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_description))?>" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>e17d2387042b630af6d7d4d38dc85ad7_1500029040-ssl.css" media="all" />
	<? include("../includes/inc_css_javascript.php");?>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<? include("../includes/inc_header.php"); ?>
				<div class="main-container col1-layout">
				<?
				include("../includes/inc_contact.php");
				?>
				</div>
			</div>
			<? include("../includes/inc_footer.php"); ?>
		</div>
	</body>
</body>
</html>
<?
ob_end_flush();
?>