<?
require_once("config.php");
ob_start("clearSpaceBuffer");

$db_static	= new db_query("SELECT * FROM statics_multi WHERE sta_id = " . $iData);
if(mysql_num_rows($db_static->result) == 0) exit("Trang khong ton tai");
$rowStatic	= mysql_fetch_assoc($db_static->result);
$db_static->close();
unset($db_static);

$con_site_title = $rowStatic["sta_title"] != "" ? $rowStatic["sta_title"] : $con_site_title;

?>
<!DOCTYPE html>

<html lang="en" id="top_main" class="no-js">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$con_site_title?></title>
	<meta name="keywords" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_keywords))?>" />
	<meta name="description" content="<?=str_replace("\n", "", htmlspecialchars($con_meta_description))?>" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?
	if($rowStatic["sta_type"] == "page"){
	?>
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>e17d2387042b630af6d7d4d38dc85ad7_1500029040-ssl.css" media="all" />
	<?
	include("../includes/inc_css_javascript.php");
	?>
	<script type="text/javascript" src="<?=$con_js_path?>js_default_one.js"></script>
	<?
	}
	else{
	?>
	<link rel="stylesheet" type="text/css" href="<?=$con_css_path?>401b5670e4004ede937dac1bbaee9481_1500030113-ssl.css" media="all" />
	<?
	include("../includes/inc_css_javascript.php");
	?>
	<script type="text/javascript" src="<?=$con_js_path?>122199871c8ef54631f0e6a6ff7aa7fb_1500030113.js"></script>
	<?
	}// End if($rowStatic["sta_type"] == "page")
	?>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3HQMVD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="wrapper">
		<div class="page">
			<div class="landing-page">
				<?
				include("../includes/inc_header.php");
				include("../includes/inc_detail_static.php");
				?>
			</div>
			<? include("../includes/inc_footer.php"); ?>
		</div>
	</body>
</body>
</html>
<?
ob_end_flush();
?>