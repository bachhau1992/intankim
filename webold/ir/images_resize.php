<?
require_once("../functions/functions.php");
require_once("../functions/file_functions.php");

//Lấy các tham số trên URL
$type							= getValue("type", "str", "GET", "");
$sub_dir						= getValue("sub_dir", "str", "GET", "");
$resize_mode				= getValue("resize_mode");
$image_file_name_base64	= getValue("image_file_name_base64", "str", "GET", "");
$image_file_name_base64	= str_replace(" ", "+", $image_file_name_base64);
$image_file_name			= base64_url_decode($image_file_name_base64);

// Khai báo array resize mode
$array_resize_mode		= array(0 => array(400, 400));

// Remove những ký tự đặc biệt để tránh nhẩy directory
$array_invalid_chars		= array("../", "..\\", "/", "\\");
$image_file_name			= str_replace($array_invalid_chars, "", $image_file_name);

// Không đúng đuôi cho phép gif,jpg,jpe,jpeg,png sẽ exit
if(check_upload_extension($image_file_name, "gif,jpg,jpe,jpeg,png") == 0){
	exit("Invalid file extention");
}

// Kiêm tra $resize_mode
if(!isset($array_resize_mode[$resize_mode])){
	exit("Invalid resize mode");
}

// Tạo đường dẫn
$img_full_path				= "../";
switch($type){
	default	: $img_full_path .= "product_pictures/"; break;
}

$img_full_path				.= $image_file_name;
$sExtension					= getExtension($image_file_name);

// Bắt đầu resize
list($width, $height) 	= @getimagesize($img_full_path);
$width						= intval($width);
$height						= intval($height);

if($width != 0 && $height !=0){
	if ($array_resize_mode[$resize_mode][0] / $width > $array_resize_mode[$resize_mode][1] / $height){
		$percent = $array_resize_mode[$resize_mode][1] / $height;
	}
	else{
		$percent = $array_resize_mode[$resize_mode][0] / $width;
	}
}
else{
	exit("Invalid image dimension");
}


// Nếu 2 chiều cùng nhỏ hơn max thì giữ nguyên
if ($width <= $array_resize_mode[$resize_mode][0] && $height <= $array_resize_mode[$resize_mode][1]){
	$new_width 		= $width;
	$new_height 	= $height;
}
// Nếu khác bắt đầu lấy tỉ lệ để resize
else{
	$new_width 		= $width * $percent;
	$new_height 	= $height * $percent;
}

// Resample
$image_p = @imagecreatetruecolor($new_width, $new_height);
// Check extension file
switch ($sExtension){
	case "jpg" :
	case "jpe" :
	case "jpeg" :
		$image = @imagecreatefromjpeg($img_full_path);
		break;
	case "gif" :
		$image = @imagecreatefromgif($img_full_path);
		break;
	case "png" :
		$image = @imagecreatefrompng($img_full_path);
		break;
}

@imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
// Output

ob_start();

$expires	= 86400;
$etag		= md5($img_full_path);

header("Cache-Control: max-age=" . $expires);
header("Last-Modified: " . gmdate("D, d M Y H:i:s", mktime(0, 0, 0, 1, 1, 2010)) . " GMT"); 
header("Expires: " . gmdate("D, d M Y H:i:s", time() + $expires) . " GMT");
header("Etag: " . $etag); 

switch ($sExtension){
	case "jpg" :
	case "jpe" :
	case "jpeg" :
		header("Content-Type: image/" . $sExtension);
		@imagejpeg($image_p, NULL , 95);
		break;
	case "gif" :
		header("Content-Type: image/" . $sExtension);
		@imagegif($image_p);
		break;
	case "png" :
		header("Content-Type: image/" . $sExtension);
		@imagepng($image_p);
		break;
}
imagedestroy($image_p);

//echo $img_full_path;
?>