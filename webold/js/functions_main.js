function changeQuantity(domEle){
	var quantity = domEle.val();
	$j("#add_cart").attr("data-value", quantity);
}

function add_to_cart(iPro, domEle){
	var iPro 		= iPro || 0;
	var quantity	= 1;
	quantity 		= parseInt(domEle.attr("data-value"));
	if(quantity <= 0 || isNaN(quantity)){ alert("Số lượng phải lớn hơn 0."); domEle.focus(); return false; }
	$j.get("/ajax/add_to_cart.php?iPro=" + iPro + "&quantity=" + quantity, function(data){
		if(data != ""){
			if($j("#total_product").length > 0) $j("#total_product").html(data);
			else $j("#gio_hang").append('<span class="total_product" id="total_product">' + data + '</span>');
		}
	});
}
function delete_cart(iPro){
	var iPro = iPro || 0;

	if(confirm("Bạn có muốn xóa sản phẩm này không?")){
		$j.get("/ajax/delete_cart.php?iPro=" + iPro, function(data){
			if(data != ""){
				if(parseInt(data) > 0){
					$j("#item_" + iPro).remove();
					window.location.reload(true);
				}
				if($j("#total_product").length > 0) $j("#total_product").html(data);
				else $j("#gio_hang").append('<span class="total_product" id="total_product">' + data + '</span>');
			}
		});
	}
}

function update_cart(iPro, domEle){
	var iPro = iPro || 0;
	var quantity = domEle.val()
	if(quantity != "undefined" && quantity != undefined && quantity != ""){
		quantity 		= quantity.replace(/[^0-9]/g, '');
	}else{
		quantity 		= 1;
	}
	quantity 		= parseInt(quantity);
	if( isNaN(quantity) || quantity < 0 ) quantity	= 1;

	$j.get("/ajax/update_cart.php?iPro=" + iPro + "&quantity=" + quantity, function(data){
		if(data != ""){
			domEle.val(quantity);
			$j("#total_product").html(data);
			window.location.reload(true);

		}
	});
}
function addCommas(nStr){
	nStr += ''; x = nStr.split(',');	x1 = x[0]; x2 = ""; x2 = x.length > 1 ? ',' + x[1] : ''; var rgx = /(\d+)(\d{3})/; while(rgx.test(x1)){ x1 = x1.replace(rgx, '$1' + '.' + '$2'); } return x1 + x2;
}

function updateTotalMoney(){

	// Tiền hàng
	var total_value	= 0;
	$(".total_money_product").each(function(){
		var money_estore 	= $(this).html();
		money_estore		= parseInt(money_estore);
		if( isNaN(money_estore) || money_estore < 0 ) money_estore	= 0;
		total_value 		= total_value + money_estore;
	});

	total_value			= parseInt(total_value);
	if( isNaN(total_value) || total_value < 0 ) total_value	= 0;

	total_value		= (total_value > 0 ? addCommas(total_value) : "Liên hệ");
	// Show tổng tiền
	$("#totalTempMoney").html(total_value);

}