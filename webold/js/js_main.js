$(function(){
  $('#horiz_container_outer').horizontalScroll();
  
  // Menu
	$(".menu_top").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

  $("input[type='text']").focus(function(){
  		$(this).addClass("focus");
  });
  $("input[type='text']").blur(function(){
  		$(this).removeClass("focus");
  });
  $("textarea").focus(function(){
  		$(this).addClass("focus");
  });
  $("textarea").blur(function(){
  		$(this).removeClass("focus");
  });
  
  // Next section
  $(".next_section").click(function(){
  		if($(window).width() >= 1300) {
  			$("html, body").animate({ scrollTop: 650 }, 800);
  		} else {
  			$("html, body").animate({ scrollTop: 500 }, 800);
  		}
  });
  
  // Toggle Map
  $(".hide_overlay").click(function(){
		var toggle = $(this).text();
  		if(toggle == "View Map"){
  			$(this).text("Hide Map");
  			$(".map_overlay").addClass("overlay_hide");
  		}else{
  			$(this).text("View Map");
  			$(".map_overlay").removeClass("overlay_hide");
  		}
  });
  
  // Form
  	//breakdown the labels into single character spans
	$(".flp label").each(function(){
		var sop = '<span class="ch">'; //span opening
		var scl = '</span>'; //span closing
		//split the label into single letters and inject span tags around them
		$(this).html(sop + $(this).html().split("").join(scl+sop) + scl);
		//to prevent space-only spans from collapsing
		$(".ch:contains(' ')").html("&nbsp;");
	})
	
	var d;
	//animation time
	$(".flp input").focus(function(){
		//calculate movement for .ch = half of input height
		var tm = $(this).outerHeight()/2 *-1 + "px";
		//label = next sibling of input
		//to prevent multiple animation trigger by mistake we will use .stop() before animating any character and clear any animation queued by .delay()
		$(this).next().addClass("focussed").children().stop(true).each(function(i){
			d = i*50;//delay
			$(this).delay(d).animate({top: tm}, 200, 'easeOutBack');
		})
	})
	$(".flp input").blur(function(){
		//animate the label down if content of the input is empty
		if($(this).val() == "")
		{
			$(this).next().removeClass("focussed").children().stop(true).each(function(i){
				d = i*50;
				$(this).delay(d).animate({top: 0}, 500, 'easeInOutBack');
			})
		}
	})
	$(".flp textarea").focus(function(){
		//calculate movement for .ch = half of input height
		var tm = $(this).outerHeight()/2 *-1 + 60 + "px";
		//label = next sibling of input
		//to prevent multiple animation trigger by mistake we will use .stop() before animating any character and clear any animation queued by .delay()
		$(this).next().addClass("focussed").children().stop(true).each(function(i){
			d = i*50;//delay
			$(this).delay(d).animate({top: tm}, 200, 'easeOutBack');
		})
	})
	$(".flp textarea").blur(function(){
		//animate the label down if content of the input is empty
		if($(this).val() == "")
		{
			$(this).next().removeClass("focussed").children().stop(true).each(function(i){
				d = i*50;
				$(this).delay(d).animate({top: 0}, 500, 'easeInOutBack');
			})
		}
	});
	
//	console.log(x);
	var new_id = $('.news').attr('data-id');
	var new_cat = $('.news').attr('data-cat');
	var load = 0;
	if($('.news').length){
		$(window).data('ajaxready', true).scroll(function(){
			if ($(window).data('ajaxready') == false) return;
			var x = $('#container_footer').offset().top - $(window).height();
			if($(window).scrollTop() >= x - $('.wrap_similar_news').height()){	
				$(window).data('ajaxready', false);
				$('.wrap_notification').css('display', 'block');
				load++;
				if(load <= 10){
					$.ajax({
						url: '/ajax/pushstate.php',
						type: 'GET',
						data: {'iData':new_id, 'load':load, 'iCat':new_cat},
						success: function(data){
							if(data){
								$('#container_content_center').append(data);
								var new_url = $('.news').last().attr('data-url');
								history.replaceState('', '', new_url);
								var noti_id = $('.wrap_notification').last().attr('data-noti-id');
								
								$('.new_notification').click(function(){
									if($(this).find('a').length){
										var link_id = $('a#next_news_'+noti_id).attr('href');
										$('html,body').animate({scrollTop: $(link_id).offset().top - 50},'slow');
									}
								})
								$(".bar_top").click(function() {
									$("html, body").animate({ scrollTop: 0 }, "slow");
									$('.wrap_notification').css('display', 'none');
								});			
							}else{
								$('#wrap_notification_'+noti_id).css('display', 'none');
							}
							$(window).data('ajaxready', true);
						}
					})
				}
			}
		})
	}
	
	// Check validate form
	$('#submit').click(function(){
		var name			= $('#name').val();
		var email		= $('#email').val();
		var phone		= $('#phone').val();
		var message		= $('#message').val();
		var reg_email	= /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		var is_email	= reg_email.test(email);
		var reg_phone	= /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		var is_phone	= reg_phone.test(phone);
		var action		= $('#action').val();
		if(name == '' || email == '' || phone == '' || message == ''){
			alert('Bạn cần nhập đầy dủ thông tin');
		}else if(!is_email){
			alert('Email không hợp lệ');
		}else if(!is_phone){
			alert('Số điện thoại không hợp lệ');
		}else if((message.length) > 500){
			alert('Bạn chỉ được nhập tối đa 500 kí tự');
		}else{
			$.ajax({
				url: '/ajax/contact.php',
				type: 'POST',
				data: {'ct_name':name, 'ct_email':email, 'ct_phone':phone, 'ct_message':message, 'ct_action':action},
				success: function(data){
					if(data == 'success'){
						$('#main_form').hide().html('<h1>Message successfully sent.</h1><h3>Thank <span>' + name + '</span>, your message has been successfully submitted to us.</h3>').slideDown("slow");
					}else{
						$('#error_show').hide().append(data).slideDown("slow");
					}
				}
			});
		}
	});
});
