<?
require_once("../home/config.php");

// Khai báo biến được post lên
$iPro				= getValue("iPro");
$quantity		= getValue("quantity", "dbl", "GET", 1);
$current_cart 	= array();
$total_product = 0;
$pro_exist 		= 0;
$new_cookie 	= "";

// Nếu số lượng > 0 thì add vào giỏ
if($quantity > 0){
	// Check xem sản phẩm có tồn tại hay không - Nếu tồn tại thì lấy thông tin sản phẩm
	$db_product = new db_query("SELECT *
										 FROM products_multi
										 		INNER JOIN categories_multi ON (cat_id = pro_category_id AND cat_active = 1 AND cat_type = 'product')
										 WHERE pro_active = 1 AND pro_id = " . $iPro);
	if(mysql_num_rows($db_product->result) == 0) exit("Sản phẩm không tồn tại");
	$rowDetail 	= mysql_fetch_assoc($db_product->result);

	//check cookie
	if(isset($_COOKIE["mycart"])){
		$cart_new 		= array();
		$current_cart 	= base64_decode($_COOKIE["mycart"], 1);
		$current_cart	= json_decode($current_cart, 1);

		if(isset($current_cart[$iPro])){
			$quantity 				= $current_cart[$iPro]["quantity"] + $quantity;
			$current_cart[$iPro] = array("quantity" => $quantity);
		}
		else{
			$cart_new[$iPro] 	= array("quantity" => $quantity);
			$current_cart 		= $current_cart + $cart_new;
		}

	}
	else $current_cart[$iPro]  = array("quantity" => $quantity);

	foreach ($current_cart as $kTotal => $vTotal){
		$total_product += intval($vTotal["quantity"]);
	}

	$cartData = json_encode($current_cart);

	//Save cookie 1 tháng
	setcookie("mycart", base64_encode($cartData), time() + (86400 * 30), "/");

	echo $total_product;

}
?>