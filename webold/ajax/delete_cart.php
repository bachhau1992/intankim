<?
require_once("../home/config.php");

$myCart 			= getValue("mycart", "str", "COOKIE", "");
$arrCart 		= base64_decode($myCart);
$arrCart 		= json_decode($arrCart, 1);
$iPro 			= getValue("iPro");
$total_product = 0;

if(count($arrCart) > 0){
	unset($arrCart[$iPro]);
	foreach($arrCart as $kCart => $vCart){
		$total_product += intval($vCart["quantity"]);
	}

	$cartData = json_encode($arrCart);

	//Save cookie
	setcookie("mycart", base64_encode($cartData), time() + (86400 * 30), "/");

	echo $total_product;
}
?>