<?
require_once("../home/config.php");

// Khai báo biến được post lên
$name				= getValue('ct_name', 'str', 'POST', "");
$email			= getValue('ct_email', 'str', 'POST', "");
$phone			= getValue('ct_phone', 'str', 'POST', "");
$project_name	= getValue('project_name', 'str', 'POST', "");
$action			= getValue('ct_action', 'str', 'POST',"");

$fs_table	= "contacts";
$time			= time();
$ct_error	= array();

// Check validate form
if($action == 'send'){
	if($name == '')																						$ct_error[] = "Vui lòng nhập tên của bạn";
	if(!filter_var($email, FILTER_VALIDATE_EMAIL))												$ct_error[] = "Email không hợp lệ";
	if(count($ct_error) > 0) {
		echo '<ul>';
		foreach ($ct_error as $errors) {
			echo '<li>' . $errors . '</li>';
		}
		echo '</ul>';
	}else{
		$db_insert_ct = new db_execute("INSERT INTO contacts (ct_name, ct_email, ct_mobile, ct_content, ct_date, ct_status, lang_id)
												  VALUES ('" . replaceMQ($name) . "', '" . replaceMQ($email) . "', '" . replaceMQ($phone) . "', '" . replaceMQ($project_name) . "', " . intval($time) . ", 0, 1)");
		if($db_insert_ct){
			echo 'success';
		}else{
			echo 'Query error';
		}
		unset($db_insert_ct);
	}
}
?>