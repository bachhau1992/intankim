<?
class order_backup{
	
	var $patch	= "";
	var $day		= 0;
	var $month	= 0;
	var $year	= 0;
	function order_backup(){
		
	}
	
	function toTxt($arr){
		$fullpatch	= $this->patch . "/" . $this->year . $this->month . $this->day . ".txt";
		$fp	= fopen($fullpatch, "w");
		fwrite($fp, json_encode($arr));
		fclose($fp);
	}
	
	function toZip($patchImg, $arrImg){
		$zip 			= new ZipArchive;
		$patchZip	= $this->patch . "/" . $this->year . $this->month . $this->day . ".zip";
		$patchTxt	= $this->patch . "/" . $this->year . $this->month . $this->day . ".txt";
		$res = $zip->open($patchZip, ZipArchive::CREATE);
		if($res === true){
			$zip->addFile($patchTxt, $this->year . $this->month . $this->day . ".txt");
			foreach($arrImg as $value){
				$zip->addFile($patchImg . "/" . $value, $value);
				$zip->addFile($patchImg . "/small_" . $value, "small_" . $value);
			}
			$zip->close();
		}
		else{
			echo 'Không nén được!';
		}
	}
	
	function download(){
		$link	= $this->patch . "/" . $this->year . $this->month . $this->day . ".zip";
		header( 'Location: ' . $link . '' );
	}
}
?>