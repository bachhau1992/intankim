<?
/*
class generate form
Created by FinalStyle.com
*/
class generate_form{
	var $array_data_field;
	var $array_data_value;
	var $array_data_type;
	var $array_data_store;
	var $array_data_default_value;
	var $array_data_require;
	var $array_data_error_message;
	var $array_data_unique;
	var $array_data_error_message2;
	var $number_of_field		= -1;
	var $table_name			= "";
	var $record_field			= "";
	var $record_value			= "";
	var $removeHTML			= 1;
	var $form_name				= "";
	var $java_code_add_on	= "";
	
	/*
	1). $data_field			: Ten truong
	2). $data_value			: Ten form
	3). $data_type				: Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). $data_store			: Noi luu giu data  0 : post, 1 : variable
	5). $data_default_value	: Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). $data_require			: Du lieu nay co can thiet hay khong
	7). $data_error_message	: Loi dua ra man hinh
	8). $data_unique			: Chi co duy nhat trong database
	9). $data_error_message2: Loi dua ra man hinh neu co duplicate
	*/
	function add($data_field, $data_value, $data_type, $data_store, $data_default_value, $data_require=0, $data_error_message="", $data_unique=0, $data_error_message2=""){
		$this->number_of_field++;
		$this->array_data_field[$this->number_of_field]				= $data_field;
		$this->array_data_value[$this->number_of_field]				= $data_value;
		$this->array_data_type[$this->number_of_field]				= $data_type;
		$this->array_data_store[$this->number_of_field]				= $data_store;
		$this->array_data_default_value[$this->number_of_field]	= $data_default_value;
		$this->array_data_require[$this->number_of_field]			= $data_require;
		$this->array_data_error_message[$this->number_of_field]	= $data_error_message;
		$this->array_data_unique[$this->number_of_field]			= $data_unique;
		$this->array_data_error_message2[$this->number_of_field] = $data_error_message2;
	}
	
	/*
	table_name : Ten bang
	*/
	function addTable($table_name){
		$this->table_name = $table_name;
	}

	/*
	form_name : Ten Form
	*/
	function addFormname($form_name){
		$this->form_name = $form_name;
	}
	
	/*
	Su dung khi update data
	record_field : Ten truong can edit
	record_value : Gia tri
	*/
	function addRecordID($record_field, $record_value){
		$this->record_field = $record_field;
		$this->record_value = $record_value;
	}
	
	/*
	Remove HTML truoc khi add vao database
	value  0: Not Remove, 1 : Remove
	*/
	function removeHTML($value){
		$this->removeHTML = $value;
	}
	
	/*
	Remove HTML truoc khi add vao database
	*/
	function htmlspecialbo($str){
		$arrDenied	= array('<', '>', '"');
		$arrReplace	= array('&lt;', '&gt;', '&quot;');
		$str = str_replace($arrDenied, $arrReplace, $str);
		return $str;
	}
	
	/*
	Generate Insert SQL
	*/
	function generate_insert_SQL(){
		$str_field	= "(";
		$str_data	= "(";
		for($i=0; $i<=$this->number_of_field; $i++){
			$str_field .= $this->array_data_field[$i] . ",";
			//gan bien temp = gia tri mac dinh
			$temp = $this->array_data_default_value[$i];
			
			//Read from method POST
			if($this->array_data_store[$i] == 0){
				if (isset($_POST[$this->array_data_value[$i]])) $temp = $_POST[$this->array_data_value[$i]];
			}
			//Read from global variable
			else{
				$temp_var = $this->array_data_value[$i];
				global $$temp_var;
				if(isset($$temp_var)){
					$temp = $$temp_var;
				}
			}
			//remove quote;
			$temp = str_replace("\'", "'", $temp);
			$temp = str_replace("'", "''", $temp);
			
			//Remove HTML tag if removeHTML = 1
			if($this->removeHTML == 1) $temp = $this->htmlspecialbo($temp);
			
			switch($this->array_data_type[$i]){
				case "0": $str_data .= "'" . $temp . "',"; break;
				case "1": $str_data .= intval($temp) . ","; break;
				case "2": $str_data .= "'" . $temp . "',"; break;
				case "3": $str_data .= doubleval($temp) . ","; break;
				case "4": $str_data .= "'" . md5($temp) . "',"; break;
			}
		}
		
		global $lang_id;
		global $con_lang_id;
		
		$lang	= (isset($lang_id) ? $lang_id : $con_lang_id);
		
		$str_field	.= "lang_id)";
		$str_data	.= $lang . ")";
		$querystr	= "INSERT INTO " . $this->table_name . $str_field . " VALUES " . $str_data;
	
		return $querystr;
	}
	
	/*
	Generate Update SQL
	update_field_name : Truong can update vi du : cat_id
	update_field_value : Gia tri can update vi du : 10
	*/
	function generate_update_SQL($update_field_name, $update_field_value){
		$str_field	= "(";
		$str_data	= "(";
		$querystr	= "";
		for($i=0; $i<=$this->number_of_field; $i++){
			$str_field = $this->array_data_field[$i] . "=";
			//gan bien temp = gia tri mac dinh
			$temp = $this->array_data_default_value[$i];
			
			//Read from method POST
			if($this->array_data_store[$i]==0){
				if(isset($_POST[$this->array_data_value[$i]])) $temp = $_POST[$this->array_data_value[$i]];
			}
			//Read from global variable
			else{
				$temp_var = $this->array_data_value[$i];
				global $$temp_var;
				$temp = $$temp_var;
			}
			//remove quote;
			$temp = str_replace("\'","'",$temp);
			$temp = str_replace("'","''",$temp);
			
			//Remove HTML tag if removeHTML = 1
			if($this->removeHTML == 1) $temp = $this->htmlspecialbo($temp);
			
			switch ($this->array_data_type[$i]){
				case "0": $str_data = "'" . $temp . "',"; break;
				case "1": $str_data = intval($temp) . ","; break;
				case "2": $str_data = "'" . $temp . "',"; break;
				case "3": $str_data = doubleval($temp) . ","; break;
				case "4": $str_data = "'" . md5($temp) . "',"; break;
			}
			$querystr .=  $str_field . $str_data;
		}
		
		$querystr = substr($querystr, 0, strlen($querystr)-1);
		$querystr = "UPDATE " . $this->table_name . " SET " . $querystr . " WHERE " . $update_field_name . " = " . $update_field_value;
	
		return $querystr;
	}	
	
	/*
	Add them ma Javascript vao check
	*/
	function addjavasrciptcode($java_code_add_on){
		$this->java_code_add_on = $java_code_add_on;
	}
	
	/*
	Kiem tra javascript
	*/
	function checkjavascript(){
		echo "<script language'javascript'>";
		echo "function trim(sString){
					while(sString.substring(0,1) == ' '){
						sString = sString.substring(1, sString.length);
					}
					while(sString.substring(sString.length-1, sString.length) == ' '){
						sString = sString.substring(0,sString.length-1);
					}
					return sString;
				}
				function checkblank(str){
					if(trim(str) == '') return true;
					else return false;
				}
				function isemail(email) {
					var re = /^(\w|[^_]\.[^_]|[\-])+(([^_])(\@){1}([^_]))(([a-z]|[\d]|[_]|[\-])+|([^_]\.[^_])*)+\.[a-z]{2,3}$/i
					return re.test(email);
				}
		      function validateForm(){
			   ";
		for($i=0; $i<=$this->number_of_field; $i++){
			//neu data_require la 1
			if($this->array_data_require[$i] == 1){
				switch($this->array_data_type[$i]){
					//String
					case 0:
						echo "if (checkblank(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						break;
					//Integer
					case 1:
						echo "if (checkblank(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						echo "if (isNaN(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						break;
					//Email
					case 2:
						echo "if (!isemail(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						break;
					//Double
					case 3:
						echo "if (checkblank(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						echo "if (isNaN(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						break;
					//Password hash
					case 4:
						echo "if (checkblank(document.all." . $this->array_data_value[$i] . ".value)) { alert('" . htmlspecialchars($this->array_data_error_message[$i]) . "'); document.all." . $this->array_data_value[$i] . ".focus(); return false;}";
						break;
				}
			}
		}
		echo $this->java_code_add_on;
		echo "document." . $this->form_name . ".submit();
	    }		
		";
		echo "</script>";
	}
	
	/*
	Kiem tra data
	*/
	function checkdata($id_field="", $id_value=0){
		$errormsg = "";
		for($i=0; $i<=$this->number_of_field; $i++){
			//lay gia tri tu post va`o bien temp
			$temp = "";
			//Read from method POST
			if($this->array_data_store[$i] == 0){
				if(isset($_POST[$this->array_data_value[$i]])) $temp = $_POST[$this->array_data_value[$i]];
			}
			//Read from global variable
			else{
				$temp_var = $this->array_data_value[$i];
				global $$temp_var;
				$temp = $$temp_var;
			}
			//neu data_require la 1
			if($this->array_data_require[$i] == 1){
				switch($this->array_data_type[$i]){
					//string
					case 0:
						//neu temp = rong -> show error
						if($temp == "" || strlen($temp) < strlen($this->array_data_default_value[$i])) $errormsg .= "&bull; " . $this->array_data_error_message[$i] . "<br />";
						break;
					case 1:
						//neu temp ko phai kieu int -> error
						if(intval($temp) < intval($this->array_data_default_value[$i])) $errormsg .= "&bull; " . $this->array_data_error_message[$i] . "<br />";
						break;
					case 2:
						//neu temp ko phai kieu email -> error
						$result = ereg("^[^@ ]+@[^@ ]+\.[^@ ]+$", $temp, $trashed);
						if(!$result) $errormsg .= "&bull; " . $this->array_data_error_message[$i] . "<br />";
						break;
					case 3:
						//neu temp ko phai kieu dbl -> error
						if(doubleval($temp) < doubleval($this->array_data_default_value[$i])) $errormsg .= "&bull; " . $this->array_data_error_message[$i] . "<br />";
						break;
						//password hash
					case 4:
						//neu temp = rong -> show error
						if($temp == "" || strlen($temp) < strlen($this->array_data_default_value[$i])) $errormsg .= "&bull; " . $this->array_data_error_message[$i] . "<br />";
						break;
				}
			}
			//Remove quote
			$temp = str_replace("\'", "'", $temp);
			$temp = str_replace("'", "''", $temp);
			//neu data_unique = 1 (duy nhat trong database)
			if($this->array_data_unique[$i] == 1){
				$db_select = new db_query("SELECT *
													FROM " . $this->table_name . "
													WHERE " . $this->array_data_field[$i] . " = '" . $temp . "'");
				if(mysql_num_rows($db_select->result) > 0){
					$errormsg .= "&bull; " . $this->array_data_error_message2[$i] . "<br />";
				}
			}
		}
		return $errormsg;
	}
	
	/*
	debug
	*/
	function debug(){
		echo "<font face='Tahoma, Verdana, Arial' style='font-size:12px'>";
		echo "<br />------------ Start debug ------------<br />";
		for($i=0; $i<=$this->number_of_field; $i++){
			$data_store = "";
			if($this->array_data_store[$i] == 0) $data_store = "Method POST";
			elseif($this->array_data_store[$i] == 1) $data_store = "Variable";
			echo " - Variable: <b>" . ($i+1) . "</b><br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp; + Data field: <b>" . $this->array_data_field[$i] . "</b><br />";
			echo "&nbsp;&nbsp;&nbsp;&nbsp; + Data store: <b>" . $data_store . "</b><br />";
		}
		echo "------------ End debug ------------<br />";
		echo "</font>";
	}
}
?>