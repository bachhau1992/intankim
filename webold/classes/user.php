<?
/**
 *class user
 *Developed by FinalStyle.com
 */
class user
{
   var $logged = 0;
   var $login_name;
   var $use_name;
   var $password;
   var $u_id = -1;
   var $use_email;
   var $use_date;
   var $use_security;
   /*
   init class
   login_name : ten truy cap
   password  : password (no hash)
   level: nhom user; 0: Normal; 1: Admin (default level = 0)
   */
   function user($login_name = "", $password = "", $tcookie = 0)
   {
      $checkcookie  = 0;
      $this->logged = 0;
      if ($login_name == "" && $tcookie == 0) {
         if (isset($_COOKIE["login_name_help"]))
            $login_name = $_COOKIE["login_name_help"];
      }
      if ($password == "" && $tcookie == 0) {
         if (isset($_COOKIE["PHPSESS1D"]))
            $password = $_COOKIE["PHPSESS1D"];
         $checkcookie = 1;
      } else {
         //remove \' if gpc_magic_quote = on
         $password = str_replace("\'", "'", $password);
      }
      if ($login_name == "" && $password == "")
         return;
      $db_user = new db_query("SELECT use_id, use_email, use_password, use_active, use_login, use_security, use_date
										 FROM users
										 WHERE use_login = '" . $this->removequote($login_name) . "' OR use_email = '" . $this->removequote($login_name) . "'");
      if ($row = mysql_fetch_array($db_user->result)) {
         //kiem tra password va use_active
         if ($checkcookie == 0)
            $password = md5($password . $row["use_security"]);

         if ($password == $row["use_password"] && $row["use_active"] == 1) {
            $this->logged       		= 1;
            $this->login_name   		= $login_name;
            $this->password     		= $password;
            $this->use_security 		= $row["use_security"];
            $this->u_id         		= intval($row["use_id"]);
            $this->use_email    		= $row["use_email"];
            $this->use_date    		= $row["use_date"];
            $this->useField     		= $row;
         }
      }
      unset($db_user);

   }
   /*
   Ham lay truong thong tin ra
   */
   function row($field)
   {
      if (isset($this->useField[$field])) {
         return $this->useField[$field];
      } else {
         return '';
      }
   }
   /*
   save to cookie
   time : thoi gian save cookie, neu = 0 thi` save o cua so hien ha`nh
   */
   function savecookie($time = 0)
   {
      if ($this->logged != 1)
         return false;
      if ($time > 0) {
         setcookie("login_name_help", $this->login_name, time() + $time, "/");
         setcookie("PHPSESS1D", $this->password, time() + $time, "/");
      } else {
         setcookie("login_name_help", $this->login_name, null, "/");
         setcookie("PHPSESS1D", $this->password, null, "/");
      }
   }


   /*
   Logout account
   */
   function logout()
   {
      setcookie("login_name_help", " ", null, "/");
      setcookie("PHPSESS1D", " ", null, "/");
      $this->logged = 0;
   }
   //kiem tra password de thay doi email
   function check_password($password)
   {
      $db_user = new db_query("SELECT use_password,use_security
										 FROM users, user_group
										 WHERE use_active=1 AND use_email = '" . $this->removequote($this->login_name) . "'");
      if ($row = mysql_fetch_array($db_user->result)) {
         $password = md5($password . $row["use_security"]);
         if ($password == $row["use_password"])
            return 1;
      }
      unset($db_user);
   }

   /*
   Remove quote
   */
   function removequote($str)
   {
      $temp = str_replace("\'", "'", $str);
      $temp = str_replace("'", "''", $temp);
      return $temp;
   }
}
?>