<?php
define('WP_AUTO_UPDATE_CORE', false);
define('WP_POST_REVISIONS', false); //Stop Revision post
define('DISALLOW_FILE_EDIT', true); //Cấm sửa theme và plugin trong bảng điều khiển
//define('DISALLOW_FILE_MODS',true);  //Cấm cài thêm theme/plugin

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'intakim');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', ')KFZO,v!*J=&e.:Eq=P>bk+Z$n/.,-4?CahT_J8FNQFR?uwCUL}fNf+:yqO0.%wF');
define('SECURE_AUTH_KEY', 'b24tSc?G{GT4i_8#u{0ZC`E{;i>-~ qX9w>X)X&a$<)BR]G*q=viKLlah$wWNq?#');
define('LOGGED_IN_KEY', '5R?U!JI.CN&c(/noy[@3`_ PN<Kj7~i6FOiYDn,Qhyh8cKc5WV3RcPOHj=$P.fYU');
define('NONCE_KEY', 'Wy8ov?!)9ZT;^aX4PGeQ;*[ga(rI&nC:/n2(6=@E2I}Xd,l0QB+7 F02*F:hn;DL');
define('AUTH_SALT', '-/>0&C>mks^F(]&j##j/JdX^pI9vdHM0C5JEB--j6)0ZGN0`nX#e1rDR@CC9DsW.');
define('SECURE_AUTH_SALT', '))yIU;z+A~|w=y8MwF{+tL:g->ka`2eV|v#{7W?Zwr$N=g=(BJwu kvK0D6Q+Gwz');
define('LOGGED_IN_SALT', 'xLGm68=XE&@w`  (IDW_H&94`AmifaECUh1@AqW67jJi1w&jSCfr}![)MQ@2Tu@a');
define('NONCE_SALT', 'ri+49QZnQ[PwxaO=?t;,O(48qll=N0pARQFlz>N3@fUQ=yr}]u#n3bPv=_?u=NZc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'hkt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
